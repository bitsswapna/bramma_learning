<?php
return ['menus' =>
    array(
        array(
            'Name' => 'Dashboard',
            'slug' => '/',
            'icon-class' => 'fas fa-tachometer-alt',
            'permission' => '',
            'child' => '',
        ),

        array(
            'Name' => 'User Management',
            'slug' => '',
            'icon-class' => 'fa fa-user',
            'permission' => '',
            'child' => array(
                array(
                    'Name' => 'Roles & permission',
                    'slug' => 'roles',
                    'icon-class' => 'fa fa-lock',
                    'permission' => 'roles',
                    'child' => ''),
                array(
                    'Name' => 'Module Management',
                    'slug' => 'module',
                    'icon-class' => 'fa fa-clone',
                    'permission' => 'module',
                    'child' => ''),
                array(
                    'Name' => 'Admin & Staff',
                    'slug' => 'admin_users',  //ecdey//
                    'icon-class' => 'fa fa-user',
                    'permission' => 'admin_user',
                    'child' => ''),
            ),
        ),

        array(
            'Name' => 'Menu',
            'slug' => '',
            'icon-class' => 'fa fa-gears',
            'permission' => '',
            'child' => array(
                array(
                    'Name' => 'Customer',
                    'slug' => 'admin-customers',
                    'icon-class' => 'fa fa-user',
                    'permission' => '',
                    'child' => '',
                ),

                array(
                    'Name' => 'Site',
                    'slug' => 'sites',
                    'icon-class' => 'fa fa-cube',
                    'permission' => 'sites',
                    'child' => ''),


                array(
                    'Name' => 'work assign',
                    'slug' => 'site-work',
                    'icon-class' => 'fa fa-leaf',
                    'permission' => 'site_work',
                    'child' => ''),
                    array(
                        'Name' => 'Amount Details',
                        'slug' => 'amount-details',
                        'icon-class' => 'fa fa-briefcase',
                        'permission' => 'site_work',
                        'child' => ''),
                        array(
                            'Name' => 'Vehicle assign',
                            'slug' => 'Vehicles',
                            'icon-class' => 'fa fa-bed',
                            'permission' => 'site_work',
                            'child' => ''),
                         
                ),
                
            ),


      
       



       
        array(
            'Name' => 'Item',
            'slug' => 'admin-utilites',
            'icon-class' => 'fa fa-tint',
            'permission' => 'utilities',
            'child' => '',
        ),
       

   
                // array(
                //     'Name' => 'Email Templates',
                //     'slug' => 'email-templates',
                //     'icon-class' => 'fa fa-cube',
                //     'permission' => 'email_templates',
                //     'child' => ''),
              
       
        

//
    )];
