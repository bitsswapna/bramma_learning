-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 22, 2019 at 11:13 AM
-- Server version: 5.7.28-0ubuntu0.18.04.4
-- PHP Version: 7.1.33-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `admin`
--

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE `activities` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `relation_id` int(11) DEFAULT NULL,
  `type` varchar(150) DEFAULT NULL,
  `activity` text,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`id`, `user_id`, `relation_id`, `type`, `activity`, `updated_at`, `created_at`) VALUES
(1, 1, 1, 'user', 'Admin user status inactivated by:', '2019-10-25 02:00:02', '2019-10-25 02:00:02'),
(2, 1, 110, 'module', 'New module has been created by:', '2019-10-25 02:02:13', '2019-10-25 02:02:13'),
(3, 1, 1, 'role', 'New role has been edited by:', '2019-10-25 02:03:23', '2019-10-25 02:03:23'),
(4, 1, 1, 'user', 'Admin user status activated by:', '2019-10-25 02:12:51', '2019-10-25 02:12:51'),
(5, 1, 1, 'user', 'User has been updated his profile by::', '2019-10-25 03:18:21', '2019-10-25 03:18:21'),
(6, 1, 1, 'user', 'User has been updated his profile by::', '2019-10-25 03:18:25', '2019-10-25 03:18:25'),
(7, 1, 1, 'user', 'User has been updated his profile by::', '2019-10-26 02:11:46', '2019-10-26 02:11:46'),
(8, 1, 1, 'role', 'New role has been edited by:', '2019-10-26 03:10:56', '2019-10-26 03:10:56'),
(9, 1, 1, 'role', 'New role has been edited by:', '2019-10-26 03:19:06', '2019-10-26 03:19:06');

-- --------------------------------------------------------

--
-- Table structure for table `admin_modules`
--

CREATE TABLE `admin_modules` (
  `id` int(20) NOT NULL,
  `parent_id` int(20) NOT NULL DEFAULT '0',
  `name` varchar(50) CHARACTER SET latin1 NOT NULL,
  `perifix` varchar(50) CHARACTER SET latin1 NOT NULL,
  `type` enum('parent','child') CHARACTER SET latin1 NOT NULL COMMENT 'two types parent and child',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_modules`
--

INSERT INTO `admin_modules` (`id`, `parent_id`, `name`, `perifix`, `type`, `created_at`, `updated_at`) VALUES
(35, 0, 'module', 'module', 'parent', '2019-08-17 07:25:23', '2019-08-17 07:25:23'),
(36, 0, 'Roles', 'roles', 'parent', '2019-08-17 07:25:42', '2019-08-31 10:33:14'),
(37, 0, 'Admin & User', 'admin_user', 'parent', '2019-08-17 07:25:53', '2019-08-17 07:25:53'),
(38, 0, 'Employee', 'employee', 'parent', '2019-08-17 07:26:26', '2019-08-31 10:32:30'),
(39, 0, 'client', 'client', 'parent', '2019-08-17 07:27:52', '2019-08-17 07:27:52'),
(40, 0, 'lead', 'lead', 'parent', '2019-08-17 07:29:25', '2019-08-17 07:29:25'),
(41, 0, 'Sub Industries', 'sub_industries', 'parent', '2019-08-17 07:29:36', '2019-08-17 07:29:36'),
(42, 0, 'Mou', 'mou', 'parent', '2019-08-17 09:59:24', '2019-08-17 10:00:01'),
(43, 0, 'Proposal', 'proposel', 'parent', '2019-08-17 10:00:35', '2019-08-17 10:00:35'),
(44, 0, 'Report', 'report', 'parent', '2019-08-19 05:50:18', '2019-08-19 05:50:18'),
(45, 0, 'Project', 'project', 'parent', '2019-08-19 06:30:08', '2019-08-19 06:30:08'),
(47, 0, 'Monthlyplan', 'monthlyplan', 'parent', '2019-08-19 06:50:43', '2019-08-19 06:50:43'),
(48, 0, 'data', 'asdaed', 'parent', '2019-08-20 11:15:33', '2019-08-20 11:15:33'),
(49, 0, 'Organization Type', 'organization_types', 'parent', '2019-08-20 12:07:43', '2019-08-20 12:07:43'),
(50, 0, 'Industries', 'industries', 'parent', '2019-08-20 12:23:58', '2019-08-20 12:23:58'),
(51, 0, 'Lead Type', 'lead_type', 'parent', '2019-08-20 12:24:30', '2019-08-20 12:24:30'),
(52, 0, 'Designations', 'designations', 'parent', '2019-08-20 12:25:26', '2019-08-20 12:25:26'),
(53, 0, 'Departments', 'departments', 'parent', '2019-08-20 12:25:44', '2019-08-20 12:25:44'),
(54, 0, 'Functions', 'functions', 'parent', '2019-08-21 04:23:24', '2019-08-21 04:23:24'),
(55, 0, 'Sections', 'sections', 'parent', '2019-08-21 04:24:16', '2019-08-21 04:24:16'),
(57, 0, 'Documents Type', 'documents_type', 'parent', '2019-08-21 04:26:20', '2019-08-21 04:56:31'),
(58, 0, 'subtraining', 'sub_training', 'parent', '2019-08-22 12:49:05', '2019-08-22 12:49:05'),
(59, 0, 'Utility', 'utility', 'parent', '2019-08-26 12:15:47', '2019-08-26 12:15:47'),
(60, 0, 'Template', 'template', 'parent', '2019-08-27 06:21:50', '2019-08-27 06:21:50'),
(61, 0, 'Project plan', 'project_plan', 'parent', '2019-08-27 11:45:08', '2019-08-27 11:45:08'),
(62, 0, 'Questions', 'questions', 'parent', '2019-08-30 04:59:44', '2019-08-30 04:59:44'),
(63, 0, 'Lead Source Types', 'lead_source_types', 'parent', '2019-08-30 05:22:20', '2019-08-30 05:22:20'),
(64, 0, 'Leadsources', 'lead_sources', 'parent', '2019-08-30 05:36:08', '2019-08-30 05:36:08'),
(65, 0, 'New module', 'gfg', 'parent', '2019-08-30 09:31:00', '2019-09-21 05:41:12'),
(70, 0, 'Employee user', 'employee_user', 'parent', '2019-09-17 11:50:05', '2019-09-17 11:50:05'),
(71, 0, 'Client relation incharge', 'client_relation_incharge', 'parent', '2019-09-23 05:00:29', '2019-09-23 05:00:29'),
(72, 0, 'Content Developer', 'content_developer', 'parent', '2019-09-24 06:39:04', '2019-09-24 06:39:04'),
(73, 0, 'Lead consultant', 'lead_consultant', 'parent', '2019-09-26 10:10:00', '2019-09-26 10:10:00'),
(74, 0, 'project approval', 'project_approval', 'parent', '2019-09-28 10:51:20', '2019-09-28 10:51:20'),
(75, 0, 'document serach', 'document_search', 'parent', '2019-09-30 05:15:58', '2019-09-30 05:15:58'),
(76, 0, 'download_document', 'download_document', 'parent', '2019-10-05 04:31:51', '2019-10-05 04:33:03'),
(77, 0, 'Usermanagement', 'usermanagement', 'parent', '2019-10-08 05:03:03', '2019-10-08 05:03:03'),
(78, 0, 'Leadmanagement', 'leadmanagement', 'parent', '2019-10-08 05:05:58', '2019-10-08 05:05:58'),
(79, 0, 'Reportmanagement', 'reportmanagement', 'parent', '2019-10-08 05:08:09', '2019-10-08 05:08:09'),
(80, 0, 'Moduledetails', 'moduledetails', 'parent', '2019-10-08 05:30:13', '2019-10-08 05:30:13'),
(81, 0, 'Permissiondetails', 'permissiondetails', 'parent', '2019-10-08 05:30:32', '2019-10-08 05:30:32'),
(82, 0, 'Project button', 'project_button', 'parent', '2019-10-08 05:37:15', '2019-10-08 05:37:15'),
(83, 0, 'Project_approval_button', 'project_approval_button', 'parent', '2019-10-08 05:44:14', '2019-10-08 05:44:14'),
(84, 0, 'projectplan_approvals_buttons', 'projectplan_approvals_buttons', 'parent', '2019-10-08 05:52:28', '2019-10-08 05:52:28'),
(85, 0, 'Clientmanagement', 'clientmanagement', 'parent', '2019-10-08 06:52:47', '2019-10-08 06:52:47'),
(86, 0, 'Leadcreate', 'leadcreate', 'parent', '2019-10-08 06:54:50', '2019-10-08 06:54:50'),
(87, 0, 'leadlist', 'leadlist', 'parent', '2019-10-08 06:59:00', '2019-10-08 06:59:00'),
(88, 0, 'dashboard', 'dashboard', 'parent', '2019-10-08 10:23:13', '2019-10-08 10:23:13'),
(89, 0, 'Employeecount', 'employeecount', 'parent', '2019-10-08 10:54:18', '2019-10-08 10:54:18'),
(90, 0, 'Projectcount', 'projectcount', 'parent', '2019-10-08 10:54:38', '2019-10-08 10:54:38'),
(91, 0, 'Leadcount', 'leadcount', 'parent', '2019-10-08 10:55:00', '2019-10-08 10:55:00'),
(92, 0, 'employee_project_count', 'employee_project_count', 'parent', '2019-10-08 11:00:24', '2019-10-08 11:00:24'),
(93, 0, 'employee_lead_count', 'employee_lead_count', 'parent', '2019-10-08 11:00:51', '2019-10-08 11:00:51'),
(94, 0, 'clientcount', 'clientcount', 'parent', '2019-10-08 11:03:21', '2019-10-08 11:03:21'),
(96, 0, 'projectplanedit', 'projectplanedit', 'parent', '2019-10-09 09:37:04', '2019-10-09 09:37:04'),
(97, 0, 'projectplanview', 'projectplanview', 'parent', '2019-10-09 09:44:50', '2019-10-09 09:44:50'),
(98, 0, 'leadprojectbtn', 'leadprojectbtn', 'parent', '2019-10-09 10:40:04', '2019-10-09 10:40:04'),
(99, 0, 'Daily plan', 'daily_plan', 'parent', '2019-10-10 04:58:43', '2019-10-10 04:58:43'),
(100, 0, 'Assignemployeebtn', 'assignemployeebtn', 'parent', '2019-10-11 06:30:20', '2019-10-11 06:30:20'),
(101, 0, 'Projectplansavebtn', 'projectplansavebtn', 'parent', '2019-10-11 06:49:15', '2019-10-11 06:49:15'),
(102, 0, 'documentrequestbtn', 'document_request_btn', 'parent', '2019-10-14 06:26:21', '2019-10-14 06:26:21'),
(106, 0, 'approval_list', 'approval_list', 'parent', '2019-10-15 12:14:04', '2019-10-15 12:14:04'),
(107, 0, 'Lead_mou_proposal_button', 'lead_mou_proposal_button', 'parent', '2019-10-16 04:38:34', '2019-10-16 04:38:34'),
(108, 0, 'document_search_request_btn', 'document_search_request_btn', 'parent', '2019-10-16 06:22:43', '2019-10-16 06:22:43'),
(109, 0, 'Lead Appove button', 'leadapprove_btn', 'parent', '2019-10-16 14:33:14', '2019-10-16 14:33:14'),
(110, 0, 'test', 'test', 'parent', '2019-10-25 07:32:12', '2019-10-25 07:32:12');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `organisation_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `promoter_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `promoter_phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `promoter_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `organisation_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `organisation_phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `organisation_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `organisation_address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `organisation_logo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by_user_id` int(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `organisation_name`, `promoter_name`, `promoter_phone`, `promoter_email`, `organisation_email`, `organisation_phone`, `organisation_description`, `organisation_address`, `organisation_logo`, `created_by_user_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'ryhtf', 'gfjj', '68678769', 'vbv@sds.dg', 'dgfdg@sf.xdcvg', '3454765756', 'gfhfvnjvghjm', 'gjnghmj', 'image.jpg', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` bigint(200) NOT NULL,
  `model` varchar(191) DEFAULT NULL,
  `model_id` bigint(191) DEFAULT NULL,
  `direction` varchar(191) DEFAULT NULL COMMENT 'CRUD',
  `key_code` varchar(191) DEFAULT NULL COMMENT 'if project remark changed then key can be project_project_status_remark',
  `auth_user_id` int(11) DEFAULT NULL,
  `value` text COMMENT 'if remark then remark, if project creation status',
  `is_read` int(2) NOT NULL DEFAULT '0' COMMENT '0-not read,1-read',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_10_26_100624_create_client_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`, `module_id`) VALUES
(171, 'browse_module', 'module', '2019-08-17 07:25:23', '2019-08-17 07:25:23', 35),
(172, 'read_module', 'module', '2019-08-17 07:25:23', '2019-08-17 07:25:23', 35),
(173, 'edit_module', 'module', '2019-08-17 07:25:23', '2019-08-17 07:25:23', 35),
(174, 'add_module', 'module', '2019-08-17 07:25:23', '2019-08-17 07:25:23', 35),
(175, 'delete_module', 'module', '2019-08-17 07:25:23', '2019-08-17 07:25:23', 35),
(176, 'browse_roles', 'roles', '2019-08-17 07:25:42', '2019-08-17 07:25:42', 36),
(177, 'read_roles', 'roles', '2019-08-17 07:25:42', '2019-08-17 07:25:42', 36),
(178, 'edit_roles', 'roles', '2019-08-17 07:25:42', '2019-08-17 07:25:42', 36),
(179, 'add_roles', 'roles', '2019-08-17 07:25:42', '2019-08-17 07:25:42', 36),
(180, 'delete_roles', 'roles', '2019-08-17 07:25:42', '2019-08-17 07:25:42', 36),
(181, 'browse_admin_user', 'admin_user', '2019-08-17 07:25:53', '2019-08-17 07:25:53', 37),
(182, 'read_admin_user', 'admin_user', '2019-08-17 07:25:53', '2019-08-17 07:25:53', 37),
(183, 'edit_admin_user', 'admin_user', '2019-08-17 07:25:53', '2019-08-17 07:25:53', 37),
(184, 'add_admin_user', 'admin_user', '2019-08-17 07:25:53', '2019-08-17 07:25:53', 37),
(185, 'delete_admin_user', 'admin_user', '2019-08-17 07:25:53', '2019-08-17 07:25:53', 37),
(186, 'browse_employee', 'employee', '2019-08-17 07:26:26', '2019-08-17 07:26:26', 38),
(187, 'read_employee', 'employee', '2019-08-17 07:26:26', '2019-08-17 07:26:26', 38),
(188, 'edit_employee', 'employee', '2019-08-17 07:26:26', '2019-08-17 07:26:26', 38),
(189, 'add_employee', 'employee', '2019-08-17 07:26:26', '2019-08-17 07:26:26', 38),
(190, 'delete_employee', 'employee', '2019-08-17 07:26:26', '2019-08-17 07:26:26', 38),
(191, 'browse_client', 'client', '2019-08-17 07:27:52', '2019-08-17 07:27:52', 39),
(192, 'read_client', 'client', '2019-08-17 07:27:52', '2019-08-17 07:27:52', 39),
(193, 'edit_client', 'client', '2019-08-17 07:27:52', '2019-08-17 07:27:52', 39),
(194, 'add_client', 'client', '2019-08-17 07:27:52', '2019-08-17 07:27:52', 39),
(195, 'delete_client', 'client', '2019-08-17 07:27:52', '2019-08-17 07:27:52', 39),
(196, 'browse_lead', 'lead', '2019-08-17 07:29:25', '2019-08-17 07:29:25', 40),
(197, 'read_lead', 'lead', '2019-08-17 07:29:25', '2019-08-17 07:29:25', 40),
(198, 'edit_lead', 'lead', '2019-08-17 07:29:25', '2019-08-17 07:29:25', 40),
(199, 'add_lead', 'lead', '2019-08-17 07:29:25', '2019-08-17 07:29:25', 40),
(200, 'delete_lead', 'lead', '2019-08-17 07:29:25', '2019-08-17 07:29:25', 40),
(201, 'browse_sub_industries', 'sub_industries', '2019-08-17 07:29:36', '2019-08-17 07:29:36', 41),
(202, 'read_sub_industries', 'sub_industries', '2019-08-17 07:29:36', '2019-08-17 07:29:36', 41),
(203, 'edit_sub_industries', 'sub_industries', '2019-08-17 07:29:36', '2019-08-17 07:29:36', 41),
(204, 'add_sub_industries', 'sub_industries', '2019-08-17 07:29:36', '2019-08-17 07:29:36', 41),
(205, 'delete_sub_industries', 'sub_industries', '2019-08-17 07:29:36', '2019-08-17 07:29:36', 41),
(206, 'browse_mou', 'mou', '2019-08-17 09:59:24', '2019-08-17 09:59:24', 42),
(207, 'read_mou', 'mou', '2019-08-17 09:59:24', '2019-08-17 09:59:24', 42),
(208, 'edit_mou', 'mou', '2019-08-17 09:59:24', '2019-08-17 09:59:24', 42),
(209, 'add_mou', 'mou', '2019-08-17 09:59:24', '2019-08-17 09:59:24', 42),
(210, 'delete_mou', 'mou', '2019-08-17 09:59:24', '2019-08-17 09:59:24', 42),
(211, 'browse_proposal', 'proposal', '2019-08-17 10:00:35', '2019-08-17 10:00:35', 43),
(212, 'read_proposal', 'proposal', '2019-08-17 10:00:35', '2019-08-17 10:00:35', 43),
(213, 'edit_proposal', 'proposal', '2019-08-17 10:00:35', '2019-08-17 10:00:35', 43),
(214, 'add_proposal', 'proposal', '2019-08-17 10:00:35', '2019-08-17 10:00:35', 43),
(215, 'delete_proposal', 'proposal', '2019-08-17 10:00:35', '2019-08-17 10:00:35', 43),
(216, 'browse_report', 'report', '2019-08-19 05:50:18', '2019-08-19 05:50:18', 44),
(217, 'read_report', 'report', '2019-08-19 05:50:18', '2019-08-19 05:50:18', 44),
(218, 'edit_report', 'report', '2019-08-19 05:50:18', '2019-08-19 05:50:18', 44),
(219, 'add_report', 'report', '2019-08-19 05:50:18', '2019-08-19 05:50:18', 44),
(220, 'delete_report', 'report', '2019-08-19 05:50:18', '2019-08-19 05:50:18', 44),
(221, 'browse_project', 'project', '2019-08-19 06:30:08', '2019-08-19 06:30:08', 45),
(222, 'read_project', 'project', '2019-08-19 06:30:08', '2019-08-19 06:30:08', 45),
(223, 'edit_project', 'project', '2019-08-19 06:30:08', '2019-08-19 06:30:08', 45),
(224, 'add_project', 'project', '2019-08-19 06:30:08', '2019-08-19 06:30:08', 45),
(225, 'delete_project', 'project', '2019-08-19 06:30:08', '2019-08-19 06:30:08', 45),
(231, 'browse_monthlyplan', 'monthlyplan', '2019-08-19 06:50:43', '2019-08-19 06:50:43', 47),
(232, 'read_monthlyplan', 'monthlyplan', '2019-08-19 06:50:43', '2019-08-19 06:50:43', 47),
(233, 'edit_monthlyplan', 'monthlyplan', '2019-08-19 06:50:43', '2019-08-19 06:50:43', 47),
(234, 'add_monthlyplan', 'monthlyplan', '2019-08-19 06:50:43', '2019-08-19 06:50:43', 47),
(235, 'delete_monthlyplan', 'monthlyplan', '2019-08-19 06:50:43', '2019-08-19 06:50:43', 47),
(236, 'browse_asdaed', 'asdaed', '2019-08-20 11:15:33', '2019-08-20 11:15:33', 48),
(237, 'read_asdaed', 'asdaed', '2019-08-20 11:15:33', '2019-08-20 11:15:33', 48),
(238, 'edit_asdaed', 'asdaed', '2019-08-20 11:15:33', '2019-08-20 11:15:33', 48),
(239, 'add_asdaed', 'asdaed', '2019-08-20 11:15:33', '2019-08-20 11:15:33', 48),
(240, 'delete_asdaed', 'asdaed', '2019-08-20 11:15:33', '2019-08-20 11:15:33', 48),
(241, 'browse_organization_types', 'organization_types', '2019-08-20 12:07:43', '2019-08-20 12:07:43', 49),
(242, 'read_organization_types', 'organization_types', '2019-08-20 12:07:43', '2019-08-20 12:07:43', 49),
(243, 'edit_organization_types', 'organization_types', '2019-08-20 12:07:43', '2019-08-20 12:07:43', 49),
(244, 'add_organization_types', 'organization_types', '2019-08-20 12:07:43', '2019-08-20 12:07:43', 49),
(245, 'delete_organization_types', 'organization_types', '2019-08-20 12:07:43', '2019-08-20 12:07:43', 49),
(246, 'browse_industries', 'industries', '2019-08-20 12:23:58', '2019-08-20 12:23:58', 50),
(247, 'read_industries', 'industries', '2019-08-20 12:23:58', '2019-08-20 12:23:58', 50),
(248, 'edit_industries', 'industries', '2019-08-20 12:23:58', '2019-08-20 12:23:58', 50),
(249, 'add_industries', 'industries', '2019-08-20 12:23:58', '2019-08-20 12:23:58', 50),
(250, 'delete_industries', 'industries', '2019-08-20 12:23:58', '2019-08-20 12:23:58', 50),
(251, 'browse_lead_type', 'lead_type', '2019-08-20 12:24:30', '2019-08-20 12:24:30', 51),
(252, 'read_lead_type', 'lead_type', '2019-08-20 12:24:30', '2019-08-20 12:24:30', 51),
(253, 'edit_lead_type', 'lead_type', '2019-08-20 12:24:30', '2019-08-20 12:24:30', 51),
(254, 'add_lead_type', 'lead_type', '2019-08-20 12:24:30', '2019-08-20 12:24:30', 51),
(255, 'delete_lead_type', 'lead_type', '2019-08-20 12:24:30', '2019-08-20 12:24:30', 51),
(256, 'browse_designations', 'designations', '2019-08-20 12:25:26', '2019-08-20 12:25:26', 52),
(257, 'read_designations', 'designations', '2019-08-20 12:25:26', '2019-08-20 12:25:26', 52),
(258, 'edit_designations', 'designations', '2019-08-20 12:25:26', '2019-08-20 12:25:26', 52),
(259, 'add_designations', 'designations', '2019-08-20 12:25:26', '2019-08-20 12:25:26', 52),
(260, 'delete_designations', 'designations', '2019-08-20 12:25:26', '2019-08-20 12:25:26', 52),
(261, 'browse_departments', 'departments', '2019-08-20 12:25:44', '2019-08-20 12:25:44', 53),
(262, 'read_departments', 'departments', '2019-08-20 12:25:44', '2019-08-20 12:25:44', 53),
(263, 'edit_departments', 'departments', '2019-08-20 12:25:44', '2019-08-20 12:25:44', 53),
(264, 'add_departments', 'departments', '2019-08-20 12:25:44', '2019-08-20 12:25:44', 53),
(265, 'delete_departments', 'departments', '2019-08-20 12:25:44', '2019-08-20 12:25:44', 53),
(266, 'browse_functions', 'functions', '2019-08-21 04:23:24', '2019-08-21 04:23:24', 54),
(267, 'read_functions', 'functions', '2019-08-21 04:23:24', '2019-08-21 04:23:24', 54),
(268, 'edit_functions', 'functions', '2019-08-21 04:23:24', '2019-08-21 04:23:24', 54),
(269, 'add_functions', 'functions', '2019-08-21 04:23:24', '2019-08-21 04:23:24', 54),
(270, 'delete_functions', 'functions', '2019-08-21 04:23:24', '2019-08-21 04:23:24', 54),
(271, 'browse_sections', 'sections', '2019-08-21 04:24:16', '2019-08-21 04:24:16', 55),
(272, 'read_sections', 'sections', '2019-08-21 04:24:16', '2019-08-21 04:24:16', 55),
(273, 'edit_sections', 'sections', '2019-08-21 04:24:16', '2019-08-21 04:24:16', 55),
(274, 'add_sections', 'sections', '2019-08-21 04:24:16', '2019-08-21 04:24:16', 55),
(275, 'delete_sections', 'sections', '2019-08-21 04:24:16', '2019-08-21 04:24:16', 55),
(281, 'browse_documents_type', 'documents_type', '2019-08-21 04:26:20', '2019-08-21 04:26:20', 57),
(282, 'read_documents_type', 'documents_type', '2019-08-21 04:26:20', '2019-08-21 04:26:20', 57),
(283, 'edit_documents_type', 'documents_type', '2019-08-21 04:26:20', '2019-08-21 04:26:20', 57),
(284, 'add_documents_type', 'documents_type', '2019-08-21 04:26:20', '2019-08-21 04:26:20', 57),
(285, 'delete_documents_type', 'documents_type', '2019-08-21 04:26:20', '2019-08-21 04:26:20', 57),
(286, 'browse_sub_training', 'sub_training', '2019-08-22 12:49:05', '2019-08-22 12:49:05', 58),
(287, 'read_sub_training', 'sub_training', '2019-08-22 12:49:05', '2019-08-22 12:49:05', 58),
(288, 'edit_sub_training', 'sub_training', '2019-08-22 12:49:05', '2019-08-22 12:49:05', 58),
(289, 'add_sub_training', 'sub_training', '2019-08-22 12:49:05', '2019-08-22 12:49:05', 58),
(290, 'delete_sub_training', 'sub_training', '2019-08-22 12:49:05', '2019-08-22 12:49:05', 58),
(291, 'browse_utility', 'utility', '2019-08-26 12:15:47', '2019-08-26 12:15:47', 59),
(292, 'read_utility', 'utility', '2019-08-26 12:15:47', '2019-08-26 12:15:47', 59),
(293, 'edit_utility', 'utility', '2019-08-26 12:15:47', '2019-08-26 12:15:47', 59),
(294, 'add_utility', 'utility', '2019-08-26 12:15:47', '2019-08-26 12:15:47', 59),
(295, 'delete_utility', 'utility', '2019-08-26 12:15:47', '2019-08-26 12:15:47', 59),
(296, 'browse_template', 'template', '2019-08-27 06:21:50', '2019-08-27 06:21:50', 60),
(297, 'read_template', 'template', '2019-08-27 06:21:50', '2019-08-27 06:21:50', 60),
(298, 'edit_template', 'template', '2019-08-27 06:21:50', '2019-08-27 06:21:50', 60),
(299, 'add_template', 'template', '2019-08-27 06:21:50', '2019-08-27 06:21:50', 60),
(300, 'delete_template', 'template', '2019-08-27 06:21:50', '2019-08-27 06:21:50', 60),
(301, 'browse_project_plan', 'project_plan', '2019-08-27 11:45:08', '2019-08-27 11:45:08', 61),
(302, 'read_project_plan', 'project_plan', '2019-08-27 11:45:08', '2019-08-27 11:45:08', 61),
(303, 'edit_project_plan', 'project_plan', '2019-08-27 11:45:08', '2019-08-27 11:45:08', 61),
(304, 'add_project_plan', 'project_plan', '2019-08-27 11:45:08', '2019-08-27 11:45:08', 61),
(305, 'delete_project_plan', 'project_plan', '2019-08-27 11:45:08', '2019-08-27 11:45:08', 61),
(306, 'browse_questions', 'questions', '2019-08-30 04:59:44', '2019-08-30 04:59:44', 62),
(307, 'read_questions', 'questions', '2019-08-30 04:59:44', '2019-08-30 04:59:44', 62),
(308, 'edit_questions', 'questions', '2019-08-30 04:59:44', '2019-08-30 04:59:44', 62),
(309, 'add_questions', 'questions', '2019-08-30 04:59:44', '2019-08-30 04:59:44', 62),
(310, 'delete_questions', 'questions', '2019-08-30 04:59:44', '2019-08-30 04:59:44', 62),
(311, 'browse_lead_source_types', 'lead_source_types', '2019-08-30 05:22:20', '2019-08-30 05:22:20', 63),
(312, 'read_lead_source_types', 'lead_source_types', '2019-08-30 05:22:20', '2019-08-30 05:22:20', 63),
(313, 'edit_lead_source_types', 'lead_source_types', '2019-08-30 05:22:20', '2019-08-30 05:22:20', 63),
(314, 'add_lead_source_types', 'lead_source_types', '2019-08-30 05:22:20', '2019-08-30 05:22:20', 63),
(315, 'delete_lead_source_types', 'lead_source_types', '2019-08-30 05:22:20', '2019-08-30 05:22:20', 63),
(316, 'browse_lead_sources', 'lead_sources', '2019-08-30 05:36:08', '2019-08-30 05:36:08', 64),
(317, 'read_lead_sources', 'lead_sources', '2019-08-30 05:36:08', '2019-08-30 05:36:08', 64),
(318, 'edit_lead_sources', 'lead_sources', '2019-08-30 05:36:08', '2019-08-30 05:36:08', 64),
(319, 'add_lead_sources', 'lead_sources', '2019-08-30 05:36:08', '2019-08-30 05:36:08', 64),
(320, 'delete_lead_sources', 'lead_sources', '2019-08-30 05:36:08', '2019-08-30 05:36:08', 64),
(321, 'browse_gfg', 'gfg', '2019-08-30 09:31:00', '2019-08-30 09:31:00', 65),
(322, 'read_gfg', 'gfg', '2019-08-30 09:31:00', '2019-08-30 09:31:00', 65),
(323, 'edit_gfg', 'gfg', '2019-08-30 09:31:00', '2019-08-30 09:31:00', 65),
(324, 'add_gfg', 'gfg', '2019-08-30 09:31:00', '2019-08-30 09:31:00', 65),
(325, 'delete_gfg', 'gfg', '2019-08-30 09:31:01', '2019-08-30 09:31:01', 65),
(326, 'browse_456566', '456566', '2019-08-30 09:41:09', '2019-08-30 09:41:09', 66),
(327, 'read_456566', '456566', '2019-08-30 09:41:09', '2019-08-30 09:41:09', 66),
(328, 'edit_456566', '456566', '2019-08-30 09:41:09', '2019-08-30 09:41:09', 66),
(329, 'add_456566', '456566', '2019-08-30 09:41:09', '2019-08-30 09:41:09', 66),
(330, 'delete_456566', '456566', '2019-08-30 09:41:09', '2019-08-30 09:41:09', 66),
(331, 'browse_dfgdgf', 'dfgdgf', '2019-08-30 09:43:51', '2019-08-30 09:43:51', 67),
(332, 'read_dfgdgf', 'dfgdgf', '2019-08-30 09:43:51', '2019-08-30 09:43:51', 67),
(333, 'edit_dfgdgf', 'dfgdgf', '2019-08-30 09:43:51', '2019-08-30 09:43:51', 67),
(334, 'add_dfgdgf', 'dfgdgf', '2019-08-30 09:43:51', '2019-08-30 09:43:51', 67),
(335, 'delete_dfgdgf', 'dfgdgf', '2019-08-30 09:43:51', '2019-08-30 09:43:51', 67),
(336, 'browse_yututu', 'yututu', '2019-08-30 10:44:26', '2019-08-30 10:44:26', 68),
(337, 'read_yututu', 'yututu', '2019-08-30 10:44:26', '2019-08-30 10:44:26', 68),
(338, 'edit_yututu', 'yututu', '2019-08-30 10:44:26', '2019-08-30 10:44:26', 68),
(339, 'add_yututu', 'yututu', '2019-08-30 10:44:26', '2019-08-30 10:44:26', 68),
(340, 'delete_yututu', 'yututu', '2019-08-30 10:44:26', '2019-08-30 10:44:26', 68),
(346, 'browse_employee_user', 'employee_user', '2019-09-17 11:50:05', '2019-09-17 11:50:05', 70),
(347, 'read_employee_user', 'employee_user', '2019-09-17 11:50:05', '2019-09-17 11:50:05', 70),
(348, 'edit_employee_user', 'employee_user', '2019-09-17 11:50:06', '2019-09-17 11:50:06', 70),
(349, 'add_employee_user', 'employee_user', '2019-09-17 11:50:06', '2019-09-17 11:50:06', 70),
(350, 'delete_employee_user', 'employee_user', '2019-09-17 11:50:06', '2019-09-17 11:50:06', 70),
(351, 'browse_client_relation_incharge', 'client_relation_incharge', '2019-09-23 05:00:29', '2019-09-23 05:00:29', 71),
(352, 'read_client_relation_incharge', 'client_relation_incharge', '2019-09-23 05:00:29', '2019-09-23 05:00:29', 71),
(353, 'edit_client_relation_incharge', 'client_relation_incharge', '2019-09-23 05:00:29', '2019-09-23 05:00:29', 71),
(354, 'add_client_relation_incharge', 'client_relation_incharge', '2019-09-23 05:00:29', '2019-09-23 05:00:29', 71),
(355, 'delete_client_relation_incharge', 'client_relation_incharge', '2019-09-23 05:00:29', '2019-09-23 05:00:29', 71),
(356, 'browse_content_developer', 'content_developer', '2019-09-24 06:39:04', '2019-09-24 06:39:04', 72),
(357, 'read_content_developer', 'content_developer', '2019-09-24 06:39:04', '2019-09-24 06:39:04', 72),
(358, 'edit_content_developer', 'content_developer', '2019-09-24 06:39:05', '2019-09-24 06:39:05', 72),
(359, 'add_content_developer', 'content_developer', '2019-09-24 06:39:05', '2019-09-24 06:39:05', 72),
(360, 'delete_content_developer', 'content_developer', '2019-09-24 06:39:05', '2019-09-24 06:39:05', 72),
(361, 'browse_lead_consultant', 'lead_consultant', '2019-09-26 10:10:00', '2019-09-26 10:10:00', 73),
(362, 'read_lead_consultant', 'lead_consultant', '2019-09-26 10:10:00', '2019-09-26 10:10:00', 73),
(363, 'edit_lead_consultant', 'lead_consultant', '2019-09-26 10:10:00', '2019-09-26 10:10:00', 73),
(364, 'add_lead_consultant', 'lead_consultant', '2019-09-26 10:10:00', '2019-09-26 10:10:00', 73),
(365, 'delete_lead_consultant', 'lead_consultant', '2019-09-26 10:10:00', '2019-09-26 10:10:00', 73),
(366, 'browse_project_approval', 'project_approval', '2019-09-28 10:51:20', '2019-09-28 10:51:20', 74),
(367, 'read_project_approval', 'project_approval', '2019-09-28 10:51:20', '2019-09-28 10:51:20', 74),
(368, 'edit_project_approval', 'project_approval', '2019-09-28 10:51:20', '2019-09-28 10:51:20', 74),
(369, 'add_project_approval', 'project_approval', '2019-09-28 10:51:20', '2019-09-28 10:51:20', 74),
(370, 'delete_project_approval', 'project_approval', '2019-09-28 10:51:20', '2019-09-28 10:51:20', 74),
(371, 'browse_document_search', 'document_search', '2019-09-30 05:15:58', '2019-09-30 05:15:58', 75),
(372, 'read_document_search', 'document_search', '2019-09-30 05:15:58', '2019-09-30 05:15:58', 75),
(373, 'edit_document_search', 'document_search', '2019-09-30 05:15:58', '2019-09-30 05:15:58', 75),
(374, 'add_document_search', 'document_search', '2019-09-30 05:15:58', '2019-09-30 05:15:58', 75),
(375, 'delete_document_search', 'document_search', '2019-09-30 05:15:58', '2019-09-30 05:15:58', 75),
(376, 'browse_download_document', 'download_document', '2019-10-05 04:31:52', '2019-10-05 04:31:52', 76),
(377, 'read_download_document', 'download_document', '2019-10-05 04:31:52', '2019-10-05 04:31:52', 76),
(378, 'edit_download_document', 'download_document', '2019-10-05 04:31:52', '2019-10-05 04:31:52', 76),
(379, 'add_download_document', 'download_document', '2019-10-05 04:31:52', '2019-10-05 04:31:52', 76),
(380, 'delete_download_document', 'download_document', '2019-10-05 04:31:52', '2019-10-05 04:31:52', 76),
(381, 'browse_usermanagement', 'usermanagement', '2019-10-08 05:03:03', '2019-10-08 05:03:03', 77),
(382, 'read_usermanagement', 'usermanagement', '2019-10-08 05:03:03', '2019-10-08 05:03:03', 77),
(383, 'edit_usermanagement', 'usermanagement', '2019-10-08 05:03:03', '2019-10-08 05:03:03', 77),
(384, 'add_usermanagement', 'usermanagement', '2019-10-08 05:03:03', '2019-10-08 05:03:03', 77),
(385, 'delete_usermanagement', 'usermanagement', '2019-10-08 05:03:03', '2019-10-08 05:03:03', 77),
(386, 'browse_leadmanagement', 'leadmanagement', '2019-10-08 05:05:59', '2019-10-08 05:05:59', 78),
(387, 'read_leadmanagement', 'leadmanagement', '2019-10-08 05:05:59', '2019-10-08 05:05:59', 78),
(388, 'edit_leadmanagement', 'leadmanagement', '2019-10-08 05:05:59', '2019-10-08 05:05:59', 78),
(389, 'add_leadmanagement', 'leadmanagement', '2019-10-08 05:05:59', '2019-10-08 05:05:59', 78),
(390, 'delete_leadmanagement', 'leadmanagement', '2019-10-08 05:05:59', '2019-10-08 05:05:59', 78),
(391, 'browse_reportmanagement', 'reportmanagement', '2019-10-08 05:08:09', '2019-10-08 05:08:09', 79),
(392, 'read_reportmanagement', 'reportmanagement', '2019-10-08 05:08:09', '2019-10-08 05:08:09', 79),
(393, 'edit_reportmanagement', 'reportmanagement', '2019-10-08 05:08:09', '2019-10-08 05:08:09', 79),
(394, 'add_reportmanagement', 'reportmanagement', '2019-10-08 05:08:09', '2019-10-08 05:08:09', 79),
(395, 'delete_reportmanagement', 'reportmanagement', '2019-10-08 05:08:10', '2019-10-08 05:08:10', 79),
(396, 'browse_moduledetails', 'moduledetails', '2019-10-08 05:30:13', '2019-10-08 05:30:13', 80),
(397, 'read_moduledetails', 'moduledetails', '2019-10-08 05:30:13', '2019-10-08 05:30:13', 80),
(398, 'edit_moduledetails', 'moduledetails', '2019-10-08 05:30:13', '2019-10-08 05:30:13', 80),
(399, 'add_moduledetails', 'moduledetails', '2019-10-08 05:30:13', '2019-10-08 05:30:13', 80),
(400, 'delete_moduledetails', 'moduledetails', '2019-10-08 05:30:13', '2019-10-08 05:30:13', 80),
(401, 'browse_permissiondetails', 'permissiondetails', '2019-10-08 05:30:32', '2019-10-08 05:30:32', 81),
(402, 'read_permissiondetails', 'permissiondetails', '2019-10-08 05:30:32', '2019-10-08 05:30:32', 81),
(403, 'edit_permissiondetails', 'permissiondetails', '2019-10-08 05:30:32', '2019-10-08 05:30:32', 81),
(404, 'add_permissiondetails', 'permissiondetails', '2019-10-08 05:30:32', '2019-10-08 05:30:32', 81),
(405, 'delete_permissiondetails', 'permissiondetails', '2019-10-08 05:30:33', '2019-10-08 05:30:33', 81),
(406, 'browse_project_button', 'project_button', '2019-10-08 05:37:15', '2019-10-08 05:37:15', 82),
(407, 'read_project_button', 'project_button', '2019-10-08 05:37:15', '2019-10-08 05:37:15', 82),
(408, 'edit_project_button', 'project_button', '2019-10-08 05:37:15', '2019-10-08 05:37:15', 82),
(409, 'add_project_button', 'project_button', '2019-10-08 05:37:15', '2019-10-08 05:37:15', 82),
(410, 'delete_project_button', 'project_button', '2019-10-08 05:37:15', '2019-10-08 05:37:15', 82),
(411, 'browse_project_approval_button', 'project_approval_button', '2019-10-08 05:44:14', '2019-10-08 05:44:14', 83),
(412, 'read_project_approval_button', 'project_approval_button', '2019-10-08 05:44:14', '2019-10-08 05:44:14', 83),
(413, 'edit_project_approval_button', 'project_approval_button', '2019-10-08 05:44:14', '2019-10-08 05:44:14', 83),
(414, 'add_project_approval_button', 'project_approval_button', '2019-10-08 05:44:14', '2019-10-08 05:44:14', 83),
(415, 'delete_project_approval_button', 'project_approval_button', '2019-10-08 05:44:14', '2019-10-08 05:44:14', 83),
(416, 'browse_projectplan_approvals_buttons', 'projectplan_approvals_buttons', '2019-10-08 05:52:28', '2019-10-08 05:52:28', 84),
(417, 'read_projectplan_approvals_buttons', 'projectplan_approvals_buttons', '2019-10-08 05:52:28', '2019-10-08 05:52:28', 84),
(418, 'edit_projectplan_approvals_buttons', 'projectplan_approvals_buttons', '2019-10-08 05:52:28', '2019-10-08 05:52:28', 84),
(419, 'add_projectplan_approvals_buttons', 'projectplan_approvals_buttons', '2019-10-08 05:52:28', '2019-10-08 05:52:28', 84),
(420, 'delete_projectplan_approvals_buttons', 'projectplan_approvals_buttons', '2019-10-08 05:52:28', '2019-10-08 05:52:28', 84),
(421, 'browse_clientmanagement', 'clientmanagement', '2019-10-08 06:52:47', '2019-10-08 06:52:47', 85),
(422, 'read_clientmanagement', 'clientmanagement', '2019-10-08 06:52:47', '2019-10-08 06:52:47', 85),
(423, 'edit_clientmanagement', 'clientmanagement', '2019-10-08 06:52:47', '2019-10-08 06:52:47', 85),
(424, 'add_clientmanagement', 'clientmanagement', '2019-10-08 06:52:47', '2019-10-08 06:52:47', 85),
(425, 'delete_clientmanagement', 'clientmanagement', '2019-10-08 06:52:47', '2019-10-08 06:52:47', 85),
(426, 'browse_leadcreate', 'leadcreate', '2019-10-08 06:54:50', '2019-10-08 06:54:50', 86),
(427, 'read_leadcreate', 'leadcreate', '2019-10-08 06:54:50', '2019-10-08 06:54:50', 86),
(428, 'edit_leadcreate', 'leadcreate', '2019-10-08 06:54:50', '2019-10-08 06:54:50', 86),
(429, 'add_leadcreate', 'leadcreate', '2019-10-08 06:54:50', '2019-10-08 06:54:50', 86),
(430, 'delete_leadcreate', 'leadcreate', '2019-10-08 06:54:50', '2019-10-08 06:54:50', 86),
(431, 'browse_leadlist', 'leadlist', '2019-10-08 06:59:00', '2019-10-08 06:59:00', 87),
(432, 'read_leadlist', 'leadlist', '2019-10-08 06:59:00', '2019-10-08 06:59:00', 87),
(433, 'edit_leadlist', 'leadlist', '2019-10-08 06:59:00', '2019-10-08 06:59:00', 87),
(434, 'add_leadlist', 'leadlist', '2019-10-08 06:59:00', '2019-10-08 06:59:00', 87),
(435, 'delete_leadlist', 'leadlist', '2019-10-08 06:59:00', '2019-10-08 06:59:00', 87),
(436, 'browse_dashboard', 'dashboard', '2019-10-08 10:23:13', '2019-10-08 10:23:13', 88),
(437, 'read_dashboard', 'dashboard', '2019-10-08 10:23:13', '2019-10-08 10:23:13', 88),
(438, 'edit_dashboard', 'dashboard', '2019-10-08 10:23:13', '2019-10-08 10:23:13', 88),
(439, 'add_dashboard', 'dashboard', '2019-10-08 10:23:13', '2019-10-08 10:23:13', 88),
(440, 'delete_dashboard', 'dashboard', '2019-10-08 10:23:13', '2019-10-08 10:23:13', 88),
(441, 'browse_employeecount', 'employeecount', '2019-10-08 10:54:18', '2019-10-08 10:54:18', 89),
(442, 'read_employeecount', 'employeecount', '2019-10-08 10:54:18', '2019-10-08 10:54:18', 89),
(443, 'edit_employeecount', 'employeecount', '2019-10-08 10:54:18', '2019-10-08 10:54:18', 89),
(444, 'add_employeecount', 'employeecount', '2019-10-08 10:54:18', '2019-10-08 10:54:18', 89),
(445, 'delete_employeecount', 'employeecount', '2019-10-08 10:54:18', '2019-10-08 10:54:18', 89),
(446, 'browse_projectcount', 'projectcount', '2019-10-08 10:54:38', '2019-10-08 10:54:38', 90),
(447, 'read_projectcount', 'projectcount', '2019-10-08 10:54:38', '2019-10-08 10:54:38', 90),
(448, 'edit_projectcount', 'projectcount', '2019-10-08 10:54:38', '2019-10-08 10:54:38', 90),
(449, 'add_projectcount', 'projectcount', '2019-10-08 10:54:38', '2019-10-08 10:54:38', 90),
(450, 'delete_projectcount', 'projectcount', '2019-10-08 10:54:38', '2019-10-08 10:54:38', 90),
(451, 'browse_leadcount', 'leadcount', '2019-10-08 10:55:00', '2019-10-08 10:55:00', 91),
(452, 'read_leadcount', 'leadcount', '2019-10-08 10:55:00', '2019-10-08 10:55:00', 91),
(453, 'edit_leadcount', 'leadcount', '2019-10-08 10:55:00', '2019-10-08 10:55:00', 91),
(454, 'add_leadcount', 'leadcount', '2019-10-08 10:55:00', '2019-10-08 10:55:00', 91),
(455, 'delete_leadcount', 'leadcount', '2019-10-08 10:55:00', '2019-10-08 10:55:00', 91),
(456, 'browse_employee_project_count', 'employee_project_count', '2019-10-08 11:00:24', '2019-10-08 11:00:24', 92),
(457, 'read_employee_project_count', 'employee_project_count', '2019-10-08 11:00:24', '2019-10-08 11:00:24', 92),
(458, 'edit_employee_project_count', 'employee_project_count', '2019-10-08 11:00:24', '2019-10-08 11:00:24', 92),
(459, 'add_employee_project_count', 'employee_project_count', '2019-10-08 11:00:24', '2019-10-08 11:00:24', 92),
(460, 'delete_employee_project_count', 'employee_project_count', '2019-10-08 11:00:24', '2019-10-08 11:00:24', 92),
(461, 'browse_employee_lead_count', 'employee_lead_count', '2019-10-08 11:00:51', '2019-10-08 11:00:51', 93),
(462, 'read_employee_lead_count', 'employee_lead_count', '2019-10-08 11:00:51', '2019-10-08 11:00:51', 93),
(463, 'edit_employee_lead_count', 'employee_lead_count', '2019-10-08 11:00:51', '2019-10-08 11:00:51', 93),
(464, 'add_employee_lead_count', 'employee_lead_count', '2019-10-08 11:00:52', '2019-10-08 11:00:52', 93),
(465, 'delete_employee_lead_count', 'employee_lead_count', '2019-10-08 11:00:52', '2019-10-08 11:00:52', 93),
(466, 'browse_clientcount', 'clientcount', '2019-10-08 11:03:21', '2019-10-08 11:03:21', 94),
(467, 'read_clientcount', 'clientcount', '2019-10-08 11:03:21', '2019-10-08 11:03:21', 94),
(468, 'edit_clientcount', 'clientcount', '2019-10-08 11:03:21', '2019-10-08 11:03:21', 94),
(469, 'add_clientcount', 'clientcount', '2019-10-08 11:03:21', '2019-10-08 11:03:21', 94),
(470, 'delete_clientcount', 'clientcount', '2019-10-08 11:03:21', '2019-10-08 11:03:21', 94),
(476, 'browse_projectplanedit', 'projectplanedit', '2019-10-09 09:37:04', '2019-10-09 09:37:04', 96),
(477, 'read_projectplanedit', 'projectplanedit', '2019-10-09 09:37:04', '2019-10-09 09:37:04', 96),
(478, 'edit_projectplanedit', 'projectplanedit', '2019-10-09 09:37:04', '2019-10-09 09:37:04', 96),
(479, 'add_projectplanedit', 'projectplanedit', '2019-10-09 09:37:04', '2019-10-09 09:37:04', 96),
(480, 'delete_projectplanedit', 'projectplanedit', '2019-10-09 09:37:04', '2019-10-09 09:37:04', 96),
(481, 'browse_projectplanview', 'projectplanview', '2019-10-09 09:44:51', '2019-10-09 09:44:51', 97),
(482, 'read_projectplanview', 'projectplanview', '2019-10-09 09:44:51', '2019-10-09 09:44:51', 97),
(483, 'edit_projectplanview', 'projectplanview', '2019-10-09 09:44:51', '2019-10-09 09:44:51', 97),
(484, 'add_projectplanview', 'projectplanview', '2019-10-09 09:44:51', '2019-10-09 09:44:51', 97),
(485, 'delete_projectplanview', 'projectplanview', '2019-10-09 09:44:51', '2019-10-09 09:44:51', 97),
(486, 'browse_leadprojectbtn', 'leadprojectbtn', '2019-10-09 10:40:04', '2019-10-09 10:40:04', 98),
(487, 'read_leadprojectbtn', 'leadprojectbtn', '2019-10-09 10:40:04', '2019-10-09 10:40:04', 98),
(488, 'edit_leadprojectbtn', 'leadprojectbtn', '2019-10-09 10:40:04', '2019-10-09 10:40:04', 98),
(489, 'add_leadprojectbtn', 'leadprojectbtn', '2019-10-09 10:40:04', '2019-10-09 10:40:04', 98),
(490, 'delete_leadprojectbtn', 'leadprojectbtn', '2019-10-09 10:40:04', '2019-10-09 10:40:04', 98),
(491, 'browse_daily_plan', 'daily_plan', '2019-10-10 04:58:43', '2019-10-10 04:58:43', 99),
(492, 'read_daily_plan', 'daily_plan', '2019-10-10 04:58:43', '2019-10-10 04:58:43', 99),
(493, 'edit_daily_plan', 'daily_plan', '2019-10-10 04:58:43', '2019-10-10 04:58:43', 99),
(494, 'add_daily_plan', 'daily_plan', '2019-10-10 04:58:43', '2019-10-10 04:58:43', 99),
(495, 'delete_daily_plan', 'daily_plan', '2019-10-10 04:58:43', '2019-10-10 04:58:43', 99),
(496, 'browse_assignemployeebtn', 'assignemployeebtn', '2019-10-11 06:30:20', '2019-10-11 06:30:20', 100),
(497, 'read_assignemployeebtn', 'assignemployeebtn', '2019-10-11 06:30:20', '2019-10-11 06:30:20', 100),
(498, 'edit_assignemployeebtn', 'assignemployeebtn', '2019-10-11 06:30:20', '2019-10-11 06:30:20', 100),
(499, 'add_assignemployeebtn', 'assignemployeebtn', '2019-10-11 06:30:20', '2019-10-11 06:30:20', 100),
(500, 'delete_assignemployeebtn', 'assignemployeebtn', '2019-10-11 06:30:21', '2019-10-11 06:30:21', 100),
(501, 'browse_projectplansavebtn', 'projectplansavebtn', '2019-10-11 06:49:15', '2019-10-11 06:49:15', 101),
(502, 'read_projectplansavebtn', 'projectplansavebtn', '2019-10-11 06:49:15', '2019-10-11 06:49:15', 101),
(503, 'edit_projectplansavebtn', 'projectplansavebtn', '2019-10-11 06:49:15', '2019-10-11 06:49:15', 101),
(504, 'add_projectplansavebtn', 'projectplansavebtn', '2019-10-11 06:49:15', '2019-10-11 06:49:15', 101),
(505, 'delete_projectplansavebtn', 'projectplansavebtn', '2019-10-11 06:49:15', '2019-10-11 06:49:15', 101),
(506, 'browse_document_request_btn', 'document_request_btn', '2019-10-14 06:26:21', '2019-10-14 06:26:21', 102),
(507, 'read_document_request_btn', 'document_request_btn', '2019-10-14 06:26:21', '2019-10-14 06:26:21', 102),
(508, 'edit_document_request_btn', 'document_request_btn', '2019-10-14 06:26:21', '2019-10-14 06:26:21', 102),
(509, 'add_document_request_btn', 'document_request_btn', '2019-10-14 06:26:22', '2019-10-14 06:26:22', 102),
(510, 'delete_document_request_btn', 'document_request_btn', '2019-10-14 06:26:22', '2019-10-14 06:26:22', 102),
(511, 'browse_document_hee', 'document_hee', '2019-10-15 08:25:35', '2019-10-15 08:25:35', 103),
(512, 'read_document_hee', 'document_hee', '2019-10-15 08:25:35', '2019-10-15 08:25:35', 103),
(513, 'edit_document_hee', 'document_hee', '2019-10-15 08:25:35', '2019-10-15 08:25:35', 103),
(514, 'add_document_hee', 'document_hee', '2019-10-15 08:25:35', '2019-10-15 08:25:35', 103),
(515, 'delete_document_hee', 'document_hee', '2019-10-15 08:25:35', '2019-10-15 08:25:35', 103),
(516, 'browse_document_hee', 'document_hee', '2019-10-15 08:25:35', '2019-10-15 08:25:35', 104),
(517, 'read_document_hee', 'document_hee', '2019-10-15 08:25:35', '2019-10-15 08:25:35', 104),
(518, 'edit_document_hee', 'document_hee', '2019-10-15 08:25:35', '2019-10-15 08:25:35', 104),
(519, 'add_document_hee', 'document_hee', '2019-10-15 08:25:35', '2019-10-15 08:25:35', 104),
(520, 'delete_document_hee', 'document_hee', '2019-10-15 08:25:35', '2019-10-15 08:25:35', 104),
(521, 'browse_document_hee', 'document_hee', '2019-10-15 08:25:36', '2019-10-15 08:25:36', 105),
(522, 'read_document_hee', 'document_hee', '2019-10-15 08:25:36', '2019-10-15 08:25:36', 105),
(523, 'edit_document_hee', 'document_hee', '2019-10-15 08:25:36', '2019-10-15 08:25:36', 105),
(524, 'add_document_hee', 'document_hee', '2019-10-15 08:25:36', '2019-10-15 08:25:36', 105),
(525, 'delete_document_hee', 'document_hee', '2019-10-15 08:25:36', '2019-10-15 08:25:36', 105),
(526, 'browse_approval_list', 'approval_list', '2019-10-15 12:14:04', '2019-10-15 12:14:04', 106),
(527, 'read_approval_list', 'approval_list', '2019-10-15 12:14:04', '2019-10-15 12:14:04', 106),
(528, 'edit_approval_list', 'approval_list', '2019-10-15 12:14:04', '2019-10-15 12:14:04', 106),
(529, 'add_approval_list', 'approval_list', '2019-10-15 12:14:04', '2019-10-15 12:14:04', 106),
(530, 'delete_approval_list', 'approval_list', '2019-10-15 12:14:04', '2019-10-15 12:14:04', 106),
(531, 'browse_Lead_mou_proposal_button', 'Lead_mou_proposal_button', '2019-10-16 04:38:34', '2019-10-16 04:38:34', 107),
(532, 'read_Lead_mou_proposal_button', 'Lead_mou_proposal_button', '2019-10-16 04:38:35', '2019-10-16 04:38:35', 107),
(533, 'edit_Lead_mou_proposal_button', 'Lead_mou_proposal_button', '2019-10-16 04:38:35', '2019-10-16 04:38:35', 107),
(534, 'add_Lead_mou_proposal_button', 'Lead_mou_proposal_button', '2019-10-16 04:38:35', '2019-10-16 04:38:35', 107),
(535, 'delete_Lead_mou_proposal_button', 'Lead_mou_proposal_button', '2019-10-16 04:38:35', '2019-10-16 04:38:35', 107),
(536, 'browse_document_search_request_btn', 'document_search_request_btn', '2019-10-16 06:22:43', '2019-10-16 06:22:43', 108),
(537, 'read_document_search_request_btn', 'document_search_request_btn', '2019-10-16 06:22:43', '2019-10-16 06:22:43', 108),
(538, 'edit_document_search_request_btn', 'document_search_request_btn', '2019-10-16 06:22:43', '2019-10-16 06:22:43', 108),
(539, 'add_document_search_request_btn', 'document_search_request_btn', '2019-10-16 06:22:43', '2019-10-16 06:22:43', 108),
(540, 'delete_document_search_request_btn', 'document_search_request_btn', '2019-10-16 06:22:43', '2019-10-16 06:22:43', 108),
(541, 'browse_leadapprove_btn', 'leadapprove_btn', '2019-10-16 14:33:14', '2019-10-16 14:33:14', 109),
(542, 'read_leadapprove_btn', 'leadapprove_btn', '2019-10-16 14:33:14', '2019-10-16 14:33:14', 109),
(543, 'edit_leadapprove_btn', 'leadapprove_btn', '2019-10-16 14:33:14', '2019-10-16 14:33:14', 109),
(544, 'add_leadapprove_btn', 'leadapprove_btn', '2019-10-16 14:33:14', '2019-10-16 14:33:14', 109),
(545, 'delete_leadapprove_btn', 'leadapprove_btn', '2019-10-16 14:33:14', '2019-10-16 14:33:14', 109),
(546, 'browse_test', 'test', '2019-10-25 07:32:12', '2019-10-25 07:32:12', 110),
(547, 'read_test', 'test', '2019-10-25 07:32:12', '2019-10-25 07:32:12', 110),
(548, 'edit_test', 'test', '2019-10-25 07:32:12', '2019-10-25 07:32:12', 110),
(549, 'add_test', 'test', '2019-10-25 07:32:12', '2019-10-25 07:32:12', 110),
(550, 'delete_test', 'test', '2019-10-25 07:32:12', '2019-10-25 07:32:12', 110);

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(20) UNSIGNED NOT NULL,
  `role_id` int(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(171, 1),
(172, 1),
(173, 1),
(173, 4),
(174, 1),
(175, 1),
(176, 1),
(177, 1),
(178, 1),
(178, 4),
(179, 1),
(180, 1),
(181, 1),
(181, 3),
(182, 1),
(183, 1),
(183, 3),
(184, 1),
(184, 3),
(185, 1),
(185, 3),
(186, 1),
(186, 2),
(186, 3),
(186, 4),
(186, 5),
(186, 7),
(187, 2),
(187, 4),
(187, 5),
(187, 7),
(188, 2),
(188, 4),
(188, 5),
(188, 7),
(189, 1),
(189, 2),
(189, 4),
(189, 5),
(189, 7),
(190, 1),
(190, 2),
(190, 4),
(190, 5),
(190, 7),
(191, 1),
(191, 2),
(191, 4),
(191, 5),
(191, 7),
(192, 1),
(192, 2),
(192, 4),
(192, 5),
(192, 7),
(193, 1),
(193, 2),
(193, 4),
(193, 5),
(193, 7),
(194, 1),
(194, 2),
(194, 4),
(194, 5),
(194, 7),
(195, 1),
(195, 2),
(195, 4),
(195, 5),
(195, 7),
(196, 1),
(196, 4),
(196, 5),
(196, 7),
(197, 4),
(197, 5),
(197, 7),
(198, 1),
(198, 4),
(198, 5),
(198, 7),
(199, 4),
(199, 5),
(199, 7),
(200, 1),
(200, 4),
(200, 5),
(200, 7),
(201, 2),
(201, 4),
(201, 5),
(201, 7),
(202, 2),
(202, 4),
(202, 5),
(202, 7),
(203, 1),
(203, 2),
(203, 4),
(203, 5),
(203, 7),
(204, 1),
(204, 2),
(204, 4),
(204, 5),
(204, 7),
(205, 2),
(205, 4),
(205, 5),
(205, 7),
(206, 2),
(206, 4),
(206, 5),
(206, 7),
(207, 1),
(207, 2),
(207, 4),
(207, 5),
(207, 7),
(208, 2),
(208, 4),
(208, 5),
(208, 7),
(209, 2),
(209, 4),
(209, 5),
(209, 7),
(210, 2),
(210, 4),
(210, 5),
(210, 7),
(211, 2),
(211, 4),
(211, 5),
(211, 7),
(212, 2),
(212, 4),
(212, 5),
(212, 7),
(213, 1),
(213, 2),
(213, 4),
(213, 5),
(213, 7),
(214, 1),
(214, 2),
(214, 4),
(214, 5),
(214, 7),
(215, 1),
(215, 2),
(215, 4),
(215, 5),
(215, 7),
(216, 1),
(216, 5),
(217, 5),
(218, 5),
(219, 1),
(219, 5),
(220, 5),
(221, 1),
(221, 2),
(221, 5),
(221, 7),
(222, 2),
(222, 5),
(222, 7),
(223, 1),
(223, 2),
(223, 5),
(223, 7),
(224, 1),
(224, 2),
(224, 5),
(224, 7),
(225, 1),
(225, 2),
(225, 5),
(225, 7),
(231, 2),
(231, 5),
(231, 7),
(232, 1),
(232, 2),
(232, 5),
(232, 7),
(233, 1),
(233, 2),
(233, 5),
(233, 7),
(234, 2),
(234, 5),
(234, 7),
(235, 2),
(235, 5),
(235, 7),
(236, 1),
(239, 1),
(240, 1),
(241, 2),
(241, 4),
(241, 5),
(242, 1),
(242, 2),
(242, 4),
(242, 5),
(243, 1),
(243, 2),
(243, 4),
(243, 5),
(244, 1),
(244, 2),
(244, 4),
(244, 5),
(245, 2),
(245, 4),
(245, 5),
(246, 1),
(246, 2),
(246, 4),
(246, 5),
(247, 2),
(247, 4),
(247, 5),
(248, 2),
(248, 4),
(248, 5),
(249, 1),
(249, 2),
(249, 4),
(249, 5),
(250, 2),
(250, 4),
(250, 5),
(251, 1),
(251, 2),
(251, 4),
(251, 5),
(252, 2),
(252, 4),
(252, 5),
(253, 2),
(253, 4),
(253, 5),
(254, 1),
(254, 2),
(254, 4),
(254, 5),
(255, 1),
(255, 2),
(255, 4),
(255, 5),
(256, 1),
(256, 2),
(256, 4),
(256, 5),
(257, 2),
(257, 4),
(257, 5),
(258, 1),
(258, 2),
(258, 4),
(258, 5),
(259, 1),
(259, 2),
(259, 4),
(259, 5),
(260, 1),
(260, 2),
(260, 4),
(260, 5),
(261, 1),
(261, 2),
(261, 4),
(261, 5),
(262, 1),
(262, 2),
(262, 4),
(262, 5),
(263, 2),
(263, 4),
(263, 5),
(264, 1),
(264, 2),
(264, 4),
(264, 5),
(265, 1),
(265, 2),
(265, 4),
(265, 5),
(266, 1),
(266, 2),
(266, 4),
(266, 5),
(266, 7),
(267, 1),
(267, 2),
(267, 4),
(267, 5),
(267, 7),
(268, 2),
(268, 4),
(268, 5),
(268, 7),
(269, 1),
(269, 2),
(269, 4),
(269, 5),
(269, 7),
(270, 2),
(270, 4),
(270, 5),
(270, 7),
(271, 1),
(271, 2),
(271, 4),
(271, 5),
(271, 7),
(272, 1),
(272, 2),
(272, 4),
(272, 5),
(272, 7),
(273, 2),
(273, 4),
(273, 5),
(273, 7),
(274, 1),
(274, 2),
(274, 4),
(274, 5),
(274, 7),
(275, 1),
(275, 2),
(275, 4),
(275, 5),
(275, 7),
(281, 2),
(281, 4),
(281, 5),
(281, 7),
(282, 1),
(282, 2),
(282, 4),
(282, 5),
(282, 7),
(283, 1),
(283, 2),
(283, 4),
(283, 5),
(283, 7),
(284, 2),
(284, 4),
(284, 5),
(284, 7),
(285, 1),
(285, 2),
(285, 4),
(285, 5),
(285, 7),
(286, 4),
(286, 5),
(287, 4),
(287, 5),
(288, 4),
(288, 5),
(289, 4),
(289, 5),
(290, 1),
(290, 4),
(290, 5),
(291, 1),
(291, 2),
(291, 5),
(292, 2),
(292, 5),
(293, 2),
(293, 5),
(294, 1),
(294, 2),
(294, 5),
(295, 2),
(295, 5),
(296, 1),
(296, 2),
(296, 4),
(296, 5),
(296, 7),
(297, 1),
(297, 2),
(297, 4),
(297, 5),
(297, 7),
(298, 1),
(298, 2),
(298, 4),
(298, 5),
(298, 7),
(299, 2),
(299, 4),
(299, 5),
(299, 7),
(300, 2),
(300, 4),
(300, 5),
(300, 7),
(301, 1),
(301, 2),
(301, 5),
(301, 7),
(302, 2),
(302, 5),
(302, 7),
(303, 2),
(303, 5),
(303, 7),
(304, 2),
(304, 5),
(304, 7),
(305, 1),
(305, 2),
(305, 5),
(305, 7),
(306, 2),
(306, 3),
(306, 4),
(306, 5),
(307, 1),
(307, 2),
(307, 3),
(307, 4),
(307, 5),
(308, 2),
(308, 3),
(308, 4),
(308, 5),
(309, 2),
(309, 3),
(309, 4),
(309, 5),
(310, 1),
(310, 2),
(310, 3),
(310, 4),
(310, 5),
(311, 1),
(311, 2),
(311, 4),
(311, 5),
(312, 1),
(312, 2),
(312, 4),
(312, 5),
(313, 1),
(313, 2),
(313, 4),
(313, 5),
(314, 2),
(314, 4),
(314, 5),
(315, 1),
(315, 2),
(315, 4),
(315, 5),
(316, 1),
(316, 2),
(316, 4),
(316, 5),
(317, 1),
(317, 2),
(317, 4),
(317, 5),
(318, 1),
(318, 2),
(318, 4),
(318, 5),
(319, 2),
(319, 4),
(319, 5),
(320, 2),
(320, 4),
(320, 5),
(321, 1),
(322, 1),
(323, 1),
(327, 1),
(330, 1),
(331, 1),
(339, 1),
(346, 1),
(346, 2),
(346, 4),
(346, 5),
(347, 1),
(347, 2),
(347, 4),
(347, 5),
(348, 2),
(348, 4),
(348, 5),
(349, 1),
(349, 2),
(349, 4),
(349, 5),
(350, 2),
(350, 4),
(350, 5),
(351, 1),
(351, 4),
(351, 7),
(352, 4),
(353, 1),
(353, 4),
(354, 4),
(355, 1),
(355, 4),
(356, 7),
(357, 1),
(357, 7),
(358, 7),
(359, 1),
(359, 7),
(361, 5),
(362, 5),
(363, 1),
(363, 5),
(364, 5),
(365, 1),
(365, 5),
(366, 1),
(366, 5),
(367, 5),
(368, 5),
(369, 1),
(369, 5),
(370, 1),
(370, 5),
(371, 1),
(371, 2),
(371, 5),
(371, 7),
(372, 1),
(372, 2),
(372, 5),
(372, 7),
(373, 1),
(373, 2),
(373, 5),
(373, 7),
(374, 2),
(374, 5),
(374, 7),
(375, 1),
(375, 2),
(375, 5),
(375, 7),
(376, 5),
(376, 7),
(377, 5),
(377, 7),
(378, 1),
(378, 5),
(378, 7),
(379, 1),
(379, 5),
(379, 7),
(380, 5),
(380, 7),
(381, 1),
(382, 1),
(384, 1),
(386, 4),
(386, 5),
(386, 7),
(387, 1),
(387, 4),
(387, 5),
(387, 7),
(388, 4),
(388, 5),
(388, 7),
(389, 4),
(389, 5),
(389, 7),
(390, 1),
(390, 4),
(390, 5),
(390, 7),
(391, 4),
(391, 5),
(392, 1),
(392, 4),
(392, 5),
(393, 1),
(393, 4),
(393, 5),
(394, 4),
(394, 5),
(395, 1),
(395, 4),
(395, 5),
(398, 1),
(399, 1),
(400, 1),
(402, 1),
(403, 1),
(404, 1),
(406, 5),
(407, 5),
(408, 5),
(409, 1),
(409, 5),
(410, 5),
(411, 1),
(411, 5),
(412, 5),
(413, 1),
(413, 5),
(414, 1),
(414, 5),
(415, 1),
(415, 5),
(416, 1),
(418, 1),
(420, 1),
(421, 2),
(421, 4),
(421, 5),
(422, 2),
(422, 4),
(422, 5),
(423, 2),
(423, 4),
(423, 5),
(424, 2),
(424, 4),
(424, 5),
(425, 2),
(425, 4),
(425, 5),
(426, 2),
(426, 4),
(426, 5),
(427, 2),
(427, 4),
(427, 5),
(428, 1),
(428, 2),
(428, 4),
(428, 5),
(429, 2),
(429, 4),
(429, 5),
(430, 2),
(430, 4),
(430, 5),
(431, 4),
(431, 5),
(431, 7),
(432, 1),
(432, 4),
(432, 5),
(432, 7),
(433, 4),
(433, 5),
(433, 7),
(434, 4),
(434, 5),
(434, 7),
(435, 4),
(435, 5),
(435, 7),
(437, 1),
(438, 1),
(440, 1),
(441, 1),
(444, 1),
(445, 1),
(447, 1),
(448, 1),
(449, 1),
(451, 1),
(453, 1),
(456, 2),
(457, 2),
(458, 2),
(459, 2),
(460, 2),
(461, 1),
(462, 1),
(465, 1),
(467, 1),
(469, 1),
(476, 2),
(476, 7),
(477, 1),
(477, 2),
(477, 7),
(478, 2),
(478, 7),
(479, 2),
(479, 7),
(480, 2),
(480, 7),
(481, 5),
(482, 5),
(483, 1),
(483, 5),
(484, 1),
(484, 5),
(485, 5),
(486, 1),
(486, 5),
(487, 1),
(487, 5),
(488, 1),
(488, 5),
(489, 1),
(489, 5),
(490, 1),
(490, 5),
(491, 2),
(491, 5),
(492, 2),
(492, 5),
(493, 2),
(493, 5),
(494, 1),
(494, 2),
(494, 5),
(495, 1),
(495, 2),
(495, 5),
(496, 1),
(496, 5),
(497, 1),
(497, 5),
(498, 5),
(499, 1),
(499, 5),
(500, 5),
(501, 5),
(502, 1),
(502, 5),
(503, 1),
(503, 5),
(504, 1),
(504, 5),
(505, 1),
(505, 5),
(506, 2),
(507, 1),
(507, 2),
(508, 2),
(509, 1),
(509, 2),
(510, 2),
(512, 1),
(515, 1),
(516, 1),
(517, 1),
(521, 1),
(522, 1),
(524, 1),
(527, 1),
(529, 1),
(530, 1),
(532, 1),
(534, 1),
(536, 2),
(537, 2),
(538, 1),
(538, 2),
(539, 1),
(539, 2),
(540, 1),
(540, 2),
(542, 1),
(543, 1),
(544, 1),
(547, 1),
(548, 1),
(549, 1);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(20) NOT NULL,
  `name` varchar(190) CHARACTER SET latin1 NOT NULL,
  `display_name` varchar(190) CHARACTER SET latin1 NOT NULL,
  `user_type` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `user_type`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'Aquila Gross', 1, '2019-08-13 12:27:47', '2019-10-26 08:48:59'),
(2, 'Employee', 'Employee', 2, '2019-08-13 12:27:47', '2019-08-15 08:06:24'),
(3, 'Receptionist', 'Receptionist', 3, '2019-08-13 12:27:47', '2019-08-23 00:00:00'),
(4, 'Customer Relation Incharge', 'Customer Relation Incharge', 4, '2019-08-13 12:27:47', '2019-08-14 00:00:00'),
(5, 'Lead Consultant', 'Lead Consultant', 5, '2019-08-13 12:27:47', '2019-08-30 11:53:47'),
(7, 'Content Developer', 'Content Developer', 7, '2019-09-24 05:21:38', '2019-09-24 05:21:38');

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `id` int(11) NOT NULL,
  `title` varchar(191) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `state_list`
--

CREATE TABLE `state_list` (
  `id` int(11) NOT NULL,
  `state` varchar(191) DEFAULT NULL,
  `title` varchar(191) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int(20) NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) DEFAULT '1',
  `active` int(11) DEFAULT NULL,
  `joining_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` datetime DEFAULT NULL,
  `password` varchar(220) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` text COLLATE utf8mb4_unicode_ci,
  `expires_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `role_id`, `first_name`, `last_name`, `status`, `active`, `joining_date`, `email`, `mobile`, `email_verified_at`, `password`, `token`, `expires_at`, `remember_token`, `created_at`, `updated_at`, `deleted_at`, `last_login`) VALUES
(1, 'Bramma Learning', 1, 'Bramma', 'Learning', 0, 1, NULL, 'admin@admin.com', '5463746748', NULL, '$2y$10$4himm3/oQqkrK83v.CbQJOQxDYuU.6RWgtLXlHpMmSoZnZVc/cXwq', '', '2019-11-22 05:22:02', 'IJRwhX9q1qOnMRE8ieZnq19jvh8teNpnWd315vH7NTZXFis3qJRI2vlfvFqz', '2019-08-14 05:38:59', '2019-10-28 04:35:33', NULL, '2019-10-28 04:35:33');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_modules`
--
ALTER TABLE `admin_modules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `model_id` (`model_id`),
  ADD KEY `auth_user_id` (`auth_user_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`),
  ADD KEY `module_id` (`module_id`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `state_list`
--
ALTER TABLE `state_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `role_id` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `admin_modules`
--
ALTER TABLE `admin_modules`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;
--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` bigint(200) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=551;
--
-- AUTO_INCREMENT for table `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `permission_id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=550;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `state_list`
--
ALTER TABLE `state_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
