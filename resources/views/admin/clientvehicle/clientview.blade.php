@extends('admin.layouts.app')
@section('css')
@endsection
    @section('content')
    <!-- Main Container -->
    <main class="app-content bg-white animated fadeIn"> 
        <!-- Sub Title---->
        <div id="submenu_subheader" class="submenu-subheader submenu-grid__item ">          
            <div class="submenu-subheader__title">
                <h4 class="title-caption"><i class="fas fa-user-shield"></i>Client</h4>
            </div>           
            <div class="submenu-subheader__toolbar">
                <div class="submenu-subheader__toolbar-wrapper">
                 <table class="table  table-bordered" id="sampleTable">
                 <div class="sub-header-body">
                <div class="table-responsive">
                        
                            <tr>
                                <th>Name</th>
                                <th>Company</th>
                                <th>Agreement</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th>Work Duration</th>
                                <th>Created</th>
                                <th>Edit Action</th>
                                <th>Delete Action</th>
                            </tr>
                                               
                        <body>
                            @foreach($data as $role)
                                <tr>
                                    <td>
                                        {{$role->name}}
                                    </td>
                                    <td>
                                        {{$role->company}}</label>
                                    </td>
                                    <td>
                                        {{$role->agreement}}</label>
                                    </td>
                                    <td>
                                        {{$role->start_date}}</label>
                                    </td>
                                    <td>
                                        {{$role->end_date}}</label>
                                    </td>
                                    <td>
                                        {{$role->work_duration}}</label>
                                    </td>
                                    <td>
                                        {{date('D M Y h:i A',strtotime($role->created_at))}}
                                    </td>
                                    <td class="text-left">
                                       
                                            <a href="{{url('data/edit/'.$role->id)}}" class="btn action-button bg-primary-blue"
                                            data-toggle="tooltip" data-placement="top" data-html="true" title="Edit Role">
                                            <i class="fa fa-pencil-square-o "></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                         
                                    </td>
                                    <td class="text-left">
                                       
                                            <a href="{{url('data/delete/'.$role->id)}}" class="btn action-button bg-primary-red"
                                            data-toggle="tooltip" data-placement="top" data-html="true" title="Delete Role">
                                            <i class="glyphicon glyphicon-trash "></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                         
                                         
                                    </td>
                                </tr>
                            @endforeach
                        </body>
                    </table>
                    
               
                     <!-- {{-- <p class="bs-component"> -->
                                <!-- @if(can('add_roles'))
                                <a class="btn btn-primary waves-effect waves-light" href="{{url('role/create')}}" role="button">
                                                    <i class="fa fa-plus-circle"></i>
                                                Create Role
                                            </a> @endif -->
    
                                <!-- @if(can('add_roles')) {{--
                                <a class="btn btn-primary waves-effect waves-light" href="{{url('role/create')}}" role="button">--}}
                                    {{--<i class="fa fa-plus-circle"></i>--}}
                                    {{--Create Role--}}
                                    {{--</a>-- @endif
                            </p>  --}} -->
                </div>
            </div>
        </div>
        <!-- END Sub Title---->
        <!-- Body Part -->
        
            <!-- @if(can('browse_roles')) -->
            <div class="sub-header-body">
                <div class="table-responsive">
                   
                </div>
            @else @include('admin.no-access-content') @endif                
        </div>
        <!-- END Body Part -->
    </main>
    <!-- Main END -->
  
        

    @endsection @section('js')

    <script type="text/javascript" src="{{asset('admin/js/plugins/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $('#sampleTable').DataTable({
            bPaginate: false,
            bSort: false,
            bFilter: false,
            bInfo: false,
            responsive:true
        });
    </script>
    @endsection