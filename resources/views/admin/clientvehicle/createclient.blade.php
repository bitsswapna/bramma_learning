@extends('admin.layouts.app')

@section('css')

@endsection

@section('content')
    <main class="app-content">
        <div class="app-title">
            <div class="col-md-6 col-sm-12 col-xs-12 rem-pad">
                <h1><i class="fa fa-plus-circle"></i> Add client Details</h1>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 btn-right rem-pad">
                @if(can('browse_roles'))
                <a class="btn btn-success waves-effect waves-light" href="{{url('http://admin.vehicleproject.local.com/clientview')}}"><i class="fa fa-list"></i>Client Lists</a>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="tile">
                    <form method="post" id="createrRole" name="createrRole" action="{{url('vehicle/create')}}">
                        <div class="row">
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                {{csrf_field()}}
                                <div class="form-group @if($errors->first('display_name')) has-danger @endif">
                                    <label for="display_name">Name</label>
                                    <input class="form-control @if($errors->first('name')) is-invalid @endif" id="display_name" name="name" type="text" placeholder="Enter name" autocomplete="off" value="{{old('name')}}">
                                    @if($errors->first('name'))
                                        <sapn class="form-control-feedback">{{$errors->first('name')}}</sapn>
                                    @endif
                                </div>
                                <div class="form-group @if($errors->first('name')) has-danger @endif">
                                    <label for="name">Company</label>
                                    <input class="form-control @if($errors->first('name')) is-invalid @endif" id="name" type="text" name="company" placeholder="Enter company name" autocomplete="off" value="{{old('company')}}">
                                    @if($errors->first('company'))
                                        <sapn class="form-control-feedback">{{$errors->first('company')}}</sapn>
                                    @endif
                                </div>
                                <div class="form-group @if($errors->first('name')) has-danger @endif">
                                    <label for="name">Agreement</label>
                                    <input class="form-control @if($errors->first('name')) is-invalid @endif" id="name" type="text" name="agreement" placeholder="Enter the company agrement" autocomplete="off" value="{{old('agreement')}}">
                                    @if($errors->first('agreement'))
                                        <sapn class="form-control-feedback">{{$errors->first('agreement')}}</sapn>
                                    @endif
                                </div>
                                <div class="form-group @if($errors->first('name')) has-danger @endif">
                                    <label for="name">Start Date</label>
                                    <input class="form-control @if($errors->first('name')) is-invalid @endif" id="name" type="date" name="startdate"  autocomplete="off" value="{{old('start_date')}}">
                                    @if($errors->first('startdate'))
                                        <sapn class="form-control-feedback">{{$errors->first('startdate')}}</sapn>
                                    @endif
                                </div>
                                <div class="form-group @if($errors->first('name')) has-danger @endif">
                                    <label for="name">End Date</label>
                                    <input class="form-control @if($errors->first('name')) is-invalid @endif" id="name" type="date" name="enddate"  autocomplete="off" value="{{old('end_date')}}">
                                    @if($errors->first('enddate'))
                                        <sapn class="form-control-feedback">{{$errors->first('enddate')}}</sapn>
                                    @endif
                                </div>
                                <div class="form-group @if($errors->first('name')) has-danger @endif">
                                    <label for="name">Work Duration</label>
                                    <input class="form-control @if($errors->first('name')) is-invalid @endif" id="name" type="text" name="wrkduration" placeholder="Enter the days for complete the work" autocomplete="off" value="{{old('work_duration')}}">
                                    @if($errors->first('wrkduration'))
                                        <sapn class="form-control-feedback">{{$errors->first('wrkduration')}}</sapn>
                                    @endif
                                </div>

                            </div>
                        </div>

                        <div class="tile-footer">
                            <button class="btn btn-primary waves-effect waves-light" type="submit">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>
@endsection

@section('js')
    <!--Notifications Message Section-->
    @include('admin.layouts.notifications')

    <script>
        $("#createrRole").validate({
            rules: {
                // simple rule, converted to {required:true}
                display_name: "required",
                name: "required",
            }
        });
    </script>
@endsection