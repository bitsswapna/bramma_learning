@extends('admin.layouts.app')

@section('css')
    <style>
        ul {
            list-style-type: none;
        }

        span.label-text::first-letter {
            text-transform: uppercase;
        }
    </style>
@endsection

@section('content')
    <main class="app-content bg-white">
        <!-- Sub Title---->
        <div id="submenu_subheader" class="submenu-subheader submenu-grid__item ">          
            <div class="submenu-subheader__title">
                <h4 class="title-caption"><i class="fas fa-user-shield"></i>  Edit Client</h4>
            </div>           
            <div class="submenu-subheader__toolbar">
                <div class="submenu-subheader__toolbar-wrapper">
                    <a  data-toggle="tooltip" data-placement="left" data-html="true"  title="edit List" class="btn bg-primary-blue btn-sm waves-effect waves-light  br-0" href="{{url('clientview')}}">
                        <i class="fa fa-list"></i>Client Lists</a>
                </div>
            </div>
        </div>
        <!-- Body Part -->
        <div class="sub-header-body">
            <form method="post" action="{{url('updat/edit/'.$ed->id)}}" name="editdata" id="editRole">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="display_name">name</label>
                            <input class="form-control br-0" id="display_name" name="name" type="text" placeholder="Enter name" required autocomplete="off" value="{{$ed->name}}">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="name">Company</label>
                            <input class="form-control br-0"  type="text" name="company" placeholder="Enter company" autocomplete="off"  value="{{$ed->company}}">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="name">Agreement</label>
                            <input class="form-control br-0" id="name" type="text" name="agreement" placeholder="Enter agreement" autocomplete="off"  value="{{$ed->agreement}}">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="name">Start Date</label>
                            <input class="form-control br-0" id="name" type="text" name="startdate" autocomplete="off"  value="{{$ed->start_date}}">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="name">End Date</label>
                            <input class="form-control br-0" id="name" type="text" name="enddate"  autocomplete="off"  value="{{$ed->end_date}}">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="name">Work Duration</label>
                            <input class="form-control br-0" id="name" type="text" name="wrkduration" placeholder="Enter work duration" autocomplete="off"  value="{{$ed->work_duration}}">
                        </div>
                    </div>
                    
                </div>    

                <!-- <h4><strong>Permission</strong></h4>
                <div class="animated-checkbox ">
                    <div class="table-responsive">
                        <table class="table table-bordered " id="sampleTable">
                            <tbody>
                                <?php
                                $role_permissions = (isset($role)) ? $role->permissions->pluck('key')->toArray() : [];
                                ?>
                            @foreach(App\Permission::all()->groupBy('table_name') as $table => $permission)
                                <tr>
                                    <th>
                                        <div class="checkbox checkbox-primary">
                                        <input type="checkbox" id="{{$table}}" class="permission-group">
                                        <label>
                                        <span class="label-text">{{ucfirst(str_replace('_',' ', $table))}}</span>
                                        </label>
                                        </div>
                                    </th>
                                    @foreach($permission as $perm)
                                        <td>
                                        <div class="checkbox checkbox-primary">
                                            <input type="checkbox" id="permission-{{$perm->id}}" name="permissions[]" class="the-permission" value="{{$perm->id}}" @if(in_array($perm->key, $role_permissions)) checked @endif>
                                            <label>
                                                <span class="label-text">{{ucfirst(str_replace('_', ' ', $perm->key))}}</span>
                                            </label>
                                        </div>
                                        </td>
                                    @endforeach
                                </tr>
                            @endforeach
                            </tbody> -->
                        </table>
                    </div>
                </div>
                <div class="sub-header-footer text-right">
                    <button class="btn bg-primary-blue br-0  waves-effect waves-light btn-sm" type="submit">Update</button>
                </div>
                
            </form>
        </div>
        <!-- Footer Part END-->

        <!-- Footer Part END-->      
    </main>
@endsection

@section('script')

    <!--Notifications Message Section-->
    @include('admin.layouts.notifications')
    <script>
        $("#editRole").validate({
            rules: {
                // simple rule, converted to {required:true}
                display_name: "required",
                name: "required",
            }
        });
    </script>

    <script>
        $('document').ready(function () {

            $('.permission-group').on('change', function(){
                $(this).parents('tr').find(".the-permission").prop('checked', this.checked);
            });

            function parentChecked(){
                $('.permission-group').each(function(){
                    var allChecked = true;
                    $(this).parents('tr').find(".the-permission").each(function(){
                        if(!this.checked) allChecked = false;
                    });
                    $(this).prop('checked', allChecked);
                });
            }

            parentChecked();

            $('.the-permission').on('change', function(){
                parentChecked();
            });
        });
    </script>
@endsection