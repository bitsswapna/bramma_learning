@extends('admin.layouts.app')

@section('css')

@endsection

@section('content')
    <main class="app-content">
        <div class="app-title">
            <div class="col-md-6 col-sm-12 col-xs-12 rem-pad">
                <h1><i class="fa fa-plus-circle"></i> Add drivers Details</h1>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 btn-right rem-pad">
                @if(can('browse_roles'))
                <a class="btn btn-success waves-effect waves-light" href="{{url('http://admin.vehicleproject.local.com/driversview')}}"><i class="fa fa-list"></i> Drivers Lists</a>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="tile">
                    <form method="post" id="createrDriver" name="createrDriver" action="{{url('drivers/create')}}">
                        <div class="row">
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                {{csrf_field()}}
                                <div class="form-group @if($errors->first('display_name')) has-danger @endif">
                                    <label for="display_name">Name</label>
                                    <input class="form-control @if($errors->first('name')) is-invalid @endif" id="display_name" name="name" type="text" placeholder="Enter name" autocomplete="off" value="{{old('name')}}">
                                    @if($errors->first('name'))
                                        <sapn class="form-control-feedback">{{$errors->first('name')}}</sapn>
                                    @endif
                                </div>
                                <div class="form-group @if($errors->first('name')) has-danger @endif">
                                    <label for="phone">Phone Number</label>
                                    <input class="form-control @if($errors->first('name')) is-invalid @endif" id="phone" type="text" name="phoneno" placeholder="Enter phone number" autocomplete="off" value="{{old('phone_no')}}">
                                    @if($errors->first('phoneno'))
                                        <sapn class="form-control-feedback">{{$errors->first('phoneno')}}</sapn>
                                    @endif
                                </div>
                                <div class="form-group @if($errors->first('name')) has-danger @endif">
                                    <label for="adress">Address</label>
                                    <input class="form-control @if($errors->first('name')) is-invalid @endif" id="name" type="text" name="adress" placeholder="Enter the address" autocomplete="off" value="{{old('addre_ss')}}">
                                    @if($errors->first('adress'))
                                        <sapn class="form-control-feedback">{{$errors->first('adress')}}</sapn>
                                    @endif
                                </div>
                                <div class="form-group @if($errors->first('name')) has-danger @endif">
                                    <label for="name">Salary Duration</label>
                                    <input class="form-control @if($errors->first('name')) is-invalid @endif" id="name" type="text" name="salryduration"  placeholder="enter the salary duration"  autocomplete="off" value="{{old('salary_duration')}}">
                                    @if($errors->first('salryduration'))
                                        <sapn class="form-control-feedback">{{$errors->first('salryduration')}}</sapn>
                                    @endif
                                </div>
                                <div class="form-group @if($errors->first('name')) has-danger @endif">
                                    <label for="name">Amount</label>
                                    <input class="form-control @if($errors->first('name')) is-invalid @endif" id="name" type="text" name="amount"  placeholder="enter the amount" autocomplete="off" value="{{old('	amount')}}">
                                    @if($errors->first('amount'))
                                        <sapn class="form-control-feedback">{{$errors->first('amount')}}</sapn>
                                    @endif
                                </div>
                                <div class="form-group @if($errors->first('display_name')) has-danger @endif">
                                    <label for="display_name">Vehicle number</label>
                                    <input class="form-control @if($errors->first('name')) is-invalid @endif" id="display_name" name="vehino" type="text" placeholder="Enter the vehicle no" autocomplete="off" value="{{old('vehicle_no')}}">
                                    @if($errors->first('vehino'))
                                        <sapn class="form-control-feedback">{{$errors->first('vehino')}}</sapn>
                                    @endif
                                </div>


                                
                                <div class="form-group @if($errors->first('name')) has-danger @endif">
                                    <label for="vehicletype">Vehicle Type</label>
                            
                            
                            <div class="col-md-6">
                            <select name="vehitype" class="form control" autocomplete="off" value="{{old('vehicle_type')}}">
                  
                                 <option value="0">Select Vehicle</option>
                                 <option value="JCB">JCB</option>
                                 <option value="Tipper">Tipper</option>
                                 
                                 
                                 <option value="Gitache">Gitache</option>
                               
                                 
                            </select>
                                @if ($errors->has('vehitype'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('vehitype') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                                <!-- <div class="form-group @if($errors->first('name')) has-danger @endif">
                                    <label for="vehicletype">Vehicle Type</label>
                                    <input class="form-control @if($errors->first('name')) is-invalid @endif" id="phone" type="text" name="vehitype" autocomplete="off" value="{{old('phone_no')}}">
                                    @if($errors->first('phoneno'))
                                        <sapn class="form-control-feedback">{{$errors->first('phoneno')}}</sapn>
                                    @endif
                                </div> -->
                                <div class="form-group @if($errors->first('name')) has-danger @endif">
                                    <label for="adress">Vehicle Cost per/hour</label>
                                    <input class="form-control @if($errors->first('name')) is-invalid @endif" id="name" type="text" name="vhiperhur" placeholder="Enter the cost per hour" autocomplete="off" value="{{old('vehicle_cost_per_hur')}}">
                                    @if($errors->first('vhiperhur'))
                                        <sapn class="form-control-feedback">{{$errors->first('vhiperhur')}}</sapn>
                                    @endif
                                </div>

                               
                                <div class="form-group @if($errors->first('name')) has-danger @endif">
                                    <label for="vehicletype">Vehicle Type</label>
                            
                            
                            <div class="col-md-6">
                            <select name="rentervehicle" class="form control" autocomplete="off" value="{{old('renter_vehicle')}}">
                  
                                 <option value="">Go for rent</option>
                                 <option value="NULL">NOT</option>
                                 <option value="1">YES</option>
                                 
                                 
                               
                               
                                 
                            </select>
                                @if ($errors->has('rentervehicle'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('rentervehicle') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                               


                                </div>
                               
                        </div>

                        <div class="tile-footer">
                            <button class="btn btn-primary waves-effect waves-light" type="submit">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>
@endsection

@section('js')
    <!--Notifications Message Section-->
    @include('admin.layouts.notifications')

    <script>
        $("#createrDriver").validate({
            rules: {
                // simple rule, converted to {required:true}
                name: "required",
                phoneno: "required",
                adress: "required",
                salryduration: "required",
                amount: "required",
               
            }
        });
    </script>
@endsection