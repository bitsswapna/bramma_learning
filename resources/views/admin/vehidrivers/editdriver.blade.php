@extends('admin.layouts.app')

@section('css')
    <style>
        ul {
            list-style-type: none;
        }

        span.label-text::first-letter {
            text-transform: uppercase;
        }
    </style>
@endsection

@section('content')
    <main class="app-content bg-white">
        <!-- Sub Title---->
        <div id="submenu_subheader" class="submenu-subheader submenu-grid__item ">          
            <div class="submenu-subheader__title">
                <h4 class="title-caption"><i class="fas fa-user-shield"></i>  Edit Drivers</h4>
            </div>           
            <div class="submenu-subheader__toolbar">
                <div class="submenu-subheader__toolbar-wrapper">
                    <a  data-toggle="tooltip" data-placement="left" data-html="true"  title="edit List" class="btn bg-primary-blue btn-sm waves-effect waves-light  br-0" href="{{url('driversview')}}">
                        <i class="fa fa-list"></i>Drivers Lists</a>
                </div>
            </div>
        </div>
        <!-- Body Part -->
        <div class="sub-header-body">
            <form method="post" action="{{url('update/edit/'.$de->id)}}" name="editdata" id="editDriver">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="display_name">name</label>
                            <input class="form-control br-0" id="display_name" name="name" type="text" placeholder="Enter name" autocomplete="off" value="{{$de->name}}">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="name">Phone Number</label>
                            <input class="form-control br-0"  type="text" name="phoneno" placeholder="Enter the phone number" autocomplete="off"  value="{{$de->phone_no}}">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="name">Address</label>
                            <input class="form-control br-0" id="name" type="text" name="adress" placeholder="Enter address" autocomplete="off"  value="{{$de->addre_ss}}">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="name">Salary Duration</label>
                            <input class="form-control br-0" id="name" type="text" name="salryduration" placeholder="Enter the salary duration" autocomplete="off"  value="{{$de->salary_duration}}">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="name">Amount</label>
                            <input class="form-control br-0" id="name" type="text" name="amount" placeholder="Enter the amount"  autocomplete="off"  value="{{$de->amount}}">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="name">Vehicle Number</label>
                            <input class="form-control br-0" id="name" type="text" name="vehino" placeholder="Enter the vehicle no" autocomplete="off"  value="{{$de->vehicle_no}}">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="name">Vehicle Type</label>
                            <input class="form-control br-0" id="name" type="text" name="vehitype" placeholder="Enter the vehicle type" autocomplete="off"  value="{{$de->vehicle_type}}">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="name">cost Per hour</label>
                            <input class="form-control br-0" id="name" type="text" name="vhiperhur" placeholder="Enter the cost per hour" autocomplete="off"  value="{{$de->vehicle_cost_per_hur}}">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="name">Go for Rent</label>
                            <input class="form-control br-0" id="name" type="text" name="rentervehicle" placeholder="rent=1/not=NULL" autocomplete="off"  value="{{$de->renter_vehicle}}">
                        </div>
                    </div>
                   
                    </div>
                    
                </div>    

            
                    </div>
                </div>
                <div class="sub-header-footer text-right">
                    <button class="btn bg-primary-blue br-0  waves-effect waves-light btn-sm" type="submit">Update</button>
                </div>
                
            </form>
        </div>
        <!-- Footer Part END-->

        <!-- Footer Part END-->      
    </main>
@endsection

@section('script')

    <!--Notifications Message Section-->
    @include('admin.layouts.notifications')
    <script>
        $("#editDriver").validate({
            rules: {
                // simple rule, converted to {required:true}
               
                display_name: "required",
                name: "required",
            }
        });
    </script>

    <script>
        $('document').ready(function () {

            $('.permission-group').on('change', function(){
                $(this).parents('tr').find(".the-permission").prop('checked', this.checked);
            });

            function parentChecked(){
                $('.permission-group').each(function(){
                    var allChecked = true;
                    $(this).parents('tr').find(".the-permission").each(function(){
                        if(!this.checked) allChecked = false;
                    });
                    $(this).prop('checked', allChecked);
                });
            }

            parentChecked();

            $('.the-permission').on('change', function(){
                parentChecked();
            });
        });
    </script>
@endsection