@extends('admin.layouts.app')
@section('css')
@endsection
    @section('content')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">

    <!-- Main Container -->
    <main class="app-content bg-white animated fadeIn"> 
        <!-- Sub Title---->
        <div id="submenu_subheader" class="submenu-subheader submenu-grid__item ">          
            <div class="submenu-subheader__title">
                <h4 class="title-caption"><i class="fas fa-user-shield"></i>Drivers</h4>
            </div>           
            <div class="submenu-subheader__toolbar">
                <div class="submenu-subheader__toolbar-wrapper">
                 <table class="table  table-bordered" id="sampleTable">
                 <div class="sub-header-body">
                <div class="table-responsive">
                        
                            <tr>
                                <th>Name</th>
                                <th>Phone Number</th>
                                <th>Address</th>
                                <th>Salary Duration</th>
                                <th>Amount</th>
                                <th>Vehicle Number</th>
                                <th>Vehicle Type</th>
                                <th>Cost per hour</th>
                                <th>Current Status</th>
                                <th>Created</th>
                            
                                <th>Edit Action</th>
                                <th>Delete Action</th>
                            </tr>
                                               
                        <body>
                            @foreach($data as $role)
                                <tr>
                                    <td>
                                        {{$role->name}}
                                    </td>
                                    <td>
                                        {{$role->phone_no}}</label>
                                    </td>
                                    <td>
                                        {{$role->addre_ss}}</label>
                                    </td>
                                    <td>
                                        {{$role->salary_duration}}</label>
                                    </td>
                                    <td>
                                        {{$role->amount}}</label>
                                    </td>
                                    <td>
                                        {{$role->vehicle_no}}</label>
                                    </td>
                                    <td>
                                        {{$role->vehicle_type}}</label>
                                    </td>
                                    <td>
                                        {{$role->vehicle_cost_per_hur}}</label>
                                    </td>
                                    <td>
                                        {{$role->renter_vehicle}}</label>
                                    </td>
                                
                                
                                    <td>
                                        {{date('D M Y h:i A',strtotime($role->created_at))}}
                                    </td>

                                    
                                    <td class="text-left">
                                       
                                            <a href="{{url('driver/edit/'.$role->id)}}" class="btn action-button bg-primary-blue"
                                            data-toggle="tooltip" data-placement="top" data-html="true" title="Edit Role">
                                            <i class="fa fa-pencil-square-o "></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                         
                                    </td>
                                    <td class="text-left">
                                       
                                            <a href="{{url('driver/delete/'.$role->id)}}" class="btn action-button bg-primary-red"
                                            data-toggle="tooltip" data-placement="top" data-html="true" title="Delete Role">
                                            <i class="glyphicon glyphicon-trash"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                         
                                    </td>
                                </tr>
                            @endforeach
                        </body>
                    </table>
                    
               
                     <!-- {{-- <p class="bs-component"> -->
                                <!-- @if(can('add_roles'))
                                <a class="btn btn-primary waves-effect waves-light" href="{{url('role/create')}}" role="button">
                                                    <i class="fa fa-plus-circle"></i>
                                                Create Role
                                            </a> @endif -->
    
                                <!-- @if(can('add_roles')) {{--
                                <a class="btn btn-primary waves-effect waves-light" href="{{url('role/create')}}" role="button">--}}
                                    {{--<i class="fa fa-plus-circle"></i>--}}
                                    {{--Create Role--}}
                                    {{--</a>-- @endif
                            </p>  --}} -->
                </div>
            </div>
        </div>
        <!-- END Sub Title---->
        <!-- Body Part -->
        
            <!-- @if(can('browse_roles')) -->
            <div class="sub-header-body">
                <div class="table-responsive">
                   
                </div>
            @else @include('admin.no-access-content') @endif                
        </div>
        <!-- END Body Part -->
    </main>
    <!-- Main END -->
  
        

    @endsection @section('js')
   
    
    <script type="text/javascript" src="{{asset('admin/js/plugins/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $('#sampleTable').DataTable({
            bPaginate: false,
            bSort: false,
            bFilter: false,
            bInfo: false,
            responsive:true
        });
    </script>

    
<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
    }
});
 $('.accept').click( function() {
var id =$(this).data("id");
    

      swal({
          title: "Are you sure?", 
          text: "Are you aproved?", 
          type: "warning",
          showCancelButton: true,
          closeOnConfirm: false,
          confirmButtonText: "Yes",
          confirmButtonColor: "#ec6c62"
        },
        function() {
            $.ajax(
                    {
                        type: "post",
                        url: "/apply/data/"+id,
                        data: "id="+id,
                        success: function(data){
                        }
                    }
        
        )  
});
 })

         </script>

    @endsection

