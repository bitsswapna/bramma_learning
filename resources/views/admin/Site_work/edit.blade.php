@extends('admin.layouts.app')

@section('css')
    <style>
        ul {
            list-style-type: none;
        }

        span.label-text::first-letter {
            text-transform: uppercase;
        }
    </style>
@endsection

@section('content')
    <main class="app-content bg-white">
        <!-- Sub Title---->
        <div id="submenu_subheader" class="submenu-subheader submenu-grid__item ">
            <div class="submenu-subheader__title">
                <h4 class="title-caption"><i class="fas fa-user-shield"></i>  Edit WorkSite</h4>
            </div>
            <div class="submenu-subheader__toolbar">
                <div class="submenu-subheader__toolbar-wrapper">
                    <a  data-toggle="tooltip" data-placement="left" data-html="true"  title="Work List" class="btn bg-primary-blue btn-sm waves-effect waves-light  br-0" href="{{url('site-work')}}">
                        <i class="fa fa-list"></i>Site WorkList</a>
                </div>
            </div>
        </div>
        <!-- Body Part -->
        <div class="sub-header-body">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
            <form method="post" action="{{url('admin_worksite/update/'.$ed->id)}}" name="editdata" id="editRole">


                        {{csrf_field()}}
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="name">Title</label>
                            <input class="form-control @if($errors->first('titles')) is-invalid @endif" id="name" type="text" name="titles" placeholder="Enter title" autocomplete="off"  value="{{$ed->titles}}">
                            @if($errors->first('titles'))
                       <sapn class="error">{{$errors->first('titles')}}</sapn>
                              @endif
                        </div>
                        <div class="form-group">
                  <label for="site">Site*</label>
                                           <select  class="form-control"  name="site" autocomplete="off">
                                           @if($site)
                                           @foreach($site as $sites)
                                               <option value="{{$sites->id}}"  @if($ed)  @if($sites->id ==($ed->site_id)) selected @endif  @endif>{{$sites->title}}</option>
                                               @endforeach
                                               @endif
                                           </select>
                                           </div>
                        <div class="form-group">
                  <label for="vehicle">Vehicle*</label>
                                           <select  class="form-control"  name="vehicle" autocomplete="off">
                                           @if($vehi)
                                           @foreach($vehi as $vehicles)
                                               <option value="{{$vehicles->id}}" @if($ed) @if($vehicles->id==($ed->vehicle_id))selected @endif  @endif>{{$vehicles->vehicle_number}}</option>
                                               @endforeach
                                               @endif
                                           </select>
                                           </div>


                                       <div class="form-group">
                                        <label for="vehicle">Driver*</label>
                                           <select  class="form-control"  name="driver" autocomplete="off">
                                           @if($driver)
                                           @foreach($driver as $insert)
                                               <option value="{{$insert->id}}" @if($ed) @if($insert->id==($ed->driver_id))selected  @endif  @endif>{{$insert->name}}</option>
                                               @endforeach
                                               @endif
                                           </select>
                                           </div>
                </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">

                    <div class="form-group">
                                           <label for="vehicle">Cleaner*</label>


                          <select  class="form-control"  name="cleaner" autocomplete="off">
                          @if($cleaner)
                          @foreach($cleaner as $insert)


                              <option value="{{$insert->id}}"@if($ed) @if($insert->id==($ed->cleaner_id))selected  @endif  @endif>{{$insert->name}}</option>

                              @endforeach
                              @endif
                          </select>
                          </div>





                        <div class="form-group">
                            <label for="name">Work Date</label>
                            <input class="form-control @if($errors->first('titles')) is-invalid @endif" id="name" type="date" name="workdate" placeholder="Enter address" autocomplete="off"  value="{{$ed->work_date}}">
                            @if($errors->first('workdate'))
                       <sapn class="error">{{$errors->first('workdate')}}</sapn>
                              @endif

                        </div>


                        <div class="form-group">
                            <label for="name">Vehicle Point</label>
                            <input class="form-control @if($errors->first('vehipoint')) is-invalid @endif" id="name" type="text" name="vehipoint" placeholder="Enter title" autocomplete="off"  value="{{$ed->vehicle_point}}">
                            @if($errors->first('vehipoint'))
                       <sapn class="error">{{$errors->first('vehipoint')}}</sapn>
                              @endif
                        </div>


                        <div class="form-group">
                            <label for="name">Vehicle Duration</label>
                            <input class="form-control @if($errors->first('vehiduration')) is-invalid @endif" id="name" type="text" name="vehiduration" placeholder="Enter description"  autocomplete="off"  value="{{$ed->vehicle_duration}}">
                            @if($errors->first('vehiduration'))
                       <sapn class="error">{{$errors->first('vehiduration')}}</sapn>
                              @endif
                        </div>


</div>
                    </div>
                </div>




                <div class="sub-header-footer text-right">
                    <button class="btn bg-primary-blue br-0  waves-effect waves-light btn-sm" type="submit">Update</button>
                </div>

            </form>
                </div>
        </div>
        <!-- Footer Part END-->

        <!-- Footer Part END-->
    </main>
@endsection

@section('script')

    <!--Notifications Message Section-->
    @include('admin.layouts.notifications')
    <script>
        $("#editRole").validate({
            rules: {
                // simple rule, converted to {required:true}
                display_name: "required",
                name: "required",
            }
        });
    </script>

    <script>
        $('document').ready(function () {

            $('.permission-group').on('change', function(){
                $(this).parents('tr').find(".the-permission").prop('checked', this.checked);
            });

            function parentChecked(){
                $('.permission-group').each(function(){
                    var allChecked = true;
                    $(this).parents('tr').find(".the-permission").each(function(){
                        if(!this.checked) allChecked = false;
                    });
                    $(this).prop('checked', allChecked);
                });
            }

            parentChecked();

            $('.the-permission').on('change', function(){
                parentChecked();
            });
        });
    </script>
@endsection
