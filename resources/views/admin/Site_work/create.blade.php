@extends('admin.layouts.app')

@section('css')

@endsection

@section('content')
<main class="app-content bg-white ">
<div id="submenu_subheader" class="submenu-subheader submenu-grid__item ">          
        <div class="submenu-subheader__title">
            <h4 class="title-caption"><i class="fas fa-list-alt"></i>  Add </h4>
        </div>           
        <div class="submenu-subheader__toolbar">
            <div class="submenu-subheader__toolbar-wrapper bs-component">
            @if(can('browse_roles'))
                <a data-toggle="tooltip" data-placement="top" data-html="true" title="" class="btn bg-primary-blue btn-sm waves-effect waves-light btn-right br-0" href="{{url('site-work')}}" role="button" data-original-title="WorkSite Lists">
                    <i class="fa fa-list"></i>Work Site
                </a> 
                @endif
            </div>
        </div>
    </div>




    <div class="row">
        <div class=" col-md-12 col-sm-12 col-xs-12">
                <div class="tile">
                <form method="post" id="createrRole" name="createrRole" action="{{url('sitework/insert')}}">
                         {{csrf_field()}}
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <div >
                  
                        
                  <label for="display_name">Title</label>
                 
                  <input class="form-control @if($errors->first('titles')) is-invalid @endif" id="display_name" name="titles" type="text" placeholder="Enter name" autocomplete="off" value="{{old('title')}}">
                  @if($errors->first('titles'))
                    <sapn class="error">{{$errors->first('titles')}}</sapn>
                                    @endif 
              
       </div>
      
                        
                         
                  <div>
                  <label for="site">Site*</label>
                          
                               
                                           <select  class="form-control br-0 @if($errors->first('site')) is-invalid @endif" name="site" autocomplete="off">
                                           <option value=""> select</option>
                                           @foreach($site as $insert)

                                              
                                               <option value="{{$insert->id}}">{{$insert->title}}</option>
                                             
                                               @endforeach 
                                           </select>
                                           @if($errors->first('site'))
                                        <sapn class="error">{{$errors->first('site')}}</sapn>
                                    @endif
                                           
                        </div>

                        <div >
                  <label for="vehicle">Vehicle*</label>
                          
                               
                                           <select  class="form-control br-0 @if($errors->first('vehicle')) is-invalid @endif"  name="vehicle" autocomplete="off">
                                           <option value=""> select</option>
                                           @foreach($vehi as $insert)

                                              
                                               <option value="{{$insert->id}}">{{$insert->vehicle_number}}</option>
                                             
                                               @endforeach 
                                           </select>
                                           @if($errors->first('vehicle'))
                                        <sapn class="error">{{$errors->first('vehicle')}}</sapn>
                                        @endif
                                           </div>

                                           <div>
                                        <label for="vehicle">Driver*</label>
                          
                               
                                           <select  class="form-control br-0 @if($errors->first('driver')) is-invalid @endif"  name="driver" autocomplete="off">
                                           <option value=""> select</option>
                                           @foreach($driver as $insert)

                                              
                                               <option value="{{$insert->id}}">{{$insert->name}}</option>
                                             
                                               @endforeach 
                                           </select>
                                           @if($errors->first('driver'))
                                        <sapn class="error">{{$errors->first('driver')}}</sapn>
                                        @endif
                                           </div>

                </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <div>
                                           <label for="vehicle">Cleaner*</label>
                          
                               
                          <select  class="form-control br-0 @if($errors->first('cleaner')) is-invalid @endif"  name="cleaner" autocomplete="off">
                          <option value=""> select</option>
                          @foreach($cleaner as $insert)

                             
                              <option value="{{$insert->id}}">{{$insert->name}}</option>
                            
                              @endforeach 
                          </select>
                          @if($errors->first('cleaner'))
                                        <sapn class="error">{{$errors->first('cleaner')}}</sapn>
                                        @endif
                          </div>

                                           <div>
                                    <label for="name">Work Date</label>
                                    <input class="form-control @if($errors->first('workdate')) is-invalid @endif" id="name" type="date" name="workdate" placeholder="Enter work date"   autocomplete="off" value="{{old('work_date')}}">
                                    @if($errors->first('workdate'))
                                  <sapn class="error">{{$errors->first('workdate')}}</sapn>
                                    @endif 
              
                                </div>
                                <div >
                                    <label for="name">Vehicle Point</label>
                                    <input class="form-control @if($errors->first('vehipoint')) is-invalid @endif" id="name" type="text" name="vehipoint" placeholder="Enter the Vehicle point"  autocomplete="off" value="{{old('vehicle_point')}}">
                                    @if($errors->first('vehipoint'))
                                <sapn class="error">{{$errors->first('vehipoint')}}</sapn>
                                    @endif 
              
                                </div>
                                <div >
                                    <label for="name">Vehicle Wor Duration(hours)</label>
                                    <input class="form-control @if($errors->first('vehiduration')) is-invalid @endif" id="name" type="text" name="vehiduration" placeholder="Enter Vehicle duration"  autocomplete="off" value="{{old('vehicle_duration')}}">
                                    @if($errors->first('vehiduration'))
                                  <sapn class="error">{{$errors->first('vehiduration')}}</sapn>
                                    @endif 
              
                                </div>
                                
                              
                    </div>

                            </div>
                       
</div>
</div>
<div class="sub-header-footer text-right">
                        <button id="submit-btn" class="btn btn-primary waves-effect waves-light br-0 btn-sm" type="submit">Save</button>
                    </div>
                    </form>
               
            </div>
        </div>
    </main>
@endsection

@section('script')
    <!--Notifications Message Section-->
    @include('admin.layouts.notifications')

    <script>
        $("#createrRole").validate({
            rules: {
                // simple rule, converted to {required:true}
                display_name: "required",
                name: "required",
            }
        });
    </script>
@endsection
