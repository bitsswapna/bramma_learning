@extends('admin.layouts.app')

@section('css')

@endsection

@section('content')
    <main class="app-content bg-white">      

            <!-- Sub Title---->
            <div id="submenu_subheader" class="submenu-subheader submenu-grid__item ">          
                <div class="submenu-subheader__title">
                    <h4 class="title-caption"><i class="fa fa-envelope"></i> Email Template</h4>
                </div>           
                <div class="submenu-subheader__toolbar">
                    <div class="submenu-subheader__toolbar-wrapper bs-component">
                            @if(can('add_email_templates'))
                            <a  data-toggle="tooltip" data-placement="top" data-html="true" title="Create Template" class="btn bg-primary-green btn-sm waves-effect waves-light btn-rightbr-0" href="{{url('email-template/create')}}" role="button"><i class="fa fa-plus-circle"></i>Create Template</a>
                        @endif
                    </div>
                </div>
            </div>          
            <!-- END Sub Title---->
            <!-- BODY -->
            <div class="sub-header-body">
                @if(can('browse_email_templates'))
                <div class="row">
    
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="tile">
                            <div class="br-0">
                                <div class="table-responsive">
                                    <table id="sampleTable" class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>S#</th>
                                            <th>Template</th>
                                            <th>From</th>
                                            <th>Reply</th>
                                            <th>Subject</th>
                                            <th>Key</th>
                                            @if(can('edit_email_templates'))
                                            <th>Action</th>
                                            @endif
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($emails as $key => $email)
                                            <tr>
                                                <td>{{$key + 1}}</td>
                                                <td>{{$email->title}}</td>
                                                <td>{{$email->from}}</td>
                                                <td>{{$email->reply}}</td>
                                                <td>{{$email->subject}}</td>
                                                <td>{{$email->key}}</td>
                                                <td>
                                                @if(can('edit_email_templates'))
                                                        <a title="edit" href="{{url('email-template/edit/'.$email->id)}}"  class="btn action-button bg-primary-blue" data-toggle="tooltip" data-placement="top" data-html="true" title="Edit"><i class="fa fa-pencil-square-o" ></i></a>
                                                        
                                                @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                @include('admin.no-access-content')
            @endif
            </div>
            <!-- END BODY -->
    </main>
@endsection

@section('script')
    <script type="text/javascript" src="{{asset('admin/js/plugins/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/dataTables.bootstrap.min.js')}}"></script>
<script>
    $('#sampleTable').DataTable({
        bPaginate: false,
        bSort: false,
        bFilter: false,
        bInfo: false
    });
</script>
@endsection