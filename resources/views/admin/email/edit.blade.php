@extends('admin.layouts.app')

@section('css')
    <style>
        ul {
            list-style-type: none;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/redactor/redactor/redactor.css')}}" />
@endsection

@section('content')
    <main class="app-content bg-white">      
        
         <!-- Sub Title---->
         <div id="submenu_subheader" class="submenu-subheader submenu-grid__item ">          
                <div class="submenu-subheader__title">
                    <h4 class="title-caption"><i class="fa fa-envelope"></i> Edit Email</h4>
                </div>           
                <div class="submenu-subheader__toolbar">
                    <div class="submenu-subheader__toolbar-wrapper bs-component">
                            <a data-toggle="tooltip" data-placement="top" data-html="true" title="Template List" class="btn bg-primary-blue btn-sm waves-effect waves-light btn-right  br-0" href="{{url('email-templates')}}"><i class="fa fa-list"></i>Template Lists</a>
                    </div>
                </div>
        </div>          
        <!-- END Sub Title---->

        <!-- BODY -->
        <div class="sub-header-body">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="tile">
                       <div class="br-0">
                            <h3 class="tile-title">Edit Email</h3>
                        <form method="post" action="{{url('email-template/edit/'.$email->id)}}" enctype="multipart/form-data" id="emailEdit">
                            <div class="row">
                                {{csrf_field()}}
                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group @if($errors->first('title')) has-danger @endif">
                                        <label for="title">Title</label>
                                        <input class="form-control br-0 @if($errors->first('title')) is-invalid @endif" id="title" name="title" type="text" placeholder="Enter page title" required autocomplete="off" value="{{$email->title}}">
                                        @if($errors->first('title'))
                                            <sapn class="form-control-feedback">{{$errors->first('title')}}</sapn>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group ">
                                        <label for="key">Key</label>
                                        <input class="form-control br-0" id="key" name="key" type="text" placeholder="Enter email template key" readonly autocomplete="off" value="{{$email->key}}">
                                        @if($errors->first('key'))
                                            <sapn class="form-control-feedback">{{$errors->first('key')}}</sapn>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group @if($errors->first('from')) has-danger @endif">
                                        <label for="from">From Email Id</label>
                                        <input class="form-control br-0 @if($errors->first('from')) is-invalid @endif" id="from" name="from" type="email" placeholder="Enter from email" required autocomplete="off" value="{{$email->from}}">
                                        @if($errors->first('from'))
                                            <sapn class="form-control-feedback">{{$errors->first('from')}}</sapn>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group @if($errors->first('reply')) has-danger @endif">
                                        <label for="reply">Reply Email Id</label>
                                        <input class="form-control br-0 @if($errors->first('reply')) is-invalid @endif" id="reply" name="reply" type="email" placeholder="Enter reply email" autocomplete="off" value="{{$email->reply}}">
                                        @if($errors->first('reply'))
                                            <sapn class="form-control-feedback">{{$errors->first('reply')}}</sapn>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group @if($errors->first('subject')) has-danger @endif">
                                        <label for="subject">Email Subject</label>
                                        <input class="form-control br-0 @if($errors->first('subject')) is-invalid @endif" id="subject" name="subject" type="text" placeholder="Enter email subject" autocomplete="off" value="{{$email->subject}}">
                                        @if($errors->first('subject'))
                                            <sapn class="form-control-feedback">{{$errors->first('subject')}}</sapn>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    
                                    <div class="form-group @if($errors->first('email_body')) has-danger @endif not-external">
                                        <label for="email_body">Email Body</label>
                                        <textarea class="form-control br-0 @if($errors->first('email_body')) is-invalid @endif" name="email_body" id="email_body" rows="3">{{$email->email_body}}</textarea>
                                        @if($errors->first('email_body'))
                                            <sapn class="form-control-feedback">{{$errors->first('email_body')}}</sapn>
                                        @endif
                                    </div>
                                </div>
                            </div>
    
                            <div class="tile-footer text-right">
                               
                                <button data-toggle="tooltip" data-placement="top" data-html="true" title="Save Changes" class="btn btn-primary btn-sm waves-effect waves-light br-0" type="submit">Update</button>
                            </div>
                        </form>
                       </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END BODY -->

       
        
    </main>
@endsection

@section('script')

    <!--Notifications Message Section-->
    @include('admin.layouts.notifications')

    <script src="{{ asset('admin/redactor/redactor/redactor.js')}}"></script>
    <script src="{{ asset('admin/redactor/plugins/fontsize/fontsize.js')}}"></script>
    <script src="{{ asset('admin/redactor/plugins/fontfamily/fontfamily.js')}}"></script>
    <script src="{{ asset('admin/redactor/plugins/fontcolor/fontcolor.js')}}"></script>
    <script src="{{ asset('admin/redactor/plugins/imagemanager/imagemanager.js')}}"></script>

    <script>
        $(document).ready(function () {
            // $("#emailEdit").validate({

                // rules: {
                //     title: {
                //         required: true,
                //     },
                //     from: {
                //         required: true,
                //         email:true
                //     },
                //     reply: {
                //         required: true,
                //         email:true
                //     },
                //     type: {
                //         required: true
                //     },
                //     subject: {
                //         required: true
                //     },
                //     email_body: {
                //         required: true
                //     }
                // },
            //     submitHandler: function(form) {
            //         form.submit();
            //     }
            // });

            $R('#email_body', {
                maxHeight:"400px",
                minHeight:"400px",
                plugins: ['fontsize','imagemanager','fontfamily','fontcolor'],
                fontcolors: [
                    '#000', '#333', '#555', '#777', '#999', '#aaa',
                    '#bbb', '#ccc', '#ddd', '#eee', '#f4f4f4', '#0096ff'
                ],
                imageUpload: '{!! asset('admin/redactor/demo/scripts/image_upload.php')!!}',
                imageResizable: true,
                imagePosition: true
            });
        });
    </script>
@endsection