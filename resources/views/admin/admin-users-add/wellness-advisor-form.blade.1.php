<div class="row">
    <div class="col-lg-12">

        @if(Auth::user()->add_child_to_child == 1)
        <div class="form-group">
            <div class="checkbox checkbox-primary">
                <input type="checkbox" name="parent_as_admin" id="vi-admin-as-parent" value="1">
                <label for="">Set parent as admin. </label>
                <span class="error" style="    display: block;"></span>
            </div>
        </div>
        <div class="form-group vi-wa-as-parent" id="vi-wa-as-parent">
            <label for="parent_id">Choose Parent</label>
            <select id="wellness-advisor-list" class="wellness-advisor-list demo-default" placeholder="Pick members..."
                name="parent_id"></select>
            <span class="error"></span>
        </div>
        @else
        <input type="hidden" name="parent_as_admin" id="vi-admin-as-parent" value="{{Auth::user()->id}}">
        @endif

        {{--<div class="form-group @if($errors->first('first_name')) has-danger @endif">--}}
        {{--<label for="first_name">First Name</label>--}}
        {{--<input class="form-control @if($errors->first('first_name')) is-invalid @endif" id="first_name" name="first_name" type="text" placeholder="Enter first name"  autocomplete="off" value="{{old('first_name')}}">--}}

        {{--<span class="error"></span>--}}
        {{--</div>--}}
        {{--<input type="hidden" id="vi-waid" value="{{$id}}">--}}
        {{--<div class="form-group @if($errors->first('last_name')) has-danger @endif">--}}
        {{--<label for="last_name">Last Name</label>--}}
        {{--<input class="form-control @if($errors->first('last_name')) is-invalid @endif" id="last_name" name="last_name" type="text" placeholder="Enter last name" autocomplete="off" value="{{old('last_name')}}">--}}

        {{--<span class="error"></span>--}}
        {{--</div>--}}
        {{--<div class="form-group @if($errors->first('mobile')) has-danger @endif">--}}
        {{--<label for="mobile">Mobile</label>--}}
        {{--<input class="form-control @if($errors->first('mobile')) is-invalid @endif" id="mobile" name="mobile" type="text" placeholder="Enter mobile number" autocomplete="off" value="{{old('mobile')}}">--}}

        {{--<span class="error"></span>--}}
        {{--</div>--}}
        {{--<div class="form-group @if($errors->first('email')) has-danger @endif">--}}
        {{--<label for="email">Email</label>--}}
        {{--<input class="form-control @if($errors->first('email')) is-invalid @endif" id="email" name="email" type="text" placeholder="Enter email" autocomplete="off" value="{{old('email')}}">--}}

        {{--<span class="error"></span>--}}
        {{--</div>--}}

        {{--<div class="form-group @if($errors->first('password')) has-danger @endif">--}}
        {{--<label for="password">Password</label>--}}
        {{--<input class="form-control @if($errors->first('password')) is-invalid @endif" id="password" name="password" type="password" placeholder="Enter Password" autocomplete="off">--}}

        {{--<span class="error"></span>--}}
        {{--</div>--}}
        {{--<div class=" form-group @if($errors->first('gender')) has-danger @endif">--}}
        {{--<label for="gender">Gender</label>--}}
        {{--<div class="form-inline" style="display: flex;">--}}
        {{--<input type="radio" name="gender" value="male"> Male<br>--}}
        {{--<input type="radio" name="gender" value="female" style="margin-left: 20px;"> Female<br>--}}
        {{--</div>--}}

        {{--<span class="error"></span>--}}
        {{--</div>--}}
        {{--<div class="form-group @if($errors->first('date_of_birth')) has-danger @endif">--}}
        {{--<label for="date_of_birth">Data of birth</label>--}}
        {{--<input class="form-control @if($errors->first('date_of_birth')) is-invalid @endif" id="date_of_birth" name="date_of_birth" type="date" placeholder="Enter date of birth" autocomplete="off" value="{{old('date_of_birth')}}">--}}

        {{--<span class="error"></span>--}}
        {{--</div>--}}

        {{--<div class="form-group ">--}}
        {{--<label for="image">Image</label>--}}
        {{--<div class="fake-input">--}}
        {{--<input id="uploadFile" disabled="disabled" type="text" style="display: none;">--}}
        {{--<div class="fileUpload">--}}
        {{--<input class="form-control-file" id="image" name="image" type="file" accept="image/x-png,image/gif,image/jpeg">--}}
        {{--</div>--}}
        {{--</div>--}}


        {{--<span class="error"></span>--}}
        {{--</div>--}}

        {{--</div>--}}
        {{--<div class="col-lg-6">--}}

        {{--<div class="form-group ">--}}
        {{--<label for="address_1">Address </label>--}}
        {{--<input class="form-control @if($errors->first('address_1')) is-invalid @endif" id="address_1" name="address_1" type="text" placeholder="Enter address" autocomplete="off" value="{{old('address_1')}}">--}}

        {{--<span class="error"></span>--}}
        {{--</div>--}}
        {{--<div class="form-group ">--}}
        {{--<label for="aadhar_card_no">Aadhar card number</label>--}}
        {{--<input class="form-control @if($errors->first('aadhar_card_no')) is-invalid @endif" id="aadhar_card_no" name="aadhar_card_no" type="text" placeholder="Enter Aadhar card number" autocomplete="off" value="{{old('aadhar_card_no')}}">--}}

        {{--<span class="error"></span>--}}
        {{--</div>--}}
        {{--<div class="form-group ">--}}
        {{--<label for="pan_card_no">PAN Card number</label>--}}
        {{--<input class="form-control @if($errors->first('pan_card_no')) is-invalid @endif" id="pan_card_no" name="pan_card_no" type="text" placeholder="Enter PAN card number" autocomplete="off" value="{{old('pan_card_no')}}">--}}

        {{--<span class="error"></span>--}}
        {{--</div>--}}
        {{--<div class="form-group ">--}}
        {{--<label for="district_state">District state</label>--}}
        {{--<input class="form-control @if($errors->first('district_state')) is-invalid @endif" id="district_state" name="district_state" type="text" placeholder="Enter district state" autocomplete="off" value="{{old('district_state')}}">--}}

        {{--<span class="error"></span>--}}
        {{--</div>--}}
        {{--<div class="form-group">--}}
        {{--<label for="country">Country</label>--}}
        {{--<input class="form-control @if($errors->first('country')) is-invalid @endif" id="country" name="country" type="text" placeholder="Enter country" autocomplete="off" value="{{old('country')}}">--}}

        {{--<span class="error"></span>--}}
        {{--</div>--}}
        {{--<div class="form-group">--}}
        {{--<label for="location">Location</label>--}}
        {{--<input class="form-control @if($errors->first('location')) is-invalid @endif" id="location" name="location" type="text" placeholder="Enter location" autocomplete="off" value="{{old('location')}}">--}}

        {{--<span class="error"></span>--}}
        {{--</div>--}}
        {{--<div class="form-group">--}}
        {{--<label for="latitude">Latitude</label>--}}
        {{--<input class="form-control @if($errors->first('latitude')) is-invalid @endif" id="latitude" name="latitude" type="text" placeholder="Enter latitude" autocomplete="off" value="{{old('latitude')}}">--}}

        {{--<span class="error"></span>--}}
        {{--</div>--}}
        {{--<div class="form-group ">--}}
        {{--<label for="longitude">Longitude</label>--}}
        {{--<input class="form-control @if($errors->first('longitude')) is-invalid @endif" id="longitude" name="longitude" type="text" placeholder="Enter longitude" autocomplete="off" value="{{old('longitude')}}">--}}

        {{--<span class="error"></span>--}}
        {{--</div>--}}
        {{--<div class="prof-img" id="prof_img">--}}
        {{--<img src="{{asset('media/no-image/no-image.png')}}" class=" img-fluid img-thumbnail" alt="San Fran"
        style="height:200px;">--}}
        {{--</div>--}}









        <div class="col-lg-12">
            <!-- Personal Details-->
            <div class="box-container">
                <h4>Personal Information</h4>
                <hr>
                <div class="row">

                    <div class="col-lg-3 col-md-4">
                        <div class="avatar-upload">
                            <div class="avatar-edit">
                                <input type="file" id="imageUpload" name="image" accept=".png, .jpg, .jpeg">
                                <span class="text-danger small error"></span>

                                <label for="imageUpload"></label>
                            </div>
                            <div class="avatar-preview">
                                <div id="imagePreview"
                                    style="background-image: url({{asset('admin/assets/images/img.png')}});">
                                </div>
                            </div>
                        </div>

                    </div> <!-- end col -->

                    <div class="col-lg-9">
                        <div class="row">
                            <div class="col-sm-6 m-b-20 form-group bmd-form-group">
                                <label for="" class="bmd-label-floating">First Name</label>
                                <input type="text" class="form-control" name="first_name" type="text"
                                     autocomplete="off" value="{{old('first_name')}}">
                                <span class="text-danger small error"></span>
                            </div>

                            <div class="col-sm-6 m-b-20 form-group bmd-form-group">
                                <label for="" class="bmd-label-floating">Last Name</label>
                                <input type="text" class="form-control" name="last_name" type="text"
                                    autocomplete="off" value="{{old('last_name')}}">
                                <span class="text-danger small error"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 m-b-20 form-group bmd-form-group">
                                <label for="" class="bmd-label-floating">Gender</label>
                                <label class="radio-inline">
                                    <input class="radio" type="radio" name="gender" checked>Male
                                </label>
                                <label class="radio-inline">
                                    <input class="radio"  type="radio" name="gender">Female
                                    <span class="text-danger small error" style="display: block;"></span>
                                </label>
                            </div>
                            <div class="col-sm-6 m-b-20 form-group bmd-form-group">
                                <label for="" class="">DOB</label>
                                <input type="date" class="form-control" name="date_of_birth"
                                    value="{{old('date_of_birth')}}">
                                <span class="text-danger small error"></span>


                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 form-group bmd-form-group">
                                <label for="" class="bmd-label-floating">Mobile Number</label>
                                <input type="number" class="form-control" name="mobile"
                                    value="{{old('mobile')}}">
                                <span class="text-danger small error"></span>
                            </div>
                            <div class="col-sm-6 form-group bmd-form-group">
                                <label for="" class="bmd-label-floating">Email Id</label>
                                <input type="email" class="form-control" name="email"
                                    value="{{old('email')}}">
                                <span class="text-danger small error"></span>

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 form-group bmd-form-group">
                                <label for="" class="bmd-label-floating">Password</label>
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control" name="password"
                                        >
                                    <div class="input-group-append">
                                        <button class="btn btn-primary btn-password-view" type="submit">Password</button>
                                    </div>
                                    <span class="text-danger small error"></span>
                                </div>
                            </div>

                            <div class="col-sm-6 form-group bmd-form-group">
                                <label for="" class="bmd-label-floating">Confirm Password</label>
                                <input type="text" class="form-control" name="password_confirmation"
                                    >
                                <span class="text-danger small error"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END Personal Details-->

            <div class="box-container">
                <h4>Contact Information</h4>
                <hr>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group bmd-form-group">
                            <label for="txt-address" class="bmd-label-floating">Address</label>
                            <textarea name="address" id="" rows="5" class="form-control" id="txt-address">{{old('address')}}</textarea>
                            <span class="text-danger small error"></span>

                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group bmd-form-group">
                            <label for="txt-country" class="bmd-label-floating">Country</label>
                            <select class="form-control" name="country" id="txt-country">
                                <option value="India">India</option>

                            </select>
                            <span class="text-danger small error"></span>

                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group bmd-form-group">
                            <label for="address_new_state" class="bmd-label-floating" >State</label>
                            {!!Form::select('district_state',$states,'',(['class'=>'validate-field form-control
                            br-0 validate-field','id'=>'address_new_state','data-id'=>'#address_new_state'
                            ,'data-formfield'=>'state', 'placeholder' => 'Please Choose'])) !!}

                            {{--<select class="form-control" name="district_state">--}}
                            {{--<option>Kerala</option>--}}
                            {{--<option>Tamilnadu</option>--}}
                            {{--<option>Karnataka</option>--}}

                            {{--</select>--}}
                            <span class="text-danger small error"></span>

                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group bmd-form-group">
                            <label for="" class="bmd-label-floating">Pin Code</label>
                            <input type="text" class="form-control" name="pincode" value="{{old('pincode')}}"
                                >
                            <span class="text-danger small error"></span>

                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group bmd-form-group">
                            <label for="" class="bmd-label-floating">Location</label>
                            <input type="text" class="form-control" name="location">
                            <span class="text-danger small error"></span>
                            <input class="form-control" id="latitude" name="latitude" type="hidden"
                                placeholder="Enter latitude" autocomplete="off">
                            <input class="form-control" id="longitude" name="longitude" type="hidden"
                                placeholder="Enter longitude" autocomplete="off">

                        </div>
                    </div>
                </div>
            </div> <!-- end col -->
        </div>
        @if ($id == 1)
        <div class="col-sm-12">
            <div class="">
                <div class="row">
                    <div class="col-lg-12">



                        <!-- WA USer-->
                        <div class="box-container">
                            <h4>Wellness Advisor Details</h4>
                            <hr>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group bmd-form-group">
                                        <label for="" class="bmd-label-floating">Aadhar Number</label>
                                        <input type="text" class="form-control" name="aadhar_card_no">
                                        <span class="text-danger small error"></span>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group bmd-form-group">
                                        <label for="" class="bmd-label-floating">PAN Card Number</label>
                                        <input type="text" class="form-control" name="pan_card_no">
                                        <span class="text-danger small error"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--WA Details -->

                        <!-- WA USer-->
                        <div class="box-container">
                            <h4>Bank Details</h4>
                            <hr>
                            <div class="row">

                                <div class="col-sm-6">
                                    <div class="form-group bmd-form-group">
                                        <label for="" class="bmd-label-floating">Bank</label>
                                        <select class="form-control" name="bank">
                                            <option value="fb">Federal Bank</option>
                                            <option value="sbi">State Bank</option>

                                        </select>
                                        <span class="text-danger small error"></span>

                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group bmd-form-group">
                                        <label for="" class="bmd-label-floating">Bank Code</label>
                                        <input type="text" class="form-control" name="branch_code">
                                        <span class="text-danger small error"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group bmd-form-group">
                                        <label for="" class="bmd-label-floating">Account Number</label>
                                        <input type="text" class="form-control" name="account_number">
                                        <span class="text-danger small error"></span>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group bmd-form-group">
                                        <label for="" class="bmd-label-floating">Branch</label>
                                        <input type="text" class="form-control" name="branch">
                                        <span class="text-danger small error"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group bmd-form-group">
                                        <label for="" class="bmd-label-floating">Name of banker</label>
                                        <input type="text" class="form-control" name="name_of_banker">
                                        <span class="text-danger small error"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group bmd-form-group">
                                        <label for="" bmd-label-floating>IFSC Code</label>
                                        <input type="text" class="form-control" name="ifsc">
                                        <span class="text-danger small error"></span>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!--WA Details -->
                    </div>
                </div>
            </div>
            <!-- End row -->
        </div>
        @endif


    </div>

</div>