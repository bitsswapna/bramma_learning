@extends('admin.layouts.app')

@section('css')
{{--    <link href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap2.css" rel="stylesheet" type="text/css"/>--}}
    <link href="{{asset('css/plugin/selectize.bootstrap2.css')}}" rel="stylesheet" type="text/css"/>

@endsection
@section('content')
    <main class="app-content bg-white">

        <!-- Sub Title---->
        <div id="submenu_subheader" class="submenu-subheader submenu-grid__item ">
            <div class="submenu-subheader__title">
                <h4 class="title-caption"> <i class="fa fa-plus-circle"></i> Create User</h4>
            </div>
            <div class="submenu-subheader__toolbar">
                <div class="submenu-subheader__toolbar-wrapper bs-component">
                    @if(can('browse_admin_user'))
                        <a data-toggle="tooltip" data-placement="top" data-html="true" title="User list" class="btn bg-primary-green btn-sm waves-effect waves-light inliner btn-right mt--6 br-0" href="{{url('admin_users')}}"><i class="fa fa-list"></i>User Lists</a>
                    @endif
                </div>
            </div>
        </div>
        <!-- END Sub Title---->

        <!-- Body Part -->
        <div class="sub-header-body">
            <form method="post" id="createAdminUser" enctype="multipart/form-data" name="createAdminUser">
                {{csrf_field()}}
                    <div class="row">
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group @if($errors->first('role_id')) has-danger @endif">
                                <label for="role_id">Role</label>
                                    <select class="role_id form-control br-0  @if($errors->first('role_id')) is-invalid @endif" id="role_id" name="role_id" >
                                        <option value="">Select Role</option>
                                        @foreach($roles as $role)
                                            <option value="{{$role->id}}" @if(old('role_id') == $role->id ) selected="selected" @elseif ($role->id == 1)selected="selected" @endif>{{$role->display_name}}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->first('role_id'))
                                        <sapn class="">{{$errors->first('role_id')}}</sapn>
                                    @endif
                                    <span class="error"></span>
                            </div>
                        </div>
                    </div>
                    <div id="new-form" class="user-account">
                        @include('admin.admin-users.basic-form')
                    </div>
                    <div class="tile-footer text-right">
                        <button id="submit-btn" class="btn btn-primary waves-effect waves-light br-0 btn-sm" type="submit">Save</button>
                    </div>
            </form>                        
        </div>
        <!-- END Body Part -->
    </main>
@endsection

@section('script')
    <script type="text/javascript" src="{{asset('admin/js/plugins/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/selectize.min.js')}}"></script>
    <script src="{{asset('admin/assets/js/custom/admin-user.js')}}"></script>
    <script>
        var no_image_url = "{{url('media/no-image/no-image.png')}}";
    </script>
    <!--Notifications Message Section-->
    @include('admin.layouts.notifications')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#imagePreview').css('background-image', 'url(' + e.target.result + ')');
                    $('#imagePreview').hide();
                    $('#imagePreview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }


        // $("#imageUpload").change(function () {
        $('body').on('change', '#imageUpload', function () {
            readURL(this);
        });




        /*Validation ends*/
    </script>
    <script>
        $( document ).ready(function() {
            // $(".role_id").val(1).trigger('change');
            formLoad('1');

        });
    </script>
@endsection