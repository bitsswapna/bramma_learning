
<form method="post" id="createAdminUser" enctype="multipart/form-data" name="createAdminUser">
    {{csrf_field()}}
    {{-- <div class="scroller scroller-left-1"><i class="glyphicon glyphicon-chevron-left"></i></div>
    <div class="scroller scroller-right-1"><i class="glyphicon glyphicon-chevron-right"></i>
    </div> --}}
    <div class="wrapper_colors">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs list_colors li1" role="tablist" id="myTab">
            <li role="presentation" data-number="1">
                <a class="active" id="usertab" href="#user-type" aria-controls="user-type" role="tab" data-toggle="tab" title="User Type">
                    <i class="fa fa-user" aria-hidden="true"></i>
                    <!-- <i class="fas fa-check is-valid has-success"></i> -->
                    <span class="hidden-text"> User type</span></a>
                </a>
            </li>
            <li role="presentation" data-number="2">
                <a href="#personal-info" aria-controls="personal-info" role="tab" data-toggle="tab" title="Personal  Info">
                    <i class="fas fa-address-card"></i>
                    <!-- <i class="fas fa-exclamation-triangle has-error is-invalid"></i> -->
                    <span class="hidden-text">
                    Personal  Info</span>
                </a>
            </li>
            <li role="presentation" data-number="3">
                <a href="#contact-info" aria-controls="contact-info" role="tab" data-toggle="tab" title="Contact
                Info">
                    <i class="fa fa-phone" aria-hidden="true"></i>
                    <span class="hidden-text">Contact
                        Info</span>
                </a>
            </li>

            <li role="presentation" data-number="4" class="privillage"><a href="#identification-details" aria-controls="identification-details" role="tab" data-toggle="tab" title="Identification                Details"><i class="fas fa-id-card-alt"></i><span class="hidden-text">Identification
                    Details</span></a>
            </li>
            <li role="presentation" data-number="5" class="privillage"><a href="#bank-details" aria-controls="bank-details" role="tab" data-toggle="tab" title="Bank Details"><i class="fas fa-university"></i><span class="hidden-text">Bank Details</span></a>
            </li>
            <li role="presentation" data-number="6" class="privillage"><a href="#advance-option" aria-controls="advance-option" role="tab" data-toggle="tab" title="Advance Options"><i class="fas fa-ellipsis-v"></i> <span class="hidden-text">Advance Options</span></a>
            </li>

        </ul>
    </div>

    <!-- Tab panes -->
    <div class="tab-content">
        <!-- User type -->
        <div role="tabpanel" class="tab-pane active " id="user-type">
            <div class="tab-content--inner animated fadeIn">
                <div class="choose-user">

                    <label class="image-radio">
                        <i class="fa fa-user" aria-hidden="true"></i>
                        <input  type="radio" name="role_id"  class="role_id" value="4" />
                        <h3>customer</h3>
                        <i class="glyphicon glyphicon-ok hidden"></i>
                    </label>

                    @if(Auth::user()->role_id!=10)
                        @if(is_array($permissions)&&$permissions['wa_add']==1)

                            <label class="image-radio">
                                <i class="fa fa-user-tie"></i>
                                <input type="radio" name="role_id"  class="role_id" value="1" />
                                <h3>wellness advisor</h3>
                                <i class="glyphicon glyphicon-ok hidden"></i>
                            </label>
                        @endif
                    @endif

                </div>
                @if(Auth::user()->role_id == 0)
                    <div class="parent-select">
                        <div class="grouping">
                            <!-- <div class="col-sm-12">
                                <div class="text-center">
                                    <div class="set-parent">
                                        <label class="pure-material-checkbox">
                                            <input type="checkbox" name="parent_as_admin" id="vi-admin-as-parent"  data-id="#vi-admin-as-parent" data-formfield="set_damin" class="validate-field" value="1">
                                            <span>Set parent as me.</span>
                                        </label>
                                    </div>
                                </div>
                            </div> -->
                            <div class="col-sm-12 col-xs-12">
                                <div class="parent-drop">
                                    <div class="form-group"><label class="label-block" for="">Enter Parent Name/ ID</label>
                                        <select id="wellness-advisor-list" class="wellness-advisor-list demo-default validate-field" data-id="#wellness-advisor-list" data-formfield="set_damin" placeholder="Enter parent name/id" name="parent_id"></select>
                                    </div>
                                    <span data-number="1" class="text-danger small text-center error error-fill-fst"></span>
                                </div>

                            </div>
                        </div>
                    </div>
                @elseif (Auth::user()->role_id == 1)
                    @if(is_array($permissions) && sizeof($permissions)>0)
                        @if($permissions['customer_wa_child'] == 1)
                            <div class="parent-select">
                                <div class="grouping">
                                    <!-- <div class="set-parent">
                                        <label class="pure-material-checkbox">
                                            <input type="checkbox" name="parent_as_admin" id="vi-admin-as-parent"  data-id="#vi-admin-as-parent" data-formfield="set_damin" class="validate-field" value="1">
                                            <span>Set parent as me.</span>
                                        </label>
                                    </div> -->
                                    <div class="parent-drop">
                                        <div class="form-group"><label class="label-block" for="">Set parent as me.</label>
                                            <select id="wellness-advisor-list" class="wellness-advisor-list demo-default validate-field" data-id="#wellness-advisor-list" data-formfield="set_damin" placeholder="Enter parent name/id" name="parent_id"></select>
                                        </div>
                                        <span data-number="1" class="text-danger small text-center error"></span>
                                    </div>
                                </div>
                            </div>
                        @else
                            <input type="hidden" name="parent_as_admin" id="vi-admin-as-parent" value="{{Auth::user()->id}}">
                        @endif
                    @else
                        <input type="hidden" name="parent_as_admin" id="vi-admin-as-parent" value="{{Auth::user()->id}}">
                    @endif
                @endif
                @if(Auth::user()->role_id == 10)
                    <input type="hidden" name="parent_as_admin" id="vi-admin-as-parent" value="{{$parent_wa_id ? $parent_wa_id->user_id : 1}}">
                @endif
            </div>
            <!-- Tab next prev btn -->
            <div class="tab-footer">
                <button type="button" class="btn-next btnNext"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
            </div>
        </div>
        <!-- User type end -->

        <!-- Personal information -->
        <div role="tabpanel" class="tab-pane" id="personal-info">
            <div class="tab-content--inner  animated fadeIn">

                <div class="personal-info">
                    <!-- Pic -->
                    <div class="avatar-upload">
                        <div class="avatar-edit">
                            <input type='file' id="imageUpload" name="image" accept=".png, .jpg, .jpeg" />
                            <label for="imageUpload"></label>
                            <span data-number="2" class="text-danger small text-center error"></span>
                        </div>
                        <div class="avatar-preview">
                            <div id="imagePreview" style="background-image: url(http://i.pravatar.cc/500?img=7);">
                            </div>
                        </div>
                        <span data-number="2" class="text-danger error small avatar-error"></span>
                    </div>
                    <!-- Pic -->
                    <!-- Presonal form -->
                    <div class="personal-form">
                        <!-- First name -->
                        <div class="col-md-6 col-xs-12 col-12  form-group">
                            <label for="firstname">First Name</label>
                            <input  type="text" class="form-control validate-field" id="first_name" data-id="#first_name" data-formfield="first_name" name="first_name" type="text"
                                    autocomplete="off" value="{{old('first_name')}}">
                            <span data-number="2" class="error"></span>
                        </div>
                        <!-- last name -->
                        <div class="col-md-6 col-xs-12 col-12  form-group">
                            <label for="lastname">Last Name</label>
                            <input  type="text" class="form-control " id="last_name" data-id="#last_name" data-formfield="last_name" name="last_name" type="text"
                                    autocomplete="off" value="{{old('last_name')}}">
                            <span data-number="2" class="error"></span>
                        </div>
                        <!-- gender -->
                        <div class="col-md-6 col-xs-12 col-12 form-group">
                            <label class="label-block" for="">Gender</label>
                            <div class="line-radio">
                                <input type="radio" id="male" name="gender" value="male" checked>
                                <label for="male">Male</label>
                            </div>
                            <div class="line-radio">
                                <input type="radio" id="female" name="gender" value="female">
                                <label for="female">Female</label>
                            </div>
                            <!-- <label class="custom-radio-btn" for="gender">
                                <input   id="css1"  type="radio" name="gender" checked value="male" />
                                <span class="custom-radio-outer">
                                    <span  class="custom-radio-inner"></span>
                                </span>
                                Male
                            </label>

                            <label class="custom-radio-btn" for="gender1">
                                <input type="radio" name="gender" id="gender1" value="female" />
                                <span class="custom-radio-outer">
                                    <span class="custom-radio-inner"></span>
                                </span>
                                Female
                            </label> -->
                        </div>
                        <!-- DOB -->
                        <div class="col-md-6 col-xs-12 col-12 form-group">
                            <label class="label-block" for="">Date of Birth</label>
                            <input type="text" class="form-control date-pick custom-field validate-field"  id="date_of_birth" data-id="#date_of_birth" data-formfield="date_of_birth" name="date_of_birth"
                                   value="{{old('date_of_birth')}}" data-date-end-date="0d"  placeholder="yyyy-mm-dd" data-provide="datepicker">
                            {{-- <input type="date" class="form-control custom-field validate-field" id="date_of_birth" data-id="#date_of_birth" data-formfield="date_of_birth" name="date_of_birth"
                                   value="{{old('date_of_birth')}}"> --}}
                            <span data-number="2" class="error"></span>
                        </div>
                        <!-- mobile -->
                        <div class="col-md-6 col-xs-12 col-12  form-group">
                            <label for="mobile">Mobile Number</label>
                            <input type="tel" class="form-control validate-field" id="mobile" data-id="#mobile" data-formfield="mobile" name="mobile"
                                   value="{{old('mobile')}}">
                            <span data-number="2" class="error"></span>
                        </div>
                        <!-- email -->
                        <div class="col-md-6 col-xs-12 col-12  form-group">
                            <label for="email">Email ID</label>
                            <input type="email" class="form-control validate-field" id="email" data-id="#email" data-formfield="email" name="email"
                                   value="{{old('email')}}" autocomplete="off">
                            <span data-number="2" class="error"></span>
                        </div>
                        <!-- password -->
                        <div class="col-md-6 col-xs-12 col-12  form-group">
                            <label for="password">Password</label>
                            <input  type="password" class="form-control validate-field" id="password" data-id="#password" data-formfield="password" name="password" autocomplete="off">
                            <span data-number="2" class="error"></span>
                        </div>
                        <!-- confirm password -->
                        <div class="col-md-6 col-xs-12 col-12  form-group">
                            <label for="conf-password">Confirm Password</label>
                            <input type="password" class="form-control validate-field" id="password_confirmation" data-id="#password_confirmation" data-formfield="password_confirmation" name="password_confirmation">
                            <span data-number="2" class="error"></span>
                        </div>
                    </div>
                    <!-- Presonal form end -->
                </div>
            </div>
            <!-- Tab next prev btn -->
            <div class="tab-footer">
                <button type="button" class="btn-prev btnPrevious"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
                <button  type="button" class="btn-next btnNext"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
            </div>
        </div>
        <!-- Personal information end -->

        <!-- Contact information -->
        <div  role="tabpanel" class="tab-pane " id="contact-info">
            <div class="tab-content--inner animated fadeIn">
                <div class="contact-info">
                    <!-- Address -->
                    <div class="col-md-6 col-xs-12 col-12  form-group">
                        <label for="address">Address line 1</label>
                        <input class="form-control validate-field" id="address1" data-id="#address1" data-formfield="address1" name="address1" type="text" autocomplete="off" value="{{old('address1')}}" >
                        <span data-number="3" class="error"></span>
                    </div>
                    <div class="col-md-6 col-xs-12 col-12  form-group">
                        <label for="address">Address line 2</label>
                        <input class="form-control validate-field" id="address2" data-id="#address2" data-formfield="address2" name="address2" type="text" autocomplete="off" value="{{old('address2')}}" >
                        <span  data-number="3"  class="error"></span>
                    </div>
                    <!-- Country -->
                    <div class="col-md-6 col-xs-12 col-12 form-group">
                        <label class="label-block" for="">Country</label>
                        <select class="form-control custom-field" id="country" name="country">
                            <option value="India">India</option>
                        </select>
                        <span  data-number="3" class="error"></span>
                    </div>
                    <!-- State -->
                    <div class="col-md-6 col-xs-12 col-12 form-group">
                        <label class="label-block" for="">State</label>
                    {!!Form::select('district_state',$states,'',(['class'=>' form-control
                    br-0 validate-field custom-field','id'=>'address_new_state','data-id'=>'#address_new_state'
                    ,'data-formfield'=>'state', 'placeholder' => 'Please Choose'])) !!}
                    <!-- <select class="form-control " name="" id="">
                            <option value="">Kerala</option>
                            <option value="">Tamil nadu</option>
                            <option value="">Karnadaka</option>
                        </select> -->
                        <span  data-number="3" class="error"></span>
                    </div>
                    <!-- Pincode -->
                    <div class="col-md-6 col-xs-12 col-12  form-group">
                        <label for="picode">Pincode</label>
                        <input type="text" class="form-control validate-field" id="pincode" data-id="#pincode" data-formfield="pincode" name="pincode" value="{{old('pincode')}}">
                        <span  data-number="3" class="error"></span>
                    </div>
                    <!-- Location -->
                    <div class="col-md-6 col-xs-12 col-12  form-group">
                        <label for="location">Location</label>
                        <input type="text" class="form-control validate-field" id="location" data-id="#location" data-formfield="location" name="location" value="{{old('location')}}">

                        <span  data-number="3" class="error"></span>
                        <input class="form-control" id="latitude" name="latitude" type="hidden"
                               placeholder="Enter latitude" autocomplete="off">
                        <input class="form-control" id="longitude" name="longitude" type="hidden"
                               placeholder="Enter longitude" autocomplete="off">
                    </div>
                </div>
            </div>
            <!-- Tab next prev btn -->
            <div class="tab-footer">
                <button type="button"  class="btn-prev btnPrevious "><i class="fa fa-angle-left" aria-hidden="true"></i></button>
                <button type="button"  class="btn-next btnNext cont_data"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
                <button type="button" class="btn-save lst_data" data-toggle="tooltip" data-placement="top" data-html="true" title="Save Data"><i class="fa fa-floppy-o" aria-hidden="true"></i>
                    Save</button>
            </div>
        </div>
        <!-- Contact information end -->

        <!-- Identification details -->
        <div role="tabpanel" class="tab-pane " id="identification-details">
            <div class="tab-content--inner  animated fadeIn">
                <div class="identification-info">
                    <!-- Aadhar -->
                    <div class="col-md-6 col-xs-12 col-12  form-group">
                        <label for="aadhar">Aadhar Number</label>
                        <input type="text" class="form-control validate-field" name="aadhar_card_no" id="aadhar_card_no" data-id="#aadhar_card_no" data-formfield="aadhar_card_no"  value="{{old('aadhar_card_no')}}">
                        <span data-number="4" class="error"></span>
                    </div>
                    <!-- Pan -->
                    <div class="col-md-6 col-xs-12 col-12  form-group">
                        <label for="pan">PAN Number</label>
                        <input type="text" class="form-control validate-field" name="pan_card_no" id="pan_card_no" data-id="#pan_card_no" data-formfield="pan_card_no"  value="{{old('pan_card_no')}}">
                        <span  data-number="4" class="error"></span>
                    </div>
                </div>
            </div>
            <!-- Tab next prev btn -->
            <div class="tab-footer">
                <button type="button" class="btn-prev btnPrevious"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
                <button  type="button" class="btn-next btnNext"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
            </div>
        </div>

        <!-- Bank details -->
        <div role="tabpanel" class="tab-pane fadeIn" id="bank-details">
            <div class="tab-content--inner  animated fadeIn">
                <div class="bank-info">
                    <!-- bank -->
                    <div class="col-md-6 col-xs-12 col-12 form-group">
                        <label class="label-block" for="">Select Bank</label>
                        <!-- <select class="form-control custom-field" name="" id="">
                            <option value="">SBI</option>
                            <option value="">ICICI</option>
                            <option value="">AXIS</option>
                        </select> -->
                        {!! Form::select('bank',bankList(),'',(['class'=>'form-control validate-field','placeholder' => 'Please Choose','id' => 'bank','data-id' => '#bank','data-formfield' => 'empty','message'=>'Please choose bank name' ])) !!}
                        <span  data-number="5" class="error"></span>
                    </div>
                    <!-- Branch code -->
                    <div class="col-md-6 col-xs-12 col-12  form-group">
                        <label for="branch-code">Branch Code</label>
                        <input type="text" class="form-control validate-field" id="branch_code" data-id="#branch_code"  name="branch_code" value="{{old('branch_code')}}">
                        <span data-number="5" class="error"></span>
                    </div>
                    <!-- Account number -->
                    <div class="col-md-6 col-xs-12 col-12  form-group">
                        <label for="account-number">Account Number</label>
                        <input type="text" class="form-control validate-field" id="account_number" data-id="#account_number" data-formfield = 'empty' message='Please enter account number' name="account_number" value="{{old('account_number')}}">
                        <span data-number="5" class="error"></span>
                    </div>
                    <!-- Branch -->
                    <div class="col-md-6 col-xs-12 col-12  form-group">
                        <label for="bank-branch">Branch</label>
                        <input type="text" class="form-control validate-field" id="branch" data-id="#branch" data-formfield = 'empty' message='Please enter branch name' name="branch" value="{{old('branch')}}">
                        <span data-number="5" class="error"></span>
                    </div>
                    <!-- IFSC -->
                    <div class="col-md-6 col-xs-12 col-12  form-group">
                        <label for="ifsc-code">IFSC code</label>
                        <input type="text" class="form-control validate-field" id="ifsc" data-id="#ifsc" data-formfield = 'empty' message='Please enter ifsc code' name="ifsc" value="{{old('ifsc')}}">
                        <span data-number="5" class="error"></span>
                    </div>
                    <!-- Name banker -->
                    <div class="col-md-6 col-xs-12 col-12  form-group">
                        <label for="name-banker">Name of Banker</label>
                        <input type="text" class="form-control validate-field" id="name_of_banker" data-id="#name_of_banker" data-formfield = 'empty' message='Please enter name of banker' name="name_of_banker" value="{{old('name_of_banker')}}">
                    </div>
                </div>
            </div>
            <!-- Tab next prev btn -->
            <div class="tab-footer">
                <button type="button" class="btn-prev btnPrevious"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
                <button  type="button" class="btn-next btnNext"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
            </div>
        </div>

        <!-- Advance options -->
        <div role="tabpanel" class="tab-pane" id="advance-option">
            <div class="tab-content--inner  animated fadeIn">
                <div class="advance-options">

                    <!-- Permissions -->
                    <div class="col-md-12 col-xs-12 col-12 permission-box">
                        @if(is_array($permissions) && sizeof($permissions)>0)
                            <label for="">Permissions</label>
                            <div class="col-xs-12 col-12">
                                @php $result = ['billing'=>'Billing','wa_add'=>'Add WA','staff_add'=>'Add Staff','customer_wa_child'=>'Add Customer as WA child'];@endphp
                                @foreach($permissions as $key => $data)
                                    @if($data == 1)
                                        <div class="col-md-12 col-sm-12 col-xs- col-12">
                                            <label class="pure-material-checkbox">
                                                <input id="{{$key}}" type="checkbox" name="permissions[]" @if($key == 'billing') checked @endif value="{{$key}}">
                                                <span>{{$result[$key]}}</span>
                                            </label>
                                        </div>
                                    @endif
                                @endforeach

                            </div>
                        @endif
                    </div>

                    <!-- Payment -->
                    <div class="col-md-12 col-xs-12 col-12">
                        @if(is_array($payment_modes) && sizeof($payment_modes)>0)
                            <label for="">Payment Method</label>
                            <div class="col-xs-12 col-12">
                                @foreach($payment_modes as $key => $data)
                                    @if($data == 1)
                                        <div class="col-md-12 col-sm-12 col-xs- col-12">
                                            <label class="pure-material-checkbox">
                                                <input  id="{{$key}}" type="checkbox" name="payment_modes[]" @if($key == 'online') checked @endif value="{{$key}}">
                                                <span> {{ucwords(str_replace("_"," ",$key))}}</span>
                                            </label>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        @endif
                    </div>

                </div>
            </div>
            <!-- Tab next prev btn -->
            <div class="tab-footer">
                <button type="button" class="btn-prev btnPrevious"><i class="fa fa-backward" aria-hidden="true"></i></button>
                <button type="button" class="btn-save  "><i class="fa fa-floppy-o" aria-hidden="true"></i>
                    Save</button>
            </div>
        </div>

    </div>
</form>
