@extends('admin.layouts.app')

@section('css')
    {{--<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />--}}
    <link href="{{asset('admin/assets/css/select2/select2.min.css')}}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    <main class="app-content">
        <div class="app-title">
            <div class="col-md-6 col-sm-12 col-xs-12 rem-pad">
                <h1><i class="fa fa-plus-circle"></i>Wellness Advisor</h1>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 btn-right rem-pad">
                @if(can('browse_admin_user'))
                    <a class="btn btn-success waves-effect waves-light" href="{{url('admin_users')}}"><i class="fa fa-list"></i>Admin User Lists</a>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="tile">
                    <div class="tile-body">
                        <form method="post" id="createAdminUserWA" enctype="multipart/form-data" name="createAdminUserWA">
                            {{csrf_field()}}
                            <input type="hidden" value="{{$user->id}}" id="user_id">
                            <input type="hidden" value="1" id="role_id" name="role_id">
                            <div class="row">
                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group @if($errors->first('first_name')) has-danger @endif">
                                        <label for="first_name">First Name</label>
                                        <input class="form-control @if($errors->first('first_name')) is-invalid @endif" id="first_name" name="first_name" type="text" placeholder="Enter first name"  autocomplete="off" value="{{$user->first_name}}">
                                        @if($errors->first('first_name'))
                                            <sapn class="">{{$errors->first('first_name')}}</sapn>
                                        @endif
                                        <span class="error"></span>
                                    </div>
                                    <div class="form-group @if($errors->first('last_name')) has-danger @endif">
                                        <label for="last_name">Last Name</label>
                                        <input class="form-control @if($errors->first('last_name')) is-invalid @endif" id="last_name" name="last_name" type="text" placeholder="Enter last name" autocomplete="off" value="{{$user->last_name}}">
                                        @if($errors->first('last_name'))
                                            <sapn class="">{{$errors->first('last_name')}}</sapn>
                                        @endif
                                        <span class="error"></span>
                                    </div>
                                    <div class="form-group @if($errors->first('mobile')) has-danger @endif">
                                        <label for="mobile">Mobile</label>
                                        <input class="form-control @if($errors->first('mobile')) is-invalid @endif" id="mobile" name="mobile" type="text" placeholder="Enter mobile number" autocomplete="off" value="{{$user->mobile}}">
                                        @if($errors->first('mobile'))
                                            <sapn class="">{{$errors->first('mobile')}}</sapn>
                                        @endif
                                        <span class="error"></span>
                                    </div>
                                    <div class="form-group @if($errors->first('email')) has-danger @endif">
                                        <label for="email">Email</label>
                                        <input class="form-control @if($errors->first('email')) is-invalid @endif" id="email" name="email" type="text" placeholder="Enter email" autocomplete="off" value="{{$user->email}}" readonly>
                                        @if($errors->first('email'))
                                            <sapn class="">{{$errors->first('email')}}</sapn>
                                        @endif
                                        <span class="error"></span>
                                    </div>
                                    {{--<div class="form-group @if($errors->first('password')) has-danger @endif" >--}}
                                        {{--<label for="password">Password</label>--}}
                                        {{--<input class="form-control @if($errors->first('password')) is-invalid @endif" id="password" name="password" type="password" placeholder="Enter password" autocomplete="off">--}}
                                        {{--@if($errors->first('password'))--}}
                                            {{--<sapn class="">{{$errors->first('password')}}</sapn>--}}
                                        {{--@endif--}}
                                        {{--<span class="error"></span>--}}
                                    {{--</div>--}}
                                    <div class="tile-footer">
                                        <button class="btn btn-primary waves-effect waves-light" type="submit">Save</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group  " >
                                            <label for="role_id">Role</label>
                                            <select name="" id="" class="form-control" disabled>
                                                <option value="1" selected>Wellness Advisor</option>
                                            </select>
                                            <span class="error"></span>
                                        </div>
                                        <div>
                                            <label  for="">Set parent as admin</label>
                                            <input style="float: left" type="checkbox" name="parent_as_admin" id="vi-admin-as-parent-wa" value="1">
                                            <span class="error" style="display: block"></span>
                                        </div>
                                        <div class="form-group" id="vi-wa-as-parent-wa">
                                            <label for="parent_id">Choose Parent</label>
                                            <select class="wellness-advisor-list form-control" placeholder="Pick members..." name="parent_id">
                                                <option value="">Select Wellness Advisor</option>
                                                @foreach ($wa as $data)
                                                    <option value="{{$data->id}}">
                                                        {{$data->name}} - {{$data->id}}
                                                    </option>
                                                @endforeach
                                            </select>
                                            <span class="error"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

    <script src="{{asset('admin/assets/js/custom/admin-user.js')}}"></script>
    <script>
        var no_image_url = "{{url('media/no-image/no-image.png')}}";
    </script>
    <!--Notifications Message Section-->
    @include('admin.layouts.notifications')

@endsection