@extends('admin.layouts.app')

@section('css')
    <link href="{{asset('admin/assets/plugins/jquery-datatables-editable/datatables.css')}}" rel="stylesheet" type="text/css"/>

@endsection

@section('content')
    <main >
        <!-- <div class="app-title">
            <div class="col-md-6 rem-pad">
                <h1><i class="fa fa-user"></i> Admin & Staff Management</h1>
            </div>
            <div class="col-md-6 btn-right rem-pad">
                <p class="bs-component">
                    @if(can('add_admin_user'))
                        <a class="btn btn-primary waves-effect waves-light" href="{{url('admin_user/from-website')}}" role="button"><i class="fa fa-list"></i>WA Registerd From website</a>
                        <a class="btn btn-primary waves-effect waves-light" href="{{url('admin_user/create')}}" role="button"><i class="fa fa-plus-circle"></i>Create User</a>
                    @endif
                </p>
            </div>
        </div> -->
        <div class="thumbnail-card">
        <div class="inliner">
            <h5 class="title-caption inliner"><i class="fa fa-user"></i> Admin & Staff Management</h5>
             @if(can('browse_admin_user'))
                <a data-toggle="tooltip" data-placement="top" data-html="true" title="Create User" class="btn bg-primary-green btn-sm waves-effect waves-light inliner btn-right mt--6 br-0"  href="{{url('admin_user/create')}}" role="button"><i class="fa fa-plus"></i>Create User</a>
                {{--<a data-toggle="tooltip" data-placement="top" data-html="true" title="WA Registerd From website" class="btn bg-primary-blue btn-sm waves-effect waves-light inliner btn-right mt--6 br-0" href="{{url('admin_user/wa-request')}}" role="button"><i class="fa fa-list"></i>WA Registerd From website</a>--}}
                <a data-toggle="tooltip" data-placement="top" data-html="true" title="User Registerd From website" class="btn bg-primary-blue btn-sm waves-effect waves-light inliner btn-right mt--6 br-0" href="{{url('admin_user/user-request')}}" role="button"><i class="fa fa-list"></i>User Registerd From website</a>
            @endif

        </div>
    </div>



        @if(can('browse_admin_user'))
            <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="tile-body data--table">
                            <div class="table-responsive">
                            <table class="table table-striped" id="adminUserWebsite">
                                <thead>
                                    <tr>
                                        <th>Sl#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Role</th>
                                        <!-- <th>Last Login</th>
                                        <th>Created At</th> -->
                                        <th>Mobile</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                            </div>
                        </div>
                    </div>
            </div>
        @else
            @include('admin.no-access-content')
        @endif
    </main>
@endsection

@section('script')
    <script type="text/javascript" src="{{asset('admin/js/plugins/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/dataTables.bootstrap.min.js')}}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script>
        $(document).ready(function(){
            var table = $('#adminUserWebsite').DataTable({

                "aaSorting": [],
                'order': [[1, 'desc']],
                "fnDrawCallback": function (oSettings) {
                    if (oSettings.bSorted || oSettings.bFiltered || oSettings._iDisplayLength) {
                        j = 0;
                        for (var i = oSettings._iDisplayStart; i < oSettings.aiDisplay.length + oSettings._iDisplayStart; i++) {
                            $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[j]].nTr).find('span').text(i + 1);
                            j++;
                        }
                    }
                },
                serverSide: true,
                scrollX: true,
                oLanguage: {
                    sProcessing: '<svg width="30px"  height="30px"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-rolling" style="background: transparent;"><circle cx="50" cy="50" fill="none" stroke="#0fb963" stroke-width="10" r="35" stroke-dasharray="164.93361431346415 56.97787143782138" transform="rotate(228 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform></circle></svg>'
                },
                processing: true,
                "pageLength": 10,
                "columnDefs": [{'orderable': false, 'targets': [0, 4, 5]}],
                mark: true,
                fixedColumns: {},
                ajax: {
                    url: '{!! URL::to("get-admin-users-website") !!}',
                },
                columns: [
                    {data: 'sl#', name: 'sl#'},
                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email'},
                    {data: 'role', name: 'role'},
                    {data: 'status', name: 'status'},
                    {data: 'action', name: 'action'}
                ]
            });

            $(document).on('click', '.change-status', function (e) {
                var token = $('meta[name="csrf-token"]').attr("content");
                var id = $(this).data("id");
                var status = $(this).data('status');

                var data = {id: id, status: status, _token: token};

                var status_html = $(this).html();
                var url = '{{url('admin-user/change_status')}}'

                $.ajax({
                    url: url, type: 'POST', data: data,
                    beforeSend: function () {

                    },
                    success: function (data) {
                        successMsg(data.msg);
                    },
                    error: function (data) {
                        errorMsg(data.responseJSON.msg);
                    },
                    complete: function () {

                    }
                });
            });
            $(document).on('click', '.delete-user-website', function (e) {

                e.preventDefault();
                var id = $(this).data("id") ;
                var url = base_url+'/admin-user-wa/delete';


                swal({
                    title: "Are you sure ?",
                    text: "Once deleted, it will be removed from User!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                    .then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                url: url,
                                type: 'POST',  // user.destroy
                                data:{
                                    'id':id,
                                },
                                beforeSend: function(){
                                    $("#if_loading").html("loading");
                                },
                                success: function(result) {
                                    table.draw()

                                    // Do something with the result
                                },
                                complete: function(){
                                    $('#if_loading').html("");
                                }
                            });

                        } else {
                            //
                        }
                    });

            });
        })




    </script>
@endsection