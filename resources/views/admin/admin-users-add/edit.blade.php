@extends('admin.layouts.app') @section('css')
    <link href="{{asset('css/plugin/select2.min.css')}}" rel="stylesheet" />
    <link href="{{asset('css/plugin/selectize.bootstrap2.css')}}" rel="stylesheet" />

@endsection
@section('content')
    <main class="app-content">
        <!-- Sub Title---->
        <div id="submenu_subheader" class="submenu-subheader submenu-grid__item ">
            <div class="submenu-subheader__title">
                <h4 class="title-caption"> <i class="fa fa-user"></i>  User Management</h4>
            </div>
            <div class="submenu-subheader__toolbar">
                <div class="submenu-subheader__toolbar-wrapper bs-component">
                    @if(can('browse_admin_user'))

                        <a data-toggle="tooltip" data-placement="top" data-html="true" title="User List" class="btn bg-primary-green btn-sm waves-effect waves-light btn-right br-0" href="{{url('admin_users')}}"><i class="fa fa-list"></i>User List</a>
                    @endif
                </div>
            </div>
        </div>        <!-- END Sub Title---->
        <form method="post" id="editAdminUser" enctype="multipart/form-data" name="editAdminUser">
            <div class="row">
                <div class=" col-md-12 col-sm-12 col-xs-12">
                    <div class="tile">
                        <div class="tile-body br-0">
                            <div class="slide-tabs">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a data-toggle="tab" href="#personal-details"> <i class="fa fa-list"></i> Personal Details</a>
                                    </li>
                                    @if ($user->role_id != '4')
                                        <li>
                                            <a data-toggle="tab" href="#bank-details"> <i class="fa fa-list"></i> Bank Details</a>
                                        </li>
                                    @endif
                                    @if ($user->role_id == '1' || $user->role_id == '10' )
                                        <li>
                                            <a data-toggle="tab" href="#permissions"><i class="fa fa-list"></i>Permissions</a>
                                        </li>
                                    @endif
                                </ul>
                                <div class="tab-content">
                                    <!-- Product Primary Details TAB -->
                                    <div id="personal-details" class="tab-pane fade in active">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                                {{csrf_field()}}
                                                <input type="hidden" value="{{$user->id}}" id="user_id">
                                                <div class="form-group @if($errors->first('role_id')) has-danger @endif">
                                                    <label for="role_id">Role</label>
                                                    @if($user->role_id == 4)
                                                        <select name="cu_wa_id" id="cu_wa_id" class="form-control br-0">
                                                            <option value="">Customer</option>
                                                            <option value="1">Wellness Advisor</option>
                                                        </select>
                                                    @else
                                                        {!! Form::select('rolee_id',$roles,$user->role_id,(['class'=>'form-control br-0','placeholder' => 'Please Choose','id' => 'role_id','disabled'=>'true'])) !!}

                                                    @endif
                                                    <span class="error"></span>

                                                </div>
                                                <input type="hidden" name="role_id" value="{{$user->role_id}}">
                                                <div class="form-group" id="new-form-append">
                                                    @if ($user->role_id == 5)
                                                        <label for="customer_care_category">Customer care category</label>

                                                        {!! Form::select('customer_care_category',$customer_care_category,$user->getCustomerCareCategory->customer_care_category_id,(['class'=>'form-control','placeholder' => 'Please Choose'])) !!}
                                                        <span class="error"></span> @endif
                                                </div>
                                                <div class="form-group @if($errors->first('first_name')) has-danger @endif">
                                                    <label for="first_name">First Name</label>
                                                    <input class="form-control br-0 @if($errors->first('first_name')) is-invalid @endif" id="first_name" name="first_name" type="text" placeholder="Enter first name" autocomplete="off" value="{{$user->first_name}}"> @if($errors->first('first_name'))
                                                        <sapn class="">{{$errors->first('first_name')}}</sapn>
                                                    @endif
                                                    <span class="error"></span>
                                                </div>
                                                <div class="form-group @if($errors->first('last_name')) has-danger @endif">
                                                    <label for="last_name">Last Name</label>
                                                    <input class="form-control br-0 @if($errors->first('last_name')) is-invalid @endif" id="last_name" name="last_name" type="text" placeholder="Enter last name" autocomplete="off" value="{{$user->last_name}}"> @if($errors->first('last_name'))
                                                        <sapn class="">{{$errors->first('last_name')}}</sapn>
                                                    @endif
                                                    <span class="error"></span>
                                                </div>
                                                <div class="form-group @if($errors->first('mobile')) has-danger @endif">
                                                    <label for="mobile">Mobile</label>
                                                    <input class="form-control br-0 @if($errors->first('mobile')) is-invalid @endif" id="mobile" name="mobile" type="text" placeholder="Enter mobile number" autocomplete="off" value="{{$user->mobile}}"> @if($errors->first('mobile'))
                                                        <sapn class="">{{$errors->first('mobile')}}</sapn>
                                                    @endif
                                                    <span class="error"></span>
                                                </div>
                                                <div class="form-group @if($errors->first('email')) has-danger @endif">
                                                    <label for="email">Email</label>
                                                    <input class="form-control br-0 @if($errors->first('email')) is-invalid @endif" id="email" name="email" type="text" placeholder="Enter email" autocomplete="off" value="{{$user->email}}" > @if($errors->first('email'))
                                                        <sapn class="">{{$errors->first('email')}}</sapn>
                                                    @endif
                                                    <span class="error"></span>
                                                </div>
                                                <div class=" form-group @if($errors->first('gender')) has-danger @endif">
                                                    <label for="gender">Gender</label>
                                                    <div class="form-inline" style="display: flex;">
                                                        <input type="radio" name="gender" value="male"  @if($user_details)  @if($user_details->gender == 'male') checked @endif  @endif > Male
                                                        <br>
                                                        <input type="radio" name="gender" value="female"  @if($user_details)   @if($user_details->gender == 'female') checked @endif @endif  style="margin-left: 20px;"> Female
                                                        <br>
                                                    </div>
                                                    @if($errors->first('gender'))
                                                        <sapn class="">{{$errors->first('gender')}}</sapn>
                                                    @endif
                                                    <span class="error"></span>
                                                </div>
                                                <div class="form-group @if($errors->first('date_of_birth')) has-danger @endif">
                                                    <label for="date_of_birth">Date of birth</label>
                                                    <input class="form-control @if($errors->first('date_of_birth')) is-invalid @endif" id="date_of_birth" name="date_of_birth" type="date" placeholder="Enter date of birth" autocomplete="off" value="{{($user_details) ? $user_details->date_of_birth : ''}}"> @if($errors->first('date_of_birth'))
                                                        <sapn class="">{{$errors->first('date_of_birth')}}</sapn>
                                                    @endif
                                                    <span class="error"></span>
                                                </div>
                                                <div class="form-group @if($errors->first('image')) has-danger @endif">
                                                    <label for="image">Image</label>
                                                    <div class="fake-input">
                                                        <input id="uploadFile" type="text" style="display: none;">
                                                        <div class="fileUpload">
                                                            <input class="form-control-file" id="image" name="image" type="file" accept="image/x-png,image/gif,image/jpeg">
                                                        </div>
                                                    </div>
                                                    @if($errors->first('image'))
                                                        <sapn class="">{{$errors->first('image')}}</sapn>
                                                    @endif
                                                    <span class="error"></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-12 col-xs-12">
                                                <div class="form-group @if($errors->first('address_1')) has-danger @endif">
                                                    <label for="address_1">Address  </label>
                                                    <input class="form-control br-0 @if($errors->first('address_1')) is-invalid @endif" id="address_1" name="address_1" type="text" placeholder="Enter address" autocomplete="off" value="{{($user_details) ? $user_details->address_1 : ''}}"> @if($errors->first('address_1'))
                                                        <sapn class="">{{$errors->first('address_1')}}</sapn>
                                                    @endif
                                                    <span class="error"></span>
                                                </div>
                                                <div class="form-group @if($errors->first('aadhar_card_no')) has-danger @endif">
                                                    <label for="aadhar_card_no">Aadhar card number</label>
                                                    <input class="form-control br-0 @if($errors->first('aadhar_card_no')) is-invalid @endif" id="aadhar_card_no" name="aadhar_card_no" type="text" placeholder="Enter Aadhar card number" autocomplete="off" value="{{($user_details) ? $user_details->aadhar_card_no : '' }}"> @if($errors->first('aadhar_card_no'))
                                                        <sapn class="">{{$errors->first('aadhar_card_no')}}</sapn>
                                                    @endif
                                                    <span class="error"></span>
                                                </div>
                                                <div class="form-group @if($errors->first('pan_card_no')) has-danger @endif">
                                                    <label for="pan_card_no">PAN Card number</label>
                                                    <input class="form-control br-0 @if($errors->first('pan_card_no')) is-invalid @endif" id="pan_card_no" name="pan_card_no" type="text" placeholder="Enter PAN card number" autocomplete="off" value="{{($user_details) ? $user_details->pan_card_no : ''}}"> @if($errors->first('pan_card_no'))
                                                        <sapn class="">{{$errors->first('pan_card_no')}}</sapn>
                                                    @endif
                                                    <span class="error"></span>
                                                </div>
                                                <div class="form-group @if($errors->first('district_state')) has-danger @endif">
                                                    <label for="district_state">State</label>
                                                    @php
                                                        if($user_details){
                                                            $user_state = $user_details->district_state ;
                                                        }else{
                                                            $user_state  = 'select';
                                                        }
                                                    @endphp
                                                    {!! Form::select('district_state',$states,$user_state,(['class'=>'form-control','placeholder' => 'Please Choose'])) !!}

                                                    <sapn class="">{{$errors->first('district_state')}}</sapn>
                                                    <span class="error"></span>
                                                </div>
                                                <div class="form-group @if($errors->first('country')) has-danger @endif">
                                                    <label for="country">Country</label>
                                                    <input class="form-control br-0 @if($errors->first('country')) is-invalid @endif" id="country" name="country" type="text" placeholder="Enter country" autocomplete="off" value="{{($user_details) ? $user_details->country : ''}}"> @if($errors->first('country'))
                                                        <sapn class="">{{$errors->first('country')}}</sapn>
                                                    @endif
                                                    <span class="error"></span>
                                                </div>
                                                <div class="form-group @if($errors->first('location')) has-danger @endif">
                                                    <label for="location">Location</label>
                                                    <input class="form-control br-0 @if($errors->first('location')) is-invalid @endif" id="location" name="location" type="text" placeholder="Enter location" autocomplete="off" value="{{($user_details) ? $user_details->location : '' }}"> @if($errors->first('location'))
                                                        <sapn class="">{{$errors->first('location')}}</sapn>
                                                    @endif
                                                    <span class="error"></span>
                                                </div>
                                                <div class="form-group @if($errors->first('new_password')) has-danger @endif">
                                                    <label for="new_password">New Password</label>
                                                    <input class="form-control br-0 @if($errors->first('new_password')) is-invalid @endif" id="new_password" name="new_password" type="password" placeholder="Enter new password" autocomplete="off" > @if($errors->first('new_password'))
                                                        <sapn class="">{{$errors->first('new_password')}}</sapn>
                                                    @endif
                                                    <span class="error"></span>
                                                </div>
                                                <div class="form-group @if($errors->first('confirm_password')) has-danger @endif">
                                                    <label for="confirm_password">Confirm  Password</label>
                                                    <input class="form-control br-0 @if($errors->first('confirm_password')) is-invalid @endif" id="confirm_password" name="confirm_password" type="password" placeholder="Enter confirm password" autocomplete="off" > @if($errors->first('confirm_password'))
                                                        <sapn class="">{{$errors->first('confirm_password')}}</sapn>
                                                    @endif
                                                    <span class="error"></span>
                                                </div>
                                                <div class="prof-img " id="prof_img">
                                                    @if ($user->avatar != "")
                                                        <img src="{{getImageByPath($user->avatar,'50x50','profile-images')}}" class=" img-fluid img-thumbnail br-0" alt="San Fran" style="height:200px;">
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tile-footer text-right">
                                            <button id="submit-btn" class="btn btn-primary waves-effect waves-light btn-sm br-0" type="submit">Save</button>
                                        </div>
                                    </div>
                                    @if ($user->role_id != '4')

                                        <div id="bank-details" class="tab-pane fade">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                                    @if ($bank_details)
                                                        <div class="form-group @if($errors->first('branch_code')) has-danger @endif">
                                                            <label for="last_name">Bank Name</label>
                                                            {!! Form::select('bank',bankList(),$bank_details->bank,(['class'=>'form-control validate-field','placeholder' => 'Please Choose','id' => 'bank','data-id' => '#bank','data-formfield' => 'bank' ])) !!}
                                                            <span class="error"></span>
                                                        </div>

                                                        <div class="form-group @if($errors->first('branch_code')) has-danger @endif">
                                                            <label for="last_name">Branch Code</label>
                                                            <input class="form-control br-0 @if($errors->first('branch_code')) is-invalid @endif" id="branch_code" name="branch_code" type="text" placeholder="Enter branch code" autocomplete="off" value="{{$bank_details->branch_code}}"> @if($errors->first('branch_code'))
                                                                <sapn class="">{{$errors->first('branch_code')}}</sapn>
                                                            @endif
                                                            <span class="error"></span>
                                                        </div>
                                                        {{--<div class="form-group @if($errors->first('address')) has-danger @endif">--}}
                                                        {{--<label for="last_name">Address</label>--}}
                                                        {{--<input class="form-control br-0 @if($errors->first('address')) is-invalid @endif" id="address" name="address" type="text" placeholder="Enter address" autocomplete="off" value="{{$bank_details->address}}"> @if($errors->first('address'))--}}
                                                        {{--<sapn class="">{{$errors->first('address')}}</sapn>--}}
                                                        {{--@endif--}}
                                                        {{--<span class="error"></span>--}}
                                                        {{--</div>--}}
                                                        <div class="form-group @if($errors->first('name_of_banker')) has-danger @endif">
                                                            <label for="last_name">Name of banker</label>
                                                            <input class="form-control br-0 @if($errors->first('name_of_banker')) is-invalid @endif" id="name_of_banker" name="name_of_banker" type="text" placeholder="Enter name of banker" autocomplete="off" value="{{$bank_details->name_of_banker}}"> @if($errors->first('name_of_banker'))
                                                                <sapn class="">{{$errors->first('name_of_banker')}}</sapn>
                                                            @endif
                                                            <span class="error"></span>
                                                        </div>
                                                        <div class="form-group @if($errors->first('branch')) has-danger @endif">
                                                            <label for="last_name">Branch</label>
                                                            <input class="form-control br-0 @if($errors->first('branch')) is-invalid @endif" id="branch" name="branch" type="text" placeholder="Enter branch" autocomplete="off" value="{{$bank_details->branch}}"> @if($errors->first('branch'))
                                                                <sapn class="">{{$errors->first('branch')}}</sapn>
                                                            @endif
                                                            <span class="error"></span>
                                                        </div>
                                                        <div class="form-group @if($errors->first('ac_number')) has-danger @endif">
                                                            <label for="last_name">A/C number</label>
                                                            <input class="form-control br-0 @if($errors->first('ac_number')) is-invalid @endif" id="ac_number" name="ac_number" type="text" placeholder="Enter A/C number" autocomplete="off" value="{{$bank_details->ac_number}}"> @if($errors->first('ac_number'))
                                                                <sapn class="">{{$errors->first('ac_number')}}</sapn>
                                                            @endif
                                                            <span class="error"></span>
                                                        </div>
                                                        <div class="form-group @if($errors->first('ifsc')) has-danger @endif">
                                                            <label for="last_name">IFSC</label>
                                                            <input class="form-control br-0 @if($errors->first('ifsc')) is-invalid @endif" id="ifsc" name="ifsc" type="text" placeholder="Enter ifsc" autocomplete="off" value="{{$bank_details->ifsc}}"> @if($errors->first('ifsc'))
                                                                <sapn class="">{{$errors->first('ifsc')}}</sapn>
                                                            @endif
                                                            <span class="error"></span>
                                                        </div>
                                                    @endif
                                                </div>

                                            </div>
                                            <div class="tile-footer text-right">
                                                <button class="btn btn-primary waves-effect waves-light btn-sm br-0" type="submit">Save</button>
                                            </div>
                                        </div>
                                    @endif
                                    <div id="permissions" class="tab-pane fade">
                                        <!-- Permissions USer-->
                                        <div class="box-container">

                                            <div class="row">
                                                @if ($user->role_id != '10' )
                                                    <h4>Advanced Options</h4>
                                                    <hr>
                                                    <div class="col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            @if(is_array($permissions))
                                                                <h4>Permissions</h4>
                                                                <hr>

                                                                @foreach($permissions as $key => $data)
                                                                    <label for="{{$key}}">{{ucwords(str_replace("_"," ",$key))}}</label>
                                                                    <input id="{{$key}}" type="checkbox" @if($data == 1) checked @endif  name="permissions[]" value="{{$key}}">
                                                                @endforeach
                                                            @endif
                                                        </div>
                                                    </div>
                                                @endif
                                                <div class="col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        @if(is_array($payment_modes))
                                                            <h4>Payment mode</h4>
                                                            <hr>
                                                            @foreach($payment_modes as $key => $data)
                                                                <label for="{{$key}}">{{ucwords(str_replace("_"," ",$key))}}</label>
                                                                <input id="{{$key}}" type="checkbox"  @if($data == 1) checked @endif  name="payment_modes[]" value="{{$key}}">
                                                            @endforeach
                                                        @endif
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="tile-footer text-right">
                                                <button class="btn btn-primary waves-effect waves-light btn-sm br-0" type="submit">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        </div>
    </main>
@endsection @section('script')
    <script src="{{asset('admin/js/plugins/selectize.min.js')}}"></script>

    <script src="{{asset('admin/assets/js/custom/admin-user.js')}}"></script>
    <script src="{{asset('admin/assets/js/sweet-alert/sweetalert.min.js')}}"></script>

    <script>
        var no_image_url = "{{url('media/no-image/no-image.png')}}"
        $("#cu_wa_id").change(function () {

            swal({
                title: "Are you sure?",
                text: "Please Confirm before changing role!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then(function(willDelete){
                    if (willDelete) {
                        // if (this.value == 1){
                            var user_id = {{$user->id}}
                            $.ajax({
                                url: base_url + "/admin_user/change_c_to_wa",
                                type: 'POST',
                                data: {user_id: user_id },
                                beforeSend: function() {
                                },
                                success: function(data) {
                                    location.reload();
                                },
                                complete: function() {
                                },
                                error: function() {
                                    swal("Something went wrong.Please try again...!")
                                }
                            });
                        // }
                    }else {
                        location.reload();
                    }
                });



        })
    </script>
    <!--Notifications Message Section-->
    @include('admin.layouts.notifications') @endsection
