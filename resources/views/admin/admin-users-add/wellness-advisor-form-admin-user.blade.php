<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="wizard-step modal-input">
            @if(Auth::user()->role_id == 0)
                <div class="form-group">
                    <div class="checkbox checkbox-custom styled-checkbox user-billing--checkbox">
                        <input type="checkbox" name="parent_as_admin" id="vi-admin-as-parent"  data-id="#vi-admin-as-parent" data-formfield="set_damin" class="validate-field" value="1" >
                        <label for="vi-admin-as-parent" class="label-inline">Set parent as me. </label>

                    </div>
                </div>


                <div class="form-group vi-wa-as-parent" id="vi-wa-as-parent">
                    <label for="parent_id">Enter parent name/id</label>
                    <select id="wellness-advisor-list" data-id="#wellness-advisor-list" data-formfield="set_damin" class="wellness-advisor-list demo-default validate-field"  placeholder="Enter parent name/id"
                            name="parent_id"></select>
                    <span class="error"></span>
                </div>

            @elseif (Auth::user()->role_id == 1)
                @if(is_array($permissions) && sizeof($permissions)>0)
                    @if($permissions['customer_wa_child'] == 1)
                        <div class="form-group">
                            <div class="checkbox checkbox-custom styled-checkbox user-billing--checkbox">
                                <input type="checkbox" name="parent_as_admin" id="vi-admin-as-parent" data-id="#vi-admin-as-parent" data-formfield="set_damin" class="validate-field" value="1">
                                <label for="vi-admin-as-parent" class="label-inline">Set parent as me. </label>

                            </div>
                        </div>
                        <div class="form-group vi-wa-as-parent" id="vi-wa-as-parent">
                            <label for="parent_id">Enter parent name/id</label>
                            <select id="wellness-advisor-list" class="wellness-advisor-list demo-default validate-field" data-id="#wellness-advisor-list" data-formfield="set_damin" placeholder="Enter parent name/id"
                                    name="parent_id"></select>
                            <span class="error"></span>
                        </div>
                    @else
                        <input type="hidden" name="parent_as_admin" id="vi-admin-as-parent" value="{{Auth::user()->id}}">
                    @endif
                @else
                    <input type="hidden" name="parent_as_admin" id="vi-admin-as-parent" value="{{Auth::user()->id}}">
                @endif
            @endif
            @if(Auth::user()->role_id == 10)
                <input type="hidden" name="parent_as_admin" id="vi-admin-as-parent" value="{{$parent_wa_id ? $parent_wa_id->user_id : 1}}">
            @endif
        </div>

        <!-- Personal Details-->
        <div class="box-container">
            <h4>Personal Information</h4>
            <hr>
            <div class="row">

                <div class="col-lg-12 col-md-12 col-sm-12 col-12">

                    <div class="avatar-upload">
                        <div class="avatar-edit">
                            <input type="file" id="imageUpload" name="image" accept=".png, .jpg, .jpeg">
                            <span class="text-danger small error"></span>

                            <label class="" for="imageUpload"></label>
                        </div>
                        <div class="avatar-preview">
                            <div id="imagePreview"
                                 style="background-image: url({{asset('admin/assets/images/img.png')}});">
                            </div>
                        </div>
                    </div>

                </div> <!-- end col -->

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-12">
                    <div class="row pad-z">
                        <div class="col-sm-6 col-md-6 col-xs-12 col-12 form-group">
                            <label for="first_name" class="">First Name</label>
                            <input required type="text" class="form-control validate-field" id="first_name" data-id="#first_name" data-formfield="first_name" name="first_name" type="text"
                                   autocomplete="off" value="{{old('first_name')}}">
                            <span class="text-danger small error"></span>
                        </div>

                        <div class="col-sm-6 col-md-6 col-xs-12 col-12 form-group">
                            <label for="last_name" class="">Last Name</label>
                            <input  type="text" class="form-control" id="last_name" data-id="#last_name" data-formfield="last_name" name="last_name" type="text"
                                    autocomplete="off" value="{{old('last_name')}}">
                            <span class="text-danger small error"></span>
                        </div>
                    </div>
                    <div class="row pad-z">
                        <div class="col-sm-6 col-md-6 col-xs-12 col-12 form-group user-gender">
                            <label for="" class=" ">Gender</label>
                            <label class="radio-inline">
                                <input class="radio" type="radio" name="gender" checked value="male">Male
                            </label>
                            <label class="radio-inline">
                                <input class="radio" type="radio" name="gender" value="female">Female
                                <span class="text-danger small error" style="display: block;"></span>
                            </label>
                        </div>
                        <div class="col-sm-6 col-md-6 col-xs-12 col-12 form-group">
                            <label for="date_of_birth" class="">DOB</label>
                            <input required type="date" class="form-control validate-field" id="date_of_birth" data-id="#date_of_birth" data-formfield="date_of_birth" name="date_of_birth"
                                   value="{{old('date_of_birth')}}">
                            <span class="text-danger small error"></span>


                        </div>
                    </div>

                    <div class="row pad-z">
                        <div class="col-sm-6 col-md-6 col-xs-12 col-12 form-group">
                            <label for="mobile" class="">Mobile Number</label>
                            <input required type="text" class="form-control validate-field" id="mobile" data-id="#mobile" data-formfield="mobile" name="mobile"
                                   value="{{old('mobile')}}">
                            <span class="text-danger small error"></span>
                        </div>
                        <div class="col-sm-6 col-md-6 col-xs-12 col-12 form-group">
                            <label for="email" class="">Email Id</label>
                            <input required type="email" class="form-control validate-field" id="email" data-id="#email" data-formfield="email" name="email"
                                   value="{{old('email')}}" autocomplete="off">
                            <span class="text-danger small error"></span>
                        </div>
                    </div>

                    <div class="row pad-z">
                        <div class="col-sm-6 col-md-6 col-xs-12 col-12 form-group">
                            <label for="password" class="">Password</label>
                            <input required type="password" class="form-control validate-field" id="password" data-id="#password" data-formfield="password" name="password" autocomplete="off">
                            <span class="text-danger small error"></span>
                        <!-- <div class="input-group-append">
                                        {{--<button class="btn btn-primary btn-password-view" type="submit">Generate Password</button>--}}
                                </div> -->
                        </div>

                        <div class="col-sm-6 col-md-6 col-xs-12 col-12 form-group">
                            <label for="password_confirmation" class="">Confirm Password</label>
                            <input required type="password" class="form-control validate-field" id="password_confirmation" data-id="#password_confirmation" data-formfield="password_confirmation" name="password_confirmation" >
                            <span class="text-danger small error"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Personal Details-->

        <div class="box-container">
            <h4>Contact Information</h4>
            <hr>
            <div class="row pad-z">
                <div class="col-sm-6 col-md-6 col-xs-12 col-12 form-group">
                    {{--<div class="form-group bmd-form-group bmd-form-address">--}}
                    {{--<textarea required id="YourAddress" name="address" class="form-control">{{old('address')}}</textarea>--}}
                    {{--<span class="text-danger small error"></span>--}}
                    {{--<label for="YourAddress" class="bmd-label-floating">Address</label>--}}
                    {{----}}

                    {{--</div>--}}

                    <label for="address1" class="">Address line 1</label>
                    <input  type="text" class="form-control validate-field" id="address1" data-id="#address1" data-formfield="address1" name="address1" type="text" autocomplete="off" value="{{old('address1')}}">
                    <span class="text-danger small error"></span>
                </div>
                <div class="col-sm-6 col-md-6 col-xs-12 col-12 form-group">
                    <label for="address2" class="">Address line 2</label>
                    <input  type="text" class="form-control validate-field" id="address2" data-id="#address2" data-formfield="address2" name="address2" type="text" autocomplete="off" value="{{old('address2')}}">
                    <span class="text-danger small error"></span>
                </div>

                <div class="col-sm-6 col-md-6 col-xs-12 col-12 form-group">
                    <label for="" class="">Country</label>
                    <select required class="form-control" name="country">
                        <option value="India">India</option>

                    </select>
                    <span class="text-danger small error"></span>

                </div>

                <div class="col-sm-6 col-md-6 col-xs-12 col-12 form-group">
                    <label for="" class="">State</label>
                    {!!Form::select('district_state',$states,'',(['class'=>'validate-field form-control
                    br-0 validate-field','id'=>'address_new_state','data-id'=>'#address_new_state'
                    ,'data-formfield'=>'state', 'placeholder' => 'Please Choose'])) !!}

                    {{--<select class="form-control" name="district_state">--}}
                    {{--<option>Kerala</option>--}}
                    {{--<option>Tamilnadu</option>--}}
                    {{--<option>Karnataka</option>--}}

                    {{--</select>--}}
                    <span class="text-danger small error"></span>
                </div>
                <div class="col-sm-6 col-md-6 col-xs-12 col-12 form-group">
                    <label for="pincode" class="">Pin Code</label>
                    <input required type="text" class="form-control validate-field" id="pincode" data-id="#pincode" data-formfield="pincode" name="pincode" value="{{old('pincode')}}"
                    >
                    <span class="text-danger small error"></span>

                </div>
                <div class="col-sm-6 col-md-6 col-xs-12 col-12 form-group">
                    <label for="location" class="">Location</label>
                    <input required type="text" class="form-control validate-field" id="location" data-id="#location" data-formfield="location" name="location" value="{{old('location')}}">
                    <span class="text-danger small error"></span>
                    <input class="form-control" id="latitude" name="latitude" type="hidden"
                           placeholder="Enter latitude" autocomplete="off">
                    <input class="form-control" id="longitude" name="longitude" type="hidden"
                           placeholder="Enter longitude" autocomplete="off">

                </div>
            </div>
        </div> <!-- end col -->
    @if ($role_id == 1 || $role_id == 10)
        <!-- WA USer-->
            <div class="box-container">
                <h4>Identification Details</h4>
                <hr>
                <div class="row pad-z">
                    <div class="col-sm-6 col-md-6 col-xs-12 col-12 form-group">
                        <label for="aadhar_card_no" class="">Aadhar Number</label>
                        <input required type="text" class="form-control validate-field" name="aadhar_card_no" id="aadhar_card_no" data-id="#aadhar_card_no" data-formfield="aadhar_card_no"  value="{{old('aadhar_card_no')}}">
                        <span class="text-danger small error"></span>

                    </div>

                    <div class="col-sm-6 col-md-6 col-xs-12 col-12 form-group">
                        <label for="pan_card_no" class="">PAN Card Number</label>
                        <input required type="text" class="form-control validate-field" name="pan_card_no" id="pan_card_no" data-id="#pan_card_no" data-formfield="pan_card_no"  value="{{old('pan_card_no')}}">
                        <span class="text-danger small error"></span>

                    </div>
                </div>
            </div>
            <!--WA Details -->

            <!-- WA USer-->
            <div class="box-container">
                <h4>Bank Details</h4>
                <hr>
                <div class="row pad-z">

                    <div class="col-sm-6 col-md-6 col-xs-12 col-12 form-group">
                        <label for="" class="">Bank</label>
                        {{--<select class="form-control validate-field" id="bank" data-id="#bank" data-formfield="bank" name="bank">--}}
                        {{--</select>--}}
                        {!! Form::select('bank',bankList(),'',(['class'=>'form-control validate-field','placeholder' => 'Please Choose','id' => 'bank','data-id' => '#bank','data-formfield' => 'empty','message'=>'Please choose bank name' ])) !!}

                        <span class="text-danger small error"></span>

                    </div>
                    <div class="col-sm-6 col-md-6 col-xs-12 col-12 form-group">
                        <label for="branch_code" class="">Branch Code</label>
                        <input type="text" class="form-control validate-field" id="branch_code" data-id="#branch_code"  name="branch_code" value="{{old('branch_code')}}">
                        <span class="text-danger small error"></span>
                    </div>
                    <div class="col-sm-6 col-md-6 col-xs-12 col-12 form-group">
                        <label for="account_number" class="">Account Number</label>
                        <input required type="text" class="form-control validate-field" id="account_number" data-id="#account_number" data-formfield = 'empty' message='Please enter account number' name="account_number" value="{{old('account_number')}}">
                        <span class="text-danger small error"></span>
                    </div>

                    <div class="col-sm-6 col-md-6 col-xs-12 col-12 form-group">
                        <label for="branch" class="">Branch</label>
                        <input required type="text" class="form-control validate-field" id="branch" data-id="#branch" data-formfield = 'empty' message='Please enter branch name' name="branch" value="{{old('branch')}}">
                        <span class="text-danger small error"></span>
                    </div>

                    <div class="col-sm-6 col-md-6 col-xs-12 col-12 form-group">
                        <label for="ifsc">IFSC Code</label>
                        <input required type="text" class="form-control validate-field" id="ifsc" data-id="#ifsc" data-formfield = 'empty' message='Please enter ifsc code' name="ifsc" value="{{old('ifsc')}}">
                        <span class="text-danger small error"></span>
                    </div>
                    <div class="col-sm-6 col-md-6 col-xs-12 col-12 form-group">
                        <label for="name_of_banker">Name of banker</label>
                        <input required type="text" class="form-control validate-field" id="name_of_banker" data-id="#name_of_banker" data-formfield = 'empty' message='Please enter name of banker' name="name_of_banker" value="{{old('name_of_banker')}}">
                        <span class="text-danger small error"></span>
                    </div>

                </div>
            </div>
            <!--WA Details -->


            <!-- Permissions USer-->
            <div class="box-container">
                <h4>Advanced Options</h4>
                <hr>
                <div class="row pad-z">
                    @if ($role_id == 1)
                        <div class="col-sm-12 col-md-12 col-xs-12 col-12 form-group">
                            @if(is_array($permissions) && sizeof($permissions)>0)
                                <h4>Permissions</h4>
                                <hr>
                                @php $result = ['billing'=>'Billing','wa_add'=>'Add WA','staff_add'=>'Add Staff','customer_wa_child'=>'Add Customer as WA child'];@endphp

                                @foreach($permissions as $key => $data)
                                    @if($data == 1)
                                        {{--<label for="">{{ucwords(str_replace("_"," ",$key))}}</label>--}}
                                        <div class="form-check checkbox-custom form-check-inline">

                                            <input id="{{$key}}" type="checkbox" name="permissions[]" @if($key == 'billing') checked @endif value="{{$key}}">
                                            <label for="{{$key}}">{{$result[$key]}}</label>
                                        </div>
                                    @endif
                                @endforeach
                            @endif
                        </div>
                    @endif
                    <div class="col-sm-12 col-md-12 col-xs-12 col-12 form-group">
                        @if(is_array($payment_modes) && sizeof($payment_modes)>0)
                            <h4>Payment mode</h4>
                            <hr>
                            @foreach($payment_modes as $key => $data)
                                @if($data == 1)

                                    <div class="form-check checkbox-custom form-check-inline">
                                        <input id="{{$key}}" type="checkbox" name="payment_modes[]" @if($key == 'online') checked @endif value="{{$key}}">
                                        <label for="{{$key}}">
                                            {{ucwords(str_replace("_"," ",$key))}}
                                        </label>
                                    </div>
                                @endif
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>

            <!--Permissions Details -->
    </div>
    @endif


</div>

</div>