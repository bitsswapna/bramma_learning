<div class="row">
    <div class="col-lg-12">

        <!-- Personal Details-->
        <div class="box-container">
            <h4>Personal Information</h4>
            <hr>
            <div class="row">

               

                <div class="col-lg-10 col-md-8 col-sm-8 col-xs-8">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12 m-b-20">
                            <div class="form-group bmd-form-group">
                                <input required type="text" class="form-control validate-field" id="first_name" data-id="#first_name" data-formfield="first_name" name="first_name" type="text"
                                       autocomplete="off" value="{{old('first_name')}}">
                                <span class="text-danger small error"></span>

                                <label for="FirstName" class="bmd-label-floating">First Name</label>
                            </div>
                        </div>

                        <div class="col-sm-6 col-xs-12 m-b-20">
                            <div class="form-group bmd-form-group">
                                <input type="text" class="form-control" id="last_name" data-id="#last_name" data-formfield="last_name" name="last_name" type="text"
                                       autocomplete="off" value="{{old('last_name')}}">
                                <span class="text-danger small error"></span>
                                <label for="LastName" class="bmd-label-floating">Last Name</label>

                            </div>
                        </div>
                    </div>
                   
                        

                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <div class="form-group bmd-form-group">
                                <input required type="number" class="form-control validate-field" id="mobile" data-id="#mobile" data-formfield="mobile" name="mobile"
                                       value="{{old('mobile')}}">
                                <span class="text-danger small error"></span>
                                <label for="MobileNumber" class="bmd-label-floating">Mobile Number</label>
                            </div>
                        </div>

                        <div class="row">
                <div class="col-sm-12 col-xs-12">
                    <div class="form-group bmd-form-group bmd-form-address">
                        <textarea required name="address" class="form-control">{{old('address')}}</textarea>
                        <label for="YourAddress" class="bmd-label-floating">Address</label>
                        <span class="text-danger small error"></span>

                    </div>
                </div>
                      
        <!-- END Personal Details-->



    </div>

</div>


