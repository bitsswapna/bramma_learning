@extends('admin.layouts.app') @section('css') @endsection @section('content')
<main class="app-content bg-white">   
    <!-- Sub Title---->
    <div id="submenu_subheader" class="submenu-subheader submenu-grid__item ">          
        <div class="submenu-subheader__title">
            <h4 class="title-caption"><i class="fas fa-boxes"></i>  Modules</h4>
        </div>           
        <div class="submenu-subheader__toolbar">
            <div class="submenu-subheader__toolbar-wrapper">
                {{--@if(can('add_module'))--}}
               <!-- <a class="btn btn-primary" href="{{url('module/create')}}" role="button"><i class="fa fa-plus-circle"></i>Create Module</a> -->
                {{--@endif--}}
            </div>
        </div>
    </div>
    <!-- END Sub Title---->  
    <!-- Body -->
    <div class="sub-header-body">
        @if(can('browse_module'))
            <div class="table-responsive">
                <table id="sampleTable" class="table table-bordered" >
                    <thead>
                        <tr>
                            <th>Module Name</th>
                            <th>Prefix</th>
                            <th>Created</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($modules as $module)
                        <tr>
                            <td>{{$module->name}}</td>
                            <td>{{$module->perifix}}</td>
                            <td>{{date('d M Y h:i A',strtotime($module->created_at))}}</td>
                            {{-- <td>
                                {{--  @if(can('edit_module'))--}}
<!--                                    <a href="{{--url('module/edit/'.$module->id)--}}" class="btn action-button bg-primary-blue" data-toggle="tooltip" data-placement="top" data-html="true" title="Edit Module">-->
<!--                                        <i class="fa fa-pencil-square-o "></i>-->
<!--                                    </a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-->
                                {{--@endif--
                            </td> --}}
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @else @include('admin.no-access-content') @endif
    </div>
    <!-- END Body -->
</main>


@endsection @section('js')
<script type="text/javascript" src="{{asset('admin/js/plugins/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/plugins/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript">
    $('#sampleTable').DataTable({
        bPaginate: false,
        responsive:true,
        bSort: false,
        bFilter: false,
        bInfo: false
    });
</script>
@endsection