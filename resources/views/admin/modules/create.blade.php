@extends('admin.layouts.app')

@section('css')

@endsection

@section('content')
    <main class="app-content">
       <!-- <div class="app-title">
            <div class="col-md-6 rem-pad">
                <h1><i class="fa fa-plus-circle"></i> Create Module</h1>
            </div>
            <div class="col-md-6 btn-right rem-pad">
                @if(can('browse_module'))
                <a class="btn btn-success waves-effect waves-light" href="{{url('module')}}"><i class="fa fa-list"></i>Module Lists</a>
                @endif
            </div>
        </div> -->

         <div class="thumbnail-card">
        <div class="inliner">
            <h5 class="title-caption inliner"><i class="fa fa-plus-circle"></i> Create Module</h5>
            
           
            @if(can('browse_module'))
                <a data-toggle="tooltip" data-placement="top" data-html="true" title="Module List" class="btn bg-primary-green btn-sm waves-effect waves-light inliner btn-right mt--6 br-0" href="{{url('module')}}"><i class="fa fa-list"></i>Module Lists</a>
            @endif

        </div> 
    </div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="tile">
                    <div class="tile-body br-0">
                        <form method="post" action="{{url('module/create')}}" id="createModule" name="createModule">
                            <div class="row">
                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                    {{csrf_field()}}
                                    <div class="form-group @if($errors->first('name')) has-danger @endif">
                                        <label for="name">Module name</label>
                                        <input class="form-control br-0 @if($errors->first('name')) is-invalid @endif" id="name" name="name" type="text" placeholder="Enter module name" required autocomplete="off" value="{{old('name')}}">
                                        @if($errors->first('name'))
                                            <sapn class="form-control-feedback">{{$errors->first('name')}}</sapn>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group @if($errors->first('perifix')) has-danger @endif">
                                        <label for="perifix">Perifix</label>
                                        <input class="form-control br-0 @if($errors->first('perifix')) is-invalid @endif" id="perifix" type="text" name="perifix" placeholder="Enter module perifix" required autocomplete="off" value="{{old('perifix')}}">
                                        @if($errors->first('perifix'))
                                            <sapn class="form-control-feedback">{{$errors->first('perifix')}}</sapn>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="tile-footer text-right">
                                <button class="btn btn-sm btn-primary waves-effect waves-light br-0" type="submit">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection

@section('js')

    <!--Notifications Message Section-->
    @include('admin.layouts.notifications')

    <script>
        $("#createModule").validate({
            rules: {
                // simple rule, converted to {required:true}
                perifix: "required",
                name: "required",
            }
        });
    </script>
@endsection