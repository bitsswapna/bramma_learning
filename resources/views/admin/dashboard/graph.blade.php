<div class="tab-content">
                    <div class="row">


    <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12"> 
        <div class="card-box widget-box-two widget-two-warning light-red-ml">
            <!-- <i class="mdi mdi-layers widget-two-icon"></i> -->
            <div class="wigdet-two-content">
                <p class="m-0 text-uppercase font-600 font-primary text-overflow" title="User This Month">Total Vehicles</p>
                <h2><strong><span data-plugin="counterup" class="count" id="infoc-totaluser">309</span></strong></h2>
                <p class="m-0"><b>Partnership Vehicle : </b><span class="count" id="infoc-totalcustomes">144</span></p>
            </div>
        </div>
    </div><!-- end col --> 

   

    <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
        <div class="card-box widget-box-two widget-two-primary light-green-ml">
            <!-- <i class="mdi mdi-account widget-two-icon"></i> -->

            <div class="wigdet-two-content">
                <p class="m-0 text-uppercase font-600 font-primary text-overflow" title="Statistics">Total Sitework</p>  
                <h2><strong><span class="count" data-plugin="counterup" id="infoc-wacount">164</span></strong></h2>
                <p class="m-0"><b> Activity Site :</b><span class="count" id="infoc-wainactivecount">5</span></p>
            </div>
        </div>
    </div><!-- end col -->

    <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
        <div class="card-box widget-box-two widget-two-danger light-blue-ml">
            <!-- <i class="mdi mdi-access-point-network widget-two-icon"></i> -->
            <div class="wigdet-two-content">
                <p class="m-0 text-uppercase font-600 font-pr text-overflow" title="Statistics">Total Sale </p>
                <h2><strong><span data-plugin="counterup" id="infoc-overallpv">4714342.00</span></strong></h2>
                <p class="m-0"><b>Total Income :</b><span class="count" id="infoc-overallbv">142</span></p>
                
            </div>
        </div>
    </div><!-- end col -->

<div class="tab-content">

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="quick-menus">
        <div class="card-box br-0 text-black">
            <h5 class="header-title m-t-0 font-primary font-600">Quick Menu</h5>
            <div class="separator--border-dashed m-b-20"></div>
            <div class="card-body m-t30 m-b-60">
                <div class="row">
                    @if(can('browse_admin_user'))
                        <div class="col-xs-6 col-sm-4 col-md-2 m-t-10">
                            <a href="{{url('sites')}}">
                                <div class="card-tile text-center">
                                    <i class="fas fa-tachometer-alt"></i>
                                </div>
                            </a>
                            <p class="small text-center">Site</p>
                        </div>
                    @endif
                        @if(can('browse_admin_user'))
                        <div class="col-xs-6 col-sm-4 col-md-2 m-t-10">
                            <a href="{{url('admin_users')}}">
                                <div class="card-tile text-center">
                                    <i class="fa fa-user"></i>
                                </div>
                            </a>
                            <p class="small text-center">User Management</p>
                        </div>
                        @endif
                        @if(can('browse_tree'))
                        <div class="col-xs-6 col-sm-4 col-md-2 m-t-10">
                            <a href="{{url('tree')}}">
                                <div class="card-tile text-center">
                                    <i class="fa fa-tree"></i>
                                </div>
                            </a>
                            <p class="small text-center">Settings</p>
                        </div>
                        @endif
                        <div class="col-xs-6 col-sm-4 col-md-2 m-t-10">
                            <a href="{{url('admin-customers')}}">
                                <div class="card-tile text-center">
                                    <i class="fa fa-file"></i>
                                </div>
                            </a>
                            <p class="small text-center">Customer</p>
                        </div>

                    <div class="col-xs-6 col-sm-4 col-md-2 m-t-10">
                        <a href="{{url('amount-details')}}">
                            <div class="card-tile text-center">
                                <i class="fa fa-rupee"></i>
                            </div>
                        </a>
                        <p class="small text-center">Amount</p>
                    </div>

                    <div class="col-xs-6 col-sm-4 col-md-2 m-t-10">
                        <a href="{{url('Vehicles')}}">
                            <div class="card-tile text-center">
                                <i class="fa fa-bus"></i>
                            </div>
                        </a>
                        <p class="small text-center">Vehicle</p>
                    </div>
                </div>
            </div>
            <!-- 
            <div class="pull-right m-b-30">
                <div id="reportrange" class="form-control">
                    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                    <span></span>
                </div>
            </div>
            <div class="clearfix"></div>

            <div id="donut-chart">
                <div id="donut-chart-container" class="flot-chart" style="height: 240px; padding: 0px; position: relative;">
                    <canvas class="flot-base" width="787" height="240" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 787px; height: 240px;"></canvas><canvas class="flot-overlay" width="787" height="240" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 787px; height: 240px;"></canvas><div class="legend"><div style="position: absolute; width: 79px; height: 80px; top: 50px; right: 50px; background-color: rgb(255, 255, 255); opacity: 0.85;"> </div><table style="position:absolute;top:50px;right:50px;;font-size:smaller;color:#545454"><tbody><tr><td class="legendColorBox"><div style="border:1px solid null;padding:1px"><div style="width:4px;height:0;border:5px solid rgb(255,152,0);overflow:hidden"></div></div></td><td class="legendLabel"><div style="font-size:14px;">&nbsp;Series 1</div></td></tr><tr><td class="legendColorBox"><div style="border:1px solid null;padding:1px"><div style="width:4px;height:0;border:5px solid rgb(141,110,99);overflow:hidden"></div></div></td><td class="legendLabel"><div style="font-size:14px;">&nbsp;Series 2</div></td></tr><tr><td class="legendColorBox"><div style="border:1px solid null;padding:1px"><div style="width:4px;height:0;border:5px solid rgb(38,166,154);overflow:hidden"></div></div></td><td class="legendLabel"><div style="font-size:14px;">&nbsp;Series 3</div></td></tr><tr><td class="legendColorBox"><div style="border:1px solid null;padding:1px"><div style="width:4px;height:0;border:5px solid rgb(127,193,252);overflow:hidden"></div></div></td><td class="legendLabel"><div style="font-size:14px;">&nbsp;Series 4</div></td></tr></tbody></table></div></div>
            </div>

            <p class="text-muted m-b-0 m-t-15 font-13 text-overflow">Pie chart is used to see the proprotion of each data groups, making Flot pie chart is pretty simple, in order to make pie chart you have to incldue jquery.flot.pie.js plugin.</p> -->
        </div>
    </div>




</div>


<div class="card-box br-0">
    <h5 class="m-b-20 header-title m-t-0 font-primary font-600">Glance</h5>
    <div class="separator--border-dashed m-b-20"></div>
    <div class="row">
        <div class="col-xs-12 col-sm-3 ">
            <div class="wrapper-tab">
                <a href="#">
                    <button class="disabled text-right btn btn-years ">2020</button>
                </a>
                <ul class="nav nav-tabs tabs-left scroll-flex">
                    <li class="active"><a data-toggle="tab" class="sales-tab" data-id="1" data-content="february">January</a></li>
                    <li><a data-toggle="tab" class="sales-tab" data-id="2" data-content="february">February</a></li>
                    <li><a data-toggle="tab" class="sales-tab" data-id="3" data-content="march">March</a></li>
                    <li><a data-toggle="tab" class="sales-tab" data-id="4" data-content="april">April</a></li>
                    <li><a data-toggle="tab" class="sales-tab" data-id="5" data-content="may">May</a></li>
                    <li><a data-toggle="tab" class="sales-tab" data-id="6" data-content="june">June</a></li>
                    <li><a data-toggle="tab" class="sales-tab" data-id="7" data-content="july">July</a></li>
                    <li><a data-toggle="tab" class="sales-tab" data-id="8" data-content="august">August</a></li>
                    <li><a data-toggle="tab" class="sales-tab" data-id="9" data-content="september">September</a></li>
                    <li><a data-toggle="tab" class="sales-tab" data-id="10" data-content="october">October</a></li>
                    <li><a data-toggle="tab" class="sales-tab" data-id="11" data-content="november">November</a></li>
                    <li><a data-toggle="tab" class="sales-tab" data-id="1" data-content="december">December</a></li>
                </ul>
            </div>
        </div>
        <div class="col-xs-12 col-sm-9">
            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active" id="ClientInfo">
                    <div class="card-body text-black m-t30 m-b-60">
                        <div class="d-overall-sale-report">

                            <div class="row flex-content">
                                <div class="col-xs-6 col-sm-4 col-md-3 m-b-15">
                                    <div class="card-outline tab-font light-skyblue-ml bx-flex">

                                        <div class="card-tile text-center">

                                            <span class="sales-count"></span>
                                        </div>

                                        <p class="small text-center">Total Site</p>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4 col-md-3 m-b-15">
                                    <div class="card-outline tab-font light-skyblue-ml bx-flex">

                                        <div class="card-tile text-center">

                                            <span class="sales-total-price"></span>
                                        </div>

                                        <p class="small text-center">Total Expense</p>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4 col-md-3 m-b-15">
                                    <div class="card-outline tab-font light-skyblue-ml bx-flex">

                                        <div class="card-tile text-center">

                                            <span class="sales-total-pv"></span>
                                        </div>

                                        <p class="small text-center">Total Income </p>
                                    </div>
                                </div>
                               
                             
                           
                           
                             

                        </div>

                    </div>
                    {{-- <div class="text-black light-blue-ml">

                        <ul class="d-overall-sale-report">
                            <li>Sales Count <span class="sales-count"></span> </li>
                            <li>Sales Total Price <span class="sales-total-price"></span></li>
                            <li>Sales Total PV <span class="sales-total-pv"></span></li>
                            <li>WA Sales <span class="sales-wa"></span></li>
                            <li>Customer Sale <span class="sales-customer"></span></li>
                            <li>WA Join <span class="wa-join"></span></li>
                            <li>Customer Count <span class="customer-count"></span></li>
                            <li>Staff Count<span class="staff-count"></span></li>
                        </ul>

                    </div> --}}
                </div>
            </div>
        </div>

    </div>
</div>

<div class="row">

    <!-- end col -->
    
<!-- end col -->
    
</div>
<!-- end row -->    
        <!-- Footer -->
<footer class="footer text-right">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                © 2018 - 2020 Vieroots.
            </div>
        </div>
    </div>
</footer>
<!-- End Footer -->    </div>