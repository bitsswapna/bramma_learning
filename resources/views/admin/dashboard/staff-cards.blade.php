<div class="row">


    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="card-box widget-box-two widget-two-warning light-green-ml">
            <!-- <i class="mdi mdi-layers widget-two-icon"></i> -->
            <a href="{{url('dashboard/staff-customers')}}">
            <div class="wigdet-two-content">
                <p class="m-0 text-uppercase font-600 font-primary text-overflow" title="User This Month">Total Customer</p>
                <h2><strong><span data-plugin="counterup" class="count" id="infoc-totaluser">{{$countStaffCustomer}}</span></strong></h2>
                <!-- <p class="m-0"><b>Customers:</b><span  class="count" id="infoc-totalcustomes">{{--$countStaffCustomer---}}</span></p> -->
            </div>
            </a>
        </div>
    </div><!-- end col -->

    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="card-box widget-box-two widget-two-primary light-orange-ml">
            <!-- <i class="mdi mdi-account widget-two-icon"></i> -->
            <a href="{{url('billing')}}">
            <div class="wigdet-two-content">
                <p class="m-0 text-uppercase font-600 font-primary text-overflow" title="Statistics">Total Billing</p>
                <h2><strong><span class="count" data-plugin="counterup" id="infoc-wacount">{{$customerBilling}}</span></strong></h2>
                <!-- <p class="m-0"><b>Inactive:</b><span class="count" id="infoc-wainactivecount">{{--$customerBilling--}}</span></p> -->
            </div>
            </a>
        </div>
    </div><!-- end col -->

    <!-- <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
        <div class="card-box widget-box-two widget-two-danger light-blue-ml">
            <!-- <i class="mdi mdi-access-point-network widget-two-icon"></i> -->
            <!-- <div class="wigdet-two-content">
                <p class="m-0 text-uppercase font-600 font-pr text-overflow" title="Statistics">Overall Personal Volume</p>
                <h2><strong><span data-plugin="counterup" id="infoc-overallpv">0</span></strong></h2>
                <!-- <p class="m-0"><b>Personal Volume :</b><span class="count" id="infoc-overallbv">0</span></p> -->
            <!-- </div>
        </div>
    </div> 
     -->
    <!-- end col -->

    {{--<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">--}}
        {{--<div class="card-box widget-box-two widget-two-success light-red-ml">--}}
            {{--<!-- <i class="mdi mdi-account-convert widget-two-icon"></i> -->--}}
            {{--<div class="wigdet-two-content">--}}
                {{--<p class="m-0 text-uppercase font-600 font-primary text-overflow" title="User Today">User Today</p>--}}
                {{--<h2><span data-plugin="counterup" class="count">895</span></h2>--}}
                {{--<p class="m-0 count"><b>Last:</b> 1250</p>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    <!-- end col -->

</div>