@extends('admin.layouts.app')
@section('css')
    <link href="{{asset('css/plugin/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/plugin/selectize.bootstrap2.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('admin/assets/plugins/jquery-datatables-editable/datatables.css')}}" rel="stylesheet" type="text/css"/>

    <style>
        .rounded-circle{
            border-radius: 50%;
        }
        .swal-title {
            font-size: 18px!important;
        }

    </style>
@endsection
@section('content')
        @include('admin.dashboard.graph')
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.1/socket.io.js"></script>
    <script src="{{asset('admin/assets/js/custom/dashboard.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/assets/js/custom/dashboard-overall-sale-report.js')}}"></script>

    <script>
        // var socket = io.connect('https://viegenomics.com:3000');
        // socket.on('pv', function (data) {
        //
        //     document.getElementById("infoc-overallbv").innerHTML = data.pv;
        //     document.getElementById("infoc-overallpv").innerHTML = data.pgv;
        //     counter();
        //     $.notify("PGV Updated" + data.pgv, {
        //         animate: {
        //             enter: 'animated flipInY',
        //             exit: 'animated flipOutX'
        //         }
        //     })
        // });
    </script>

    {{--Graph Scripts--}}
    <script src="{{asset('admin/assets/plugins/flot-chart/jquery.flot.min.js')}}"></script>
    <script src="{{asset('admin/assets/plugins/flot-chart/jquery.flot.time.js')}}"></script>
    <script src="{{asset('admin/assets/plugins/flot-chart/jquery.flot.tooltip.min.js')}}"></script>
    <script src="{{asset('admin/assets/plugins/flot-chart/jquery.flot.resize.js')}}"></script>
    <script src="{{asset('admin/assets/plugins/flot-chart/jquery.flot.pie.js')}}"></script>
    <script src="{{asset('admin/assets/plugins/flot-chart/jquery.flot.selection.js')}}"></script>
    <script src="{{asset('admin/assets/plugins/flot-chart/jquery.flot.crosshair.js')}}"></script>
    <script src="{{asset('admin/js/main/dashboard.js')}}"></script>


    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0/dist/Chart.min.js" integrity="sha256-Uv9BNBucvCPipKQ2NS9wYpJmi8DTOEfTA/nH2aoJALw=" crossorigin="anonymous"></script>
    <script src="{{asset('admin/assets/plugins/chart-plugin/morris.min.js')}}"></script>
    <script src="{{asset('admin/assets/plugins/chart-plugin/raphael-min.js')}}"></script>
    <!-- Vertical Tabs -->
    {{-- <script src="{{asset('admin/assets/js/custom/script-vertical-tab.js')}}"></script> --}}

    <script>
        $(document).ready(function () {

            var url = base_url + "/get-admin-graph";
            var barData =[];
            var barDataWA =[];

            $.ajax({
                url: url,
                type: 'GET',

                beforeSend: function () {
                    $("#graph-report").html("loading");

                },
                success: function (result) {
                    barData = result.barData;
                    barDataWA = result.barDataWA;

                    !function($) {
                        "use strict";

                        var Dashboard1 = function() {
                            this.$realData = []
                        };

                        //creates Bar chart
                        Dashboard1.prototype.createBarChart  = function(element, data, xkey, ykeys, labels, lineColors) {

                        var y=data.map(function (value) {
                            return value.y;
                        });
                        var x=data.map(function (value) {
                            return value.a;
                        })
                        var color=new Array(data.length).fill(lineColors);

                            var ctx = document.getElementById('myChart').getContext('2d');
                            var myChart = new Chart(ctx, {
                                type: 'bar',
                                data: {
                                    labels: y,
                                    datasets: [{
                                        label: labels,
                                        data: x,
                                        backgroundColor:color,
                                        borderColor:color
                                    }]
                                },
                                options: {

                                }
                            });



                            // Morris.Bar({
                            //     element: element,
                            //     data: data,
                            //     xkey: xkey,
                            //     ykeys: ykeys,
                            //     labels: labels,
                            //     hideHover: 'auto',
                            //     resize: true, //defaulted to true
                            //     gridLineColor: '#eeeeee',
                            //     barSizeRatio: 0.2,
                            //     barColors: lineColors,
                            //     postUnits: 'k'
                            // });
                        },

                            Dashboard1.prototype.init = function() {

                                //creating bar chart
                                // barDataWA=barDataWA.filter(function (value) {
                                //     return value.a!=0;
                                // })
                               // this.createBarChart('morris-pgv-monthly', barData, 'y', ['a'], ['BV'], ['#3bafda']);
                                this.createBarChart('morris-bestwa-month', barDataWA, 'y', ['a'], ['Total price'], ['#3bafda']);
                            },
                            //init
                            $.Dashboard1 = new Dashboard1, $.Dashboard1.Constructor = Dashboard1
                    }(window.jQuery),

                        //initializing
                        function($) {
                            "use strict";
                            $.Dashboard1.init();
                        }(window.jQuery);
                }
            }).complete(function () {



            });
        });

    </script>

    <!-- Accordion Script -->
    <script>


    </script>
    <!-- Accordion Script End -->


    
    <script type="text/javascript" src="{{asset('admin/js/plugins/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/assets/js/sweetalert.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/selectize.min.js')}}"></script>

    <script type="text/javascript" src="{{asset('admin/assets/js/custom/admin_payout.js')}}"></script>
@endsection
