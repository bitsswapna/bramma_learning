<?php
return ['cards' =>
    array(
        array(
            'Name' => 'Dashboard',
            'slug' => '/',
            'icon-class' => 'fa fa-dashboard',
            'permission' => '',
            'child' => ''),
        array(
            'Name' => 'Roles & permission',
            'slug' => 'roles',
            'icon-class' => 'fa fa-lock',
            'permission' => 'roles',
            'child' => ''),

    )];