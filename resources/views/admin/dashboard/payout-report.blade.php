<div class="card-box widget-box-two br-0 text-black">
    <h5 class="m-b-20 header-title m-t-0 font-primary font-600">Payout Summary</h5>
    <div class="separator--border-dashed m-b-20"></div>
    <table id="admin-payout-dashbord-report" class="table table-bordered" style="color:black">
        <thead class="bg-light">
            <tr>
                <th>Payout Date</th>
                <th>User ID</th>
                <th>Name</th>
                <th>PSI</th>
                <th>FSI</th>
                <th>GSI</th>
                <th>UCR</th>
                <th>GCR</th>
                <th>WAmbR</th>
                <th>Total</th>
                <th>TDS</th>
                <th>Net Pay</th>
                <th>UTR Number</th>
            </tr>
        </thead>
    </table>
</div>
