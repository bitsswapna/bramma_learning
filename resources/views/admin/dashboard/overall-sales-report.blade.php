<div class="card-box br-0">
    <h5 class="m-b-20 header-title m-t-0 font-primary font-600">Glance</h5>
    <div class="separator--border-dashed m-b-20"></div>
    <div class="row">
        <div class="col-xs-12 col-sm-3 ">
            <div class="wrapper-tab">
                <a href="#">
                    <button class="disabled text-right btn btn-years ">2019</button>
                </a>
                <ul class="nav nav-tabs tabs-left scroll-flex">
                    <li><a data-toggle="tab" class="sales-tab" data-id="1" data-content="february">January</a></li>
                    <li><a data-toggle="tab" class="sales-tab" data-id="2" data-content="february">February</a></li>
                    <li><a data-toggle="tab" class="sales-tab" data-id="3" data-content="march">March</a></li>
                    <li><a data-toggle="tab" class="sales-tab" data-id="4" data-content="april">April</a></li>
                    <li class="active"><a data-toggle="tab" class="sales-tab" data-id="5" data-content="may">May</a></li>
                    <li><a data-toggle="tab" class="sales-tab" data-id="6" data-content="june">June</a></li>
                    <li><a data-toggle="tab" class="sales-tab" data-id="7" data-content="july">July</a></li>
                    <li><a data-toggle="tab" class="sales-tab" data-id="8" data-content="august">August</a></li>
                    <li><a data-toggle="tab" class="sales-tab" data-id="9" data-content="september">September</a></li>
                    <li><a data-toggle="tab" class="sales-tab" data-id="10" data-content="october">October</a></li>
                    <li><a data-toggle="tab" class="sales-tab" data-id="11" data-content="november">November</a></li>
                    <li><a data-toggle="tab" class="sales-tab" data-id="1" data-content="december">December</a></li>
                </ul>
            </div>
        </div>
        <div class="col-xs-12 col-sm-9">
            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active" id="ClientInfo">
                    <div class="card-body text-black m-t30 m-b-60">
                        <div class="d-overall-sale-report">

                            <div class="row flex-content">
                                <div class="col-xs-6 col-sm-4 col-md-3 m-b-15">
                                    <div class="card-outline tab-font light-skyblue-ml bx-flex">

                                        <div class="card-tile text-center">

                                            <span class="sales-count"></span>
                                        </div>

                                        <p class="small text-center">Total Sales Count</p>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4 col-md-3 m-b-15">
                                    <div class="card-outline tab-font light-skyblue-ml bx-flex">

                                        <div class="card-tile text-center">

                                            <span class="sales-total-price"></span>
                                        </div>

                                        <p class="small text-center">Overall sales volume(Price)</p>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4 col-md-3 m-b-15">
                                    <div class="card-outline tab-font light-skyblue-ml bx-flex">

                                        <div class="card-tile text-center">

                                            <span class="sales-total-pv"></span>
                                        </div>

                                        <p class="small text-center">Overall sales volume(PV) </p>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4 col-md-3 m-b-15">
                                    <div class="card-outline tab-font light-skyblue-ml bx-flex">

                                        <div class="card-tile text-center">

                                            <span class="sales-wa"></span>
                                        </div>

                                        <p class="small text-center">WA's Purchase </p>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4 col-md-3 m-b-15">
                                    <div class="card-outline tab-font light-skyblue-ml bx-flex">

                                        <div class="card-tile text-center">

                                            <span class="sales-customer"></span>
                                        </div>

                                        <p class="small text-center">Customer's purchase </p>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4 col-md-3 m-b-15">
                                    <div class="card-outline tab-font light-skyblue-ml bx-flex">

                                        <div class="card-tile text-center">

                                            <span class="wa-join"></span>
                                        </div>

                                        <p class="small text-center">WA Registered</p>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4 col-md-3 m-b-15">
                                    <div class="card-outline tab-font light-skyblue-ml bx-flex">

                                        <div class="card-tile text-center">

                                            <span class="customer-count"></span>
                                        </div>

                                        <p class="small text-center">Customer Registered </p>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4 col-md-3 m-b-15">
                                    <div class="card-outline tab-font light-skyblue-ml bx-flex">

                                        <div class="card-tile text-center">

                                            <span class="staff-count"></span>
                                        </div>

                                        <p class="small text-center">Staff Registered </p>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4 col-md-3 m-b-15">
                                    <div class="card-outline tab-font light-skyblue-ml bx-flex">

                                        <div class="card-tile text-center">

                                            <span class="referral-link-purchase"></span>
                                        </div>

                                        <p class="small text-center">Referral Link Purchase</p>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4 col-md-3 m-b-15">
                                    <div class="card-outline tab-font light-skyblue-ml bx-flex">

                                        <div class="card-tile text-center">

                                            <span class="billing-purchase"></span>
                                        </div>

                                        <p class="small text-center">Billing count</p>
                                    </div>
                                </div>
                            </div>


                        </div>

                    </div>
                    {{-- <div class="text-black light-blue-ml">

                        <ul class="d-overall-sale-report">
                            <li>Sales Count <span class="sales-count"></span> </li>
                            <li>Sales Total Price <span class="sales-total-price"></span></li>
                            <li>Sales Total PV <span class="sales-total-pv"></span></li>
                            <li>WA Sales <span class="sales-wa"></span></li>
                            <li>Customer Sale <span class="sales-customer"></span></li>
                            <li>WA Join <span class="wa-join"></span></li>
                            <li>Customer Count <span class="customer-count"></span></li>
                            <li>Staff Count<span class="staff-count"></span></li>
                        </ul>

                    </div> --}}
                </div>
            </div>
        </div>

    </div>
</div>