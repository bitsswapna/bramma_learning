<script>
    @if(Session::has('smessage'))
        $.notify("{!! Session::get('smessage') !!}");
    @endif
    @if(Session::has('emessage'))
         $.notify("{!! Session::get('emessage') !!}");
    @endif
    @if(Session::has('wmessage'))
         $.notify("{!! Session::get('wmessage') !!}");
    @endif
    
</script>
