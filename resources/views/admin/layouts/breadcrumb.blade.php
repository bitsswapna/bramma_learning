<div class="row">
    <div class="col-sm-12 col-xs-12">
        <div class="page-title-box">
            <div class="btn-group pull-right">
                <ol class="breadcrumb hide-phone p-0 m-0">
                    <li>
                        <a href="#">{{env("APP_NAME")}}</a>
                    </li>
                    <li>
                        <a href="#">Dashboard</a>
                    </li>
                    <li class="active">
                        Dashboard 2
                    </li>
                </ol>
            </div>
            <h4 class="page-title">@if(isset($title)&&$title) $title @endif </h4>
        </div>
    </div>
</div>