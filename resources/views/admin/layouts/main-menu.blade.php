<?php $menus = \Illuminate\Support\Facades\Config::get('backend-menu.menus'); ?>


<div class="navbar-custom">
    <div class="container-fluid">
        <div id="navigation">
            <!-- Navigation Menu-->
            <ul class="navigation-menu">
                @foreach($menus as $key => $value)
                    <?php $flag = false;
                    $mainPer = false; ?>
                    @if(module_permission($value['permission']))
                        <?php $mainPer = true; ?>

                        @if(is_array($value['child']))
                            <?php $mainPer = false; ?>
                            @foreach($value['child'] as $k => $v)
                                @if(Request::path() == $v['slug'])
                                    <?php $flag = true; ?>
                                @endif
                                {{--{{dd($value['permission'])}}--}}

                                @if($v['permission'])
                                    @if(module_permission($v['permission']) && !$mainPer )
                                        <?php $mainPer = true; ?>
                                    @endif
                                @endif

                            @endforeach
                        @endif

                        @if($mainPer)
                            <li class="has-submenu">

                                <a  @if(!is_array($value['child'])) href="{!! URL::to($value['slug']) !!}" @endif
                                class="@if(Request::path() == $value['slug']) active @endif"
                                   @if(is_array($value['child'])) data-toggle="treeview" href="#" @endif>
                                    <i class="{!! $value['icon-class'] !!}"></i>{!! $value['Name'] !!}
                                    @if(is_array($value['child'])&&count($value['child'])>1)
                                        <i class="treeview-indicator fa fa-angle-down left-align-icon"></i>
                                    @endif
                                </a>
                                @if(is_array($value['child']))
                                    <ul class="submenu">
                                        @foreach($value['child'] as $k => $v)
                                            @if(module_permission($v['permission']))
                                                <li>
                                                    <a href="{!! URL::to($v['slug']) !!}"
                                                       class="@if(Request::path() == $v['slug']) active @endif">
                                                        <i class="{!! $v['icon-class'] !!}"></i>&nbsp;{!! $v["Name"] !!}
                                                    </a>
                                                </li>
                                            @endif
                                        @endforeach
                                    </ul>
                                @endif
                            </li>
                        @endif
                    @endif
                @endforeach
            </ul>
            <!-- End navigation menu
        </div> <!-- end #navigation -->
        </div> <!-- end container-fluid -->
    </div> <!-- end navbar-custom -->