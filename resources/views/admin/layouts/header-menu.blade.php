<div class="topbar-main">
    <div class="container">
        <div class="logo">
            <a href="{{url('')}}" class="logo">
                <img src="{{url('admin/assets/images/logo/new.png')}}" alt="" height="30">
                <!-- <h3>Viegenomics</h3> -->
            </a>

        </div>
        <div class="menu-extras">

            <ul class="nav navbar-nav navbar-right pull-right">

                    <li class="dropdown navbar-c-items">
                        <a href="#" class="right-menu-item dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <i class="mdi mdi-bell"></i>
                            <span class="badge up badge-success badge-pill">100</span>
                        </a>

                        <ul class="notify-menu dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right dropdown-lg user-list notify-list">
                            <li class="text-center">
                                <h5>Notifications</h5>
                            </li>


                                <li>
                                    <a class="user-list-item header_notification" data-id="nid">
                                        <div class="user-desc">

                                            <div class="media">
                                            <div class="media-left">

                                                            <img src="{{asset('admin/assets/images/logo.svg')}}"
                                                                 class="img-responsive" alt="product image">
                                                            <span>Product</span>


                                            </div>
                                            <div class="media-body">

                                                            <span>1 user has made new (website) purchase</span>

                                                            <span>{2 userhas billed
                                                            for<span>name</span>
                                                            </span>
                                            </div>
                                            </div>

                                               

                                              

                                                <span class="name">New Registration  </span>


                                            <span class="time">
{{--                                                 {{$notification->created_at->format("d-M-Y")}}--}}
                                             1
                                            </span>
                                        </div>
                                    </a>
                                </li>


                            <li class="all-msgs text-center">
                                <p class="m-0"><a href="{{url('/')}}">See all Notification</a></p>
                            </li>
                        </ul>
                    </li>
                <li class="dropdown navbar-c-items">
                    <a href="" class="dropdown-toggle waves-effect waves-light profile" data-toggle="dropdown"
                       aria-expanded="true">
                            <img src="" alt="user-img"
                                 class="img-circle">

                            <img src="http://admin.vieroots.local.com/media/profile-images/2018/11/person-25x25.png"
                                 alt="user-img" class="img-circle">


                    </a>
                    <ul class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right user-list notify-list">
                        <li class="text-center">
                            @auth
                                <span>{{Auth::user()->name}}</span>
                                <span>{{Auth::user()->id}}</span>
                            @endauth
                        </li>
                        <li><a href="{{url('profile')}}"><i class="ti-user m-r-5"></i> Profile</a></li>
                        <li><a href="{{url('change-password')}}"><i class="ti-settings m-r-5"></i> Settings</a></li>
                        <li><a href="{{url('/logout')}}"><i class="ti-power-off m-r-5"></i> Logout</a></li>
                    </ul>

                </li>
            </ul>
            <div class="menu-item">
                <!-- Mobile menu toggle-->
                <a class="navbar-toggle">
                    <div class="lines">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </a>
                <!-- End mobile menu toggle-->
            </div>
        </div>
        <!-- end menu-extras -->

    </div>
    <!-- end container -->
</div>
