<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Viegenomics admin dashboard">
    <meta name="author" content="Nandul Das">
    <meta content="{!! csrf_token() !!}" name="csrf-token">
    <link rel="shortcut icon" href="">
    <title>{{env('APP_NAME')}}</title>
    <!-- date range picker -->
    <link href="{{asset('admin/assets/plugins/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">
    <!-- App css -->

    <link href="{{asset('admin/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('admin/assets/css/core.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('admin/assets/css/components.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('admin/assets/css/icons.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('admin/assets/css/pages.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('admin/assets/css/menu.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('admin/assets/css/responsive.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('admin/assets/css/custom-dashboard.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('admin/assets/plugins/switchery/css/switchery.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('admin/assets/plugins/toastr/css/toastr.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('admin/assets/plugins/animate/animate.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('admin/assets/js/pretty-switch/pretty-checkbox.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('admin/font-icon/css/fontawesome.min.css')}}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="{{asset('admin/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}">

    @yield('css')
    <style>

        .label-pending{
            background-color:#ffc107;
        }
        .label-new{
            background-color:#17a2b8;
        }
        .label-processing{
            background-color:#007bff;
        }
        .label-cancelled{
            background-color:#dc3545;
        }
        .label-admin_cancelled{
            background-color:#dc3545;
        }
        .label-shipped{
            background-color:#17a2b8;
        }
        .label-delivered{
            background-color:#28a745;
        }
        .label-confirmed{
            background-color:#28a745;
        }

        .label-payment_error{
            background-color:#ff8787;
        }
        .label-payment{
            background-color:#ff8787;
        }
        .label-rejected{
            background-color:#dc3545;
        }

        .label-vendoor_rejected{
            background-color:#dc3545;
        }

        .label-accepted{
            background-color:#17a2b8;
        }
        /* new */
        .label-refund_process , .label-return_process{
            background-color:#17a2b8;
        }

        .label-refund_initiated , .label-return_initiated{
            background-color:#28a745;
        }

        .label-refunded , .label-returned{
            background-color:#00ff00;
        }




        .light-green-pending{
            border-left: 5px solid #ffc107 !important;
            border-radius: 0px;
            position: relative;
        }
        .light-green-processing{
            border-left: 5px solid #188ae2 !important;
            border-radius: 0px;
            position: relative;
        }
        .light-green-cancelled{
            border-left: 5px solid #dc3545 !important;
            border-radius: 0px;
            position: relative;
        }
        /* for yajara datatable */
        .dt-buttons {
            float: left;
            margin-left: 50px !important;
        }

    </style>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>

</head>
<body>


<!-- Navigation Bar-->
<header id="topnav">
    @include('admin.layouts.header-menu')
    @include('admin.layouts.main-menu')
</header>
<!-- End Navigation Bar-->


<div class="wrapper">
    <div class="container">
        @yield('content')
        @include('admin.layouts.footer')
    </div>
</div>


<!-- jQuery  -->
<script> var base_url ='{{url('')}}'</script>


<script src="{{asset('admin/assets/js/modernizr.min.js')}}"></script>
<script src="{{asset('admin/assets/js/jquery.min.js')}}"></script>
<script src="{{asset('admin/assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('admin/assets/js/detect.js')}}"></script>
<script src="{{asset('admin/assets/js/fastclick.js')}}"></script>
<script src="{{asset('admin/assets/js/jquery.blockUI.js')}}"></script>
<script src="{{asset('admin/assets/js/waves.js')}}"></script>
<script src="{{asset('admin/assets/js/jquery.slimscroll.js')}}"></script>
<script src="{{asset('admin/assets/js/jquery.scrollTo.min.js')}}"></script>

<script src="{{asset('admin/assets/js/pie-chart.js')}}"></script>
<script src="{{asset('admin/assets/js/slick.min.js')}}"></script>
<script src="{{asset('admin/assets/js/customized-js.js')}}"></script>


{{--<script src="{{asset('admin/assets/plugins/waypoints/jquery.waypoints.min.js')}}"></script>--}}
{{--<script src="{{asset('admin/assets/plugins/counterup/jquery.counterup.min.js')}}"></script>--}}
<script src="{{asset('admin/assets/js/custom/index-functions.js')}}"></script>

<!-- App js !-->
<script src="{{asset('admin/assets/js/jquery.core.js')}}"></script>
<script src="{{asset('admin/assets/js/jquery.app.js')}}"></script>
<script src="{{asset('admin/assets/plugins/bootstrap-notify-master/bootstrap-notify.min.js')}}"></script>

<script src="{{asset('admin/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script>

    $('.form-control.date-pick').datepicker({
        format: "yyyy-mm-dd",
        todayHighlight:true,
        defaultViewDate:'today',
        autoclose:true,
    });
    var now=new Date();
    now.setMonth(now.getMonth() - 1);
    $(function() {
        $(".month-pick").datepicker( {
            format: "mm-yyyy",
            viewMode: "months",
            minViewMode: "months",
            orientation: 'auto bottom',
            autoclose:true,
            endDate: "today",
            maxDate: "today",
            // defaultDate:("0"+(now.getMonth()+1)).slice(-2)+"-"+now.getFullYear()
        });


        $(".month-pick").datepicker("setDate",("0"+(now.getMonth()+1)).slice(-2)+"-"+now.getFullYear())
    });

</script>
<script>
    $.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
    $(document).ready(function() {
        base_url = "{{url('')}}";
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-135104825-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-135104825-1');
</script>
@yield('script')
<script src="{{asset('admin/assets/js/custom/validation.js')}}"></script>
<script>
    $(".header_notification").click(function () {
        var id =  $(this).data("id");
        $.ajax({
            url: base_url+'/get-notification',
            type: 'POST',
            data: {
                'id':id
            },
            beforeSend: function() {

            },
            success: function(result) {
                window.location.href =result;
            },
            error:function f() {
                swal("Something went wrong..!Please try again.");
            }

        });
    });

</script>
</body>
</html>
