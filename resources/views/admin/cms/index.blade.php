@extends('admin.layouts.app')

@section('css')

@endsection

@section('content')
    <main class="app-content bg-white">
        
        <!-- Sub Title---->
        <div id="submenu_subheader" class="submenu-subheader submenu-grid__item ">          
            <div class="submenu-subheader__title">
                <h4 class="title-caption"><i class="fa fa-file-text"></i> CMS</h4>
            </div>           
            <div class="submenu-subheader__toolbar">
                <div class="submenu-subheader__toolbar-wrapper bs-component">
                        @if(can('add_cms'))
                        <a data-toggle="tooltip" data-placement="top" data-html="true" title="Create Page" class="btn bg-primary-green btn-sm waves-effect waves-light btn-right  br-0"  href="{{url('cms/create')}}" role="button"><i class="fa fa-plus-circle"></i>Create Page</a>
                    @endif
                </div>
            </div>
        </div>          
        <!-- END Sub Title---->

       <div class="sub-header-body">
        @if(can('browse_cms'))
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="tile">
                    <div class="br-0">
                        <div class="table-responsive">
                        <table id="sampleTable" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Title</th>
                                <th>Slug</th>
                                <th>Type</th>
                                <th>Target</th>
                                <th>Created at</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($cms as $data)
                                <tr>
                                    <td>{{$data->title}}</td>
                                    <td>{{$data->slug}}</td>
                                    <td>
                                        @if($data->footer == 1)
                                            <span class="badge badge-success">Footer</span>&nbsp;&nbsp;
                                        @endif
                                        @if($data->page == 1)
                                            <span class="badge badge-warning">Page</span>&nbsp;&nbsp;
                                        @endif
                                        @if($data->link == 1)
                                            <span class="badge badge-info">Link</span>
                                        @endif
                                    </td>
                                    <td><span class="badge badge-primary">{{$data->target}}</span></td>
                                    <td>{{date('d M Y h:i A',strtotime($data->created_at))}}</td>
                                    <td>
                                        @if(can('edit_cms'))
                                            <div class="pretty p-switch p-fill">
                                                <input type="checkbox"  @if($data->status == 1) checked @endif data-id="{{$data->id}}" class="change-status">
                                                <div class="state p-success">
                                                    <label></label>
                                                </div>
                                            </div>
                                        @endif
                                    </td>
                                    <td>
                                        @if(can('edit_cms'))
                                        @if($data->slug == 'home')
                                            <a href="{{url('cms/homeedit/'.$data->id)}}" class="btn action-button bg-primary-blue" data-toggle="tooltip" data-placement="top" data-html="true"  title="Edit"><i class="fa fa-pencil-square-o "></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        @else
                                            <a href="{{url('cms/edit/'.$data->id)}}" class="btn action-button bg-primary-blue" data-toggle="tooltip" data-placement="top" data-html="true"  title="Edit"><i class="fa fa-pencil-square-o "></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        @endif
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @else
            @include('admin.no-access-content')
        @endif
       </div>
    </main>
@endsection

@section('script')
    <script type="text/javascript" src="{{asset('admin/js/plugins/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/assets/js/custom/cms-functions.js')}}"></script>
  
@endsection