@extends('admin.layouts.app')

@section('css')
<style>
    ul {
        list-style-type: none;
    }
</style>
<link rel="stylesheet" type="text/css" href="{{ asset('admin/redactor/redactor/redactor.css')}}" />
@endsection

@section('content')
    <main class="app-content bg-white">    
        

        <!-- Sub Title---->
        <div id="submenu_subheader" class="submenu-subheader submenu-grid__item ">          
                <div class="submenu-subheader__title">
                    <h4 class="title-caption"><i class="fa fa-plus-circle"></i> Create Page</h4>
                </div>           
                <div class="submenu-subheader__toolbar">
                    <div class="submenu-subheader__toolbar-wrapper bs-component">
                            <a data-toggle="tooltip" data-placement="top" data-html="true" title="CMS List" class="btn bg-primary-blue btn-sm waves-effect waves-light btn-right br-0"  href="{{url('cms')}}"><i class="fa fa-list"></i>CMS Lists</a>
                    </div>
                </div>
            </div>          
            <!-- END Sub Title---->
        
        <!-- BODY -->
        <div class="sub-header-body">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="tile">
                <div class="br-0">
                    <form method="post" action="{{url('cms/create')}}" enctype="multipart/form-data" name="cmsCreate" id="cmsCreate">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                {{csrf_field()}}

                                <div class="col-sm-12 col-xs-12">
                                    <div class="form-group @if($errors->first('external')) has-danger @endif">
                                        <div class="animated-checkbox">
                                            <div class="checkbox checkbox-primary">
                                                <input type="checkbox" id="external" name="external" class="permission-group" value="1" @if(old('external')) checked @endif>
                                                <label>
                                                    <span class="label-text">External Link</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                               <div class="col-sm-6 col-xs-12">
                                    <div class="form-group @if($errors->first('title')) has-danger @endif">
                                        <label for="title">Title</label>
                                        <input class="form-control br-0 @if($errors->first('title')) is-invalid @endif" id="title" name="title" type="text" placeholder="Enter page title" required autocomplete="off" value="{{old('title')}}">
                                            @if($errors->first('title'))
                                                <span class="form-control-feedback">{{$errors->first('title')}}</span>
                                            @endif
                                    </div>
                               </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group @if($errors->first('sub_title')) has-danger @endif">
                                       <label for="sub_title">Sub Title</label>
                                            <input class="form-control br-0 @if($errors->first('sub_title')) is-invalid @endif" id="sub_title" name="sub_title" type="text" placeholder="Enter page sub title" required autocomplete="off" value="{{old('sub_title')}}">
                                             @if($errors->first('sub_title'))
                                                <span class="form-control-feedback">{{$errors->first('sub_title')}}</span>
                                            @endif
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group @if($errors->first('slug')) has-danger @endif">
                                        <label for="slug">Page Slug</label>
                                        <input class="form-control br-0 @if($errors->first('slug')) is-invalid @endif" id="slug" name="slug" type="text" placeholder="Enter page slug" required autocomplete="off" value="{{old('slug')}}">
                                            @if($errors->first('slug'))
                                                <span class="form-control-feedback">{{$errors->first('slug')}}</span>
                                            @endif
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group @if($errors->first('target')) has-danger @endif">
                                        <label for="slug">Page Target</label>
                                            <select class="form-control br-0 @if($errors->first('target')) is-invalid @endif" id="target" name="target">
                                                <option value="">Select Page Target</option>
                                                    @foreach($target as $key => $value)
                                                <option value="{{$value}}" @if(old('target') == $value) selected="selected" @endif>{{$value}}</option>
                                                    @endforeach
                                            </select>
                                                @if($errors->first('target'))
                                                    <span class="form-control-feedback">{{$errors->first('target')}}</span>
                                                @endif
                                    </div>
                                </div>
                               <div class="col-sm-12 col-xs-12">
                                    <div class="form-group @if($errors->first('external_url')) has-danger @endif is-external">
                                        <label for="slug">External Url</label>
                                        <input class="form-control br-0 @if($errors->first('external_url')) is-invalid @endif" id="external_url" name="external_url" type="text" placeholder="Enter External Url" autocomplete="off" value="{{old('external_url')}}">
                                        @if($errors->first('external_url'))
                                            <span class="form-control-feedback">{{$errors->first('external_url')}}</span>
                                        @endif
                                     </div>
                               </div>

                                <div class="col-sm-12 col-xs-12">
                                    <div class="form-group @if($errors->first('description')) has-danger @endif not-external">
                                        <label for="description">Description</label>
                                        <textarea class="form-control @if($errors->first('description')) is-invalid @endif" name="description" id="description" rows="3">{{old('description')}}</textarea>
                                            @if($errors->first('description'))
                                                <span class="form-control-feedback">{{$errors->first('description')}}</span>
                                             @endif
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group @if($errors->first('meta_title')) has-danger @endif not-external">
                                        <label for="meta_title">Meta Title</label>
                                            <input class="form-control br-0 @if($errors->first('meta_title')) is-invalid @endif" id="meta_title" name="meta_title" type="text" placeholder="Enter meta title"  autocomplete="off" value="{{old('meta_title')}}">
                                            @if($errors->first('meta_title'))
                                                <span class="form-control-feedback">{{$errors->first('meta_title')}}</span>
                                            @endif
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group @if($errors->first('meta_key')) has-danger @endif not-external">
                                        <label for="meta_key">Meta Key</label>
                                            <input class="form-control br-0 @if($errors->first('meta_key')) is-invalid @endif" id="meta_key" name="meta_key" type="text" placeholder="Enter meta key"  autocomplete="off" value="{{old('meta_key')}}">
                                            @if($errors->first('meta_key'))
                                                <span class="form-control-feedback">{{$errors->first('meta_key')}}</span>
                                            @endif
                                    </div>
                                </div>
                                <div class="col-sm-12 col-xs-12">
                                        <div class="form-group @if($errors->first('meta_description')) has-danger @endif not-external">
                                                <label for="meta_description">Meta Description</label>
                                                <textarea class="form-control br-0 @if($errors->first('meta_description')) is-invalid @endif" name="meta_description" id="meta_description" placeholder="Enter Meta Description" rows="3">{{old('meta_description')}}</textarea>
                                                @if($errors->first('meta_description'))
                                                    <span class="form-control-feedback">{{$errors->first('meta_description')}}</span>
                                                @endif
                                            </div>
                                </div>

                                <div class="col-sm-12 col-xs-12">
                                        <div class="form-group @if($errors->first('type')) has-danger @endif">
                                                <label for="type">Type *</label>
                                                <div class="animated-checkbox">
                                                    @php
                                                    $arr = [];
                                                    if(old('type')){
                                                        $arr = old('type');
                                                    }
                                                    @endphp
                                                   
                                                    <ul>
                                                        <li>
                                                            <label>
                                                                <input type="checkbox" id="type" name="type[]" class="permission-group" value="footer" @if(in_array('footer', $arr)) checked @endif >
                                                                <span class="label-text">Footer</span>
                                                            </label>&nbsp;&nbsp;&nbsp;
                                                            <label>
                                                                <input type="checkbox" id="type" name="type[]" class="permission-group" value="page" @if(in_array('page', $arr)) checked @endif >
                                                                <span class="label-text">Page</span>
                                                            </label>&nbsp;&nbsp;&nbsp;
                                                            <label>
                                                                <input type="checkbox" id="type" name="type[]" class="permission-group" value="link" @if(in_array('link', $arr)) checked @endif >
                                                                <span class="label-text">Link</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                                @if($errors->first('type'))
                                                    <span class="form-control-feedback">{{$errors->first('type')}}</span>
                                                @endif
                                            </div>
                                </div>

                               <div class="col-sm-12 col-xs-12">
                                    <div class="form-group @if($errors->first('status')) has-danger @endif">
                                            <div class="animated-radio-button">
                                                <ul>
                                                    <li>
                                                        <label>
                                                            <input id="status" type="radio" name="status" value="1" checked><span class="label-text">Published</span>
                                                        </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <label>
                                                            <input id="status" type="radio" name="status" value="0"><span class="label-text">Not Published</span>
                                                        </label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                               </div>

                            </div>
                        </div>

                        <div class="tile-footer text-right">
                            <button class="btn bg-primary-blue btn-sm br-0" type="submit">Save</button>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <!-- END BODY -->
    </main>
@endsection

@section('script')

    <!--Notifications Message Section-->
    @include('admin.layouts.notifications')

    <script src="{{ asset('admin/redactor/redactor/redactor.js')}}"></script>
    <script src="{{ asset('admin/redactor/plugins/fontsize/fontsize.js')}}"></script>
    <script src="{{ asset('admin/redactor/plugins/imagemanager/imagemanager.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/assets/js/custom/cms-functions.js')}}"></script>

    <script>
        $(document).ready(function () {

            $R('#description', {
                maxHeight:"400px",
                minHeight:"400px",
                plugins: ['fontsize','imagemanager'],
                imageUpload: '{!! asset('admin/redactor/demo/scripts/image_upload.php')!!}',
                imageResizable: true,
                imagePosition: true
            });

           
        });
    </script>
@endsection