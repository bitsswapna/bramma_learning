@extends('admin.layouts.app')

@section('css')

@endsection

@section('content')
<main class="app-content bg-white ">
<div id="submenu_subheader" class="submenu-subheader submenu-grid__item ">          
        <div class="submenu-subheader__title">
            <h4 class="title-caption"><i class="fas fa-list-alt"></i> Add Item</h4>
        </div>           
        <div class="submenu-subheader__toolbar">
            <div class="submenu-subheader__toolbar-wrapper bs-component">
            @if(can('browse_roles'))
                <a data-toggle="tooltip" data-placement="top" data-html="true" title="" class="btn bg-primary-blue btn-sm waves-effect waves-light btn-right br-0" href="{{url('admin-utilites')}}" role="button" data-original-title="Item Lists">
                    <i class="fa fa-list"></i>Item
                </a> 
                @endif
            </div>
        </div>
    </div>



    

    
                   
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="tile">
                   
                    <form method="post" id="createrRole" name="createrRole" enctype="multipart/form-data" action="{{url('insert/utilites')}}">
                       {{csrf_field()}}
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                       <div >
                                            <label for="type">Type</label>
                                        
                                            <select  class="form-control @if($errors->first('type')) is-invalid @endif" name="type" autocomplete="off" value="{{old('type')}}">
                                            <option value=""> select</option>
                                                <option value="1">credit</option>
                                                <option value="2">Debit</option>
                                            </select>
                                            @if($errors->first('type'))
                                        <sapn class="error">{{$errors->first('type')}}</sapn>
                                    @endif
                                          
                                       </div> 

                                       <div >
                                    <label for="display_name">Name</label>
                                    <input class="form-control @if($errors->first('name')) is-invalid @endif" id="display_name" name="name" type="text" placeholder="Enter name" autocomplete="off" value="{{old('name')}}">
                                    @if($errors->first('name'))
                                        <sapn class="error">{{$errors->first('name')}}</sapn>
                                    @endif
                                </div>

                            <div class="form-group @if($errors->first('image')) has-danger @endif">
                                <label for="image"><strong>icon</strong></label><br>

                                <input onchange="PreviewImage();"  id="image" name="image" type="file" accept="image/x-png">  <div class="image">Upload PNG Image only</div>
                                @if($errors->first('image'))<span class="alert-danger">{{$errors->first('image')}}</span> @endif
                            </div>
                            <div class="main-image" style="display: none;">
                                <img id="uploadPreview" style="width: 100px; height: 100px; " />
                            </div>

                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">

                        <div >

                            <label for="display_name">Key word</label>
                            <input class="form-control @if($errors->first('key_word')) is-invalid @endif" id="key_word" name="key_word" type="text" placeholder="Enter key word" autocomplete="off" value="{{old('key_word')}}">
                            @if($errors->first('key_word'))
                            <sapn class="error">{{$errors->first('key_word')}}</sapn>
                            @endif
                        </div>

                                <div >
                                    <label for="name">Description</label>
                                    <input class="form-control @if($errors->first('descrpton')) is-invalid @endif" id="name" type="text" name="descrpton" placeholder="Enter description"   autocomplete="off"> </input>
                                    @if($errors->first('descrpton'))
                                        <sapn class="error">{{$errors->first('descrpton')}}</sapn>
                                    @endif
                                </div>

                            <div class="tile-footer text-right">
                        <button id="submit-btn" class="btn btn-primary waves-effect waves-light br-0 btn-sm" type="submit">Save</button>
                    </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>


@endsection

@section('js')
    <!--Notifications Message Section-->
    @include('admin.layouts.notifications')


<script>


    function PreviewImage() {
        alert('hai');
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("image").files[0]);

        oFReader.onload = function (oFREvent) {
            $(".main-image").show();
            document.getElementById("uploadPreview").src = oFREvent.target.result;
        };
    };
</script>
@endsection
