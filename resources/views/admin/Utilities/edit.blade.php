@extends('admin.layouts.app')

@section('css')
    <style>
        ul {
            list-style-type: none;
        }

        span.label-text::first-letter {
            text-transform: uppercase;
        }
    </style>
@endsection

@section('content')
    <main class="app-content bg-white">
        <!-- Sub Title---->
        <div id="submenu_subheader" class="submenu-subheader submenu-grid__item ">          
            <div class="submenu-subheader__title">
                <h4 class="title-caption"><i class="fas fa-user-shield"></i>  Edit Item</h4>
            </div>           
            <div class="submenu-subheader__toolbar">
                <div class="submenu-subheader__toolbar-wrapper">
                    <a  data-toggle="tooltip" data-placement="left" data-html="true"  title="Item List" class="btn bg-primary-blue btn-sm waves-effect waves-light  br-0" href="{{url('admin-utilites')}}">
                        <i class="fa fa-list"></i>Item Lists</a>
                </div>
            </div>
        </div>
        <!-- Body Part -->
        <div class="row">
            <div class="col-lg-10 col-md-8 col-sm-8 col-xs-8">
                <div class="tile">
        <div class="sub-header-body">
            <form method="post" action="{{url('admin_utilite/update/'.$ed->id)}}" enctype="multipart/form-data" name="editdata" id="editRole">
                {{csrf_field()}}

                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">

                   
                     
                        
                 <div >
                                            <label for="type">Type</label>
                                            <select  class="form-control"  name="type" autocomplete="off" value="{{$ed->type}}">
                                                <option value="1" selected>credit</option>
                                                <option value="2" selected>Debit</option>
                                            </select>
                                            @if($errors->first('type'))
                                        <sapn class="form-control-feedback">{{$errors->first('type')}}</sapn>
                                    @endif
                                        </div>
                        <div >
                            <label for="display_name">Name</label>
                            <input class="form-control @if($errors->first('name')) is-invalid @endif" id="display_name" name="name" type="text" placeholder="Enter name" autocomplete="off" value="{{$ed->name}}">
                            @if($errors->first('name'))
                            <sapn class="error">{{$errors->first('name')}}</sapn>
                            @endif
                        </div>

                        <div class="form-group @if($errors->first('image')) has-danger @endif">
                            <label for="image"><strong>Image</strong></label><br>
                            <input onchange="Image();"  id="image" name="image" type="file" accept="image/x-png,image/gif,image/jpeg">
                            @if($errors->first('image'))<span class="alert-danger">{{$errors->first('image')}}</span> @endif

                        </div>

                        <img src="{{asset('media/item/'.$ed->image)}}" id="uploadPreview2" width="200px">
                    </div>
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">

                    <div >

                        <label for="display_name">Key word</label>
                        <input class="form-control @if($errors->first('key_word')) is-invalid @endif" id="key_word" name="key_word" type="text" placeholder="Enter key word" autocomplete="off" value="{{$ed->key_word}}">
                        @if($errors->first('key_word'))
                        <sapn class="error">{{$errors->first('key_word')}}</sapn>
                        @endif
                    </div>

                    <div >

                            <label for="name">Description</label>
                            <input class="form-control br-0" id="name" type="text" name="descrpton" placeholder="Enter description"  autocomplete="off"  value=" {{$ed->description}}">


                    </div>
                    


              



                <div class="sub-header-footer text-right">
                    <button class="btn bg-primary-blue br-0  waves-effect waves-light btn-sm" type="submit">Update</button>
                </div>
                    </div>
            </form>
        </div>
        <!-- Footer Part END-->
                </div>
            </div>
        </div>

        <!-- Footer Part END-->      
    </main>
@endsection

@section('script')

    <!--Notifications Message Section-->
    @include('admin.layouts.notifications')
    <script>
        $("#editRole").validate({
            rules: {
                // simple rule, converted to {required:true}
                display_name: "required",
                name: "required",
            }
        });
    </script>

    <script>
        $('document').ready(function () {

            $('.permission-group').on('change', function(){
                $(this).parents('tr').find(".the-permission").prop('checked', this.checked);
            });

            function parentChecked(){
                $('.permission-group').each(function(){
                    var allChecked = true;
                    $(this).parents('tr').find(".the-permission").each(function(){
                        if(!this.checked) allChecked = false;
                    });
                    $(this).prop('checked', allChecked);
                });
            }

            parentChecked();

            $('.the-permission').on('change', function(){
                parentChecked();
            });
        });
    </script>

<script>

    function Image() {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("image").files[0]);

        oFReader.onload = function (oFREvent) {
            $(".main-image2").show();
            document.getElementById("uploadPreview2").src = oFREvent.target.result;
        };
    };

</script>
@endsection
