@extends('admin.layouts.app')

@section('css')
    <link href="{{asset('css/plugin/selectize.bootstrap2.css')}}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    <main class="app-content">
        <!-- <div class="app-title">
            <div class="col-md-6 col-sm-12 col-xs-12 rem-pad">
                <h1><i class="fa fa-plus-circle"></i>Customer</h1>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 btn-right rem-pad">
                @if(can('browse_admin_user'))
                    <a class="btn btn-success waves-effect waves-light" href="{{url('admin_users')}}"><i class="fa fa-list"></i>Admin User Lists</a>
                @endif
            </div>
        </div> -->

        <div id="submenu_subheader" class="submenu-subheader submenu-grid__item ">
            <div class="submenu-subheader__title">
                <h4 class="title-caption"> <i class="fa fa-user"></i> Customer</h4>
            </div>
            <div class="submenu-subheader__toolbar">
                <div class="submenu-subheader__toolbar-wrapper bs-component">
                   @if(can('browse_admin_user'))
                    <a class="btn bg-primary-green btn-sm waves-effect waves-light br-0  m-b-10" href="{{url('admin_users')}}"><i class="fa fa-list"></i>Admin User Lists</a>
                @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="tile">
                    <div class="tile-body">
                        <form method="post" action="{{('admin-user-wa/create/'.$ed->id)}}" enctype="multipart/form-data"  id="createAdminUserWA" name="createAdminUserWA">
                            {{csrf_field()}}
                            <input type="hidden" value="{{$ed->id}}" id="user_id">
                            <input type="hidden" value="4" id="role_id" name="role_id">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 userReqEdit">
                                    <div class="form-group col-sm-6 col-xs-12 @if($errors->first('first_name')) has-danger @endif">
                                        <label for="first_name">First Name</label>
                                        <input class="form-control @if($errors->first('first_name')) is-invalid @endif" id="first_name" name="first_name" type="text" placeholder="Enter first name"  autocomplete="off" value="{{$ed->first_name}}">
                                        @if($errors->first('first_name'))
                                            <sapn class="">{{$errors->first('first_name')}}</sapn>
                                        @endif
                                        <span class="error text-danger"></span>
                                    </div>
                                    <div class="form-group col-sm-6 col-xs-12 @if($errors->first('last_name')) has-danger @endif">
                                        <label for="last_name">Last Name</label>
                                        <input class="form-control @if($errors->first('last_name')) is-invalid @endif" id="last_name" name="last_name" type="text" placeholder="Enter last name" autocomplete="off" value="{{$ed->last_name}}">
                                        @if($errors->first('last_name'))
                                            <sapn class="">{{$errors->first('last_name')}}</sapn>
                                        @endif
                                        <span class="error text-danger"></span>
                                    </div>
                                    <div class="form-group col-sm-6 col-xs-12 @if($errors->first('mobile')) has-danger @endif">
                                        <label for="mobile">Mobile</label>
                                        <input class="form-control @if($errors->first('mobile')) is-invalid @endif" id="mobile" name="mobile" type="text" placeholder="Enter mobile number" autocomplete="off" value="{{$ed->mobile}}">
                                        @if($errors->first('mobile'))
                                            <sapn class="">{{$errors->first('mobile')}}</sapn>
                                        @endif
                                        <span class="error text-danger"></span>
                                    </div>
                                    <div class="form-group col-sm-6 col-xs-12 @if($errors->first('email')) has-danger @endif">
                                        <label for="email">Email</label>
                                        <input class="form-control @if($errors->first('email')) is-invalid @endif" id="email" name="email" type="text" placeholder="Enter email" autocomplete="off" value="{{$ed->email}}" >
                                        @if($errors->first('email'))
                                            <sapn class="">{{$errors->first('email')}}</sapn>
                                        @endif
                                        <span class="error text-danger"></span>
                                    </div>

                                    <div class="form-group col-sm-6 col-xs-12 @if($errors->first('email')) has-danger @endif">
                                        <label for="date_of_birth" class="label-fixed">DOB</label>
                                        <div class="form-group">
                                            <input required type="date" class="form-control validate-field" id="date_of_birth" data-id="#date_of_birth" data-formfield="date_of_birth" name="date_of_birth"
                                                   value="{{$ed->dob }}">
                                                  
                                            <span class="text-danger small error"></span>
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-6 col-xs-12 @if($errors->first('email')) has-danger @endif">
                                        <label for="" class="bmd-label-floating label-fixed">Gender</label>
                                        <div class="form-group user-gender">
                                            <label class="radio-inline">
                                                <input class="radio" type="radio" name="gender" @if($ed->gender == 'male') checked @endif value="male">Male
                                            </label>
                                            <label class="radio-inline">
                                                <input class="radio" type="radio" name="gender" @if($ed->gender == 'female') checked @endif value="female">Female
                                                <span class="text-danger small error" style="display: block;"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-xs-12">
                                    <hr>
                                    </div>
                                    <div class="col-md-6 col-sm-12 col-xs-12 rem-pad">
                                        <div class="form-group col-sm-12 col-xs-12" >
                                                <label for="role_id">Role</label>
                                                <select name="" id="" class="form-control" name="role">
                                                   
                                                    <option value="3" selected>Driver</option>
                                                    <option value="11" selected>Cleaner</option>
                                                   
                                                </select>
                                                <span class="error text-danger"></span>
                                            </div>
                                            
                                            <div class="form-group col-sm-6 col-xs-12 @if($errors->first('address')) has-danger @endif">
                                        <label for="Address">Address</label>
                                        <input class="form-control @if($errors->first('address')) is-invalid @endif" id="address" name="address" type="text" placeholder="Enter address" autocomplete="off" value="{{$ed->address}}">
                                        @if($errors->first('address'))
                                            <sapn class="">{{$errors->first('address')}}</sapn>
                                        @endif
                                        <span class="error text-danger"></span>
                                    </div>
                                   


                                            
                                        <div class="col-sm-12 col-xs-12 tile-footer">
                                            <button class="btn btn-primary waves-effect waves-light" type="submit">Save</button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection

@section('script')
    <script src="{{asset('admin/assets/js/sweetalert.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/selectize.min.js')}}"></script>
    <script src="{{asset('admin/assets/js/custom/admin-user-tab.js')}}"></script>
    <script>
        var no_image_url = "{{url('media/no-image/no-image.png')}}";
    </script>
    <!--Notifications Message Section-->
    @include('admin.layouts.notifications')

@endsection