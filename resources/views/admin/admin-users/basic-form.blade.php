<div class="row">
    <div class="col-lg-12">

        <!-- Personal Details-->
        <div class="box-container">
            <h4>Personal Information</h4>
            <hr>
             <form method="post" id="createrRole" name="createrRole" action="{{url('admin_user/create')}}">
            <div class="row">

            <div class="col-lg-12 col-md-10 col-sm-9 col-xs-10">
            
                    <div class="avatar-upload">
                        <div class="avatar-edit">
                            <input type="file" id="imageUpload" name="image" accept=".png, .jpg, .jpeg">
                            <span class="text-danger small error"></span>

                            <label for="imageUpload"></label>
                        </div>
                        <div class="avatar-preview">
                            <div id="imagePreview"
                                 style="background-image: url({{asset('admin/assets/images/img.png')}});">
                                 </div>
                        </div>
                    </div>

                </div> <!-- end col -->

               
                    <div class="row">
                    <div class="col-sm-6 col-xs-12 m-b-20">
                            <div class="form-group bmd-form-group">
                                <input required type="text" class="form-control validate-field" id="first_name" data-id="#first_name" data-formfield="first_name" name="first_name" type="text"
                                       autocomplete="off" value="{{old('first_name')}}">
                                <span class="text-danger small error"></span>

                                <label for="FirstName" class="bmd-label-floating">First Name</label>
                            </div>
                        </div>

                        <div class="col-sm-6 col-xs-12 m-b-20">
                            <div class="form-group bmd-form-group">
                                <input required type="text" class="form-control validate-field" id="last_name" data-id="#last_name" data-formfield="last_name" name="last_name" type="text"
                                       autocomplete="off" value="{{old('last_name')}}">
                                <span class="text-danger small error"></span>
                                <label for="LastName" class="bmd-label-floating">Last Name</label>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-xs-12 m-b-20 ">
                            <div class="form-group bmd-form-group user-gender">
                                <label for="" class="bmd-label-floating label-fixed">Gender</label>
                                <label class="radio-inline">
                                    <input type="radio" name="gender" checked value="1">Male
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="gender" value="2">Female
                                    <span class="text-danger small error" style="display: block;"></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12 m-b-20">
                            <div class="form-group bmd-form-group">
                                <input required type="date" class="form-control validate-field" id="date_of_birth" data-id="#date_of_birth" data-formfield="date_of_birth" name="date_of_birth"
                                       value="{{old('date_of_birth')}}">
                                <span class="text-danger small error"></span>
                                <label for="DateOfBirth" class="label-fixed">DOB</label>
                            </div>


                        </div>
                    </div>

                   
                    <div class="row">
                    <div class="col-sm-6 col-xs-12 m-b-20">
                            <div class="form-group bmd-form-group">
                                <div class="input-group input-flex mb-3">
                                    <input required type="password" class="form-control validate-field" id="password" data-id="#password" data-formfield="password" name="password" autocomplete="off">
                                    <span class="text-danger small error"></span>
                                    <label for="YourPassword" class="bmd-label-floating">Password</label>

                                    <div class="input-group-append">
                                        {{--<button class="btn btn-primary btn-password-view" type="submit">Generate Password</button>--}}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6 col-xs-12 m-b-20">
                            <div class="form-group bmd-form-group">
                                <input required type="password" class="form-control validate-field" id="password_confirmation" data-id="#password_confirmation" data-formfield="password_confirmation" name="password_confirmation" >
                                <span class="text-danger small error"></span>
                                <label for="ConfirmYourPassword" class="bmd-label-floating">Confirm Password</label>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
               
        <!-- END Personal Details-->

        <div class="box-container">
            <h4>Contact Information</h4>
            <hr>
            <div class="row">
            <div class="col-sm-6 col-xs-12 m-b-20">
                            <div class="form-group bmd-form-group">
                                <input required type="number" class="form-control validate-field" id="mobile" data-id="#mobile" data-formfield="mobile" name="mobile"
                                       value="{{old('mobile')}}">
                                <span class="text-danger small error"></span>
                                <label for="MobileNumber" class="bmd-label-floating">Mobile Number</label>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12 m-b-20">
                            <div class="form-group bmd-form-group">
                                <input required type="email" class="form-control validate-field" id="email" data-id="#email" data-formfield="email" name="email"
                                       value="{{old('email')}}" autocomplete="off">
                                <span class="text-danger small error"></span>
                                <label for="YourEmailID" class="bmd-label-floating">Email Id</label>
                            </div>

                        </div>
                    </div>

            <div class="row">
            <div class="col-sm-6 col-xs-12 m-b-20">
                    <div class="form-group bmd-form-group bmd-form-address">
                        <textarea required name="address" class="form-control">{{old('address')}}</textarea>
                        <label for="YourAddress" class="bmd-label-floating">Address</label>
                        <span class="text-danger small error"></span>

                    </div>
                </div>

                <!-- <div class="col-sm-4 col-xs-12">
                    <div class="form-group bmd-form-group">
                        <select required class="form-control" name="country">
                            <option value="India">India</option>

                        </select>
                        <label for="" class="bmd-label-floating label-fixed">Country</label>
                        <span class="text-danger small error"></span>

                    </div>
                </div> -->

                <!-- <div class="col-sm-4 col-xs-12">
                    <div class="form-group bmd-form-group">
                        <label for="" class="bmd-label-floating label-fixed">State</label> -->
                  
                        <!-- <span class="text-danger small error"></span>

                    </div>
                </div> -->
                <!-- <div class="col-sm-4 col-xs-12">
                    <div class="form-group bmd-form-group">
                        <input required type="text" class="form-control validate-field" id="pincode" data-id="#pincode" data-formfield="pincode" name="pincode" value="{{old('pincode')}}"
                        >
                        <label for="YourPinCode" class="bmd-label-floating">Pin Code</label>
                        <span class="text-danger small error"></span>

                    </div>
                </div> -->
                <!-- <div class="col-sm-12 col-xs-12">
                    <div class="form-group bmd-form-group">
                        <input required type="text" class="form-control validate-field" id="location" data-id="#location" data-formfield="location" name="location" value="{{old('location')}}">
                        <label for="YourLocation" class="bmd-label-floating">Location</label>
                        <span class="text-danger small error"></span>
                        <input class="form-control" id="latitude" name="latitude" type="hidden"
                               placeholder="Enter latitude" autocomplete="off">
                        <input class="form-control" id="longitude" name="longitude" type="hidden"
                               placeholder="Enter longitude" autocomplete="off">

                    </div>
                </div> -->
            </div>
        </div> <!-- end col -->


    </div>

</div>


{{--<div class="row">--}}
    {{--<div class="col-lg-6 col-md-12 col-xs-12">--}}
        {{--<input type="hidden" name="parent_as_admin" id="vi-admin-as-parent" value="{{Auth::user()->id}}">--}}
        {{--<div class="form-group @if($errors->first('first_name')) has-danger @endif">--}}
            {{--<label for="first_name">First Name</label>--}}
            {{--<input class="form-control br-0 @if($errors->first('first_name')) is-invalid @endif" id="first_name" name="first_name" type="text" placeholder="Enter first name"  autocomplete="off" value="{{old('first_name')}}">--}}
            {{--@if($errors->first('first_name'))--}}
                {{--<sapn class="">{{$errors->first('first_name')}}</sapn>--}}
            {{--@endif--}}
            {{--<span class="error"></span>--}}
        {{--</div>--}}
        {{--<div class="form-group @if($errors->first('last_name')) has-danger @endif">--}}
            {{--<label for="last_name">Last Name</label>--}}
            {{--<input class="form-control br-0 @if($errors->first('last_name')) is-invalid @endif" id="last_name" name="last_name" type="text" placeholder="Enter last name" autocomplete="off" value="{{old('last_name')}}">--}}
            {{--@if($errors->first('last_name'))--}}
                {{--<sapn class="">{{$errors->first('last_name')}}</sapn>--}}
            {{--@endif--}}
            {{--<span class="error"></span>--}}
        {{--</div>--}}
        {{--<div class="form-group @if($errors->first('mobile')) has-danger @endif">--}}
            {{--<label for="mobile">Mobile</label>--}}
            {{--<input class="form-control br-0 @if($errors->first('mobile')) is-invalid @endif" id="mobile" name="mobile" type="text" placeholder="Enter mobile number" autocomplete="off" value="{{old('mobile')}}">--}}
            {{--@if($errors->first('mobile'))--}}
                {{--<sapn class="">{{$errors->first('mobile')}}</sapn>--}}
            {{--@endif--}}
            {{--<span class="error"></span>--}}
        {{--</div>--}}
        {{--<div class="form-group @if($errors->first('email')) has-danger @endif">--}}
            {{--<label for="email">Email</label>--}}
            {{--<input class="form-control br-0 @if($errors->first('email')) is-invalid @endif" id="email" name="email" type="text" placeholder="Enter email" autocomplete="off" value="{{old('email')}}">--}}
            {{--@if($errors->first('email'))--}}
                {{--<sapn class="">{{$errors->first('email')}}</sapn>--}}
            {{--@endif--}}
            {{--<span class="error"></span>--}}
        {{--</div>--}}

        {{--<div class="form-group @if($errors->first('password')) has-danger @endif">--}}
            {{--<label for="password">Password</label>--}}
            {{--<input class="form-control br-0 @if($errors->first('password')) is-invalid @endif" id="password" name="password" type="password" placeholder="Enter Password" autocomplete="off">--}}
            {{--@if($errors->first('password'))--}}
                {{--<sapn class="">{{$errors->first('password')}}</sapn>--}}
            {{--@endif--}}
            {{--<span class="error"></span>--}}
        {{--</div>--}}
        {{--<div class="form-group @if($errors->first('password_confirmation')) has-danger @endif">--}}
            {{--<label for="password_confirmation">Confirm Password</label>--}}
            {{--<input class="form-control br-0 @if($errors->first('password_confirmation')) is-invalid @endif" id="password_confirmation" name="password_confirmation" type="password" placeholder="Enter Password" autocomplete="off">--}}
            {{--@if($errors->first('password_confirmation'))--}}
                {{--<sapn class="">{{$errors->first('password_confirmation')}}</sapn>--}}
            {{--@endif--}}
            {{--<span class="error"></span>--}}
        {{--</div>--}}
        {{--<div class=" form-group @if($errors->first('gender')) has-danger @endif">--}}
            {{--<label for="gender">Gender</label>--}}
            {{--<div class="form-inline" style="display: flex;">--}}
                {{--<input type="radio" name="gender" value="male"> Male<br>--}}
                {{--<input type="radio" name="gender" value="female" style="margin-left: 20px;"> Female<br>--}}
            {{--</div>--}}

            {{--@if($errors->first('gender'))--}}
                {{--<sapn class="">{{$errors->first('gender')}}</sapn>--}}
            {{--@endif--}}
            {{--<span class="error"></span>--}}
        {{--</div>--}}
        {{--<div class="form-group @if($errors->first('date_of_birth')) has-danger @endif">--}}
            {{--<label for="date_of_birth">Data of birth</label>--}}
            {{--<input class="form-control br-0 @if($errors->first('date_of_birth')) is-invalid @endif" id="date_of_birth" name="date_of_birth" type="date" placeholder="Enter date of birth" autocomplete="off" value="{{old('date_of_birth')}}">--}}
            {{--@if($errors->first('date_of_birth'))--}}
                {{--<sapn class="">{{$errors->first('date_of_birth')}}</sapn>--}}
            {{--@endif--}}
            {{--<span class="error"></span>--}}
        {{--</div>--}}

        {{--<div class="form-group @if($errors->first('image')) has-danger @endif">--}}
            {{--<label for="image">Image</label>--}}
            {{--<div class="fake-input">--}}
                {{--<input id="uploadFile" disabled="disabled" type="text" style="display: none;">--}}
                {{--<div class="fileUpload">--}}
                    {{--<input class="form-control-file" id="image" name="image" type="file" accept="image/x-png,image/gif,image/jpeg" required>--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--@if($errors->first('image'))--}}
                {{--<sapn class="">{{$errors->first('image')}}</sapn>--}}
            {{--@endif--}}
            {{--<span class="error"></span>--}}
        {{--</div>--}}

    {{--</div>--}}
    {{--<div class="col-lg-6">--}}
    {{----}}
        {{--<div class="form-group @if($errors->first('address_1')) has-danger @endif">--}}
            {{--<label for="address_1">Address </label>--}}
            {{--<input class="form-control br-0 @if($errors->first('address_1')) is-invalid @endif" id="address_1" name="address_1" type="text" placeholder="Enter address" autocomplete="off" value="{{old('address_1')}}">--}}
            {{--@if($errors->first('address_1'))--}}
                {{--<sapn class="">{{$errors->first('address_1')}}</sapn>--}}
            {{--@endif--}}
            {{--<span class="error"></span>--}}
        {{--</div>--}}
        {{--<div class="form-group @if($errors->first('aadhar_card_no')) has-danger @endif">--}}
            {{--<label for="aadhar_card_no">Aadhar card number</label>--}}
            {{--<input class="form-control br-0 @if($errors->first('aadhar_card_no')) is-invalid @endif" id="aadhar_card_no" name="aadhar_card_no" type="text" placeholder="Enter Aadhar card number" autocomplete="off" value="{{old('aadhar_card_no')}}">--}}
            {{--@if($errors->first('aadhar_card_no'))--}}
                {{--<sapn class="">{{$errors->first('aadhar_card_no')}}</sapn>--}}
            {{--@endif--}}
            {{--<span class="error"></span>--}}
        {{--</div>--}}
        {{--<div class="form-group @if($errors->first('pan_card_no')) has-danger @endif">--}}
            {{--<label for="pan_card_no">PAN Card number</label>--}}
            {{--<input class="form-control br-0 @if($errors->first('pan_card_no')) is-invalid @endif" id="pan_card_no" name="pan_card_no" type="text" placeholder="Enter PAN card number" autocomplete="off" value="{{old('pan_card_no')}}">--}}
            {{--@if($errors->first('pan_card_no'))--}}
                {{--<sapn class="">{{$errors->first('pan_card_no')}}</sapn>--}}
            {{--@endif--}}
            {{--<span class="error"></span>--}}
        {{--</div>--}}
        {{--<div class="form-group @if($errors->first('district_state')) has-danger @endif">--}}
            {{--<label for="district_state">District state</label>--}}
            {{--<input class="form-control br-0 @if($errors->first('district_state')) is-invalid @endif" id="district_state" name="district_state" type="text" placeholder="Enter district state" autocomplete="off" value="{{old('district_state')}}">--}}
            {{--@if($errors->first('district_state'))--}}
                {{--<sapn class="">{{$errors->first('district_state')}}</sapn>--}}
            {{--@endif--}}
            {{--<span class="error"></span>--}}
        {{--</div>--}}
        {{--<div class="form-group @if($errors->first('country')) has-danger @endif">--}}
            {{--<label for="country">Country</label>--}}
            {{--<input class="form-control br-0 @if($errors->first('country')) is-invalid @endif" id="country" name="country" type="text" placeholder="Enter country" autocomplete="off" value="{{old('country')}}">--}}
            {{--@if($errors->first('country'))--}}
                {{--<sapn class="">{{$errors->first('country')}}</sapn>--}}
            {{--@endif--}}
            {{--<span class="error"></span>--}}
        {{--</div>--}}
        {{--<div class="form-group @if($errors->first('location')) has-danger @endif">--}}
            {{--<label for="location">Location</label>--}}
            {{--<input class="form-control br-0 @if($errors->first('location')) is-invalid @endif" id="location" name="location" type="text" placeholder="Enter location" autocomplete="off" value="{{old('location')}}">--}}
            {{--@if($errors->first('location'))--}}
                {{--<sapn class="">{{$errors->first('location')}}</sapn>--}}
            {{--@endif--}}
            {{--<span class="error"></span>--}}
        {{--</div>--}}
        {{--<div class="form-group @if($errors->first('latitude')) has-danger @endif">--}}
            {{--<label for="latitude">Latitude</label>--}}
            {{--<input class="form-control br-0 @if($errors->first('latitude')) is-invalid @endif" id="latitude" name="latitude" type="text" placeholder="Enter latitude" autocomplete="off" value="{{old('latitude')}}">--}}
            {{--@if($errors->first('latitude'))--}}
                {{--<sapn class="">{{$errors->first('latitude')}}</sapn>--}}
            {{--@endif--}}
            {{--<span class="error"></span>--}}
        {{--</div>--}}
        {{--<div class="form-group @if($errors->first('longitude')) has-danger @endif">--}}
            {{--<label for="longitude">Longitude</label>--}}
            {{--<input class="form-control br-0 @if($errors->first('longitude')) is-invalid @endif" id="longitude" name="longitude" type="text" placeholder="Enter longitude" autocomplete="off" value="{{old('longitude')}}">--}}
            {{--@if($errors->first('longitude'))--}}
                {{--<sapn class="">{{$errors->first('longitude')}}</sapn>--}}
            {{--@endif--}}
            {{--<span class="error"></span>--}}
        {{--</div>--}}
        {{--<div class="prof-img" id="prof_img">--}}
            {{--<img src="{{asset('media/no-image/no-image.png')}}" class=" img-fluid img-thumbnail" alt="San Fran" style="height:200px;">--}}
        {{--</div>--}}
    {{--</div>--}}

{{--</div>--}}

