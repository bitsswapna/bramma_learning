<!-- Profile -->
<div class="text-center">
    <div class="md-header">
        <!-- Image Header -->
        <div class="member-card">
            <div class="thumb-xl member-thumb m-b-10 mx-auto d-block pt-5">
                @if ($users->avatar != "")
                    <img src="{{getImageByPath($users->avatar,'200x157','profile-images')}}" class="rounded-circle img-thumbnail img-fluid img-responsive" alt="profile-image">
                    {{-- <i class="mdi mdi-star-circle member-star text-success" title="verified user"></i> --}}
                @endif
            </div>
        </div>
        <div class="">
            <h4>{{$users->name}}</h4>
            <p class="text-muted mb-2">{{$users->role_name}}</p>
            <p class="text-muted mb-2">{{$users->uid}}</p>
        </div>
    </div>
    <!-- Image Header -->
</div>

<div class="pt-20">

    <div class="nav-center">
        <ul class="nav nav-pills" role="tablist">
            <!-- Tab Profile-->
            <li class="nav-item active">
                <a class="nav-item nav-link " id="profile1-tab" data-toggle="tab" href="#profile1" role="tab" aria-controls="profile1" aria-selected="true">
                    <span class="d-block d-sm-none"><i class="fas fa-user"></i></span>
                    <span class="d-none d-sm-block">Profile</span>
                </a>
            </li>
            <!-- END Tab -->
            <!-- Tab Bank -->
            @if ($users->role_id == 1 ||$users->role_id == 10)
                @if ($bank_details)
                    <li class="nav-item">
                        <a class="nav-link" id="bank-info-tab" data-toggle="tab" href="#bank-info" role="tab" aria-controls="home1" aria-selected="false">
                            <span class="d-block d-sm-none"><i class="fas fa-bank"></i></span>
                            <span class="d-none d-sm-block">Bank Details</span>
                        </a>
                    </li>
            @endif
        @endif
        <!-- END Bank -->
        </ul>
    </div>
    <!-- Tab Content -->
    <div class="tab-content text-left">

        <div class="tab-pane active" id="profile1" role="tabpanel" aria-labelledby="profile1-tab">
            <p class="text-muted font-13">
                @if($users->parent != '' )
                    <strong>Parent :</strong> <span class="m-l-15">{{$users->getWAParent->getParentDetails->name}}</span>

                @else
                    @if ($users->role_id == 10 )
                        <strong>Parent :</strong> <span class="m-l-15">
                                                 @if ($users->getStaffParent)
                                @if ($users->getStaffParentDetails($users->getStaffParent->user_id))
                                    {{$users->getStaffParentDetails($users->getStaffParent->user_id)->name}}
                                @endif
                            @endif
                                             </span>
                    @endif
                @endif
            </p>
            <div class="table-container">
                <table class="w-100">
                    <tbody>

                    <tr>
                        <td style="width:10%;" class="text-center"><i class="fas fa-phone-volume"></i></i></td>
                        <td style="width:90%">{{$users->mobile}}</td>
                    </tr>
                    <tr>
                        <td><i class="fas fa-envelope"></i></td>
                        <td>{{$users->email}}</td>
                    </tr>
                    <tr>
                        <td><i class="fas fa-map-marker-alt"></i></td>
                        <td>{{$users->getUserDetails ? $users->getUserDetails->address_1 : ""}}
                            {{$users->getUserDetails ? $users->getUserDetails->address_2 : ""}}</td>
                    </tr>
                </table>
            </div>
            {{-- <p class="text-muted font-13"><strong>Name :</strong> <span class="m-l-15">{{$users->name}}</span></p>
            <p class="text-muted font-13"><strong>Mobile :</strong><span class="m-l-15">{{$users->mobile}}</span></p>
            <p class="text-muted font-13"><strong>Email :</strong> <span class="m-l-15">{{$users->email}}</span></p>
            <p class="text-muted font-13"><strong>Address :</strong> <span class="m-l-15">{{$users->user_address}}</span></p> --}}
        </div>
        @if ($users->role_id == 1 ||$users->role_id == 10)
            @if ($bank_details && $users->role_id != 4)
                <div class="tab-pane" id="bank-info" role="tabpanel" aria-labelledby="bank-info-tab">

                    <div class="table-container bnk-info">
                        <label for="">Bank Name</label>
                        <p class="small">{{$bank_details->getBank ? $bank_details->getBank->name:''}}</p>
                        <label for="">Name of banker</label>
                        <p class="small">{{$bank_details->name_of_banker}}</p>
                        <label for="">A/C number</label>
                        <p class="small">{{$bank_details->ac_number}}</p>
                        <label for="">Branch</label>
                        <p class="small">{{$bank_details->branch }}</p>
                        <label for="">IFSC</label>
                        <p>{{$bank_details->ifsc}}</p>


                    </div>
                </div>
        @endif
    @endif



    <!-- End Tab Content -->
    </div>
</div>
</div>
</div>

<!-- END Profile -->
