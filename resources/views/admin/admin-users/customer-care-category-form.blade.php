

<div class="row">
    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">

        <div class="form-group @if($errors->first('customer_care_category')) has-danger @endif">
            <label for="customer_care_category">Customer care category</label>
            <select name="customer_care_category" id="customer-care-category" class="form-control">
                <option value="">Select Category</option>
                @foreach ($customer_care_category as $data)
                    <option value="{{$data->id}}">{{$data->name}}</option>
                @endforeach
            </select>
            @if($errors->first('customer_care_category'))
                <sapn class="form-control-feedback">{{$errors->first('customer_care_category')}}</sapn>
            @endif
            <span class="error"></span>
        </div>


        <div class="form-group @if($errors->first('first_name')) has-danger @endif">
            <label for="first_name">First Name</label>
            <input class="form-control @if($errors->first('first_name')) is-invalid @endif" id="first_name" name="first_name" type="text" placeholder="Enter first name"  autocomplete="off" value="{{old('first_name')}}">
            @if($errors->first('first_name'))
                <sapn class="">{{$errors->first('first_name')}}</sapn>
            @endif
            <span class="error"></span>
        </div>
        <div class="form-group @if($errors->first('last_name')) has-danger @endif">
            <label for="last_name">Last Name</label>
            <input class="form-control @if($errors->first('last_name')) is-invalid @endif" id="last_name" name="last_name" type="text" placeholder="Enter last name" autocomplete="off" value="{{old('last_name')}}">
            @if($errors->first('last_name'))
                <sapn class="">{{$errors->first('last_name')}}</sapn>
            @endif
            <span class="error"></span>
        </div>
        <div class="form-group @if($errors->first('mobile')) has-danger @endif">
            <label for="mobile">Mobile</label>
            <input class="form-control @if($errors->first('mobile')) is-invalid @endif" id="mobile" name="mobile" type="text" placeholder="Enter mobile number" autocomplete="off" value="{{old('mobile')}}">
            @if($errors->first('mobile'))
                <sapn class="">{{$errors->first('mobile')}}</sapn>
            @endif
            <span class="error"></span>
        </div>
        <div class="form-group @if($errors->first('email')) has-danger @endif">
            <label for="email">Email</label>
            <input class="form-control @if($errors->first('email')) is-invalid @endif" id="email" name="email" type="text" placeholder="Enter email" autocomplete="off" value="{{old('email')}}">
            @if($errors->first('email'))
                <sapn class="">{{$errors->first('email')}}</sapn>
            @endif
            <span class="error"></span>
        </div>

        <div class="form-group @if($errors->first('password')) has-danger @endif">
            <label for="password">Password</label>
            <input class="form-control @if($errors->first('password')) is-invalid @endif" id="password" name="password" type="password" placeholder="Enter Password" autocomplete="off">
            @if($errors->first('password'))
                <sapn class="">{{$errors->first('password')}}</sapn>
            @endif
            <span class="error"></span>
        </div>
        <div class=" form-group @if($errors->first('gender')) has-danger @endif">
            <label for="gender">Gender</label>
            <div class="form-inline" style="display: flex;">
                <input type="radio" name="gender" value="male"> Male<br>
                <input type="radio" name="gender" value="female" style="margin-left: 20px;"> Female<br>
            </div>

            @if($errors->first('gender'))
                <sapn class="">{{$errors->first('gender')}}</sapn>
            @endif
            <span class="error"></span>
        </div>
        <div class="form-group @if($errors->first('date_of_birth')) has-danger @endif">
            <label for="date_of_birth">Data of birth</label>
            <input class="form-control @if($errors->first('date_of_birth')) is-invalid @endif" id="date_of_birth" name="date_of_birth" type="date" placeholder="Enter date of birth" autocomplete="off" value="{{old('date_of_birth')}}">
            @if($errors->first('date_of_birth'))
                <sapn class="">{{$errors->first('date_of_birth')}}</sapn>
            @endif
            <span class="error"></span>
        </div>

        <div class="form-group @if($errors->first('image')) has-danger @endif">
            <label for="image">Image</label>
            <div class="fake-input">
                <input id="uploadFile" disabled="disabled" type="text" style="display: none;">
                <div class="fileUpload">
                    <input class="form-control-file" id="image" name="image" type="file" accept="image/x-png,image/gif,image/jpeg" required>
                </div>
            </div>

            @if($errors->first('image'))
                <sapn class="">{{$errors->first('image')}}</sapn>
            @endif
            <span class="error"></span>
        </div>

    </div>
    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">

        <div class="form-group @if($errors->first('address_1')) has-danger @endif">
            <label for="address_1">Address </label>
            <input class="form-control @if($errors->first('address_1')) is-invalid @endif" id="address_1" name="address_1" type="text" placeholder="Enter address" autocomplete="off" value="{{old('address_1')}}">
            @if($errors->first('address_1'))
                <sapn class="">{{$errors->first('address_1')}}</sapn>
            @endif
            <span class="error"></span>
        </div>
        <div class="form-group @if($errors->first('aadhar_card_no')) has-danger @endif">
            <label for="aadhar_card_no">Aadhar card number</label>
            <input class="form-control @if($errors->first('aadhar_card_no')) is-invalid @endif" id="aadhar_card_no" name="aadhar_card_no" type="text" placeholder="Enter Aadhar card number" autocomplete="off" value="{{old('aadhar_card_no')}}">
            @if($errors->first('aadhar_card_no'))
                <sapn class="">{{$errors->first('aadhar_card_no')}}</sapn>
            @endif
            <span class="error"></span>
        </div>
        <div class="form-group @if($errors->first('pan_card_no')) has-danger @endif">
            <label for="pan_card_no">PAN Card number</label>
            <input class="form-control @if($errors->first('pan_card_no')) is-invalid @endif" id="pan_card_no" name="pan_card_no" type="text" placeholder="Enter PAN card number" autocomplete="off" value="{{old('pan_card_no')}}">
            @if($errors->first('pan_card_no'))
                <sapn class="">{{$errors->first('pan_card_no')}}</sapn>
            @endif
            <span class="error"></span>
        </div>
        <div class="form-group @if($errors->first('district_state')) has-danger @endif">
            <label for="district_state">District state</label>
            <input class="form-control @if($errors->first('district_state')) is-invalid @endif" id="district_state" name="district_state" type="text" placeholder="Enter district state" autocomplete="off" value="{{old('district_state')}}">
            @if($errors->first('district_state'))
                <sapn class="">{{$errors->first('district_state')}}</sapn>
            @endif
            <span class="error"></span>
        </div>
        <div class="form-group @if($errors->first('country')) has-danger @endif">
            <label for="country">Country</label>
            <input class="form-control @if($errors->first('country')) is-invalid @endif" id="country" name="country" type="text" placeholder="Enter country" autocomplete="off" value="{{old('country')}}">
            @if($errors->first('country'))
                <sapn class="">{{$errors->first('country')}}</sapn>
            @endif
            <span class="error"></span>
        </div>
        <div class="form-group @if($errors->first('location')) has-danger @endif">
            <label for="location">Location</label>
            <input class="form-control @if($errors->first('location')) is-invalid @endif" id="location" name="location" type="text" placeholder="Enter location" autocomplete="off" value="{{old('location')}}">
            @if($errors->first('location'))
                <sapn class="">{{$errors->first('location')}}</sapn>
            @endif
            <span class="error"></span>
        </div>
        <div class="form-group @if($errors->first('latitude')) has-danger @endif">
            <label for="latitude">Latitude</label>
            <input class="form-control @if($errors->first('latitude')) is-invalid @endif" id="latitude" name="latitude" type="text" placeholder="Enter latitude" autocomplete="off" value="{{old('latitude')}}">
            @if($errors->first('latitude'))
                <sapn class="">{{$errors->first('latitude')}}</sapn>
            @endif
            <span class="error"></span>
        </div>
        <div class="form-group @if($errors->first('longitude')) has-danger @endif">
            <label for="longitude">Longitude</label>
            <input class="form-control @if($errors->first('longitude')) is-invalid @endif" id="longitude" name="longitude" type="text" placeholder="Enter longitude" autocomplete="off" value="{{old('longitude')}}">
            @if($errors->first('longitude'))
                <sapn class="">{{$errors->first('longitude')}}</sapn>
            @endif
            <span class="error"></span>
        </div>
        <div class="prof-img" id="prof_img">
            <img src="{{asset('media/no-image/no-image.png')}}" class=" img-fluid img-thumbnail" alt="San Fran" style="height:200px;">
        </div>
    </div>

</div>