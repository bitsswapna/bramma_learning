@extends('admin.layouts.app') @section('css')
    <link href="{{asset('css/plugin/select2.min.css')}}" rel="stylesheet" />
    <link href="{{asset('css/plugin/selectize.bootstrap2.css')}}" rel="stylesheet" />

@endsection
@section('content')
    <main class="app-content">
        <!-- Sub Title---->
        <div id="submenu_subheader" class="submenu-subheader submenu-grid__item ">
            <div class="submenu-subheader__title">
                <h4 class="title-caption"> <i class="fa fa-user"></i>  User Management</h4>
            </div>
            <div class="submenu-subheader__toolbar">
                <div class="submenu-subheader__toolbar-wrapper bs-component">
                    @if(can('browse_admin_user'))

                        <a data-toggle="tooltip" data-placement="top" data-html="true" title="User List" class="btn bg-primary-green btn-sm waves-effect waves-light btn-right br-0" href="{{url('admin_users')}}"><i class="fa fa-list"></i>User List</a>
                    @endif
                </div>
            </div>
        </div>        <!-- END Sub Title---->
        <form method="post" id="editAdminUser" enctype="multipart/form-data" name="editAdminUser">
            <div class="row">
                <div class=" col-md-12 col-sm-12 col-xs-12">
                    <div class="tile">
                        <div class="tile-body br-0">
                            <div class="slide-tabs">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a data-toggle="tab" href="#personal-details"> <i class="fa fa-list"></i> Personal Details</a>
                                    </li>
                                   
                                   
                                </ul>
                                <div class="tab-content">
                                    <!-- Product Primary Details TAB -->
                                    <div id="personal-details" class="tab-pane fade in active">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                                {{csrf_field()}}
                                                <input type="hidden" value="{{$user->id}}" id="user_id">
                                                <div class="form-group @if($errors->first('role_id')) has-danger @endif">
                                                    <label for="role_id">Role</label>
                                                    @if($user->role_id == 4)
                                                        <select name="cu_wa_id" id="cu_wa_id" class="form-control br-0">
                                                            <option value="">Staff</option>
                                                           
                                                        </select>
                                                    @else
                                                        {!! Form::select('role_id',$roles,$user->role_id,(['class'=>'form-control br-0','placeholder' => 'Please Choose','id' => 'role_id','disabled'=>'true'])) !!}

                                                    @endif
                                                    <span class="error"></span>

                                                </div>
                                                <input type="hidden" name="role_id" value="{{$user->role_id}}">
                                                <div class="form-group" id="new-form-append">
                                                    @if ($user->role_id == 5)
                                                        <label for="customer_care_category">Customer care category</label>

                                                        {!! Form::select('customer_care_category',$customer_care_category,$user->getCustomerCareCategory->customer_care_category_id,(['class'=>'form-control','placeholder' => 'Please Choose'])) !!}
                                                        <span class="error"></span> @endif
                                                </div>
                                                <div class="form-group @if($errors->first('first_name')) has-danger @endif">
                                                    <label for="first_name">First Name</label>
                                                    <input class="form-control br-0 @if($errors->first('first_name')) is-invalid @endif" id="first_name" name="first_name" type="text" placeholder="Enter first name" autocomplete="off" value="{{$user->first_name}}"> @if($errors->first('first_name'))
                                                        <sapn class="">{{$errors->first('first_name')}}</sapn>
                                                    @endif
                                                    <span class="error"></span>
                                                </div>
                                                <div class="form-group @if($errors->first('last_name')) has-danger @endif">
                                                    <label for="last_name">Last Name</label>
                                                    <input class="form-control br-0 @if($errors->first('last_name')) is-invalid @endif" id="last_name" name="last_name" type="text" placeholder="Enter last name" autocomplete="off" value="{{$user->last_name}}"> @if($errors->first('last_name'))
                                                        <sapn class="">{{$errors->first('last_name')}}</sapn>
                                                    @endif
                                                    <span class="error"></span>
                                                </div>
                                                <div class="form-group @if($errors->first('mobile')) has-danger @endif">
                                                    <label for="mobile">Mobile</label>
                                                    <input class="form-control br-0 @if($errors->first('mobile')) is-invalid @endif" id="mobile" name="mobile" type="text" placeholder="Enter mobile number" autocomplete="off" value="{{$user->mobile}}"> @if($errors->first('mobile'))
                                                        <sapn class="">{{$errors->first('mobile')}}</sapn>
                                                    @endif
                                                    <span class="error"></span>
                                                </div>
                                                <div class="form-group @if($errors->first('email')) has-danger @endif">
                                                    <label for="email">Email</label>
                                                    <input class="form-control br-0 @if($errors->first('email')) is-invalid @endif" id="email" name="email" type="text" placeholder="Enter email" autocomplete="off" value="{{$user->email}}" > @if($errors->first('email'))
                                                        <sapn class="">{{$errors->first('email')}}</sapn>
                                                    @endif
                                                    <span class="error"></span>
                                                </div>
                                                <div class=" form-group @if($errors->first('gender')) has-danger @endif">
                                                    <label for="gender">Gender</label>
                                                    <div class="form-inline" style="display: flex;">
                                                        <input type="radio" name="gender"  value="1"  @if($user)  @if($user->gender == 1) checked @endif  @endif > Male
                                                        <br>
                                                        <input type="radio" name="gender" value="2"  @if($user)   @if($user->gender == 2) checked @endif @endif  style="margin-left: 20px;"> Female
                                                        <br>
                                                    </div>
                                                    @if($errors->first('gender'))
                                                        <sapn class="">{{$errors->first('gender')}}</sapn>
                                                    @endif
                                                    <span class="error"></span>
                                                </div>
                                               
                                                <div class="form-group @if($errors->first('image')) has-danger @endif">
                                                    <label for="image">Image</label>
                                                    <div class="fake-input">
                                                        <input id="uploadFile" type="text" style="display: none;">
                                                        <div class="fileUpload">
                                                            <input class="form-control-file" id="image" name="image" type="file" accept="image/x-png,image/gif,image/jpeg">
                                                        </div>
                                                    </div>
                                                    @if($errors->first('image'))
                                                        <sapn class="">{{$errors->first('image')}}</sapn>
                                                    @endif
                                                    <span class="error"></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-12 col-xs-12">
                                                <div class="form-group @if($errors->first('address_1')) has-danger @endif">
                                                    <label for="address_1">Address  </label>
                                                    <input class="form-control br-0 @if($errors->first('address_1')) is-invalid @endif" id="address_1" name="address" type="text" placeholder="Enter address" autocomplete="off" value="{{$user->address}}"> @if($errors->first('address_1'))
                                                        <sapn class="">{{$errors->first('address_1')}}</sapn>
                                                    @endif
                                                    <span class="error"></span>
                                                </div>
                                              
                                               
                                               
                                                <div class="form-group @if($errors->first('new_password')) has-danger @endif">
                                                    <label for="new_password">New Password</label>
                                                    <input class="form-control br-0 @if($errors->first('new_password')) is-invalid @endif" id="new_password" name="new_password" type="password" placeholder="Enter new password" autocomplete="off" > @if($errors->first('new_password'))
                                                        <sapn class="">{{$errors->first('new_password')}}</sapn>
                                                    @endif
                                                    <span class="error"></span>
                                                </div>
                                                <div class="form-group @if($errors->first('confirm_password')) has-danger @endif">
                                                    <label for="confirm_password">Confirm  Password</label>
                                                    <input class="form-control br-0 @if($errors->first('confirm_password')) is-invalid @endif" id="confirm_password" name="confirm_password" type="password" placeholder="Enter confirm password" autocomplete="off" > @if($errors->first('confirm_password'))
                                                        <sapn class="">{{$errors->first('confirm_password')}}</sapn>
                                                    @endif
                                                    <span class="error"></span>
                                                </div>
                                                <div class="prof-img " id="prof_img">
                                                    @if ($user->avatar != "")
                                                        <img src="{{getImageByPath($user->avatar,'50x50','profile-images')}}" class=" img-fluid img-thumbnail br-0" alt="San Fran" style="height:200px;">
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tile-footer text-right">
                                            <button id="submit-btn" class="btn btn-primary waves-effect waves-light btn-sm br-0" type="submit">Save</button>
                                        </div>
                                    </div>
                                   
                                    <div id="permissions" class="tab-pane fade">
                                        <!-- Permissions USer-->
                                        <div class="box-container">

                                            <div class="row">
                                              
                                              

                                            </div>
                                            <div class="tile-footer text-right">
                                                <button class="btn btn-primary waves-effect waves-light btn-sm br-0" type="submit">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        </div>
    </main>
@endsection @section('script')
    <script src="{{asset('admin/js/plugins/selectize.min.js')}}"></script>

    <script src="{{asset('admin/assets/js/custom/admin-user.js')}}"></script>
    <script src="{{asset('admin/assets/js/sweet-alert/sweetalert.min.js')}}"></script>

    <script>
        var no_image_url = "{{url('media/no-image/no-image.png')}}"
        $("#cu_wa_id").change(function () {

            swal({
                title: "Are you sure?",
                text: "Please Confirm before changing role!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then(function(willDelete){
                    if (willDelete) {
                        // if (this.value == 1){
                            var user_id = {{$user->id}}
                            $.ajax({
                                url: base_url + "/admin_user/change_c_to_wa",
                                type: 'POST',
                                data: {user_id: user_id },
                                beforeSend: function() {
                                },
                                success: function(data) {
                                    location.reload();
                                },
                                complete: function() {
                                },
                                error: function() {
                                    swal("Something went wrong.Please try again...!")
                                }
                            });
                        // }
                    }else {
                        location.reload();
                    }
                });



        })
    </script>
    <!--Notifications Message Section-->
    @include('admin.layouts.notifications') @endsection
