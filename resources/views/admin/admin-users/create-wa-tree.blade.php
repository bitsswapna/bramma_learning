{{--@extends('admin.layouts.app')--}}

{{--@section('css')--}}
    {{--<link href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap2.css" rel="stylesheet" type="text/css"/>--}}

{{--@endsection--}}

{{--@section('content')--}}
    <main class="app-content">
        <div class="app-title">
            <div class="col-md-6 col-sm-12 col-xs-12 rem-pad">
                <h1><i class="fa fa-plus-circle"></i> Create User</h1>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 btn-right rem-pad">
                @if(can('browse_admin_user'))
                    {{--<a class="btn btn-success waves-effect waves-light" href="{{url('admin_users')}}"><i class="fa fa-list"></i>Admin User Lists</a>--}}
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="tile">
                    <div class="tile-body shadow-none text-black" >
                        {{--<form method="post" id="createAdminUser" enctype="multipart/form-data" name="createAdminUser">--}}
                            {{--{{csrf_field()}}--}}
                            <div class="row" style="display: none;">
                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">


                                    <div class="form-group @if($errors->first('role_id')) has-danger @endif">
                                        <label for="role_id">Role</label>
                                        <select class="form-control @if($errors->first('role_id')) is-invalid @endif" id="role_id" name="role_id" >
                                            <option value="1" selected="selected">
                                                WA
                                            </option>
                                        </select>
                                        <span class="error"></span>
                                    </div>
                                </div>
                            </div>
                            <div id="new-form">
                                @include('admin.admin-users.wellness-advisor-form')
                            </div>
                            <div class="tile-footer">
                                {{--<button class="btn btn-primary waves-effect waves-light" type="submit" >Save</button>--}}
                            </div>
                        {{--</form>--}}
                    </div>
                </div>
            </div>
        </div>
    </main>
{{--@endsection--}}

{{--@section('script')--}}
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js"></script>--}}
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>--}}
{{--    <script src="{{asset('admin/assets/js/custom/admin-user.js')}}"></script>--}}
    {{--<script>--}}
        {{--var no_image_url = "{{url('media/no-image/no-image.png')}}";--}}
    {{--</script>--}}
    <!--Notifications Message Section-->
{{--    @include('admin.layouts.notifications')--}}

{{--@endsection--}}