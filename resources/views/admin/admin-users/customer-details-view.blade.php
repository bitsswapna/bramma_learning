<!-- Profile -->
<div class="text-center">
    <div class="md-header">
        <!-- Image Header -->
        <div class="member-card">
           
        </div>
        <div class="">
            <h4>{{$users->name}}</h4>
            <p class="text-muted mb-2">{{$users->mobile}}</p>
            <p class="text-muted mb-2">{{$users->id}}</p>
        </div>
    </div>
    <!-- Image Header -->
</div>

<div class="pt-20">

    <div class="nav-center">
        <ul class="nav nav-pills" role="tablist">
            <!-- Tab Profile-->
            <li class="nav-item active">
                <a class="nav-item nav-link " id="profile1-tab" data-toggle="tab" href="#profile1" role="tab" aria-controls="profile1" aria-selected="true">
                    <span class="d-block d-sm-none"><i class="fas fa-user"></i></span>
                    <span class="d-none d-sm-block">Profile</span>
                </a>
            </li>
            <!-- END Tab -->
            <!-- Tab Bank -->
          
        <!-- END Bank -->
        </ul>
    </div>
    <!-- Tab Content -->
   
            <div class="table-container">
                <table class="w-100">
                    <tbody>

                    <tr>
                        <td style="width:10%;" class="text-center"><i class="fas fa-phone-volume"></i></i></td>
                        <td style="width:90%">{{$users->mobile}}</td>
                    </tr>
                    <tr>
                        <td><i class="fas fa-envelope"></i></td>
                        <td>{{$users->email}}</td>
                    </tr>
                    <tr>
                       
                    </tr>
                </table>
            </div>
            {{-- <p class="text-muted font-13"><strong>Name :</strong> <span class="m-l-15">{{$users->name}}</span></p>
            <p class="text-muted font-13"><strong>Mobile :</strong><span class="m-l-15">{{$users->mobile}}</span></p>
            <p class="text-muted font-13"><strong>Email :</strong> <span class="m-l-15">{{$users->email}}</span></p>
            <p class="text-muted font-13"><strong>Address :</strong> <span class="m-l-15">{{$users->address}}</span></p> --}}
        </div>
      
    <!-- End Tab Content -->
    </div>
</div>               
</div>
</div>

<!-- END Profile -->
