@extends('admin.layouts.app')
@section('css')
    <link href="{{asset('admin/assets/plugins/jquery-datatables-editable/datatables.css')}}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    <main class="app-content bg-white">
        <!-- Sub Title---->
        <div id="submenu_subheader" class="submenu-subheader submenu-grid__item ">
            <div class="submenu-subheader__title">
                <h4 class="title-caption"> <i class="fa fa-user"></i> User Management</h4>
            </div>
            <div class="submenu-subheader__toolbar">
                <div class="submenu-subheader__toolbar-wrapper bs-component">
                    @if(can('add_admin_user'))
                        <span id="toggle" data-toggle="tooltip" data-placement="top" data-html="true" title="filter here"  class="btn bg-default-light btn-sm waves-effect waves-light br-0  m-b-10"> 
                            <i class="fa fa-filter"></i>
                       </span>
                        {{--<a data-toggle="tooltip" data-placement="top" data-html="true" title="WA" class="btn bg-primary-blue btn-sm waves-effect waves-light br-0 m-b-10" href="{{url('admin_user/wa-request')}}" role="button"><i class="fa fa-list"></i>WA Registerd From website</a>--}}
                        <!-- <a data-toggle="tooltip" data-placement="top" data-html="true" title="User Registerd From website" class="btn bg-primary-blue btn-sm waves-effect waves-light br-0 m-b-10" href="{{url('admin_user/user-request')}}" role="button"><i class="fa fa-list"></i>Users Registered</a> -->
                        <a data-toggle="tooltip" data-placement="top" data-html="true" title="Create Users" class="btn bg-primary-green btn-sm waves-effect waves-light br-0  m-b-10" href="{{url('admin_user/create')}}" role="button"><i class="fa fa-plus-circle"></i>Create User</a>
                        <!-- <a data-toggle="tooltip" data-placement="top" data-html="true" title="Driver" class="btn bg-primary-blue btn-sm waves-effect waves-light br-0 m-b-10" href="{{url('admin-drivers')}}" role="button"><i class="fa fa-list"></i>Driver</a>
                        <a data-toggle="tooltip" data-placement="top" data-html="true" title="Cleaner" class="btn bg-primary-blue btn-sm waves-effect waves-light br-0 m-b-10" href="{{url('admin-cleaner')}}" role="button"><i class="fa fa-list"></i>Cleaner</a> -->
                        <a data-toggle="tooltip" data-placement="top" data-html="true" title="User Registerd From website" class="btn bg-primary-blue btn-sm waves-effect waves-light br-0 m-b-10" href="{{url('admin_user/user-request')}}" role="button"><i class="fa fa-list"></i>Users Registered</a>
                        <!-- <a data-toggle="tooltip" data-placement="top" data-html="true" title="Create Users" class="btn bg-primary-green btn-sm waves-effect waves-light br-0  m-b-10" href="{{url('admin_user/send-greetings')}}" role="button"><i class="fa fa-plus-circle"></i>Send Greetings</a> -->
                                                                     
                     @endif

                </div>
            </div>
        </div>
        <!-- END Sub Title---->
        <!-- Body Part-->
        <div class="sub-header-body">
            @if(can('browse_admin_user'))          
                <div class="filter-scroll" style="display: none" >
                    <div class="row">
                        <div class="bg-light izxnliner col-sm-10  col-md-6 col-md-offset-3 col-sm-offset-1 card-outline text-center padd-justify ">
                          
                             <form class="form-inline " >
                                 <div class="form-group">
                                     <input type="text" class="form-control br-0 user_id input_keys input-sm-2" id="user_id" placeholder="User ID" >
                                 </div>
                                 <div class="form-group">
                                     {{-- <label for="user_name">Name</label> --}}
                                     <input type="text" class="form-control br-0 input_keys user_name input-sm-2" id="user_name" placeholder="User Name" >
                                 </div>
                                 <button type="button" class="btn btn-primary filter_button br-0 minus-mt-6 " >Filter</button>
                             </form>
                            </div>
                                 
                    </div>                    
                               
                </div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="tile">
                            {{-- <div class="data--table br-0"> --}}
                                <table  id="sampleTable" class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Sl#</th>
                                            <th>User Id</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Role</th>
                                            <th>Address</th>
                                            
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            {{-- </div> --}}
                        </div>
                    </div>
                </div>
            @else
                @include('admin.no-access-content')
            @endif
        </div>
    </main>


     
    <!-- Modal -->
    <div class="profile-modal">
        <div id="myModal_users" class="modal fade user-modal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body details-body" >              
                    </div>
                    <div class="modal-footer text-center">
                        <div class="text-center">
                            <button type="button" class="btn-link text-uppercase" data-dismiss="modal">Close</button>
                        </div>                     
                    </div>
                </div>
            </div>
        </div> 
    </div>
    @endsection

    @section('script')
        <script>
            $(function()
            {
            
                $("span#toggle").click(function()
                {
                    $(".plus-toggle").toggle();
                    $(".minus-toggle").toggle();
                    $(".filter-scroll").slideToggle();
                    return false;
                });
            });

            /*responsive DataTable */
            $(document).ready(function() {
                $('#sampleTable').DataTable();
                var table = $('#example').DataTable( {
                    responsive: true
                } );  
            
            } );
        </script>

    <script type="text/javascript" src="{{asset('admin/js/plugins/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/select2.min.js')}}"></script>
    <script src="{{asset('admin/js/plugins/selectize.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/assets/js/sweet-alert/sweetalert.min.js')}}"></script>
    <script src="{{asset('website/assets/js/jquery.bpopup.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/admin-js.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/assets/js/custom/admin-user.js')}}"></script>
    <script>
        $(document).on('click', '.change-status', function (e) {
            var token = $('meta[name="csrf-token"]').attr("content");
            var id = $(this).data("id");
            var status = $(this).data('status');

            var data = {id: id, status: status, _token: token};

            var status_html = $(this).html();
            var url = base_url+'/admin-user/change_status';

            $.ajax({
                url: url, type: 'POST', data: data,
                beforeSend: function () {

                },
                success: function (data) {
                    successMsg(data.msg);
                },
                error: function (data) {
                    errorMsg(data.responseJSON.msg);
                },
                complete: function () {

                }
            });
        });


    </script>
@endsection