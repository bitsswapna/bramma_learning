@extends('admin.layouts.app')

@section('css')
    <link href="{{asset('admin/assets/plugins/jquery-datatables-editable/datatables.css')}}" rel="stylesheet" type="text/css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap2.css" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    <main class="app-content bg-white">
       <!-- Sub Title---->
        <div id="submenu_subheader" class="submenu-subheader submenu-grid__item ">
            <div class="submenu-subheader__title">
                <h4 class="title-caption"> <i class="fa fa-user"></i> Admin & Staff Management</h4>
            </div>
            <div class="submenu-subheader__toolbar">
                <div class="submenu-subheader__toolbar-wrapper">
                    @if(can('browse_admin_user'))
                        <a data-toggle="tooltip" data-placement="top" data-html="true" title="Create User" class="btn bg-primary-green btn-sm waves-effect waves-light inliner btn-right mt--6 br-0"  href="{{url('admin_user/create')}}" role="button"><i class="fa fa-plus"></i>Create User</a>
                        {{--<a data-toggle="tooltip" data-placement="top" data-html="true" title="WA Registerd From website" class="btn bg-primary-blue btn-sm waves-effect waves-light inliner btn-right mt--6 br-0" href="{{url('admin_user/wa-request')}}" role="button"><i class="fa fa-list"></i>WA Registerd From website</a>--}}
                        <a data-toggle="tooltip" data-placement="top" data-html="true" title="All User" class="btn bg-primary-blue btn-sm waves-effect waves-light inliner btn-right mt--6 br-0" href="{{url('admin_users')}}" role="button"><i class="fa fa-list"></i>All User</a>
                        <a data-toggle="tooltip" data-placement="top" data-html="true" title="Driver" class="btn bg-primary-blue btn-sm waves-effect waves-light inliner btn-right mt--6 br-0" href="{{url('admin-drivers')}}" role="button"><i class="fa fa-list"></i>Driver</a>
                        <a data-toggle="tooltip" data-placement="top" data-html="true" title="Cleaner" class="btn bg-primary-blue btn-sm waves-effect waves-light inliner btn-right mt--6 br-0" href="{{url('admin-cleaner')}}" role="button"><i class="fa fa-list"></i>Cleaner</a>
                    @endif
                </div>
            </div>
        </div>
        <!-- END Sub Title---->
        <!-- Body Part -->
        <div class="sub-header-body">
            @if(can('browse_admin_user'))
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="table-responsive">
                            <table id="adminUserWebsiteUser" class="table table-bordered" >
                                <thead>
                                <tr>
                                    <th>Sl#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                  
                                    <th>Mobile</th>
                                  
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            @else
                @include('admin.no-access-content')
            @endif
        </div>
        <!-- End Body-->
    </main>
@endsection

@section('script')
          
    <script type="text/javascript" src="{{asset('admin/js/plugins/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/dataTables.bootstrap.min.js')}}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{asset('admin/js/plugins/selectize.min.js')}}"></script>
 

    <script type="text/javascript">
    
        $(document).ready(function(){
           
      
            var table = $('#adminUserWebsiteUser').DataTable({
                
                "aaSorting": [],
                'order': [[1, 'desc']],
                "fnDrawCallback": function (oSettings) {
                    if (oSettings.bSorted || oSettings.bFiltered || oSettings._iDisplayLength) {
                        j = 0;
                        for (var i = oSettings._iDisplayStart; i < oSettings.aiDisplay.length + oSettings._iDisplayStart; i++) {
                            $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[j]].nTr).find('span').text(i + 1);
                            j++;
                        }
                    }
                },
                serverSide: true,
                scrollX: true,
                oLanguage: {
                    sProcessing: '<svg width="30px"  height="30px"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-rolling" style="background: transparent;"><circle cx="50" cy="50" fill="none" stroke="#0fb963" stroke-width="10" r="35" stroke-dasharray="164.93361431346415 56.97787143782138" transform="rotate(228 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform></circle></svg>'
                },
                processing: true,
                "pageLength": 10,
                "columnDefs": [{'orderable': false, 'targets': [0, 4, 4]}],
                mark: true,
                fixedColumns: {},
                ajax: {
                    url: '{!!URL::to("get-admin-drivers")!!}',
                },
              
                columns: [
                    {data: 'sl#', name: 'sl#'},
                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email'},          
                    {data: 'role', name: 'role'},
                   
                    {data: 'mobile', name: 'mobile'},
                  
                ]
            });
        });

           
    </script>
@endsection