@extends('admin.layouts.app')

@section('css')

@endsection

@section('content')
    <main class="app-content bg-white ">
    <div id="submenu_subheader" class="submenu-subheader submenu-grid__item ">
        <div class="submenu-subheader__title">
            <h4 class="title-caption"><i class="fas fa-list-alt"></i>  Add Vehicle Details</h4>
        </div>
        <div class="submenu-subheader__toolbar">
            <div class="submenu-subheader__toolbar-wrapper bs-component">
            @if(can('browse_roles'))
                <a data-toggle="tooltip" data-placement="top" data-html="true" title="" class="btn bg-primary-blue btn-sm waves-effect waves-light btn-right br-0" href="{{url('Vehicles')}}" role="button" data-original-title="Vehicle Lists">
                    <i class="fa fa-list"></i>Vehicles
                </a>
                @endif
            </div>
        </div>
    </div>




    <div class="row">
    <div class="col-lg-10 col-md-8 col-sm-8 col-xs-8">
                <div class="tile">
                    <form method="post" id="createrRole" name="createrRole" action="{{url('details/insert')}}">
                                {{csrf_field()}}

                          <div class="col-sm-8 col-lg-9 col-xs-12 m-b-20">
                                         <label for="Type">Type</label>
                                         <div class="form-group bmd-form-group">
                                           <select  class="form-control br-0" id="menu-item"  name="type" autocomplete="off">
                                               <option value="1"selected >Rent</option>
                                               <option value="2"selected >Partnership</option>
                                               <option value="3"selected>Own Vehicle</option>


                                           </select>
                                </div>
                                </div>



        <div class="form-group" id="FieldGroupDiv">

         <div class="col-sm-8 col-lg-9 col-xs-12 m-b-20">
        <label for="field1">Name</label>
        <input type="text" class="form-control @if($errors->first('name')) is-invalid @endif" id="field1" type="text" name="name" placeholder="Enter name"   autocomplete="off" value="{{old('name')}}">
        @if($errors->first('name'))
        <sapn class="error">{{$errors->first('name')}}</sapn>
             @endif
      </div>

      <div class="col-sm-8 col-lg-9 col-xs-12 m-b-20">
        <label for="field2">Address</label>
        <textarea type="text" class="form-control @if($errors->first('adress')) is-invalid @endif" id="field2" name="adress" placeholder="Enter Address"  autocomplete="off" value="{{old('address')}}"></textarea>
        @if($errors->first('adress'))
        <sapn class="error">{{$errors->first('adress')}}</sapn>
             @endif
      </div>



      <div class="col-sm-8 col-lg-9 col-xs-12 m-b-20">
        <label for="field3">Contact Number</label>
        <input  class="form-control @if($errors->first('number')) is-invalid @endif" type="text" id="field3" name="number" placeholder="Enter Contact number"  autocomplete="off" value="{{old('contact_number')}}" >
        @if($errors->first('number'))
        <sapn class="error">{{$errors->first('number')}}</sapn>
             @endif
      </div>

    </div>



                                  <div class="col-sm-8 col-lg-9 col-xs-12 m-b-20">
                                    <label for="name">Vehicle Number</label>
                                    <input class="form-control @if($errors->first('vehinumber')) is-invalid @endif" id="name" type="text" name="vehinumber" placeholder="Enter Vehicle Number"   autocomplete="off" value="{{old('vehicle_number')}}">
                                    @if($errors->first('vehinumber'))
                                        <sapn class="error">{{$errors->first('vehinumber')}}</sapn>
                                    @endif
                                </div>
                                <div class="col-sm-8 col-lg-9 col-xs-12 m-b-20">
                                    <label for="name">Hourly Amount</label>
                                    <input class="form-control @if($errors->first('hrlyamount')) is-invalid @endif" id="name" type="text" name="hrlyamount" placeholder="Enter the Hourly amount"  autocomplete="off" value="{{old('hourly_amount')}}">
                                    @if($errors->first('hrlyamount'))
                                        <sapn class="error">{{$errors->first('hrlyamount')}}</sapn>
                                    @endif
                                </div>


                            </div>
                            </div>
                            </div>


                            <div class="tile-footer text-right">
                            <button class="btn btn-primary waves-effect waves-light" type="submit">Save</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </main>


@endsection


@section('js')
    <!--Notifications Message Section-->
    @include('admin.layouts.notifications')


@endsection

    <script src="http://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/dataTables.bootstrap.min.js')}}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{asset('admin/js/plugins/selectize.min.js')}}"></script>


<script>

$(document).ready(function(){
    $("#menu-item").change(function()
   {
     if ($(this).val() == "1" || $(this).val() == "2") {
         $('#FieldGroupDiv').show();
      $('#field1').attr('required', '');
      $('#field1').attr('data-error', 'This field is required.');
      $('#field2').attr('required', '');
      $('#field2').attr('data-error', 'This field is required.');
      $('#field3').attr('required', '');
      $('#field3').attr('data-error', 'This field is required.');
     }

     else{
         $('#FieldGroupDiv').hide();
    $('#field1').removeAttr('required');
    $('#field1').removeAttr('data-error');
    $('#field2').removeAttr('required');
    $('#field2').removeAttr('data-error');
    $('#field3').removeAttr('required');
    $('#field3').removeAttr('data-error');
     }

   });
   $("#menu-item").trigger("change");



});

</script>
