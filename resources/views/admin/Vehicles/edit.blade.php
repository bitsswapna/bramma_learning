@extends('admin.layouts.app')

@section('css')
    <style>
        ul {
            list-style-type: none;
        }

        span.label-text::first-letter {
            text-transform: uppercase;
        }
    </style>
@endsection

@section('content')
    <main class="app-content bg-white">
        <!-- Sub Title---->
        <div id="submenu_subheader" class="submenu-subheader submenu-grid__item ">          
            <div class="submenu-subheader__title">
                <h4 class="title-caption"><i class="fas fa-user-shield"></i>  Edit Details</h4>
            </div>           
            <div class="submenu-subheader__toolbar">
                <div class="submenu-subheader__toolbar-wrapper">
                    <a  data-toggle="tooltip" data-placement="left" data-html="true"  title="edit List" class="btn bg-primary-blue btn-sm waves-effect waves-light  br-0" href="{{url('Vehicles')}}">
                        <i class="fa fa-list"></i>Vehicle List</a>
                </div>
            </div>
        </div>
        <!-- Body Part -->
        <div class="sub-header-body">
            <form method="post" action="{{url('vehicle/update/'.$user->id)}}" name="editdata" >
                {{csrf_field()}}
                <div class="row">
                <div class="col-lg-10 col-md-8 col-sm-8 col-xs-8">
                 <div class="tile">
                        {{csrf_field()}}
                       
                      
                  
                    
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="name">Vehicle Number</label>
                            <input class="form-control @if($errors->first('vehinumber')) is-invalid @endif" id="name" type="text" name="vehinumber" placeholder="Enter vehicle number" autocomplete="off"  value="{{$user->vehicle_number}}">
                            @if($errors->first('vehinumber'))
                                        <sapn class="error">{{$errors->first('vehinumber')}}</sapn>
                                    @endif
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input class="form-control @if($errors->first('name')) is-invalid @endif" id="name" type="text" name="name" placeholder="Enter name" autocomplete="off"  value="{{$user->name}}">
                            @if($errors->first('name'))
                         <sapn class="error">{{$errors->first('name')}}</sapn>
                                 @endif
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="name">Contact Number</label>
                            <input class="form-control @if($errors->first('number')) is-invalid @endif" id="name" type="text" name="number" placeholder="Enter contact number"  autocomplete="off"  value="{{$user->contact_number}}">
                            @if($errors->first('number'))
                       <sapn class="error">{{$errors->first('number')}}</sapn>
                              @endif
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="name">Hourly Amount</label>
                            <input class="form-control @if($errors->first('hrlyamount')) is-invalid @endif" id="name" type="text" name="hrlyamount" placeholder="Enter Hourly amount" autocomplete="off"  value="{{$user->hourly_amount}}">
                            @if($errors->first('hrlyamount'))
                                        <sapn class="error">{{$errors->first('hrlyamount')}}</sapn>
                                    @endif
                        </div>
                    </div>
                  
                    
                    
                </div>    

              
                        </table>
                    </div>
                </div>
                <div class="sub-header-footer text-right">
                    <button class="btn bg-primary-blue br-0  waves-effect waves-light btn-sm" type="submit">Update</button>
                </div>
                
            </form>
        </div>
        <!-- Footer Part END-->

        <!-- Footer Part END-->      
    </main>
@endsection

@section('script')

    <!--Notifications Message Section-->
    @include('admin.layouts.notifications')
    <script>
        $("#editRole").validate({
            rules: {
                // simple rule, converted to {required:true}
                display_name: "required",
                name: "required",
            }
        });
    </script>

    <script>
        $('document').ready(function () {

            $('.permission-group').on('change', function(){
                $(this).parents('tr').find(".the-permission").prop('checked', this.checked);
            });

            function parentChecked(){
                $('.permission-group').each(function(){
                    var allChecked = true;
                    $(this).parents('tr').find(".the-permission").each(function(){
                        if(!this.checked) allChecked = false;
                    });
                    $(this).prop('checked', allChecked);
                });
            }

            parentChecked();

            $('.the-permission').on('change', function(){
                parentChecked();
            });
        });
    </script>
@endsection