@extends('admin.layouts.app')

@section('css')

@endsection

@section('content')
    <main class="app-content">
        <div class="app-title">
            <div>
                <h1><i class="fa fa-pencil-square-o"></i> Update your details</h1>
                <p>Form for update your detail</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="tile">
                    <h3 class="tile-title">Update</h3>
                    <form method="post" name="editAdminUser" id="editAdminUser" action="{{url('profile')}}" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-lg-6">
                                {{csrf_field()}}
                                <div class="form-group @if($errors->first('first_name')) has-danger @endif">
                                    <label for="first_name">First Name</label>
                                    <input class="form-control @if($errors->first('first_name')) is-invalid @endif" id="first_name" name="first_name" type="text" placeholder="Enter first name" required autocomplete="off" value="{{$user->first_name}}">
                                    @if($errors->first('first_name'))
                                        <sapn class="form-control-feedback">{{$errors->first('first_name')}}</sapn>
                                    @endif
                                </div>
                                <div class="form-group @if($errors->first('last_name')) has-danger @endif">
                                    <label for="last_name">Last Name</label>
                                    <input class="form-control @if($errors->first('last_name')) is-invalid @endif" id="last_name" name="last_name" type="text" placeholder="Enter last name" required autocomplete="off" value="{{$user->last_name}}">
                                </div>
                                    <div class="form-group @if($errors->first('email')) has-danger @endif">
                                        <label for="email">Email</label>
                                        <input class="form-control @if($errors->first('email')) is-invalid @endif" id="email" name="email" type="text" placeholder="Enter email" autocomplete="off" value="{{$user->email}}" readonly="readonly">
                                        @if($errors->first('email'))
                                            <sapn class="form-control-feedback">{{$errors->first('email')}}</sapn>
                                        @endif
                                    </div>

                                {{--<div class="form-group @if($errors->first('password')) has-danger @endif">--}}
                                    {{--<label for="password">Password</label>--}}
                                    {{--<input class="form-control @if($errors->first('password')) is-invalid @endif" id="password" name="password" type="password" placeholder="Enter Password" autocomplete="off">--}}
                                    {{--@if($errors->first('password'))--}}
                                        {{--<sapn class="form-control-feedback">{{$errors->first('password')}}</sapn>--}}
                                    {{--@endif--}}
                                {{--</div>--}}
                                {{--<div class="form-group @if($errors->first('password_confirmation')) has-danger @endif">--}}
                                    {{--<label for="password_confirmation">Confirm password</label>--}}
                                    {{--<input class="form-control @if($errors->first('password_confirmation')) is-invalid @endif" id="password_confirmation" name="password_confirmation" type="password" placeholder="Confirm password" autocomplete="off">--}}
                                    {{--@if($errors->first('password'))--}}
                                        {{--<sapn class="form-control-feedback">{{$errors->first('password_confirmation')}}</sapn>--}}
                                    {{--@endif--}}
                                {{--</div>--}}
                                <div class="form-group">
                                    <label for="image">Image</label>
                                    <div class="fake-input">
                                        <input id="uploadFile" disabled="disabled" type="hidden">
                                        <div class="fileUpload">
                                            <input class="form-control-file" id="image" name="image" type="file" accept="image/x-png,image/gif,image/jpeg">
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="prof-img" id="prof_img">
                                    <img src="{{asset('media/profile-images/'.$user->avatar)}}" class=" img-fluid img-thumbnail" alt="San Fran" style="height:200px;">
                                </div>
                            </div>
                        </div>

                        <div class="tile-footer">
                            <button class="btn btn-primary" type="submit">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>
@endsection

@section('js')

    <!--Notifications Message Section-->
    @include('admin.layouts.notifications')

    <script>
        $("#editAdminUser").validate({
            rules: {
                first_name: "required",
            }
        });
        function readURL(input, img_con) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#'+img_con).find('img').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#image").change(function() {
            readURL(this, 'prof_img');
            $(this).parents('.form-group').find("#uploadFile").val(this.value);
        });
    </script>
@endsection