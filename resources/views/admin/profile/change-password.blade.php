@extends('admin.layouts.app')

@section('css')

@endsection

@section('content')
    <main class="app-content">
        <div class="app-title">
            <div>
                <h1><i class="fa fa-pencil-square-o"></i> Update your password</h1>
                <p>Form for update your password</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="tile">
                    <h3 class="tile-title">Update</h3>
                    <form method="post" name="editAdminUser" id="editAdminUser" action="{{url('change-password')}}" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-lg-6">
                                {{csrf_field()}}

                                <div class="form-group @if($errors->first('current_password')) has-danger @endif">
                                    <label for="current_password">Current Password</label>
                                    <input class="form-control @if($errors->first('current_password')) is-invalid @endif" id="current_password" name="current_password" type="password" placeholder="Enter current password" autocomplete="off">
                                    @if($errors->first('current_password'))
                                        <sapn class="form-control-feedback">{{$errors->first('current_password')}}</sapn>
                                    @endif
                                </div>

                                <div class="form-group @if($errors->first('password')) has-danger @endif">
                                    <label for="password">Password</label>
                                    <input class="form-control @if($errors->first('password')) is-invalid @endif" id="password" name="password" type="password" placeholder="Enter Password" autocomplete="off">
                                    @if($errors->first('password'))
                                        <sapn class="form-control-feedback">{{$errors->first('password')}}</sapn>
                                    @endif
                                </div>
                                <div class="form-group @if($errors->first('password_confirmation')) has-danger @endif">
                                    <label for="password_confirmation">Confirm password</label>
                                    <input class="form-control @if($errors->first('password_confirmation')) is-invalid @endif" id="password_confirmation" name="password_confirmation" type="password" placeholder="Confirm password" autocomplete="off">
                                    @if($errors->first('password'))
                                        <sapn class="form-control-feedback">{{$errors->first('password_confirmation')}}</sapn>
                                    @endif
                                </div>
                            </div>

                        </div>

                        <div class="tile-footer">
                            <button class="btn btn-primary" type="submit">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>
@endsection

@section('js')

    <!--Notifications Message Section-->
    @include('admin.layouts.notifications')

@endsection