@extends('admin.layouts.app')

@section('css')
<style>
    .card {
        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
        transition: 0.3s;
        }

        .card:hover {
        box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
        }
        .related-products{
            min-height:300px;
        }
        .alert-msg-content{
            text-align:center;
        }
        .btn-msgs{
            padding:10px;
        }

    </style>

@endsection

@section('content')
    <main class="app-content bg-white">
      
            <!-- Sub Title---->
            <div id="submenu_subheader" class="submenu-subheader submenu-grid__item ">          
                <div class="submenu-subheader__title">
                    <h4 class="title-caption"><i class="fa fa-file-text"></i> Alert Messages</h4>
                </div>           
                <div class="submenu-subheader__toolbar">
                    <div class="submenu-subheader__toolbar-wrapper bs-component">
                            @if(can('add_alert-messages'))
                            <a data-toggle="tooltip" data-placement="top" data-html="true" title="Create Page" class="btn bg-primary-green btn-sm waves-effect waves-light btn-right br-0" href="{{url('alert-messages/create')}}" role="button"><i class="fa fa-plus-circle"></i>Create Page</a>
                        @endif
                    </div>
                </div>
            </div>          
            <!-- END Sub Title---->

            <!-- Body -->
            <div class="sub-header-body">
                @if(can('browse_alert-messages'))
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="tile">
                            <div class="br-0">
                                    <div class="slide-tabs">
                                <ul class="nav nav-tabs">
                                @if(count($messages)>0)
                                                @foreach($messages as $key => $messages)
                                    <li data-slug="{{$messages->slug}}" class=" tab-alert @if($key == 0 ) ? active : '' @endif "><a data-toggle="tab"  href="#home{{$messages->id}}">{{$messages->cms_title}}</a></li>
                                    @endforeach   
                              @endif
                                </ul>
        
                                <div class="tab-content">
                                    <div id="home" class="tab-pane fade in active">
                                    <div class="row">
                                    @if(count($homeMessage)>0)
                                                @foreach($homeMessage as $key => $homessaage)
                                                <div class= "col-xs-12 col-sm-6 col-md-6 col-lg-4 related-products  min-200  display-home{{$key}}">
                                                        <div class="card-outline-dark">
                                                            <div class="alert-msg-content">
                                                                <div class="row  card-body">
                                                                    <div class="col-sm-12 col-xs-12 text-left text-justify">
                                                                        <h5><strong>{{$homessaage->title}}</strong></h5> 
                                                                        <p>{{$homessaage->message}}</p>
                                                                    </div>
                                                                </div>
                                                            </div>                                                   
                                                            <div class="card-footer">
                                                                <div class="row pl-15">
                                                                    <div class="col-sm-12 col-xs-12 text-right">
                                                                        <div class="btn-msgs">
                                                                            <i data-id={{$key}} class="fa fa-edit edit-home-data btn action-button bg-primary-blue"></i>
                                                                            <i  data-msgid="{{$homessaage->id}}" class="fa fa-trash delete-msg btn action-button bg-primary-red"></i>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
        
        
                                                    <div style="display:none" class= "col-md-3 col-xs-12 col-sm-6 col-md-6 col-lg-4 min-200 related-products edit-home{{$key}}">
                                                            <div class="card-outline-dark">
                                                                    <div class="alert-msg-content">
                                                                        <div class="card-body text-left">
                                                                            <form id="save-home{{$key}}" >
                                                                                {{csrf_field()}}
                                                                                    <div class="form-group ">
                                                                                        <label for="title" class="text-right">Title</label>
                                                                                            <input class="form-control br-0" id="hometitle{{$key}}"  type="text" name="title" value="{{$homessaage->title}}">
                                                                                        </div>
                                                                                        <div class="form-group ">
                                                                                            <label for="message" class="text-right" class="text-right">Message</label>
                                                                                            <input class="form-control br-0" id="homemessage{{$key}}"  type="text" name="message" value="{{$homessaage->message}}">
                                                                                    </div>
                                                                            <form> 
                                                                        </div>
                                                                    </div>                                                   
                                                                    <div class="card-footer">
                                                                        <div class="row pl-15">
                                                                            <div class="col-sm-12 col-xs-12 text-right">
                                                                                <div class="btn-msgs">
                                                                                    <i  data-toggle="tooltip" data-placement="top" data-html="true" title="Save Changes" data-id={{$key}} data-homeid={{$homessaage->id}}  class="fa fa-save save-home-data btn action-button bg-primary-green"></i>
                                                                                    <i  data-toggle="tooltip" data-placement="top" data-html="true" title="Delete"data-msgid="{{$homessaage->id}}" class="fa fa-trash delete-msg btn action-button bg-primary-red"></i>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>    
                                                        
                                                    
                                                    
                                                        <!-- <div class="card">
                                                            <div class="container alert-msg-content">
                                                                <div class="row pull-right btn-msgs">
                                                                    <i  data-id={{$key}}   class="fa fa-save save-home-data" data-homeid={{$homessaage->id}}></i>
                                                                  
                                                                </div>
                                                                <div class="row">
                                                                    <form id="save-home{{$key}}" >
                                                                    {{csrf_field()}}
                                                                        <div class="form-group ">
                                                                            <label for="title">Title</label>
                                                                             <input class="form-control" id="hometitle{{$key}}"  type="text" name="title" value="{{$homessaage->title}}">
                                                                        </div>
                                                                        <div class="form-group ">
                                                                            <label for="message">Message</label>
                                                                             <input class="form-control" id="homemessage{{$key}}"  type="text" name="message" value="{{$homessaage->message}}">
                                                                        </div>
                                                                    <form> 
                                                                </div>
                                                                <div class="row pull-right btn-msgs">
                                                                    <button class="btn btn-success" >ok</button>
                                                                    <button class="btn btn-warning" >cancel</button>
                                                                </div>
                                                            </div>
                                                        </div> -->
                                                    </div>
                                                @endforeach  
                                                @else
                                                    <div class="text-center">
                                                        <div class="col-sm-6 col-xs-12 blank-pg" style="">
                                                                <i class="fa fa-warning"></i>
                                                           
                                                                <h4> No Active Alerts</h4>
                                                        </div>
                                                    </div>
                                            @endif
                                        </div>
                                        
                                    </div>
                                   
                                
                                   
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                @else
                    @include('admin.no-access-content')
                @endif
            </div>
            <!-- End Body -->
        
       
    </main>
@endsection

@section('script')
<script type="text/javascript" src="{{asset('admin/assets/js/custom/alert-messages.js')}}"></script>
<!-- Scroll Tabs -->
<script type="text/javascript" src="{{asset('admin/assets/tabscroll/scroll-navbar.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/assets/js/custom/scrolling-tabs.js')}}"></script>


@endsection