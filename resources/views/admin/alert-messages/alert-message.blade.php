<div class="row">
                            @if(count($homeMessage)>0)
                                        @foreach($homeMessage as $key => $homessaage)
                                         <div class= "col-md-3 col-sm-12 col-xs-12 related-products display-home{{$key}}">
                                                <div class="card">
                                                    <div class="container alert-msg-content">
                                                        <div class="row pull-right btn-msgs">
                                                            <i data-id={{$key}} class="fa fa-edit edit-home-data"></i>
                                                            <i  data-msgid="{{$homessaage->id}}" class="fa fa-trash delete-msg"></i>
                                                        </div>
                                                        <div class="row">
                                                            <h5><b>{{$homessaage->title}}</b></h5> 
                                                            <p>{{$homessaage->message}}</p> 
                                                        </div>
                                                        <div class="row pull-right btn-msgs">
                                                            <button class="btn btn-success" >ok</button>
                                                            <button class="btn btn-warning" >cancel</button>
                                                        </div>
                                                    </div>
                                                </div>
                                         </div>
                                            <div style="display:none" class= "col-md-3 col-sm-12 col-xs-12 related-products edit-home{{$key}}">
                                            <div class="card">
                                                    <div class="container alert-msg-content">
                                                        <div class="row pull-right btn-msgs">
                                                            <i  data-id={{$key}} data-homeid="{{$homessaage->id}}"  class="fa fa-save save-home-data"></i>
                                                          
                                                        </div>
                                                        <div class="row">
                                                            <form id="save-home{{$key}}" >
                                                            {{csrf_field()}}
                                                                <div class="form-group ">
                                                                    <label for="title">Title</label>
                                                                     <input class="form-control" id="hometitle{{$key}}"  type="text" name="title" value="{{$homessaage->title}}">
                                                                </div>
                                                                <div class="form-group ">
                                                                    <label for="message">Message</label>
                                                                     <input class="form-control" id="homemessage{{$key}}"  type="text" name="message" value="{{$homessaage->message}}">
                                                                </div>
                                                            <form> 
                                                        </div>
                                                        <div class="row pull-right btn-msgs">
                                                            <button class="btn btn-success" >ok</button>
                                                            <button class="btn btn-warning" >cancel</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach   
                                    @endif
                                </div>