@extends('admin.layouts.app')

@section('css')

@endsection

@section('content')
    <main class="app-content">
        

        <!-- Sub Title---->
        <div id="submenu_subheader" class="submenu-subheader submenu-grid__item ">          
                <div class="submenu-subheader__title">
                    <h4 class="title-caption"><i class="fa fa-plus-circle"></i> Create Messages</h4>
                </div>           
                <div class="submenu-subheader__toolbar">
                    <div class="submenu-subheader__toolbar-wrapper bs-component">
                            @if(can('browse_alert-messages'))
                            <a data-toggle="tooltip" data-placement="top" data-html="true" title="List" class="btn bg-primary-blue btn-sm waves-effect waves-light btn-right  br-0" href="{{url('alert-messages')}}"><i class="fa fa-list"></i> Lists</a>
                        @endif
                    </div>
                </div>
            </div>          
            <!-- END Sub Title---->

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="tile">
                    <div class="tile-body br-0">
                        <form method="post" action="{{url('alert-messages/create')}}" id="createModule" name="createModule">
                            <div class="row">
                                <div class="col-lg-6 col-md-12 col-xs-12">
                                    {{csrf_field()}}
                                    <div class="form-group  @if($errors->first('pages')) has-danger @endif">
                                        <label for="pages">Pages</label>
                                        {!! Form::select('pages',$pages,'',(['class'=>'form-control br-0','placeholder' => 'Please Choose'])) !!} @if($errors->first('pages'))
                                        <span class="alert-danger">{{$errors->first('pages')}}</span> @endif
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12 col-xs-12">
                                    <div class="form-group @if($errors->first('title')) has-danger @endif">
                                        <label for="title">Title</label>
                                        <input class="form-control br-0 @if($errors->first('title')) is-invalid @endif" id="title" name="title" type="text" placeholder="Enter title"  autocomplete="off" value="{{old('title')}}">
                                        @if($errors->first('title'))
                                        <span class="alert-danger">{{$errors->first('title')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12 col-xs-12">
                                    <div class="form-group @if($errors->first('session_key')) has-danger @endif">
                                        <label for="session_key">Session Key</label>
                                        <select class="form-control br-0 @if($errors->first('session_key')) is-invalid @endif" id="session_key" name="session_key">
                                            <option value="">Select</option>
                                            <option value="complete">Delete</option>
                                            <option value="complete">Complete</option>
                                            <option value="checkout">Checkout</option>
                                            <option value="pc-creation">PC Creation</option>

                                        </select>
                                        @if($errors->first('session_key'))
                                        <span class="alert-danger">{{$errors->first('session_key')}}</span> @endif
                                   </div>
                                </div>
                                <div class="col-lg-6 col-md-12 col-xs-12">
                                   <div class="form-group @if($errors->first('language')) has-danger @endif">
                                        <label for="language">Languages</label>
                                        <select class="form-control br-0 @if($errors->first('language')) is-invalid @endif" id="language" name="language">
                                            <option value="en">English</option>
                                           

                                        </select>
                                        @if($errors->first('language'))
                                        <span class="alert-danger">{{$errors->first('language')}}</span> @endif
                                   </div>
                                </div>
                                <div class="col-sm-12 col-xs-12">
                                    <div class="form-group @if($errors->first('message')) has-danger @endif"">
                                        <label for="comment">Message</label>
                                        <input class="form-control br-0 @if($errors->first('message')) is-invalid @endif" id="message" type="text" name="message" placeholder="Enter module message"  autocomplete="off" value="{{old('message')}}">
                                        @if($errors->first('message'))
                                        <span class="alert-danger">{{$errors->first('message')}}</span>
                                        @endif
                                    
                                    </div>
                                </div>
                                    <!-- <div class="form-group @if($errors->first('message')) has-danger @endif">
                                        <label for="message">Message</label>
                                        <input class="form-control @if($errors->first('message')) is-invalid @endif" id="message" type="text" name="message" placeholder="Enter module message"  autocomplete="off" value="{{old('message')}}">
                                        @if($errors->first('message'))
                                        <span class="alert-danger">{{$errors->first('message')}}</span>
                                        @endif
                                    </div> -->
                                </div>
                                <div class="tile-footer text-right">
                                    <button class="btn bg-primary-blue btn-sm waves-effect waves-light br-0" type="submit">Submit</button>
                                </div>
                                              
                        </form>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection

@section('script')

  
@endsection