<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="admin/css/main.css">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css"
          href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Login - New Project</title>
</head>

<body>
<section class="login-content">
    <div class="login-content--left" id="particles-js">
        <div class="login-content--left_content">
            <div class="logo">


                <h1>New Project</h1>
                {{--<img src="{{asset('admin/assets/images/logo.svg')}}" alt="" height="30">--}}
            </div>
        </div>
    </div>

    <div class="login-content--right">

        <div class="login-box">

            <form method="post" class="login-form" action="{{url('login')}}">
                {{ csrf_field() }}
                <h3 class="login-head"><i class="fa fa-user-circle" aria-hidden="true"></i><br><span>Login below to get start</span>
                </h3>
                <div class="form-group">
                    <input class="form-control" type="text" placeholder="&#xf003; Email Address" name="email" autofocus>
                    @if($errors->has('email'))
                    <span class="help-block">
                            {{ $errors->first('email') }}
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <input class="form-control" type="password" placeholder="&#61475; Your Password" name="password">
                    @if($errors->has('password'))
                    <span class="help-block">
                            {{ $errors->first('password') }}
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <div class="utility">
                        <div class="animated-checkbox">
                            <label>
                                <input type="checkbox"><span class="label-text">Stay Signed in</span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group btn-container">
                    <button class="btn btn-primary btn-block">LOGIN</button>
                    <p class="semibold-text text-center mb-2"><a href="{{url('password/reset')}}">Forgot Password ?</a>
                    </p>

                </div>

            </form>

        </div>
    </div>

</section>
<!-- Essential javascripts for application to work-->
<script src="{{asset('admin/js/jquery-3.2.1.min.js')}}"></script>

<script src="{{asset('admin/js/popper.min.js')}}"></script>
<script src="{{asset('admin/js/bootstrap.min.js')}}"></script>
<script src="{{asset('admin/js/main.js')}}"></script>
<!-- The javascript plugin to display page loading on top-->
<script src="{{asset('admin/js/plugins/pace.min.js')}}"></script>

<!-- Page specific javascripts-->
<script src="{{asset('admin/js/plugins/bootstrap-notify.min.js')}}"></script>
<script src="{{asset('admin/js/plugins/sweetalert.min.js')}}"></script>
<script src="{{asset('admin/assets/plugins/particle-js/particles.min.js')}}"></script>
<script src="{{asset('admin/assets/plugins/particle-js/particle-control.js')}}"></script>
<script src="{{asset('admin/js/admin-js.js')}}"></script>


<script src="{{asset('admin/assets/js/sweet-alert/sweetalert.min.js')}}"></script>

<!--Notifications Message Section-->
@include('admin.layouts.notifications')

<script>

    // Login Page Flipbox control
    $('.login-content [data-toggle="flip"]').click(function () {
        $('.login-box').toggleClass('flipped');
        return false;
    });

    $(document).on('click', '.check_truncate', function (e) {

        $('.check_truncate_value').show();
        var value = $('.check_truncate_value').val();
        if (value == '') {

           e.preventDefault();
        }

    });
    $(document).on('click', '.check_seed_table', function (e) {


        $('.check_seed_value').show();
        var value = $('.check_seed_value').val();
        if (value == '') {

            e.preventDefault();
        }

    });

</script>
</body>
</html>
