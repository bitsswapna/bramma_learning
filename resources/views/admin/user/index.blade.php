@extends('admin.layouts.app')

@section('css')

@endsection

@section('content')
    <main class="app-content">
        <div class="app-title">
            <div>
                <h1><i class="fa fa-user"></i> Users</h1>
            </div>
        </div>
        @if(can('browse_user'))
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 ">
                <div class="tile">
                    <div class="tile-body">
                        <table class="table table-hover table-bordered" id="sampleTable">
                            <thead>
                                <tr>
                                    <th>SL#</th>
                                    <th>Date of Registration</th>
                                    <th>Username</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone Number</th>
                                    <th>Number of Facilities</th>
                                    <th>Number of Orders</th>
                                    <th>Last Logged in</th>
                                    <th>Email Verification</th>
                                    <th>Approved</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @else
            @include('admin.no-access-content')
        @endif
    </main>
@endsection



@section('js')
    <script type="text/javascript" src="{{asset('admin/js/plugins/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        var table = $('#sampleTable').DataTable({
            "aaSorting": [],
            'order': [[1, 'desc']],
            "fnDrawCallback": function (oSettings) {
                if (oSettings.bSorted || oSettings.bFiltered || oSettings._iDisplayLength) {
                    j = 0;
                    for (var i = oSettings._iDisplayStart; i < oSettings.aiDisplay.length + oSettings._iDisplayStart; i++) {
                        $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[j]].nTr).find('span').text(i + 1);
                        j++;
                    }
                }
            },
            serverSide: true,
            scrollX: true,
            oLanguage: {
                sProcessing: '<svg width="30px"  height="30px"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-rolling" style="background: transparent;"><circle cx="50" cy="50" fill="none" stroke="#0fb963" stroke-width="10" r="35" stroke-dasharray="164.93361431346415 56.97787143782138" transform="rotate(228 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform></circle></svg>'
            },
            processing: true,
            "pageLength": 10,
            "columnDefs": [{'orderable': false, 'targets': [9,10,11,12]}],
            mark: true,
            fixedColumns: {},
            ajax: {
                url: '{!! URL::to("/get-users") !!}',
            },
            columns: [
                {data: 's#', name: 's#'},
                {data: 'date_of_registration', name: 'date_of_registration'},
                {data: 'username', name: 'username'},
                {data: 'name', name: 'name'},
                {data: 'email', name: 'email'},
                {data: 'phone', name: 'phone'},
                {data: 'number_of_facilities', name: 'number_of_facilities'},
                {data: 'number_of_orders', name: 'number_of_orders'},
                {data: 'last_logged_in', name: 'last_logged_in'},
                {data: 'email_verification', name: 'email_verification'},
                {data: 'approved', name: 'approved'},
                {data: 'status', name: 'status'},
                {data: 'action', name: 'action'},
            ]
        });

        $(document).on('click', '.change-status', function (e) {
            e.preventDefault();
            var token = $('meta[name="csrf-token"]').attr("content");
            var id = $(this).data("id");
            var status = $(this).data('status');

            var data = {id: id, status: status, _token: token};

            var status_html = $(this).html();
            var alert_content = 'Are You Sure To '+status_html+' This User?<br> <small>! Once it confirm will not be roll back</small>';
            alertify.confirm('Confirm status', alert_content, function(){
                var url = '{{url('user/change-status')}}'

                $.ajax({ url: url, type: 'POST', data: data,
                    beforeSend: function () {

                    },
                    success: function (data) {
                        table.draw();
                        successMsg(data.msg);
                    },
                    error: function (data) {
                        table.draw();
                        errorMsg(data.responseJSON.msg);
                    },
                    complete: function () {

                    }
                });
            }, function(){

            });
        });

        $(document).on('click', '.change-active', function (e) {
            var $this = this;
            var $status =  $(this).is(":checked");

            var token = $('meta[name="csrf-token"]').attr("content");
            var id = $(this).data("id");
            var data = {id: id, _token: token};


            var alert_content = 'Are You Sure To Change The User Visibility?';
            alertify.confirm('Confirm status', alert_content, function(){
                var url = '{{url('user/change-active')}}'

                $.ajax({ url: url, type: 'POST', data: data,
                    beforeSend: function () {

                    },
                    success: function (data) {
                        successMsg(data.msg);
                    },
                    error: function (data) {
                        if($status){
                            $($this).prop('checked', false);
                        }else{
                            $($this).prop('checked', true);
                        }
                        errorMsg(data.responseJSON.msg);
                    },
                    complete: function () {

                    }
                });
            }, function(){
                if($status){
                    $($this).prop('checked', false);
                }else{
                    $($this).prop('checked', true);
                }
            });
        });

        $(document).on('click', '.delete-user', function (e) {
            e.preventDefault();
            var token = $('meta[name="csrf-token"]').attr("content");
            var id = $(this).data("id");

            var data = {id: id, _token: token};


            var alert_content = 'Are you sure to delete this user ?<br> <small>! Once it confirm will not be roll back</small>';
            alertify.confirm('Confirm status', alert_content, function(){
                var url = '{{url('user/delete')}}'

                $.ajax({ url: url, type: 'POST', data: data,
                    beforeSend: function () {

                    },
                    success: function (data) {
                        table.draw();
                        successMsg(data.msg);
                    },
                    error: function (data) {
                        table.draw();
                        errorMsg(data.responseJSON.msg);
                    },
                    complete: function () {

                    }
                });
            }, function(){

            });
        });

        $(document).on('click', '.loginas', function (e) {
            e.preventDefault();
            var CSRF_TOKEN = "{!! Session::token() !!}";
            var sendData = {slug: $(this).data('slug'), '_token': CSRF_TOKEN}
            var url = "{!! url('users/login') !!}";
            $.ajax({
                type: "post",
                url: url,
                crossDomain: true,
                data: sendData,
                success: function (data) {
                    window.open(data, 'Filtr3', 'fullscreen=0');
                }
            });
        });
    </script>
@endsection