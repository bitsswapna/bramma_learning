<div class="col-lg-6 col-md-12 col-sm-12 col-xs-20">
  
    @if($debit)
    <div>
    
        <label for="name">Item*</label>
        <select  class="form-control br-0"  name="utilityd[]" id="itemd" >
            <option value=""> select</option>
            @foreach($debit as $utilit)
            <option value="{{$utilit->id}}">{{$utilit->name}}</option>
            @endforeach
        </select>
       
    </div>
    @endif
    @if($admin)
    <div>
                                    <label for="name">Given By*</label>
                                    <select class="form-control givenbyd2" id="givenbyd"  name="givenbyd[]"   multiple="multiple">
                                    @foreach($admin as $admi)
                                    <option value="{{$admi->id}}">{{$admi->name}}</option>
                                        @endforeach
                                    </select>

                                </div>
                                @endif
    </div>
    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-20">
    <div>
        <label for="name">Amount</label>
        <input class="form-control br-0" type="text"  id ="amountd" name="amountd[]" placeholder="Enter the amount"   >
      
    </div>

    <div>
    
        <div class="form-group @if($errors->first('collectedbyd')) has-danger @endif">
            <label for="title">Collected</label>
            {!! Form::select('collectedbyd[]',$staff,'',(['class'=>'form-control collectedbyd10','id'=>'collectedbyd','multiple'=>'multiple'])) !!}
            @if($errors->first('collectedbyd'))
            <span class="alert-danger">{{$errors->first('collectedbyd')}}</span>
            @endif
        </div>
    </div>
   
    <div class="form-group">
    <a class="btn btn-danger i-trash"><i class="fa fa-trash"></i></a><br>
    </div>
   


