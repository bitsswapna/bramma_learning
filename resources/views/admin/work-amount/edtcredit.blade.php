<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
  
    @if($credit)
    <div>
    
        <label for="name">Item*</label>
        <select  class="form-control br-0"  name="utility[]"  >
            <option value=""> select</option>
            @foreach($credit as $utilit)
            <option value="{{$utilit->id}}">{{$utilit->name}}</option>
            @endforeach
        </select>
       
    </div>
    @endif
    @if($admin)
    <div>
                                    <label for="name">Given By*</label>
                                    <select class="form-control givenby1" id="givenby1"  name="givenby[]"   multiple="multiple">
                                  
                                    @foreach($admin as $admi)
                                    <option value="{{$admi->id}}">{{$admi->name}}</option>
                                        @endforeach
                                    </select>

                                </div>
                                @endif
    </div>

    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
    <div>
        <label for="name">Amount</label>
        <input class="form-control br-0" id ="amount" type="text" name="amount[]" placeholder="Enter the amount"   >

    </div>

    <div>

        <div class="form-group @if($errors->first('collectedby')) has-danger @endif">
            <label for="title">Collected</label>
            {!! Form::select('collectedby[]',$staff,'',(['class'=>'form-control collectedby1','id'=>'collectedby1','multiple'=>'multiple'])) !!}
            @if($errors->first('collectedby'))
            <span class="alert-danger">{{$errors->first('collectedby')}}</span>
            @endif
        </div>
    </div>
    

    <div class="form-group">
    <a class="btn btn-danger i-trash"><i class="fa fa-trash"></i></a><br>
    </div>
<script>
$(document).ready(function() {
    $('#givenby1').select2({
        allowClear: true,
        minimumResultsForSearch: -1,
           width: 670,
        placeholder: 'Please Choose'
   });
});

$(document).ready(function() {
        $('#collectedby1').select2({
            allowClear: true,
            minimumResultsForSearch: -1,
            width: 670,
            placeholder: 'Please Choose'
        });
    });
</script>