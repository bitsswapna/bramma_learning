@extends('admin.layouts.app')

@section('css')
    <link href="{{asset('admin/assets/plugins/jquery-datatables-editable/datatables.css')}}" rel="stylesheet" type="text/css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap2.css" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    <main class="app-content bg-white">
       <!-- Sub Title---->
        <div id="submenu_subheader" class="submenu-subheader submenu-grid__item ">
            <div class="submenu-subheader__title">
                <h4 class="title-caption"> <i class="fa fa-user"></i> Amount Details</h4>
            </div>
            <div class="submenu-subheader__toolbar">
                <div class="submenu-subheader__toolbar-wrapper">
                    @if(can('browse_admin_user'))
                        <a data-toggle="tooltip" data-placement="top" data-html="true" title="Add Amount" class="btn bg-primary-green btn-sm waves-effect waves-light inliner btn-right mt--6 br-0"  href="{{url('amount-detail')}}" role="button"><i class="fa fa-plus"></i>Add Worksite Amount</a>

                        {{--<a data-toggle="tooltip" data-placement="top" data-html="true" title="WA Registerd From website" class="btn bg-primary-blue btn-sm waves-effect waves-light inliner btn-right mt--6 br-0" href="{{url('admin_user/wa-request')}}" role="button"><i class="fa fa-list"></i>WA Registerd From website</a>--}}


                    @endif
                </div>
            </div>
        </div>
        <!-- END Sub Title---->
        <!-- Body Part -->
        <div class="sub-header-body">
            @if(can('browse_admin_user'))
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="table-responsive">
                            <table id="amountid" class="table table-bordered" >
                                <thead>
                                <tr>
                                   
                                    <th>S1#</th>
                                    <th>Site</th>
                                    <th>date</th>
                                    <th>Action</th>
<!--                                    <td>title</td>-->
<!--                                    <td>item</td>-->
<!--                                    <td>amount</td>-->
<!--                                    <td>admin</td>-->
<!--                                    <td>collected by</td>-->
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            @else
                @include('admin.no-access-content')
            @endif
        </div>
        <!-- End Body-->
    </main>
@endsection

@section('script')
@include('admin.layouts.notifications')
    <script type="text/javascript" src="{{asset('admin/js/plugins/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/dataTables.bootstrap.min.js')}}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{asset('admin/js/plugins/selectize.min.js')}}"></script>


    <script type="text/javascript">

        $(document).ready(function(){


            var table = $('#amountid').DataTable({

                "aaSorting": [],
                'order': [[1, 'desc']],
                "fnDrawCallback": function (oSettings) {
                    if (oSettings.bSorted || oSettings.bFiltered || oSettings._iDisplayLength) {
                        j = 0;
                        for (var i = oSettings._iDisplayStart; i < oSettings.aiDisplay.length + oSettings._iDisplayStart; i++) {
                            $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[j]].nTr).find('span').text(i + 1);
                            j++;
                        }
                    }
                },
                serverSide: true,
                scrollX: true,
                oLanguage: {
                    sProcessing: '<svg width="30px"  height="30px"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-rolling" style="background: transparent;"><circle cx="50" cy="50" fill="none" stroke="#0fb963" stroke-width="10" r="35" stroke-dasharray="164.93361431346415 56.97787143782138" transform="rotate(228 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform></circle></svg>'
                },
                processing: true,
                "pageLength": 10,
                "columnDefs": [{'orderable': false, 'targets': [0, 3, 3]}],
                mark: true,
                fixedColumns: {},
                ajax: {
                    url: '{!!URL::to("get-admin-amounts")!!}',
                },

                columns: [
                    {data: 'sl#', name: 'sl#'},
                    {data: 'site', name: 'site'},
                    {data: 'date', name: 'date'},
                    {data: 'action', name: 'action'}
                ]
            });
            $(document).on('click', '.change-status', function (e) {
            var token = $('meta[name="csrf-token"]').attr("content");
            var id = $(this).data("id");
            var status = $(this).data('status');

            var data = {id: id, status: status, _token: token};

            var status_html = $(this).html();
            var url = base_url+'/admin-vehicle/change_status';

            $.ajax({
                url: url, type: 'POST', data: data,
                beforeSend: function () {

                },
                success: function (data) {
                    successMsg(data.msg);
                },
                error: function (data) {
                    errorMsg(data.responseJSON.msg);
                },
                complete: function () {

                }
            });
        });



            $(document).on('click', '.delete-user-website', function (e) {

                e.preventDefault();
                var id = $(this).data("id") ;
                
                var url = base_url+'/admin-amount/delete';


                swal({
                    title: "Are you sure ?",
                    text: "Once deleted,the item will be removed!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then(function (willDelete) {
                    if (willDelete) {
                        $.ajax({
                            url: url,
                            type: 'POST',  // user.destroy
                            data:{
                                'id':id,
                            },
                            beforeSend: function(){
                                $("#if_loading").html("loading");
                            },
                            success: function(result) {
                                table.draw()
                                swal(result.msg);
                                
                            },
                            complete: function(){
                                $('#if_loading').html("");
                            }
                        });

                    } else {
                        //
                    }
                });

            });
        })
    </script>
@endsection
