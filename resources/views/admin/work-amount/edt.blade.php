@extends('admin.layouts.app')

@section('css')
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="{{ asset('admin/redactor/redactor/redactor.css')}}" />
@endsection

@section('content')
<main class="app-content bg-white ">
<div id="submenu_subheader" class="submenu-subheader submenu-grid__item ">
        <div class="submenu-subheader__title">
            <h4 class="title-caption"><i class="fas fa-list-alt"></i>  Add Amount Details</h4>
        </div>
        <div class="submenu-subheader__toolbar">
            <div class="submenu-subheader__toolbar-wrapper bs-component">
            @if(can('browse_roles'))
                <a data-toggle="tooltip" data-placement="top" data-html="true" title="" class="btn bg-primary-blue btn-sm waves-effect waves-light btn-right br-0" href="{{url('amount-details')}}" role="button" data-original-title="Amount Details">
                    <i class="fa fa-list"></i>Amount
                </a>
                @endif
            </div>
        </div>
    </div>




    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="tile">
                <form method="post" action="{{url('amount/update/'.$ed->id)}}" name="editdata" >
                         {{csrf_field()}}
                    <div class="row">
                        <div class="col-lg-8">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Basic</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav" id="debit-tab" data-toggle="tab" href="#debit" role="tab" aria-controls="debit" aria-selected="true">Debit </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav" id="credit-tab" data-toggle="tab" href="#credit" role="tab" aria-controls="credit" aria-selected="true">Credit </a>
                            </li>
                        </ul>

                        <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade in  active" id="home" role="tabpanel" aria-labelledby="home-tab"><br>
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                  <div>
                  <div class="form-group">
                      @if($title)
                  <label for="name">Work Site*</label>
                                           <select  class="form-control"  name="titles" id="tab" value="{{old('titles')}}">
                                           <option value=""> select</option>
                                           @foreach($title as $titles)
                                               <option value="{{$titles->id}}"@if($ed)  @if($titles->id ==($ed->work_site_id)) selected @endif  @endif>{{$titles->titles}}</option>
                                               @endforeach
                                           </select>
                                           @if($errors->first('titles'))
                                        <sapn class="error">{{$errors->first('titles')}}</sapn>
                                    @endif
                      @endif
                                           </div>
                                           </div>
                                </div>
                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                    <div>
                                    <div class="form-group">
                                                                    <label for="name">Date</label>
                                                                    <input class="form-control br-0 @if($errors->first('date')) is-invalid @endif" id="date" type="date" name="date" placeholder="date"  autocomplete="off" value="{{$ed->date}}">
                                                                    @if($errors->first('date'))
                                                                    <sapn class="error">{{$errors->first('date')}}</sapn>
                                                                    @endif
                                                                </div>
                                    </div>
                                    </div>
                        </div>
<br>
<!--//////////DEBIT ////////////-->
                    <div class="tab-pane fade" id="debit" role="tabpanel" aria-labelledby="debit-tab" >
                        @if($ed->siteWorkAmountDebit)

                        @foreach($ed->siteWorkAmountDebit  as $amountDebit)

                        @if($debit)
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">

                           
                            <div>

                                                  <label for="name">Item*</label>
                                                                           <select  class="form-control br-0 " id="itemd" name="utilityd[]" value="{{old('utilityd')}}" >
                                                                          <option value=""> select</option>
                                                                           @foreach($debit as $utilit)


                                                                           
                                                                               <option value="{{$utilit->id}}"  @if($utilit->id ==($amountDebit->site_id)) selected   @endif >{{$utilit->name}}</option>
                                                                               @endforeach
                                                                           </select>
                                                                           @if($errors->first('utilityd'))
                                                               <sapn class="error">{{$errors->first('utilityd')}}</sapn>
                                                               @endif
                              
                                                        </div>
                                                        @endif

                            @if($admin)
                            <div class="form-group">
                            <label for="name">Given By*(select name)</label>
                                <select  class="form-control givenbyd"  name="givenbyd[]"  multiple="multiple">


                                    @foreach($admin as $admi)

                                    <option value="{{$admi->id}}"@if($admi->getsDebitGivenByUser($admi->id,$amountDebit->siteWorkAmountDebitGivenBy->site_work_amount_debit_id)) selected @endif data>{{$admi->name}}</option>

                                    @endforeach
                                </select> 
                                </div>
                                @endif





                        </div>
                       
                        
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                        <div>
                            <label for="name">Amount</label>
                            <input class="form-control br-0 "  type="text" id ="amountd" name="amountd[]" placeholder="Enter the amount"  value="{{$amountDebit->amount}}">
                           
                        </div>



                            <div>
                                @if($staff)
                                <div class="form-group">
                                    <label for="name">Collected By*(select name)</label>
                                    <select  class="form-control givenbyd"  name="givenbyd[]"  multiple="multiple">


                                        @foreach($staff as $staffName)

                                        <option value="{{$staffName->id}}"@if($staffName->getsDebitCollectedByUser($staffName->id,$amountDebit->siteWorkAmountDebitCollectedBy->site_work_amount_debit_id)) selected @endif data>{{$staffName->name}}</option>

                                        @endforeach
                                    </select>
                                </div>
                                @endif


                            </div>

                        </div>

                        <div class="details button">
                            <div class="more-dai">
                            </div>
<!--                            <input type="button" id="add-i-details" value="more detail">-->
                        </div>
                        <div class="i-day-details" hidden>
                            <div class="i-details">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">

                                        @include('admin.work-amount.edtdebit')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                            @endforeach
                            @endif



                    </div>




<!--//////////CREDIT//////////-->
                        <div class="tab-pane fade" id="credit" role="tabpanel" aria-labelledby="credit-tab" >
                            @if($ed->siteWorkAmountCreadit)
                            @foreach($ed->siteWorkAmountCreadit as $creadit)

                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                @if($credit)
                                <div>
                                    <label for="name">Item*</label>
                                    <select  class="form-control br-0" id="utility"  name="utility[]" autocomplete="off" >
                                        <option value=""> select</option>
                                        @foreach($credit as $utilit)
                                        <option value="{{$utilit->id}}"@if($utilit->id ==($creadit->site_id)) selected   @endif>{{$utilit->name}}</option>
                                        @endforeach
                                    </select>

                                </div>
                                @endif

                                @if($admin)
                                <div>
                                    <label for="name">Given By*</label>
                                    <select class="form-control givenby" id="givenby"  name="givenby[]"   multiple="multiple">

                                    @foreach($admin as $admi)

                                        @endforeach
                                    </select>

                                </div>
                                @endif


                        </div>
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                <div>
                                    <label for="name">Amount</label>
                                    <input class="form-control br-0 " id="amount" type="text" name="amount[]" placeholder="Enter the amount" value="{{$creadit->amount}}">

                                </div>

                                @if($staff)
                                <div>
                                    <label for="name"> Collected By*</label>
                                    <select class="form-control givenby" id="givenby"  name="givenby[]"   multiple="multiple">

                                        @foreach($staff as $staffCreaditUser)


                                        @endforeach
                                    </select>

                                </div>
                                @endif

<br>
                            <br>

                            </div>

@endforeach
                            @endif

                    </form>
<!--                    <more credit section>-->
                    <div class="detail button">
                        <div class="more-day">
                        </div>
                                                            <input type="button" id="add-i-detail" value="more details">
                    </div>
                    <div class="i-day-detail" hidden>
                        <div class="i-detail">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">

                                    @include('admin.work-amount.edtcredit')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
<!--                    <end more creadit section>-->






            <div class="tile-footer text-right">
                <button id="submit-btn" class="btn btn-primary waves-effect waves-light br-0 btn-sm" type="submit">Save</button>
            </div>
                </div>
        </div>
    </div>
    </main>
@endsection

@section('script')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

    <!--Notifications Message Section-->
    @include('admin.layouts.notifications')

  
  <script type="text/javascript">
    
    $(document).ready(function() {
        $('.collectedby').select2({
            allowClear: true,
            minimumResultsForSearch: -1,
            width: 670,
            placeholder: 'Please Choose'
        });
    });
    $(document).ready(function() {
    $('.givenby').select2({
        allowClear: true,
        minimumResultsForSearch: -1,
           width: 670,
        placeholder: 'Please Choose'
   });
});

      $(document).ready(function() {
    $('.givenbyd').select2({
        allowClear: true,
        minimumResultsForSearch: -1,
           width: 670,
        placeholder: 'Please Choose'
   });
});


$(document).ready(function() {
    $('.collectedbyd').select2({
        allowClear: true,
        minimumResultsForSearch: -1,
           width: 670,
        placeholder: 'Please Choose'
   });
});



        $('#add-i-detail').click(function() {
            var data=$('.i-day-detail').html()
            console.log("here",data)
            $('.more-day').append(data);
            
            $('.collectedby1').select2({
        allowClear: true,
        minimumResultsForSearch: -1,
           width: 670,
        placeholder: 'Please Choose'
   });
   $('.givenby1').select2({
            allowClear: true,
            minimumResultsForSearch: -1,
            width: 670,
            placeholder: 'Please Choose'
        });
 
        });

        $(document).on('click','.i-trash',function(e){

            $(this).closest('.i-detail').remove();
        })




        $('#add-i-details').click(function() {
            var data=$('.i-day-details').html()
            $('.more-dai').append(data);
              
            $('.collectedbyd10').select2({
            allowClear: true,
            minimumResultsForSearch: -1,
            width: 670,
            placeholder: 'Please Choose'
        });
 
   
        $('.givenbyd2').select2({
            allowClear: true,
            minimumResultsForSearch: -1,
            width: 670,
            placeholder: 'Please Choose'
        });

        });

        $(document).on('click','.i-trash',function(e){

            $(this).closest('.i-details').remove();
        })

    </script>
@endsection
