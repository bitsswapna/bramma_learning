@extends('admin.layouts.app')

@section('css')
    <style>
        ul {
            list-style-type: none;
        }

        span.label-text::first-letter {
            text-transform: uppercase;
        }
    </style>
@endsection

@section('content')
    <main class="app-content bg-white">
        <!-- Sub Title---->
        <div id="submenu_subheader" class="submenu-subheader submenu-grid__item ">          
            <div class="submenu-subheader__title">
                <h4 class="title-caption"><i class="fas fa-user-shield"></i>  Edit Details</h4>
            </div>           
            <div class="submenu-subheader__toolbar">
                <div class="submenu-subheader__toolbar-wrapper">
                    <a  data-toggle="tooltip" data-placement="left" data-html="true"  title="edit List" class="btn bg-primary-blue btn-sm waves-effect waves-light  br-0" href="{{url('amount-details')}}">
                        <i class="fa fa-list"></i>Amount List</a>
                </div>
            </div>
        </div>
        <!-- Body Part -->
        <div class="sub-header-body">
            <div class="row">
            <div class="col-lg-10 col-md-8 col-sm-8 col-xs-8">
            <form method="post" action="{{url('amount/update/')}}" name="editdata" >
                {{csrf_field()}}

                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                  <label for="site">Title*</label>
                            
                               
                                           <select  class="form-control"  name="titles" autocomplete="off">
                                           @if($sitework)
                                           @foreach($sitework as $siteworks)

                                          
                                               <option value="{{$siteworks->id}}"  >{{$siteworks->titles}}</option>
                                             
                                               @endforeach 
                                               @endif
                                           </select>
                                           </div>

                                           <div class="form-group">
                                            <label for="type">Type</label>
                                            <select  class="form-control"  name="type" autocomplete="off" value="">
                                                <option value="1" selected>credit</option>
                                                <option value="2" selected>Debit</option>
                                            </select>
                                            @if($errors->first('type'))
                                        <sapn class="form-control-feedback">{{$errors->first('type')}}</sapn>
                                    @endif
                                        </div>
                                     
                        <div class="form-group">
                  <label for="site">Name*</label>
                                           <select  class="form-control"  name="utility" autocomplete="off">
                                           @if($uti)
                                           @foreach($uti as $utiliti)
                                               <option value="{{$utiliti->id}}"  >{{$utiliti->name}}</option>
                                               @endforeach 
                                               @endif
                                           </select>
                        </div>

                        </div>

                     <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">

<div >

                        <div class="form-group">
                            <label for="name">Amount</label>
                            <input class="form-control @if($errors->first('collectedby')) is-invalid @endif" id="name" type="text" name="amount" placeholder="Enter the amount" autocomplete="off"  value="">
                            @if($errors->first('amount'))
                       <sapn class="error">{{$errors->first('amount')}}</sapn>
                              @endif
                            </div>

<div >

                       <div class="form-group">
                            <label for="name">Collected By</label>
                            <input class="form-control @if($errors->first('collectedby')) is-invalid @endif" id="name" type="text" name="collectedby" placeholder="Enter collection" autocomplete="off"  value="">
                            @if($errors->first('collectedby'))
                       <sapn class="error">{{$errors->first('collectedby')}}</sapn>
                              @endif
                       
                        </div>


                                <div class="form-group">
                            <label for="name">Given By</label>
                            <input class="form-control @if($errors->first('givenby')) is-invalid @endif" id="name" type="text" name="givenby" placeholder="Enter given amount"  autocomplete="off"  value="">
                            @if($errors->first('givenby'))
                       <sapn class="error">{{$errors->first('givenby')}}</sapn>
                              @endif
                       
</div>
                    </div>
                </div>
                        
                
                <div class="sub-header-footer text-right">
                    <button class="btn bg-primary-blue br-0  waves-effect waves-light btn-sm" type="submit">Update</button>
                    </div>
                    </div>
                    
            </form>
            </div>
        <!-- Footer Part END-->
        </div>
      
        <!-- Footer Part END-->

        <!-- Footer Part END-->      
    </main>
@endsection

@section('script')

    <!--Notifications Message Section-->
    @include('admin.layouts.notifications')
    <script>
        $("#editRole").validate({
            rules: {
                // simple rule, converted to {required:true}
                display_name: "required",
                name: "required",
            }
        });
    </script>

    <script>
        $('document').ready(function () {

            $('.permission-group').on('change', function(){
                $(this).parents('tr').find(".the-permission").prop('checked', this.checked);
            });

            function parentChecked(){
                $('.permission-group').each(function(){
                    var allChecked = true;
                    $(this).parents('tr').find(".the-permission").each(function(){
                        if(!this.checked) allChecked = false;
                    });
                    $(this).prop('checked', allChecked);
                });
            }

            parentChecked();

            $('.the-permission').on('change', function(){
                parentChecked();
            });
        });
    </script>
@endsection
