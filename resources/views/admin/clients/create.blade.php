@extends('admin.layouts.app')

@section('css')

@endsection

@section('content')
    <main class="app-content">
        <div class="app-title">
            <div class="col-md-6 col-sm-12 col-xs-12 rem-pad">
                <h1><i class="fa fa-plus-circle"></i> Create Client</h1>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 btn-right rem-pad">
                @if(can('browse_roles'))
                <a class="btn btn-success waves-effect waves-light" href="{{url('client')}}"><i class="fa fa-list"></i>Client Lists</a>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="tile">
                    <form method="post" id="createrRole" name="createrRole" action="{{url('client/create')}}">
                        <div class="row">
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                {{csrf_field()}}
                                <div class="form-group @if($errors->first('organisation_name')) has-danger @endif">
                                    <label for="display_name">Organization name</label>
                                    <input class="form-control @if($errors->first('organisation_name')) is-invalid @endif" id="organisation_name" name="organisation_name" type="text" placeholder="Enter organization name" autocomplete="off" value="{{old('organisation_name')}}">
                                    @if($errors->first('organisation_name'))
                                        <sapn class="form-control-feedback">{{$errors->first('organisation_name')}}</sapn>
                                    @endif
                                </div>
                                <div class="form-group @if($errors->first('promoter_name')) has-danger @endif">
                                    <label for="display_name">Promoter name</label>
                                    <input class="form-control @if($errors->first('promoter_name')) is-invalid @endif" id="promoter_name" name="promoter_name" type="text" placeholder="Enter promoter name" autocomplete="off" value="{{old('organisation_name')}}">
                                    @if($errors->first('promoter_name'))
                                    <sapn class="form-control-feedback">{{$errors->first('promoter_name')}}</sapn>
                                    @endif
                                </div>
                                <div class="form-group @if($errors->first('promoter_phone')) has-danger @endif">
                                    <label for="display_name">Promoter phone</label>
                                    <input class="form-control @if($errors->first('promoter_phone')) is-invalid @endif" id="promoter_phone" name="promoter_phone" type="text" placeholder="Enter promoter phone" autocomplete="off" value="{{old('promoter_phone')}}">
                                    @if($errors->first('promoter_phone'))
                                    <sapn class="form-control-feedback">{{$errors->first('promoter_phone')}}</sapn>
                                    @endif
                                </div>

                                <div class="form-group @if($errors->first('promoter_email')) has-danger @endif">
                                    <label for="display_name">Promoter email</label>
                                    <input class="form-control @if($errors->first('promoter_email')) is-invalid @endif" id="promoter_email" name="promoter_email" type="text" placeholder="Enter promoter email" autocomplete="off" value="{{old('promoter_email')}}">
                                    @if($errors->first('promoter_email'))
                                    <sapn class="form-control-feedback">{{$errors->first('promoter_email')}}</sapn>
                                    @endif
                                </div>

                                <div class="form-group @if($errors->first('organisation_email')) has-danger @endif">
                                    <label for="display_name">Organization email</label>
                                    <input class="form-control @if($errors->first('organisation_email')) is-invalid @endif" id="organisation_email" name="organisation_email" type="text" placeholder="Enter organization email" autocomplete="off" value="{{old('organisation_email')}}">
                                    @if($errors->first('organisation_email'))
                                    <sapn class="form-control-feedback">{{$errors->first('organisation_email')}}</sapn>
                                    @endif
                                </div>
                                <div class="form-group @if($errors->first('organisation_address')) has-danger @endif">
                                    <label for="display_name">Organization Address</label>
                                    <textarea class="form-control" id="organisation_address" name="organisation_address" ></textarea>
                                    <sapn class="form-control-feedback">{{$errors->first('organisation_address')}}</sapn>
                                </div>

                                <div class="form-group @if($errors->first('organisation_email')) has-danger @endif">
                                    <label for="display_name">Organization Description</label>
                                   <textarea class="form-control" id="organisation_description" name="organisation_description" ></textarea>
                                    <sapn class="form-control-feedback">{{$errors->first('organisation_email')}}</sapn>
                                </div>

                            </div>
                        </div>

                        <div class="tile-footer">
                            <button class="btn btn-primary waves-effect waves-light" type="submit">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>
@endsection

@section('js')
    <!--Notifications Message Section-->
    @include('admin.layouts.notifications')

    <script>
        $("#createrRole").validate({
            rules: {
                // simple rule, converted to {required:true}
                display_name: "required",
                name: "required",
            }
        });
    </script>
@endsection
