@extends('admin.layouts.app')
@section('css')
@endsection
    @section('content')
    <!-- Main Container -->
    <main class="app-content bg-white animated fadeIn"> 
        <!-- Sub Title---->
        <div id="submenu_subheader" class="submenu-subheader submenu-grid__item ">          
            <div class="submenu-subheader__title">
                <h4 class="title-caption"><i class="fas fa-user-shield"></i> Roles</h4>
            </div>           
            <div class="submenu-subheader__toolbar">
                <div class="submenu-subheader__toolbar-wrapper">
                     {{-- <p class="bs-component">
                                <!-- @if(can('add_roles'))
                                <a class="btn btn-primary waves-effect waves-light" href="{{url('role/create')}}" role="button">
                                                    <i class="fa fa-plus-circle"></i>
                                                Create Role
                                            </a> @endif -->
    
                                @if(can('add_roles')) {{--
                                <a class="btn btn-primary waves-effect waves-light" href="{{url('role/create')}}" role="button">--}}
                                    {{--<i class="fa fa-plus-circle"></i>--}}
                                    {{--Create Role--}}
                                    {{--</a>-- @endif
                            </p>  --}}
                </div>
            </div>
        </div>
        <!-- END Sub Title---->
        <!-- Body Part -->
        <div class="sub-header-body">
            @if(can('browse_roles'))
                <div class="table-responsive">
                    <table class="table  table-bordered" id="sampleTable">
                        <thead>
                            <tr>
                                <th>Role name</th>
                                <th>Role</th>
                                <th>Created</th>
                                <th>Action</th>
                            </tr>
                        </thead>                        
                        <tbody>
                            @foreach($roles as $role)
                                <tr>
                                    <td>
                                        {{$role->display_name}}
                                    </td>
                                    <td>
                                        <label class="small badge badge-info{{$role->name}}">{{$role->name}}</label>
                                    </td>
                                    <td>
                                        {{date('D M Y h:i A',strtotime($role->created_at))}}
                                    </td>
                                    <td class="text-left">
                                        @if(can('edit_roles'))
                                            <a href="{{url('role/edit/'.$role->id)}}" class="btn action-button bg-primary-blue"
                                            data-toggle="tooltip" data-placement="top" data-html="true" title="Edit Role">
                                            <i class="fa fa-pencil-square-o "></i></a>&nbsp;&nbsp;&nbsp;&nbsp; @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @else @include('admin.no-access-content') @endif                
        </div>
        <!-- END Body Part -->
    </main>
    <!-- Main END -->
  
        

    @endsection @section('js')

    <script type="text/javascript" src="{{asset('admin/js/plugins/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $('#sampleTable').DataTable({
            bPaginate: false,
            bSort: false,
            bFilter: false,
            bInfo: false,
            responsive:true
        });
    </script>
    @endsection