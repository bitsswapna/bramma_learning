@extends('admin.layouts.app')

@section('css')
    <style>
        ul {
            list-style-type: none;
        }

        span.label-text::first-letter {
            text-transform: uppercase;
        }
    </style>
@endsection

@section('content')
    <main class="app-content bg-white">
        <!-- Sub Title---->
        <div id="submenu_subheader" class="submenu-subheader submenu-grid__item ">          
            <div class="submenu-subheader__title">
                <h4 class="title-caption"><i class="fas fa-user-shield"></i>  Edit Site</h4>
            </div>           
            <div class="submenu-subheader__toolbar">
                <div class="submenu-subheader__toolbar-wrapper">
                    <a  data-toggle="tooltip" data-placement="left" data-html="true"  title="List" class="btn bg-primary-blue btn-sm waves-effect waves-light  br-0" href="{{url('sites')}}">
                        <i class="fa fa-list"></i>Site List</a>
                </div>
            </div>
        </div>
        <!-- Body Part -->
        <div class="sub-header-body">
            <div class="row">
                <div class=" col-md-12 col-sm-12 col-xs-12">
            <form method="post" action="{{url('admin_sites/update/'.$ed->id)}}" name="editdata" id="editRole">
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                 <div class="tile">
                        {{csrf_field()}}
                       
                   
                        

                        <div class="form-group">
                                            <label for="name">Customer</label>
                                            <select  class="form-control"  name="customer" autocomplete="off"disabled>
                                           
                                                <option value="" selected>{{$ed->name}}</option>
                                             
                                            </select>
                                            @if($errors->first('customer'))
                                        <sapn class="form-control-feedback">{{$errors->first('customer')}}</sapn>
                                    @endif
                                        </div>

                                        

                        <div class="form-group">
                            <label for="name">Title</label>
                            <input class="form-control @if($errors->first('title')) is-invalid @endif" id="name" type="text" name="title" placeholder="Enter title" autocomplete="off"  value="{{$ed->title}}">
                            @if($errors->first('title'))
                       <sapn class="error">{{$errors->first('title')}}</sapn>
                              @endif
                    </div>
                   
                    

                        <div class="form-group">
                            <label for="name">Address</label>
                            <textarea class="form-control @if($errors->first('adress')) is-invalid @endif" id="name" type="text" name="adress" placeholder="Enter address" autocomplete="off">{{$ed->address}}</textarea>
                           
                            @if($errors->first('adress'))
                       <sapn class="error">{{$errors->first('adress')}}</sapn>
                              @endif
                    </div>
                 </div>
                </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">

                        <div class="form-group">
                            <label for="name">Description</label>
                            <textarea class="form-control @if($errors->first('descrption')) is-invalid @endif" id="name" type="text" name="descrption" placeholder="Enter description"  autocomplete="off"  >{{$ed->description}}</textarea>
                            @if($errors->first('descrption'))
                       <sapn class="error">{{$errors->first('descrption')}}</sapn>
                              @endif
                    </div>

                        <div class="form-group">
                            <label for="name">Work in day</label>
                            <input class="form-control @if($errors->first('estimwork')) is-invalid @endif" id="name" type="text" name="estimwork" placeholder="Enter estimated work in day" autocomplete="off"  value="{{$ed->estimated_work_in_day}}">
                            @if($errors->first('estimwork'))
                       <sapn class="error">{{$errors->first('estimwork')}}</sapn>
                              @endif
                    </div>

                        <div class="form-group">
                            <label for="name">Expense</label>
                            <input class="form-control @if($errors->first('estexpensive')) is-invalid @endif" id="name" type="text" name="estexpensive" placeholder="Enter estimated expense" autocomplete="off"  value="{{$ed->estimated_expense}}">
                            @if($errors->first('estexpensive'))
                       <sapn class="error">{{$errors->first('estexpensive')}}</sapn>
                              @endif
                    </div>
                </div>
                </div>

          
              

                    </div>
                </div>
                <div class="sub-header-footer text-right">
                    <button class="btn bg-primary-blue br-0  waves-effect waves-light btn-sm" type="submit">Update</button>
                    </div>

</form>
    </div>
        <!-- Footer Part END-->

        <!-- Footer Part END-->      
    </main>
@endsection

@section('script')

    <!--Notifications Message Section-->
    @include('admin.layouts.notifications')
    <script>
        $("#editRole").validate({
            rules: {
                // simple rule, converted to {required:true}
                display_name: "required",
                name: "required",
            }
        });
    </script>

    <script>
        $('document').ready(function () {

            $('.permission-group').on('change', function(){
                $(this).parents('tr').find(".the-permission").prop('checked', this.checked);
            });

            function parentChecked(){
                $('.permission-group').each(function(){
                    var allChecked = true;
                    $(this).parents('tr').find(".the-permission").each(function(){
                        if(!this.checked) allChecked = false;
                    });
                    $(this).prop('checked', allChecked);
                });
            }

            parentChecked();

            $('.the-permission').on('change', function(){
                parentChecked();
            });
        });
    </script>
@endsection
