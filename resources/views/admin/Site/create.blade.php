@extends('admin.layouts.app')

@section('css')
<link href="{{asset('css/plugin/selectize.bootstrap2.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('admin/assets/plugins/jquery-datatables-editable/datatables.css')}}" rel="stylesheet" type="text/css" />
<style>
    .rounded-circle {
        border-radius: 50%;
    }
    .moadl-close{
        position: absolute;
        right: 0;
        top: 0;
        width: 20px;
        height: 20px;
        background: #f59b9b;
        padding: 3px 3px;
        color: #fff;
        box-shadow: 0 2px 5px rgba(0, 0, 0, 0.16), 0 2px 10px rgba(0, 0, 0, 0.012);
        cursor: pointer;
    }
    .radio-toolbar input[type="radio"] {
        display:none;
    }
    .radio-toolbar li{
        display: none;
    }
    .radio-toolbar label {
        display: inline-block;
        cursor: pointer;
        color: white;
        border-radius: 0px !important;
        padding: 3px 10px;
        border-radius: 2px;
        font-size: 13px;
        box-shadow: 2px 2px 4px rgba(158, 158, 158, 0.3);
        background: #2196F3;
        position: relative;
        overflow: hidden;
        margin-top:10px;
    }
    .radio-toolbar label :after {
        content: '';
        position: absolute;
        top: 50%;
        left: 50%;
        width: 5px;
        height: 5px;
        background: rgba(255, 255, 255, .5);
        opacity: 0;
        border-radius: 100%;
        transform: scale(1, 1) translate(-50%);
        transform-origin: 50% 50%;
    }
    .loading-btn{
        display: none;
    }
    .cart-form{
        display: inline;
        width:20px;
        margin: 0px 0px 0px 0px
    }
    .add-remove-cart{
        cursor: pointer;
    }
    /*@keyframes ripple {*/
    /*    0% {*/
    /*        transform: scale(0, 0);*/
    /*        opacity: 1;*/
    /*    }*/
    /*    20% {*/
    /*        transform: scale(25, 25);*/
    /*        opacity: 1;*/
    /*    }*/
    /*    100% {*/
    /*        opacity: 0;*/
    /*        transform: scale(40, 40);*/
    /*    }*/
    /*}*/
    .radio-toolbar input[type="radio"]:checked + label {

    }

    .radio-toolbar label:hover {
        /*animation: ripple 1s ease-out;*/
    }
</style>
<link rel="stylesheet" href="{{asset('admin/css/style.css')}}">
@endsection

@section('content')
<!--popup customer-->
<div class="vg-modal">
    <div class="tab-modal modal fade" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="ModalScrollableTitle" aria-hidden="true" data-toggle="modal" data-backdrop="static" data-keyboard="false" >
        <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document" >
            <div class="modal-content">
                <div class="modal-header relative">
                    <h5 class="modal-title" id="ModalScrollable">Create Customer</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body user-create ">
                    <p>
                        @include('admin.Site.newCustomer')
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<main class="app-content bg-white ">
<div id="submenu_subheader" class="submenu-subheader submenu-grid__item ">
        <div class="submenu-subheader__title">
            <h4 class="title-caption"><i class="fas fa-list-alt"></i>  Add Site Details</h4>
        </div>
        <div class="submenu-subheader__toolbar">
            <div class="submenu-subheader__toolbar-wrapper bs-component">
            @if(can('browse_roles'))
                <a data-toggle="tooltip" data-placement="top" data-html="true" title="" class="btn bg-primary-blue btn-sm waves-effect waves-light btn-right br-0" href="{{url('sites')}}" role="button" data-original-title="Site Lists">
                    <i class="fa fa-list"></i>Site
                </a>
                @endif
            </div>
        </div>
    </div>




    <div class="row">
        <div class=" col-md-12 col-sm-12 col-xs-12">
                <div class="tile">
                <form method="post" id="createrRole" name="createrRole" action="{{url('sites/insert')}}">
                         {{csrf_field()}}

                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                        <label for="name">Customer*</label>
                        <div class="col-sm-20 col-xs-20 m-b-20 btn-inline form-group">
<!--                            <div class="col-sm-6 col-xs-12 m-b-20">-->



                                           <select  class="form-control br-0 @if($errors->first('customer')) is-invalid @endif"  name="customer" autocomplete="off">
                                           <option value=""> select</option>
                                           @foreach($insert as $custom)


                                               <option value="{{$custom->id}}">{{$custom->name}}</option>

                                               @endforeach

                                           </select>
                            <span data-toggle="tooltip">
                                                <button type="button" class="btn bg-primary-blue br-0  add-new-customer btn-addUser" data-toggle="modal" data-placement="top" title="My Tooltip text!" data-target="#exampleModalScrollable" data-backdrop="static" data-keyboard="false">
                                                        <i class="fa fa-user-plus" aria-hidden="true"></i>
                                                </button>
                                            </span>
                                           @if($errors->first('customer'))
                                        <sapn class="error">{{$errors->first('customer')}}</sapn>
                                        @endif

                        </div>

<div class="form-group @if($errors->first('title')) has-danger @endif">


                                    <label for="name">Title</label>
                                    <input class="form-control @if($errors->first('title')) is-invalid @endif" id="name" type="text" name="title" placeholder="Enter the title"  autocomplete="off" value="{{old('title')}}">
                                    @if($errors->first('title'))
                                        <sapn class="error">{{$errors->first('title')}}</sapn>
                                        @endif
</div>
                    <div>
                                    <label for="name">Address</label>
                                    <textarea class="form-control @if($errors->first('adress')) is-invalid @endif" id="name" type="text" name="adress" placeholder="Enter adress"   autocomplete="off" value="{{old('address')}}"> </textarea>
                                    @if($errors->first('adress'))
                                        <sapn class="error">{{$errors->first('adress')}}</sapn>
                                        @endif
                    </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">

                        <div class="form-group @if($errors->first('estimwork')) has-danger @endif">
                                    <label for="name">Estimated work  day(s)</label>
                                    <input class="form-control @if($errors->first('estimwork')) is-invalid @endif" id="name" type="text" name="estimwork" placeholder="Estimated work in a day"  autocomplete="off" value="{{old('estimated_work_in_day')}}">
                                    @if($errors->first('estimwork'))
                                        <sapn class="error">{{$errors->first('estimwork')}}</sapn>
                                        @endif
                        </div>
                        <div class="form-group @if($errors->first('estexpensive')) has-danger @endif">
                                    <label for="name">Estimated Expense</label>
                                    <input class="form-control @if($errors->first('estexpensive')) is-invalid @endif" id="name" type="text" name="estexpensive" placeholder="Enter estimated expense"  autocomplete="off" value="{{old('estimated_expense')}}">
                                    @if($errors->first('estexpensive'))
                                        <sapn class="error">{{$errors->first('estexpensive')}}</sapn>
                                        @endif
                        </div>
                        <div class="form-group @if($errors->first('descrption')) has-danger @endif">
                            <label for="name">Description</label>
                            <textarea class="form-control @if($errors->first('descrption')) is-invalid @endif" id="name" type="text" name="descrption" placeholder="Enter description"  autocomplete="off" >{{old('description')}}</textarea>
                            @if($errors->first('descrption'))
                            <sapn class="error">{{$errors->first('descrption')}}</sapn>
                            @endif
                        </div>

</div>



                            </div>


</div>
</div>

<div class="sub-header-footer text-right">
                        <button  class="btn btn-primary waves-effect waves-light br-0 btn-sm" type="submit">Save</button>
                    </div>

    </form>
            </div>
        </div>
</main>




@endsection

@section('js')
<script type="text/javascript" src="{{asset('admin/js/plugins/selectize.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/assets/js/sweetalert.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/plugins/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/plugins/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/assets/js/custom/offer-tab.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/assets/js/custom/billing-tab.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/assets/js/custom/admin-user-tab.js')}}"></script>
<script src="{{asset('website/assets/js/jquery.bpopup.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/assets/js/custom/bill-create.js')}}"></script>
<script src="{{asset('admin/assets/js/jquery-ui.min.js')}}"></script>
    <!--Notifications Message Section-->
    @include('admin.layouts.notifications')

    <script>
        $("#createrRole").validate({
            rules: {
                // simple rule, converted to {required:true}
                display_name: "required",
                name: "required",
            }
        });
    </script>
<script>

    $('#createNewCustomer').submit(function(evt) {
        alert('gai');
        $("#submit-btn").prop("disabled",true);
        evt.preventDefault();
        var formData = new FormData(this);
        var actionurl = base_url+'/admin_user/create';
        $.ajax({
            type: 'POST',
            url: actionurl,
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            success: function (result) {
                $("#submit-btn").prop("disabled",false);
                if (result.status == false) {
                    $(".error").html("");
                    $.each($.parseJSON(result.errors), function (i, item) {
                        err = item;
                        field = $('[name="' + i + '"]');
                        field.next('span.error').html(item);
                        return;
                    });

                }else if(result.status == true){


                    var notify = $.notify('<strong>Saving</strong> Do not close this page...', {
                        allow_dismiss: false,
                        showProgressbar: true,
                        z_index:5000,
                    });

                    setTimeout(function() {
                        notify.update({'type': 'success', 'message': '<strong>Success</strong> Your page has been saved!', 'progress': 25});
                        $('#createAdminUser').trigger("reset");
                        $('#prof_img').find('img').attr('src', no_image_url);
                        $(".error").html("");

                    }, 4500);



                }
            },
            error:function () {
                swal("Something went wrong ! Please try again");
                $("#submit-btn").prop("disabled",false);
            }
        });
    });


</script>


<script>
    $('body').on('click', '.btn-save', function (e) {
        alert('hai');
        e.preventDefault();
        var actionurl = base_url + '/admin_user/create';
        var formdata = new FormData($("#createAdminUser").get(0))
        formdata.append("role_id", $(".image-radio-checked").children("input").val())
        $(".save-wa-form").prop("disabled", true);
        $.ajax({
            type: 'POST',
            url: actionurl,
            contentType: false,
            processData: false,
            data: formdata,
            // data:$("#createAdminUser").serialize(),
            success: function (result) {
                // console.log(result)

                if (result.permission == true) {
                    if (result.status == false) {
                        $(".error").html("");
                        $(".save-wa-form").prop("disabled", false);
                        $.each($.parseJSON(result.errors), function (i, item) {
                            err = item;
                            if (i == "parent_id") {
                                field = $('[name="' + i + '"]').parent("div").parent("div");
                                field.children('span.error').html(item);
                                return;
                            } else {
                                field = $('[name="' + i + '"]');
                                field.next('span.error').html(item);
                                return;
                            }

                        });
                        console.log($("li[data-number=" + $("span.error:not(:empty)").first().data("number") + "]").children("a"));
                        $("li[data-number=" + $("span.error:not(:empty)").first().data("number") + "]").children("a").trigger("click")
                    } else if (result.status == true) {
                        var notify = $.notify('<strong>Saving</strong> Do not close this page...', {
                            allow_dismiss: false,
                            showProgressbar: true,
                            z_index: 15000,
                        });

                        // setTimeout(function () {
                        $('#exampleModalScrollable').modal('hide');
                        $('#createAdminUser').trigger("reset");

                        // notify.update({ 'type': 'success', 'message': '<strong>Success</strong> Your page has been saved!', 'progress': 25 });
                        // $('#prof_img').find('img').attr('src', no_image_url);
                        $(".error").html("");
                        var waid = result.user_id;
                        $(".wellness-advisor-list-input").val(waid)
                        $(".wellness-advisor-list-main").val(waid)
                        $(".loader").show();
                        $(".varification-wa").html('');
                        $(".address-form").html('');
                        $(".payment-form").html('');
                        var url = base_url + '/get-wellness-advisor-detail-tab/' + waid;
                        $.ajax({
                            url: url,
                            type: 'GET',
                            beforeSend: function () {
                            },
                            success: function (data) {
                                $(".varification-wa").html(data);

                            },
                            complete: function () {
                                $(".loader").hide();
                                $(".profile-tab").trigger("click");
                            }
                        });
                        // }, 4500);
                    }

                } else {
                    swal("You have no permission")
                }


            },
            error: function () {
                swal("Something went wrong ! Please try again");
                $(".save-wa-form").prop("disabled", false);
            }
        });
    });
</script>
@endsection
