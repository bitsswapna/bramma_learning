@extends('admin.layouts.app')

@section('css')
    <link href="{{asset('admin/assets/plugins/jquery-datatables-editable/datatables.css')}}" rel="stylesheet" type="text/css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap2.css" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    <main class="app-content bg-white">
       <!-- Sub Title---->
        <div id="submenu_subheader" class="submenu-subheader submenu-grid__item ">
            <div class="submenu-subheader__title">
                <h4 class="title-caption"> <i class="fa fa-user"></i> Site List</h4>
            </div>
            <div class="submenu-subheader__toolbar">
                <div class="submenu-subheader__toolbar-wrapper">
                    @if(can('browse_admin_user'))
                        <a data-toggle="tooltip" data-placement="top" data-html="true" title="Create User" class="btn bg-primary-green btn-sm waves-effect waves-light inliner btn-right mt--6 br-0"  href="{{url('site')}}" role="button"><i class="fa fa-plus"></i>Add</a>
                        {{--<a data-toggle="tooltip" data-placement="top" data-html="true" title="WA Registerd From website" class="btn bg-primary-blue btn-sm waves-effect waves-light inliner btn-right mt--6 br-0" href="{{url('admin_user/wa-request')}}" role="button"><i class="fa fa-list"></i>WA Registerd From website</a>--}}
              
                        
                    @endif
                </div>
            </div>
        </div>
        <!-- END Sub Title---->
        <!-- Body Part -->
        <div class="sub-header-body">
            @if(can('browse_admin_user'))
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="table-responsive">
                            <table id="siteid" class="table table-bordered" >
                                <thead>
                                <tr>
                                    <th>Sl#</th>
                                    <th>Title</th>
                                    <th>Address</th>
                                    <th>Description</th>
                                    <th>Total Duration (day)</th>
                                    <th>Expense</th>
                                    <th>Status</th>
                                    
                                    <th>Action</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            @else
                @include('admin.no-access-content')
            @endif
        </div>
        <!-- End Body-->
    </main>
@endsection

@section('script')
@include('admin.layouts.notifications')
          
<script type="text/javascript" src="{{asset('admin/js/plugins/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/select2.min.js')}}"></script>
    <script src="{{asset('admin/js/plugins/selectize.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/assets/js/sweet-alert/sweetalert.min.js')}}"></script>
    <script src="{{asset('website/assets/js/jquery.bpopup.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/admin-js.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/assets/js/custom/admin-user.js')}}"></script>
 

   

    <script type="text/javascript">
    
        $(document).ready(function(){
           
      
            var table = $('#siteid').DataTable({
                
                "aaSorting": [],
                'order': [[1, 'desc']],
                "fnDrawCallback": function (oSettings) {
                    if (oSettings.bSorted || oSettings.bFiltered || oSettings._iDisplayLength) {
                        j = 0;
                        for (var i = oSettings._iDisplayStart; i < oSettings.aiDisplay.length + oSettings._iDisplayStart; i++) {
                            $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[j]].nTr).find('span').text(i + 1);
                            j++;
                        }
                    }
                },
                serverSide: true,
                scrollX: true,
                oLanguage: {
                    sProcessing: '<svg width="30px"  height="30px"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-rolling" style="background: transparent;"><circle cx="50" cy="50" fill="none" stroke="#0fb963" stroke-width="10" r="35" stroke-dasharray="164.93361431346415 56.97787143782138" transform="rotate(228 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform></circle></svg>'
                },
                processing: true,
                "pageLength": 10,
                "columnDefs": [{'orderable': false, 'targets': [0, 7, 7]}],
                mark: true,
                fixedColumns: {},
                ajax: {
                    url: '{!!URL::to("get-admin-site")!!}',
                    data: function (d) {
                        // x = document.getElementById("product_status").selectedIndex;
                        // d.product_status = document.getElementsByTagName("option")[x].value;
                        d.product_status = $("#product_status").val();
                        // d.unique_code = $('#unique_code_list').val();
                        d.order_id = $('#order_id_filter').val();
                        d.start_date = $('#start_date').val();
                        d.end_date =  $('#end_date').val();
                    }
                },
              
                columns: [
                    {data: 'sl#', name: 'sl#'},
                 
                    {data: 'title', name: 'title'},
                    {data: 'address', name: 'address'},
                    {data: 'description', name: 'description'},
                    {data: 'estiwork', name: 'estiwork'},
                    {data: 'estiexpense', name: 'estiexpense'},
                    {data: 'status', name: 'status'},
                    {data: 'action', name: 'action'}
                ]
            });
            $(document).on('click', '.change-status', function (e) {
            var token = $('meta[name="csrf-token"]').attr("content");
            var id = $(this).data("id");
            var status = $(this).data('status');

            var data = {id: id, status: status, _token: token};

            var status_html = $(this).html();
            var url = base_url+'/admin-site/change_status';

            $.ajax({
                url: url, type: 'POST', data: data,
                beforeSend: function () {

                },
                success: function (data) {
                    successMsg(data.msg);
                },
                error: function (data) {
                    errorMsg(data.responseJSON.msg);
                },
                complete: function () {

                }
            });
        });
      

           
            $(document).on('click', '.delete-user-website', function (e) {

                e.preventDefault();
                var id = $(this).data("id") ;
                var url = base_url+'/admin-site/delete';


                swal({
                    title: "Are you sure ?",
                    text: "Once deleted,the site will be removed!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then(function (willDelete) {
                    if (willDelete) {
                        $.ajax({
                            url: url,
                            type: 'POST',  // user.destroy
                            data:{
                                'id':id,
                            },
                            beforeSend: function(){
                                $("#if_loading").html("loading");
                            },
                            success: function(result) {
                                table.draw()

                                // Do something with the result
                            },
                            complete: function(){
                                $('#if_loading').html("");
                            }
                        });

                    } else {
                        //
                    }
                });

            });
        })
    </script>


<script>

    $(document).on('change', '.change-order-status', function (e) {


        var id = $(this).data("id");
        var value = this.value;


        var url_site= base_url + '/order/check-test-tube';
        var data_check = {id: id, value: value};
        $.ajax({
            url: url_site, type: 'get', data: data_check,
            beforeSend: function () {
                //
            },
            success: function (result) {
                if (result.status == true) {/*Billing or test tube bar code entered*/
                    if (value == 4 || value == 6) {
                        $("#status").val(value);
                        $(".product_name_ship").html(result['product']['name']);
                        $(".product_id_ship").html(result['product']['id']);
                        $('#myModal').modal('show');

                    } else {
                        changeOrderStatus(id, value);
                    }

                } else {
                    addBracodeTestTube(id, value);
                }
            },
            error: function (data) {
                // errorMsg(data.responseJSON.msg);
            },
            complete: function () {
                //
            }
        });


    });
</script>
@endsection
