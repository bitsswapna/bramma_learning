@extends('admin.layouts.app')

@section('css')

@endsection

@section('content')
<main class="app-content bg-white ">
<div id="submenu_subheader" class="submenu-subheader submenu-grid__item ">          
        <div class="submenu-subheader__title">
            <h4 class="title-caption"><i class="fas fa-list-alt"></i>  Add customers Details</h4>
        </div>           
        <div class="submenu-subheader__toolbar">
            <div class="submenu-subheader__toolbar-wrapper bs-component">
            @if(can('browse_roles'))
                <a data-toggle="tooltip" data-placement="top" data-html="true" title="" class="btn bg-primary-blue btn-sm waves-effect waves-light btn-right br-0" href="{{url('admin-customers')}}" role="button" data-original-title="Customers Lists">
                    <i class="fa fa-list"></i>customers
                </a> 
                @endif
            </div>
        </div>
    </div>

    


    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="tile">
                    <form method="post" id="createrRole" name="createrRole" action="{{url('insert/customers')}}">
                    {{csrf_field()}}
                               
                               
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                  
                        
                                    <label for="display_name">First Name</label>
                                    <input class="form-control @if($errors->first('first_name')) is-invalid @endif" id="display_name" name="first_name" type="text" placeholder="Enter First Name" autocomplete="off" value="{{old('first_name')}}">
                                    @if($errors->first('name'))
                                        <sapn class="error">{{$errors->first('first_name')}}</sapn>
                                        @endif
                         </div>

                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">


                            <label for="display_name">Last name</label>
                            <input class="form-control @if($errors->first('last_name')) is-invalid @endif" id="display_name" name="last_name" type="text" placeholder="Enter Last Name" autocomplete="off" value="{{old('last_name')}}">
                            @if($errors->first('last_name'))
                            <sapn class="error">{{$errors->first('last_name')}}</sapn>
                            @endif
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">


                            <label for="display_name">Email</label>
                            <input class="form-control @if($errors->first('email')) is-invalid @endif" id="display_name" name="email" type="text" placeholder="Enter Email" autocomplete="off" value="{{old('email')}}">
                            @if($errors->first('last_name'))
                            <sapn class="error">{{$errors->first('email')}}</sapn>
                            @endif
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                            <label for="name">Contact Number</label>
                            <input class="form-control @if($errors->first('contnumber')) is-invalid @endif" id="name" type="text" name="contnumber" placeholder="Enter contact number"  autocomplete="off" value="{{old('contnumber')}}">
                            @if($errors->first('contnumber'))
                            <sapn class="error">{{$errors->first('contnumber')}}</sapn>
                            @endif
                        </div>
                        <div class="col-sm-6 col-xs-12">
                                    <label for="name">Address</label>
                                    <textarea class="form-control @if($errors->first('adress')) is-invalid @endif" id="name" type="text" name="adress" placeholder="Enter adress"   autocomplete="off" > {{old('adress')}}</textarea>
                                    @if($errors->first('adress'))
                                        <sapn class="error">{{$errors->first('adress')}}</sapn>
                                        @endif
                                </div>


                              

                            </div>
                                            
</div>
</div>
<div class="tile-footer text-right">
<button class="btn btn-primary waves-effect waves-light" type="submit">Save</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </main>
@endsection

@section('js')
    <!--Notifications Message Section-->
    @include('admin.layouts.notifications')

    <script>
        $("#createrRole").validate({
            rules: {
                // simple rule, converted to {required:true}
                display_name: "required",
                name: "required",
            }
        });
    </script>
@endsection
