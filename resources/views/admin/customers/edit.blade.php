@extends('admin.layouts.app')

@section('css')
    <style>
        ul {
            list-style-type: none;
        }

        span.label-text::first-letter {
            text-transform: uppercase;
        }
    </style>
@endsection

@section('content')
    <main class="app-content bg-white">
        <!-- Sub Title---->
        <div id="submenu_subheader" class="submenu-subheader submenu-grid__item ">          
            <div class="submenu-subheader__title">
                <h4 class="title-caption"><i class="fas fa-user-shield"></i>  Edit Customers</h4>
            </div>           
            <div class="submenu-subheader__toolbar">
                <div class="submenu-subheader__toolbar-wrapper">
                    <a  data-toggle="tooltip" data-placement="left" data-html="true"  title="customer List" class="btn bg-primary-blue btn-sm waves-effect waves-light  br-0" href="{{url('admin-customers')}}">
                        <i class="fa fa-list"></i>Customers Lists</a>
                </div>
            </div>
        </div>
        <!-- Body Part -->
        <div class="sub-header-body">
            <form method="post" action="{{url('admin_customer/edit/'.$user->id)}}" name="editdata" id="editRole">
                {{csrf_field()}}
                <div class="row">
                <div class="col-lg-10 col-md-8 col-sm-8 col-xs-8">
                 <div class="tile">
                        {{csrf_field()}}
                          <div class="col-lg-10 col-md-8 col-sm-8 col-xs-8">
                          <div class="form-group">
                            <label for="display_name">Name</label>
                            <input class="form-control @if($errors->first('name')) is-invalid @endif" id="display_name" name="name" type="text" placeholder="Enter name" autocomplete="off" value="{{$user->name}}">
                            @if($errors->first('name'))
                                        <sapn class="error">{{$errors->first('name')}}</sapn>
                                        @endif
                        </div>
                    </div>
                   
                    
                    <div class="col-lg-10 col-md-8 col-sm-8 col-xs-8">
                        <div class="form-group">
                     
                            <label for="name">Address</label>
                            <textarea class="form-control @if($errors->first('adress')) is-invalid @endif" id="name" type="text" name="adress" placeholder="Enter address" autocomplete="off" > {{$user->address}}</textarea>
                            @if($errors->first('adress'))
                                        <sapn class="error">{{$errors->first('adress')}}</sapn>
                                        @endif
                        </div>
                    </div>
                   
                    <div class="col-lg-10 col-md-8 col-sm-8 col-xs-8">
                        <div class="form-group">
                    
                            <label for="name">Contact Number</label>
                            <input class="form-control @if($errors->first('contnumber')) is-invalid @endif" id="name" type="text" name="contnumber" placeholder="Enter contact number"  autocomplete="off"  value="{{$user->number}}">
                            @if($errors->first('contnumber'))
                                        <sapn class="error">{{$errors->first('contnumber')}}</sapn>
                                        @endif
                        </div>
                    </div>
                    </div>
                

              
                </table>
                    </div>
                </div>
                <div class="sub-header-footer text-right">
                    <button class="btn bg-primary-blue br-0  waves-effect waves-light btn-sm" type="submit">Update</button>
                </div>
                
            </form>
            </div>
        <!-- Footer Part END-->

        <!-- Footer Part END-->      
    </main>
@endsection

@section('script')

    <!--Notifications Message Section-->
    @include('admin.layouts.notifications')
    <script>
        $("#editRole").validate({
            rules: {
                // simple rule, converted to {required:true}
                display_name: "required",
                name: "required",
            }
        });
    </script>

    <script>
        $('document').ready(function () {

            $('.permission-group').on('change', function(){
                $(this).parents('tr').find(".the-permission").prop('checked', this.checked);
            });

            function parentChecked(){
                $('.permission-group').each(function(){
                    var allChecked = true;
                    $(this).parents('tr').find(".the-permission").each(function(){
                        if(!this.checked) allChecked = false;
                    });
                    $(this).prop('checked', allChecked);
                });
            }

            parentChecked();

            $('.the-permission').on('change', function(){
                parentChecked();
            });
        });
    </script>
@endsection