<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <link href="{{asset('admin/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <script src="{{asset('admin/assets/js/bootstrap.min.js')}}"></script>
    <style>
        ul{
            list-style: none;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="banner-sml">
    <div class="container">
            <h2><a href="{{url('/')}}">login</a></h2>
        </div>
        <div class="container">
            <h2>Reset Password</h2>
        </div>
    </div>
    <div class="content-area">
        <div class="container">


            <div class="login-mc">
                <form class="form-horizontal" id="loginFrm" method="POST" action="{{url('password/email')}}">
                    {{ csrf_field() }}
                    <span class="login-message">
                        @if (session('status'))
                            {{ session('status') }}
                        @endif
                    </span>
                    <div class="form-group col-md-6">
                        <div class="popup-input {{ $errors->has('email') ? ' has-error' : '' }}">
                            <input  id="email" type="email" class="input form-control  mb-3" name="email" value="{{ old('email') }}" required placeholder="E-Mail Address">

                        </div>

                        
                    </div>
                    <div class="popup-input  mt-2">
                            <button type="submit" class="btn btn-success ">Send Password Reset Link</button>
                       
                    </div>
                    <div class="form-group col-md-6">
                    
                    @if ($errors->has('email'))
                                <span class="error">
                                    <ul>
                                        <li>{{ $errors->first('email') }}</li>
                                    </ul>
                                </span>
                            @endif
                    </div>
                    
                </form>
            </div>

        </div>
    </div>
</div>
</body>
</html>