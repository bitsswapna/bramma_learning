
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <link href="{{asset('admin/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <script src="{{asset('admin/assets/js/bootstrap.min.js')}}"></script>
    <style>
        ul{
            list-style: none;
        }
    </style>

</head>
<body>
<div class="container">
    <div class="banner-sml">
        <div class="container">
            <h2>Reset Password</h2>
        </div>
    </div>
    <div class="content-area">
        <div class="container">


            <div class="login-mc">
                <form class="form-horizontal" method="POST" action="{{ url('password/reset') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="token" value="{{ $token }}">
                    <span class="login-message">
                        @if (session('status'))
                            {{ session('status') }}
                        @endif
                    </span>
                    <ul class="form-content">
                        <li class="popup-input {{ $errors->has('email') ? ' has-error' : '' }}">
                            <input id="email" type="email" class="input" name="email" value="{{ $email or old('email') }}" required autofocus placeholder="E-Mail Address">

                            @if ($errors->has('email'))
                                <span class="error">
                                    <ul>
                                        <li>{{ $errors->first('email') }}</li>
                                    </ul>
                                </span>
                            @endif
                        </li>
                        <li class="popup-input {{ $errors->has('password') ? ' has-error' : '' }}">
                            <input id="password" type="password" class="input" name="password" required placeholder="Password">
                            @if ($errors->has('password'))
                                <span class="error">
                                    <ul>
                                        <li>{{ $errors->first('password') }}</li>
                                    </ul>
                                </span>
                            @endif
                        </li>
                        <li class="popup-input {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <input id="password-confirm" type="password" class="input" name="password_confirmation" required placeholder="Confirm Password">
                            @if ($errors->has('password_confirmation'))
                                <span class="error">
                                    <ul>
                                        <li>{{ $errors->first('password_confirmation') }}</li>
                                    </ul>
                                </span>
                            @endif
                        </li>

                        <li class="popup-input">
                            <button type="submit" class="button">Reset Password</button>
                        </li>
                    </ul>
                </form>
            </div>

        </div>
    </div>
{{--@endsection--}}

{{--@extends('layouts.app')--}}

{{--@section('content')--}}
{{--<div class="container">--}}
    {{--<div class="row">--}}
        {{--<div class="col-md-8 col-md-offset-2">--}}
            {{--<div class="panel panel-default">--}}
                {{--<div class="panel-heading">Reset Password</div>--}}

                {{--<div class="panel-body">--}}
                    {{--<form class="form-horizontal" method="POST" action="{{ route('password.request') }}">--}}
                        {{--{{ csrf_field() }}--}}

                        {{--<input type="hidden" name="token" value="{{ $token }}">--}}

                        {{--<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">--}}
                            {{--<label for="email" class="col-md-4 control-label">E-Mail Address</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" required autofocus>--}}

                                {{--@if ($errors->has('email'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('email') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">--}}
                            {{--<label for="password" class="col-md-4 control-label">Password</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="password" type="password" class="form-control" name="password" required>--}}

                                {{--@if ($errors->has('password'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('password') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">--}}
                            {{--<label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>--}}
                            {{--<div class="col-md-6">--}}
                                {{--<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>--}}

                                {{--@if ($errors->has('password_confirmation'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('password_confirmation') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group">--}}
                            {{--<div class="col-md-6 col-md-offset-4">--}}
                                {{--<button type="submit" class="btn btn-primary">--}}
                                    {{--Reset Password--}}
                                {{--</button>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}
{{--@endsection--}}
</div>
</body>
</html>