/**
/**
 * Created by COMPUTER on 3/28/2018.
 */

 // Notification messages START::
function successMsg($msg) {
    $(document).ready(function () {
        $.notify({
            title: "Update Complete : ",
            message: $msg,
            icon: 'fa fa-check',
        },{
            type: "success",
            delay: 500,
            placement: {
                align: 'right',
                from: "bottom"
            }
        });
    });
}

function errorMsg($msg) {
    $(document).ready(function () {
        $.notify({
            title: "error : ",
            message: $msg,
            icon: 'fa fa-ban',
        },{
            type: "danger",
            delay: 500,
            placement: {
                align: 'right',
                from: "bottom"
            }
        });
    });
}

function warningMsg($msg) {
    $(document).ready(function () {
        $.notify({
            title: "Warning : ",
            message: $msg,
            icon: 'fa fa-exclamation-triangle'
        },{
            type: "warning",
            delay: 500,
            placement: {
                align: 'right'
            }
        });
    });
}
// Notification messages END::
