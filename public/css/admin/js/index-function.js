

  //$('.change-status').change(function(){
    $(document).on('click','.change-status',function(){
      
                 var $this = this;
                 var $status =  $(this).is(":checked");
                 var $token = $('meta[name="csrf-token"]').attr("content");
                 var $data ={ id:$(this).data('id'),modal:$(this).data('modal'), _token:$token};
                 var $url = base_url + '/ajax/status-change';


                 $.ajax({
                     url: $url,
                     type: 'POST',
                     data: $data,
                     beforeSend: function() {

                     },
                     success: function (data) {
                        //  successMsg(data.msg);
                        $.notify({
                            title: '<strong>Success!</strong>',
                            message: 'You have Successfully Changed Status.'
                        },{
                            type: 'success'
                        });
                     },
                     error: function (data) {
                        
                         if($status){
                             $($this).prop('checked', false);
                         }else{
                             $($this).prop('checked', true);
                         }
                         var data = $.parseJSON(data.responseText);
                         //$.notify(data.msg);
                     },
                     complete: function() {

                     }
                 });
});