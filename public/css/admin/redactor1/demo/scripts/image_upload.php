<?php
 
// This is a simplified example, which doesn't cover security of uploaded images. 
// This example just demonstrate the logic behind the process. 
 
 
// files storage folder

$dir = $_SERVER['DOCUMENT_ROOT'].'/redactor/images/';

$len = count($_FILES['file']['name']);
$res_array = array();
for($i = 0; $i < $len; $i++) {
    $file_type = strtolower($_FILES['file']['type'][$i]);
    if ($file_type == 'image/png'
        || $file_type == 'image/jpg'
        || $file_type == 'image/gif'
        || $file_type == 'image/jpeg'
        || $file_type == 'image/pjpeg')
    {
        // setting file's mysterious name
        $filename = md5(date('YmdHis')).'.jpg';
        $file = $dir.$filename;

        // copying
        copy($_FILES['file']['tmp_name'][$i], $file);

        $res_array['file-'.$i] = ['url' => '/redactor/images/'.$filename,
            'id'=>time()];
    }
}
echo stripslashes(json_encode($res_array)); 
?>