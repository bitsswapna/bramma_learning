function readURL(input, img_con) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#' + img_con).find('img').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}


$("#image").change(function() {
    readURL(this, 'prof_img');
    $(this).parents('.form-group').find("#uploadFile").val(this.value);
});

$(function () {
    var $select =  $('.wellness-advisor-list').selectize({
        valueField: 'id',
        labelField: 'text',
        load: function (query, callback) {
            $.ajax({
                url:base_url+'/admin_user/getselectedusers?q=' + encodeURIComponent(query),
                success: function (response) {
                    $('#wellness-advisor-list').selectize({
                        options : response
                    });
                 
                    callback(response);
                }
            });
        }
    });

   // fetch the instance
// var selectize = $select[0].selectize;
// var validate= function(){
//     // console.log(e,checkSampleValidation)
//      var error=checkSampleValidation("set_damin",$('.wellness-advisor-list').val(),"Please enter the value");
//      $("#wellness-advisor-list").parent("div").parent("div").children("span.error").html(error)
// };
// var remove_error=function(){
//     $("#wellness-advisor-list").parent("div").parent("div").children("span.error").html("")
// }
// selectize.on('blur',validate);
// selectize.on('change',validate);
// selectize.on('focus',validate);


});
function formLoad(arg){
    var waid = $("#vi-waid").val();
    // RoleId = 0, 'admin', 'System Administrator'
    // RoleId = 1, 'wa', 'Wellness Adviser'
    // RoleId = 2, 'pc', 'Privilege Customer'
    // RoleId = 3, 'ma', 'Marketing Affiliate'
    // RoleId = 4, 'c', 'Customer'
    // RoleId = 5, 'cc', 'Customer Care'
    // RoleId = 6, 'production', 'Production Department'
    // RoleId = 7, 'stock', 'Stock Department'
    // RoleId = 8, 'pack', 'Packing Department'
    // RoleId = 9, 'sale', 'Sales Department'
    switch (arg){
        case '1':
        case '4':
        case '10':

            var url = base_url+'/get-wellness-advisor-form-admin-user/'+arg;
            $.ajax({
                url: url,
                type: 'GET',
                beforeSend:function(){
                    $("#new-form").html('<div style="display: inline-block;margin-right: 50%; margin-left: 50%;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>\n' +
                        '<span class="sr-only">Loading...</span></div>');
                },
                success: function(data) {
                    $("#new-form").html(data);
                },
                complete:function () {

                    $(function () {
                        $('.wellness-advisor-list').selectize({
                            valueField: 'id',
                            labelField: 'text',
                            load: function (query, callback) {
                                $.ajax({
                                    url:base_url+'/admin_user/getselectedusers?q=' + encodeURIComponent(query),
                                    success: function (response) {
                                        $('#wellness-advisor-list').selectize({
                                            options : response
                                        });
                                        callback(response);
                                    }
                                });
                            }
                        });

                        // var selectize = $select[0].selectize;

                    });
                }
            });
            break;

        case '5':

            var url = base_url+'/get-customer-care-category-form';
            $.ajax({
                url: url,
                type: 'GET',
                success: function(data) {
                    $("#new-form").html(data);
                }
            });
            break;
        default:
            var url = base_url+'/get-normal-form';
            $.ajax({
                url: url,
                type: 'GET',
                beforeSend:function(){
                },
                success: function(data) {
                    $("#new-form").html(data);
                }
            });
            break;
    }
}
$('body').on('change', '.role_id', function (e) {
    formLoad(this.value);
});

$('#createAdminUserWA').submit(function(evt) {
    $("#submit-btn").prop("disabled",true);
    evt.preventDefault();
    var formData = new FormData(this);
    var actionurl = base_url+'/admin-user-wa/create/'+$("#user_id").val();
    $.ajax({
        type: 'POST',
        url: actionurl,
        data:formData,
        cache:false,
        contentType: false,
        processData: false,
        success: function (result) {
            $("#submit-btn").prop("disabled",false);
            if (result.status == false) {
                $(".error").html("");

                $.each($.parseJSON(result.errors), function (i, item) {
                    err = item;
                    field = $('[name="' + i + '"]');
                    field.next('span.error').html(item);
                    return;
                });

            }else if(result.status == true){
                // if (result.permission == true) {

                var notify = $.notify('<strong>Saving</strong> Do not close this page...', {
                    allow_dismiss: false,
                    showProgressbar: true,
                    z_index:5000,

                });

                setTimeout(function () {
                    notify.update({
                        'type': 'success',
                        'message': '<strong>Success</strong> Your page has been saved!',
                        'progress': 25
                    });
                    $('#createAdminUser').trigger("reset");
                    $('#prof_img').find('img').attr('src', no_image_url);
                    $(".error").html("");
                    window.location.href = base_url + "/admin_users";
                }, 4500);

                // }else{
                //     swal("You have no permission to add Wellness Advisor")
                //     window.location.href = base_url + "/admin_user";
                // }

            }
        },
        error:function () {
            swal("Something went wrong ! Please try again");
            $("#submit-btn").prop("disabled",false);
        }
    });
});
$('#createAdminUser').submit(function(evt) {
    $("#submit-btn").prop("disabled",true);
    evt.preventDefault();
    var formData = new FormData(this);
    var actionurl = base_url+'/admin_user/create';
    $.ajax({
        type: 'POST',
        url: actionurl,
        data:formData,
        cache:false,
        contentType: false,
        processData: false,
        success: function (result) {
            $("#submit-btn").prop("disabled",false);
            if (result.status == false) {
                $(".error").html("");
                $.each($.parseJSON(result.errors), function (i, item) {
                    err = item;
                    field = $('[name="' + i + '"]');
                    field.next('span.error').html(item);
                    return;
                });

            }else if(result.status == true){


                var notify = $.notify('<strong>Saving</strong> Do not close this page...', {
                    allow_dismiss: false,
                    showProgressbar: true,
                    z_index:5000,
                });

                setTimeout(function() {
                    notify.update({'type': 'success', 'message': '<strong>Success</strong> Your page has been saved!', 'progress': 25});
                    $('#createAdminUser').trigger("reset");
                    $('#prof_img').find('img').attr('src', no_image_url);
                    $(".error").html("");

                }, 4500);



            }
        },
        error:function () {
            swal("Something went wrong ! Please try again");
            $("#submit-btn").prop("disabled",false);
        }
    });
});
$('#editAdminUser').submit(function(evt) {
    evt.preventDefault();
    $("#submit-btn").prop("disabled",true);
    var formData = new FormData(this);
    var actionurl = base_url+'/admin_user/edit/'+$('#user_id').val();
    $.ajax({
        type: 'POST',
        url: actionurl,
        data:formData,
        cache:false,
        contentType: false,
        processData: false,
        success: function (result) {
            $("#submit-btn").prop("disabled",false);
            $(".error").html("");
            if (result.status == false) {

                $.each($.parseJSON(result.errors), function (i, item) {
                    err = item;
                    field = $('[name="' + i + '"]');
                    field.next('span.error').html(item);
                    return;
                });

            }else if(result.status == true){

                $.notify(" Your page has been saved!");


            }
        },
        error:function () {
            swal("Something went wrong ! Please try again");
            $("#submit-btn").prop("disabled",false);
        }
    });
});


$('body').on('change', '#vi-admin-as-parent', function (e) {
    if($(this).is(':checked')){
        $("#vi-wa-as-parent").hide()
    }else{
        $("#vi-wa-as-parent").show()
    }
});
$('body').on('change', '#vi-admin-as-parent-wa', function (e) {
    if($(this).is(':checked')){
        $("#vi-wa-as-parent-wa").hide()
    }else{
        $("#vi-wa-as-parent-wa").show()
    }
});


//  $(document).ready(function(){
var table = $('#sampleTable').DataTable({

    "aaSorting": [],
    'order': [[1, 'desc']],
    "fnDrawCallback": function (oSettings) {
        if (oSettings.bSorted || oSettings.bFiltered || oSettings._iDisplayLength) {
            j = 0;
            for (var i = oSettings._iDisplayStart; i < oSettings.aiDisplay.length + oSettings._iDisplayStart; i++) {
                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[j]].nTr).find('span').text(i + 1);
                j++;
            }
        }
    },
    serverSide: true,
    scrollX: true,
    oLanguage: {
        sProcessing: '<svg width="30px"  height="30px"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-rolling" style="background: transparent;"><circle cx="50" cy="50" fill="none" stroke="#0fb963" stroke-width="10" r="35" stroke-dasharray="164.93361431346415 56.97787143782138" transform="rotate(228 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform></circle></svg>'
    },
    processing: true,
    "pageLength": 10,
    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
    "columnDefs": [{'orderable': false, 'targets': [0, 5, 5]}],
    mark: true,
    fixedColumns: {},
    ajax: {
        url: base_url+'/get-admin-users',
        data: function (d) {
            d.user_id = $('#user_id').val();
            d.user_name =  $('#user_name').val();

        }
    },
    columns: [
        {data: 'sl#', name: 'sl#'},
        {data: 'user_id', name: 'user_id'},
        {data: 'name', name: 'name'},
        {data: 'email', name: 'email'},
        {data: 'role', name: 'role'},
        {data: 'parent', name: 'parent'},
        {data: 'created_at', name: 'created_at'},
        {data: 'entry_type', name: 'entry_type'},
        {data: 'status', name: 'status'},
        {data: 'action', name: 'action'}
    ]
});
// })



function delay(callback, ms) {
    var timer = 0;
    return function() {
    var context = this, args = arguments;
    clearTimeout(timer);
    timer = setTimeout(function () {
        callback.apply(context, args);
    }, ms || 0);
    };
}


$('.input_keys').keyup(delay(function (e) {
    table.draw();
   // console.log('Time elapsed!', this.value);
}, 1000));

$('.filter_button').click(function (e) {
  //alert( $('#doj').val());
   table.draw();
    e.preventDefault();
});


$(document).ready(function () {
    $.fn.dataTableExt.sErrMode = 'throw';

    $('#sampleTable').on( 'error.dt', function ( e, settings, techNote, message ) {
        console.log( 'An error has been reported by DataTables: ', message );
        swal("Hey!", "Please Wait ! We are loading your datas!", "info")
            .then(function(willDelete) {
                if (willDelete) {
                    location.reload(true);
                }
            });
    } ) ;
});





/*Login as usre*/

$(document).on('click', '.adminuser-login', function (e) {
    e.preventDefault();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var sendData = {id: $(this).data('user'), '_token': CSRF_TOKEN}
    $.ajax({
        type: "post",
        url: base_url+'/adminusers/login',
        data: sendData,
        success: function (a) {
            // console.log(a)

            window.open(a, 'viegenomics', 'fullscreen=0');
        }
    });
});


$(document).on('click', '.view-user', function (e) {
    e.preventDefault();
    // $('#myModal').modal('toggle');
    var id = $(this).data("id");
    $.ajax({
        type: "get",
        url: base_url+'/adminusers/details',
        data: {id:id},
        success: function (a) {
            // console.log(a)
            $('.details-body').html(a);

            $('#myModal_users').modal('toggle');
            // window.open(a, 'viegenomics', 'fullscreen=0');
        }
    });
});

