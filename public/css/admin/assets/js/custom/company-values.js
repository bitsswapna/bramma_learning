$(document).ready(function(){
    var table = $('#vi-company-values-table').DataTable({
        "aaSorting": [],
        'order': [[1, 'desc']],
        "fnDrawCallback": function (oSettings) {
            if (oSettings.bSorted || oSettings.bFiltered || oSettings._iDisplayLength) {
                j = 0;
                for (var i = oSettings._iDisplayStart; i < oSettings.aiDisplay.length + oSettings._iDisplayStart; i++) {
                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[j]].nTr).find('span').text(i + 1);
                    j++;
                }
            }
        },
        serverSide: true,
        scrollX: true,
        oLanguage: {
            sProcessing: '<svg width="30px"  height="30px"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-rolling" style="background: transparent;"><circle cx="50" cy="50" fill="none" stroke="#0fb963" stroke-width="10" r="35" stroke-dasharray="164.93361431346415 56.97787143782138" transform="rotate(228 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform></circle></svg>'
        },
        processing: true,
        "pageLength": 10,
        "columnDefs": [{'orderable': false, 'targets': [0, 8, 8]}],
        mark: true,
        fixedColumns: {},
        ajax: {
            url: base_url + '/get-comapny-values',
        },
        columns: [
            {data: 'sl#', name: 'sl#'},
            {data: 'bv_value', name: 'bv_value'},
            {data: 'pv_value', name: 'pv_value'},
            {data: 'executive_active_value', name: 'executive_active_value'},
            {data: 'bh_active_value', name: 'bh_active_value'},
            {data: 'bm_active_value', name: 'bm_active_value'},
            {data: 'gm_active_value', name: 'gm_active_value'},
            {data: 'executive_pool_percentage', name: 'executive_pool_percentage'},
            {data: 'status', name: 'status'}
        ]
    });

})