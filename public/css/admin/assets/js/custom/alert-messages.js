
       
        $('.tab-alert').click(function(){
            var slug = $(this).data("slug") ;
            var $data ={slug:slug};
          // alert($data.slug);
            var $url = base_url +  '/alert-messages/get-alert-data';
                    $.ajax({
                        url: $url,
                        type: 'POST',
                        data: $data,
                        beforeSend: function() {
                            
                        },
                        success: function (data) {
                            $('#home').html(data.html);
                        },
                        error: function (data) {
                           
                        },
                        complete: function() {

                        }
                    });

        });
      
        
//home messages

        $('body').on('click', '.edit-home-data', function () {
            var id = $(this).data("id") ;
            $('.edit-home'+ id).show();
            $('.display-home'+ id).hide();
        });
 
        $('body').on('click', '.save-home-data', function () {
            var id = $(this).data("id") ;
            var $data ={id:$(this).data("homeid"),title:$('#hometitle'+id).val(),message:$('#homemessage'+id).val()};
            var $url = base_url +  '/alert-messages/change-data';
                    $.ajax({
                        url: $url,
                        type: 'POST',
                        data: $data,
                        beforeSend: function() {

                        },
                        success: function (data) {
                            location.reload();
                          
                        },
                        error: function (data) {
                           
                        },
                        complete: function() {

                        }
                    });
            $('.edit-home'+ id).hide();
            $('.display-home'+ id).show();

        });
 
        $('body').on('click', '.delete-msg', function () {
            var id = $(this).data("msgid") ;
           var $data ={id:id};
             var $url = base_url +  '/alert-messages/delete-alert-data';
                     $.ajax({
                         url: $url,
                         type: 'POST',
                         data: $data,
                         beforeSend: function() {
                             
                         },
                         success: function (data) {
                            location.reload();
                         },
                         error: function (data) {
                            
                         },
                         complete: function() {
 
                         }
                     });
 
        });