// $('.wellness-advisor-list').select2();
$(function () {
    $('.wellness-advisor-list-main').selectize({
        valueField: 'id',
        labelField: 'text',
        load: function (query, callback) {
            $.ajax({
                url:base_url+'/billing/getselectedusers?q=' + encodeURIComponent(query),
                success: function (response) {
                    $('#wellness-advisor-list-main').selectize({
                        options : response
                    });
                    callback(response);
                }
            });
        }
    });

    // var selectize = $select[0].selectize;

});
$('.wellness-advisor-list-main').on('change', function() {
    var waid = this.value;
    if (waid) {
        $(".wellness-advisor-list-input").val(waid);
        $(".loader").show();
        $(".varification-wa").html('');
        $(".address-form").html('');
        $(".payment-form").html('');
        $(".barcode-form").html('');
        var url = base_url + '/get-wellness-advisor-detail/' + waid;
        $.ajax({
            url: url,
            type: 'GET',
            beforeSend: function () {
                $(".varification-wa").html('<div style="margin-right: 50%; margin-left: 50%;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>\n' +
                    '<span class="sr-only">Loading...</span></div>');
            },
            success: function (data) {
                $(".varification-wa").html(data);
            },
            complete: function () {
                $(".loader").hide();

            }
        });
    }
});

$(document).ready(function(){

    $('body').on('click', '.purchase', function (e) {

        var url = base_url+'/get-barcode-form/'+$(".wellness-advisor-list-input").val();
        $.ajax({
            url: url,
            type: 'GET',

            beforeSend:function(){
                $(".address-form").html('');
                $(".payment-form").html('');
                // $(".loader").show();
                $(".barcode-form").html('<div style="margin-right: 50%; margin-left: 50%;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>\n' +
                    '<span class="sr-only">Loading...</span></div>');
            },
            success: function(data) {
                $('html, body').animate({
                    scrollTop: $(".barcode-form").offset().top
                }, 2000);
                $(".barcode-form").html(data.html);

            },
            complete:function () {
                // $(".loader").hide();
                //
                // var scrollPos =  $(".purchase").offset().top;
                // $(window).scrollTop(scrollPos);
            }
        });
    });
    $('body').on('click', '.barcode_validation', function (e) {

        var url = base_url+'/get-address-form/'+$(".wellness-advisor-list-input").val();
        $.ajax({
            url: url,
            type: 'POST',
            data:{
                barcode:$("#barcode_number").val(),
                productarray:productarray,
            },
            beforeSend:function(){
                // $(".loader").show();
                $(".address-form").html('<div style="margin-right: 50%; margin-left: 50%;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>\n' +
                    '<span class="sr-only">Loading...</span></div>');
            },
            success: function(data) {
                $('html, body').animate({
                    scrollTop: $(".address-form").offset().top
                }, 2000);

                if(data.status == true) {
                    $(".address-form").html(data.html);
                    $(".payment-form").html("");
                }else{
                    $(".address-form").html(data.message);
                    $(".payment-form").html("");
                }
                // if (data.address_first != 0){
                //
                // }

            },
            complete:function (data) {

                // $(".loader").hide();
                //
                // var scrollPos =  $(".purchase").offset().top;
                // $(window).scrollTop(scrollPos);
            }
        });
    });
    $(function () {

        $('body').on('click', '.save-customer-staff-form', function (e) {
            var actionurl = base_url+'/admin_user/create-customer';
            $(".save-customer-staff-form").prop("disabled",true);
            $.ajax({
                type: 'POST',
                url: actionurl,
                data:$("#createAdminUser").serialize(),
                success: function (result) {
                    if (result.permission == true) {
                        if (result.status == false) {
                            $(".error").html("");
                            $(".save-customer-staff-form").prop("disabled",false);
                            $.each($.parseJSON(result.errors), function (i, item) {
                                err = item;
                                field = $('[name="' + i + '"]');
                                field.next('span.error').html(item);
                                return;
                            });

                        }else if(result.status == true){
                            var notify = $.notify('<strong>Saving</strong> Do not close this page...', {
                                allow_dismiss: false,
                                showProgressbar: true,
                                z_index:15000,
                            });
                            setTimeout(function() {
                                $('#exampleModalScrollable').modal('hide');
                                $('#createAdminUser').trigger("reset");

                                notify.update({'type': 'success', 'message': '<strong>Success</strong> Your page has been saved!', 'progress': 25});
                                // $('#prof_img').find('img').attr('src', no_image_url);
                                $(".error").html("");
                                var waid = result.user_id;
                                $(".wellness-advisor-list-input").val(waid)
                                $(".wellness-advisor-list-main").val(waid)
                                $(".loader").show();
                                $(".varification-wa").html('');
                                $(".address-form").html('');
                                $(".payment-form").html('');
                                var url = base_url+'/get-wellness-advisor-detail/'+waid;
                                $.ajax({
                                    url: url,
                                    type: 'GET',
                                    beforeSend:function(){
                                    },
                                    success: function(data) {
                                        $(".varification-wa").html(data);
                                    },
                                    complete:function () {
                                        $(".loader").hide();

                                    }
                                });
                            }, 4500);
                        }

                    }else{
                        swal("You have no permission")
                    }
                },
                error:function () {
                    swal("Something went wrong ! Please try again");
                    $(".save-customer-staff-form").prop("disabled",false);
                }
            });
        });

        // $('#myform-edit-address').bind('submit', function () {

        $('body').on('submit', '#myform-edit-address', function (e) {
            var actionurl = base_url+'/billing/address-edit';

            $.ajax({
                type: 'post',
                url: actionurl,
                data: $('#myform-edit-address').serialize(),
                beforeSend: function () {
                    $("#submit-btn").prop("disabled",true);

                },
                success: function (result) {
                    if (result.status == false) {
                        $("#submit-btn").prop("disabled",false);
                        $.each($.parseJSON(result.errors), function (i, item) {
                            err = item;
                            field = $('[name="' + i + '"]');
                            field.next('span.error').html(item);
                            return;
                        });

                    }else if(result.status == true){
                        $(".new-delivery-address").html('');
                        $(".edit-delivery-address").html('');
                        $(".address-form").html('');
                        fetchDeliveryAddress();
                    }
                },error:function () {
                    swal("Something went wrong ! Please try again");
                    $("#submit-btn").prop("disabled",false);
                }

            });
            return false;
        });
    });
    $(function () {

        $('body').on('click', '.save-wa-form', function (e) {
            var actionurl = base_url+'/admin_user/create';
            $(".save-wa-form").prop("disabled",true);
            $.ajax({
                type: 'POST',
                url: actionurl,
                data:$("#createAdminUser").serialize(),
                success: function (result) {
                    // console.log(result)

                    if (result.permission == true) {
                        if (result.status == false) {
                            $(".error").html("");
                            $(".save-wa-form").prop("disabled",false);
                            $.each($.parseJSON(result.errors), function (i, item) {
                                err = item;
                                field = $('[name="' + i + '"]');
                                field.next('span.error').html(item);
                                return;
                            });
                        }else if(result.status == true){
                            var notify = $.notify('<strong>Saving</strong> Do not close this page...', {
                                allow_dismiss: false,
                                showProgressbar: true,
                                z_index:15000,
                            });

                            setTimeout(function() {
                                $('#exampleModalScrollable').modal('hide');
                                $('#createAdminUser').trigger("reset");

                                notify.update({'type': 'success', 'message': '<strong>Success</strong> Your page has been saved!', 'progress': 25});
                                // $('#prof_img').find('img').attr('src', no_image_url);
                                $(".error").html("");
                                var waid = result.user_id;
                                $(".wellness-advisor-list-input").val(waid)
                                $(".wellness-advisor-list-main").val(waid)
                                $(".loader").show();
                                $(".varification-wa").html('');
                                $(".address-form").html('');
                                $(".payment-form").html('');
                                var url = base_url+'/get-wellness-advisor-detail/'+waid;
                                $.ajax({
                                    url: url,
                                    type: 'GET',
                                    beforeSend:function(){
                                    },
                                    success: function(data) {
                                        $(".varification-wa").html(data);
                                    },
                                    complete:function () {
                                        $(".loader").hide();

                                    }
                                });
                            }, 4500);
                        }

                    }else{
                        swal("You have no permission")
                    }




                },
                error:function () {
                    swal("Something went wrong ! Please try again");
                    $(".save-wa-form").prop("disabled",false);
                }
            });
        });

        // $('#myform-edit-address').bind('submit', function () {

        $('body').on('submit', '#myform-edit-address', function (e) {
            var actionurl = base_url+'/billing/address-edit';

            $.ajax({
                type: 'post',
                url: actionurl,
                data: $('#myform-edit-address').serialize(),
                beforeSend: function () {
                    $("#submit-btn").prop("disabled",true);

                },
                success: function (result) {
                    if (result.status == false) {
                        $("#submit-btn").prop("disabled",false);
                        $.each($.parseJSON(result.errors), function (i, item) {
                            err = item;
                            field = $('[name="' + i + '"]');
                            field.next('span.error').html(item);
                            return;
                        });

                    }else if(result.status == true){
                        $(".new-delivery-address").html('');
                        $(".edit-delivery-address").html('');
                        $(".address-form").html('');
                        fetchDeliveryAddress();
                    }
                },
                error:function () {
                    swal("Something went wrong ! Please try again");
                    $("#submit-btn").prop("disabled",false);
                }

            });
            return false;
        });
    });
    function fetchDeliveryAddress() {
        var url = base_url+'/get-address-form/'+$(".wellness-advisor-list-input").val();
        $.ajax({
            url: url,
            type: 'POST',
            data:{
                barcode:$("#barcode_number").val(),
                productarray:productarray,
            },
            beforeSend:function(){
                // $(".loader").show();
                $(".address-form").html('<div style="margin-right: 50%; margin-left: 50%;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>\n' +
                    '<span class="sr-only">Loading...</span></div>');
            },
            success: function(data) {
                $('html, body').animate({
                    scrollTop: $(".address-form").offset().top
                }, 2000);

                if(data.status == true) {
                    $(".address-form").html(data.html);
                }else{
                    $(".address-form").html(data.message);
                }


            },
            complete:function () {
                // $(".loader").hide();
                //
                // var scrollPos =  $(".purchase").offset().top;
                // $(window).scrollTop(scrollPos);
            }
        });
    }
    var productarray = {};
    /*Product quantity update*/
    // $('body').on('click', '.remove-from-cart', function (e) {
    //     var product_id = $(this).data("id") ;
    //     var slug = "#vi-product-slug"+product_id;
    //     var slug = $(slug).val();
    //     delete productarray[slug];
    //     var deleteItem = "#delete-cart-item"+product_id;
    //     $(deleteItem).html('');
    //
    // });
    $('body').on('click', '.add-to-cart', function (e) {
        var product_id = $(this).data("id");
        var product_name = "#vi-product-product_name"+product_id;
        var product_name_val = $(product_name).val();
        $(".product_selected").val(product_id);

        if(Object.keys(productarray).length == 0){
            var title = "Thank you for selecting "+product_name_val+ " test";
            var text = "";
        }else{
            var title = "Thank you for selecting "+product_name_val+ " test";
            var text = "Previously selected test will be discarded.!";

        }
        swal({
            title: title,
            text: text,
            icon: "success",
            buttons: true,
            dangerMode: true,
        })

            .then((willDelete) => {
                if (willDelete) {

                    if(Object.keys(productarray).length == 0){
                        //
                    }else {
                        $('.delete-cart-item').html('');
                        $('.delete-cart-item').removeClass('chip');
                        productarray = {};
                    }

                    var quantity_ = "#vi-product-quantity" + product_id;
                    var quantity = $(quantity_).val();

                    var total = "#vi-product-total" + product_id;
                    var price_ = "#vi-product-price" + product_id;
                    var price = $(price_).val();
                    var slug = "#vi-product-slug" + product_id;
                    var slug = $(slug).val();

                    var productarray_ = {};
                    productarray_['product_id'] = product_id;
                    productarray_['qty'] = quantity;
                    productarray_['price'] = price;
                    productarray[slug] = productarray_;

                    if (parseInt(quantity) > 0) {
                        var url = base_url + '/cart/get-stock';
                        $.ajax({
                            url: url,
                            type: 'POST',
                            data: {
                                'quantity': quantity,
                                'product_id': product_id
                            },
                            beforeSend: function () {
                                $(".address-form").html('');
                                $(".payment-form").html('');
                                $(".barcode-form").html('');
                                $(".purchase").hide();
                                $(".purchase-btn-div").html('<div style="margin-right: 50%; margin-left: 50%;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>\n' +
                                    '<span class="sr-only">Loading...</span></div>');
                            },
                            success: function (result) {

                                $(".purchase-btn-div").html("");

                                if (result > 0) {
                                    var i = parseInt(quantity);
                                    /*Todo: Set max limit dynamic*/
                                    if (parseInt(i) <= result) {
                                        if (i == 1) {
                                            var deleteItem = "#delete-cart-item" + product_id;
                                            $(deleteItem).addClass('chip');
                                            $(deleteItem).html('<div class="icon"><i class="fa fa-check"> </i> </div>Item Selected')
                                            // $(deleteItem).html('<button type="button" class="btn btn-link remove-from-cart br-0 btn-sm btn-btn-rm text-danger" id="vi-add-cart' + product_id + '" data-id="' + product_id + '"><i class="fa fa-trash" aria-hidden="true"></i> Remove Item(s)</button>\n')
                                        }
                                        var total_price = parseFloat(price) * parseFloat(quantity);
                                        $(quantity).val(i);
                                        $(total).html('Total Price : <i class="fa fa-inr" aria-hidden="true">' + total_price);
                                        $(".purchase").show();
                                        $('html, body').animate({
                                            scrollTop: $(".purchase").offset().top
                                        }, 2000);

                                    } else {
                                        swal("Available stock quantity " + result);
                                        $(quantity_).val(result);
                                    }
                                } else {
                                    swal("Stock not available");
                                }
                            },
                            complete: function () {
                                $('#if_loading').html("");
                            }
                        });
                    } else {
                        swal("Please enter quantity");
                        $(".purchase").hide();
                    }

                } else {
                    swal("Not Selected!");
                }
            });

    });
    // $('body').on('click', '.proceed', function (e) {
    //     let address_id = $(this).data("id");
    //     var url = base_url+'/get-Payment-form';
    //     var urlNext = base_url+'/get-price-product';
    //     var firstname = $("#first_name").val();
    //     var address = $("#address1").val();
    //     var mobile = $("#phonenumber").val();
    //     if (firstname && address && mobile) {
    //         if(phonenumber(mobile)){
    //             $("#phonenumber_check").html("");
    //             $.ajax({
    //                 url: url,
    //                 type: 'GET',
    //                 beforeSend: function () {
    //                     //
    //                 },
    //                 success: function (data) {
    //                     $(".payment-form").html(data);
    //                 },
    //                 complete: function () {
    //                     // console.log(productarray)
    //                     /*Todo*/
    //                     var gtotal = 0;
    //                     for (var productarray1 in productarray) {
    //                         var price = productarray[productarray1]['price'];
    //                         var qty = productarray[productarray1]['qty'];
    //                         var total = price * qty;
    //                         var gtotal = total + gtotal;
    //                     }
    //                     $(".payment-detail").html("<p> Price :" + gtotal + "</p>");
    //                 }
    //             });
    //         }else{
    //             $("#phonenumber").focus();
    //             $("#phonenumber_check").html("Not valid");
    //             return false
    //         }
    //     }else{
    //         swal("Some fields are missing . Please fill all fields");
    //     }
    // });
    $('body').on('click', '.proceed', function (e) {
        $(".product-taxes-tax_per").html('')
        let address_id = $(this).data("id");
        $(".proceed").html('Deliver Here')
        $(this).html('<span class="badge badge-light"><i class="fa fa-check"></i></span>Deliver Here')
        $("#form_address_id").val(address_id);
        var url = base_url+'/get-payment-form';

        $.ajax({
            url: url,
            type: 'GET',
            data:{
                address_id:address_id,
                product_id:$(".product_selected").val()
            },
            beforeSend: function () {
                //
                $(".payment-form").html('<div style="margin-right: 50%; margin-left: 50%;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>\n' +
                    '<span class="sr-only">Loading...</span></div>');
            },
            success: function (data) {
                for (i=0; i< data.product_taxes.length;i++) {
                    $(".product-taxes-tax_per").append(' '+
                        data.product_taxes[i]['tax_name'] +"-"+ parseInt(data.product_taxes[i]['tax_percentage'])+"%"
                        +' ');
                }
                $('html, body').animate({
                    scrollTop: $(".payment-form").offset().top
                }, 2000);
                $(".payment-form").html(data.html);

            },
            complete: function () {

            }
        });

    });
    function phonenumber(inputtxt)
    {
        if (inputtxt.length != 10)
        {
            return false
        }else{
            return true
        }
    }
    // $('body').on('change', 'input[type=radio][name=payment_method]', function (e) {
    $('body').on('click', '.payment_method_cls', function (e) {
        // $('.confirm-message').html('Rs '+$(".total-amount").val()+' will collected from customer.');
        var radioValue = $(this).data("id");
        $(".payment_method").val(radioValue);

        $.ajax({
            url: base_url+'/billing/get-payment-form/'+radioValue,
            type: 'GET',
            data:{amount:$(".total-amount").val()},
            beforeSend: function () {
                //
            },
            success: function (data) {
                $('.confirm-message').html(data.html);
                $('.confirm-order').bPopup();
            },
            complete: function () {

            }
        });
    });

    $('body').on('click', '.delivery-edit-btn', function () {

        $(".new-delivery-address").html('');
        $(".edit-delivery-address").html('');
        $(".show-delivery-address").hide();
        var id = $(this).data("id") ;
        $.ajax({
            type: 'GET',
            url: base_url+'/billing/get-shipping-address-form/'+id+'/'+$(".wellness-advisor-list-main").val(),
            beforeSend: function () {

            },
            success: function (result) {
                $(".edit-delivery-address").html(result);
            }

        });
    });

    $('body').on('click', '.delivery-cancel-btn', function () {

        $(".new-delivery-address").html('');
        $(".edit-delivery-address").html('');
        $(".show-delivery-address").show();

    });
    $(function () {
        $('body').on('click', '.delivery-add-btn', function () {
            $(".new-delivery-address").html('');
            $(".edit-delivery-address").html('');
            $(".show-delivery-address").hide();
            $.ajax({
                type: 'GET',
                url: base_url+'/billing/get-shipping-address-form/new/'+$(".wellness-advisor-list-main").val(),
                beforeSend: function () {
                    //
                },
                success: function (result) {
                    $(".new-delivery-address").html(result);
                }
            });
        });
    });

    $(function () {
        // $('#myform-edit-address').bind('submit', function () {
        $('body').on('submit', '#myform-new-address', function (e) {
            $(".payment-form").html('')
            var actionurl = base_url+'/billing/address-add';

            $.ajax({
                type: 'post',
                url: actionurl,
                data: $('#myform-new-address').serialize(),
                beforeSend: function () {
                    $("#submit-btn").prop("disabled",true);

                },
                success: function (result) {
                    if (result.status == false) {
                        $("#submit-btn").prop("disabled",false);
                        $.each($.parseJSON(result.errors), function (i, item) {
                            err = item;
                            field = $('[name="' + i + '"]');
                            field.next('span.error').html(item);
                            return;
                        });

                    }else if(result.status == true){
                        $(".new-delivery-address").html('');
                        $(".edit-delivery-address").html('');
                        $(".address-form").html('');
                        fetchDeliveryAddress();
                    }
                }

            });
            return false;
        });
    });


    $('body').on('click', '.checkout', function (e) {
        $(".checkout").prop("disabled",true);
        if (!$(".payment_method").val()) {
            // if (!$("input[name='payment_method']:checked").val()) {
            swal('Choose Payment method.');
            return false;
        }
        else {
            var url = base_url+'/checkout';

            var form_address_id = $("#form_address_id").val();

            $.ajax({
                url: url,
                type: 'POST',
                data:{
                    'cart':productarray,
                    'user_id':$(".wellness-advisor-list-input").val(),
                    'form_address_id':form_address_id,
                    'payment_type':$(".payment_method").val(),
                    // 'sample_type':$("input[name='sample_type']:checked").val(),
                    'barcode':$("#barcode_number").val(),
                    'id_who_collected_billing':$("#id_who_collected_billing").val(),
                    'name_who_collected_billing':$("#name_who_collected_billing").val(),
                    'amount_billing':$("#amount_billing").val(),
                    'cheque_number_billing':$("#cheque_number_billing").val(),
                    'number_billing':$("#number_billing").val(),
                    'date_billing':$("#date_billing").val(),

                    'id_type_who_collected_billing':$("#id_type_who_collected_billing").val(),
                    'cheque_date_billing':$("#cheque_date_billing").val(),
                    'cheque_bank_billing':$("#cheque_bank_billing").val(),

                },

                beforeSend: function () {
                    $(".invoice").html("<p>Loading</p>");
                },
                success: function (data) {
                    $(".checkout").prop("disabled",false);
                    if (data.status == false) {
                        $(".error").html("");
                        $.each($.parseJSON(data.errors), function (i, item) {
                            err = item;
                            field = $('[name="' + i + '"]');
                            field.next('span.error').html(item);
                            return;
                        });

                    }else if(data.status == true){
                        // $(".invoice").html(data);
                        if (data.pay == '3') {
                            var form = data.form;
                            $('body').append(form);
                            $("#admin-payment-form").submit();
                        }else{
                            window.location.href = base_url + "/invoice/" + data.invoice_id;
                        }
                    }
                },

                complete: function () {
                }
            });
        }
    });

});
$(document).ready(function(){
    var table = $('#vi-billings-table').DataTable({
        "aaSorting": [],
        'order': [[1, 'desc']],
        "fnDrawCallback": function (oSettings) {
            if (oSettings.bSorted || oSettings.bFiltered || oSettings._iDisplayLength) {
                j = 0;
                for (var i = oSettings._iDisplayStart; i < oSettings.aiDisplay.length + oSettings._iDisplayStart; i++) {
                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[j]].nTr).find('span').text(i + 1);
                    j++;
                }
            }
        },
        serverSide: true,
        scrollX: true,
        oLanguage: {
            sProcessing: '<svg width="30px"  height="30px"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-rolling" style="background: transparent;"><circle cx="50" cy="50" fill="none" stroke="#0fb963" stroke-width="10" r="35" stroke-dasharray="164.93361431346415 56.97787143782138" transform="rotate(228 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform></circle></svg>'
        },
        processing: true,
        "pageLength": 10,
        "columnDefs": [{'orderable': false, 'targets': [0, 5, 5]}],
        mark: true,
        fixedColumns: {},
        ajax: {
            url: base_url + '/get-billings',
        },
        columns: [
            {data: 'sl#', name: 'sl#'},
            {data: 'date', name: 'date'},
            {data: 'orderid', name: 'orderid'},
            {data: 'billed_by', name: 'billed_by'},
            {data: 'user_id', name: 'user_id'},
            {data: 'order_price', name: 'order_price'},
            {data: 'purchase_price', name: 'purchase_price'},
            {data: 'status', name: 'status'}
        ]
    });


});

