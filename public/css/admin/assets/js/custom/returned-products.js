var product_status = null;
var table = $('#returned_products').DataTable({
    "aaSorting": [],
    'order': [[1, 'desc']],
    "fnDrawCallback": function (oSettings) {
        if (oSettings.bSorted || oSettings.bFiltered || oSettings._iDisplayLength) {
            j = 0;
            for (var i = oSettings._iDisplayStart; i < oSettings.aiDisplay.length + oSettings._iDisplayStart; i++) {
                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[j]].nTr).find('span').text(i + 1);
                j++;
            }
        }
    },
    serverSide: true,
    scrollX: true,
    oLanguage: {
        sProcessing: '<svg width="30px"  height="30px"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-rolling" style="background: transparent;"><circle cx="50" cy="50" fill="none" stroke="#0fb963" stroke-width="10" r="35" stroke-dasharray="164.93361431346415 56.97787143782138" transform="rotate(228 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform></circle></svg>'
    },
    processing: true,
    "pageLength": 15,
    "columnDefs": [{'orderable': false, 'targets': [0, 7, 6]}],
    mark: true,
    fixedColumns: {},
    ajax: {
        url: base_url + '/order/get-returnedProducts',
        data: function (d) {
            x = document.getElementById("product_status").selectedIndex;
            d.product_status = document.getElementsByTagName("option")[x].value;
            d.start_date = $('#start_date').val();
            d.end_date = $('#end_date').val();
        }
    },
    columns: [
        {data: 'sl#', name: 'Sl#'},
        {data: 'request_id', name: 'request_id'},
        {data: 'order_id', name: 'order_id'},
        {data: 'product_name', name: 'product_name'},
        {data: 'quantity', name: 'quantity'},
        {data: 'product_type', name: 'product_type'},
        {data: 'date', name: 'date'},
        {data: 'status', name: 'status'},
        {data: 'action', name: 'action'}
    ]
});

$('#product_status').change(function (e) {
    table.draw();
    e.preventDefault();
});


$(function () {
    $('input[name="daterange"]').daterangepicker({
        opens: 'left'
    }, function (start, end, label) {
        var startDate = start.format('YYYY-MM-DD');
        var endDate = end.format('YYYY-MM-DD');
        // alert(startDate);
        $('#start_date').val(startDate);
        $('#end_date').val(endDate);
        $('.showDate_field').show();
        table.draw();

    });
});


$(document).on('change', '.change-return-status', function (e) {
    var id = $(this).data("id");
    var value = this.value;
    var url = base_url + '/order/change_return_status';
    //alert(id);

    swal({
        title: "Are you sure?",
        text: "Please Input your Description Before Submit",
        icon: "warning",
        buttons: true,
        dangerMode: true,

        content: {
            element: "textarea",
            attributes: {
                placeholder: "Type your comment",
                id: "comment",
                class: "form-control",
            },
        },


    })


        .then((willDelete) => {
            if (willDelete) {
                var comment = document.getElementById('comment').value;
                var data = {id: id, value: value, comment: comment};
                if (comment != '') {
                    $.ajax({
                        url: url, type: 'get', data: data,
                        beforeSend: function () {

                        },
                        success: function (data) {
                            table.draw();
                        },
                        error: function (data) {

                        },
                        complete: function () {

                        }
                    });
                    swal("OK! Successfully Changed!", {
                        icon: "success",
                    });
                } else {
                    table.draw(swal("please input comment before submitting", {
                        icon: "warning",
                    }));


                }
            } else {
                swal("Status Not Changed!");
                table.draw();
            }
        });

});


$(document).on('click mouseover', '.product_details', function () {

    $('[rel="popover"]').popover({
        container: 'body',
        html: true,
        content: function () {
            var clone = $($(this).data('popover-content')).clone(true).removeClass('hide');
            return clone;
        }
    }).click(function (e) {
        e.preventDefault();
    });
});


$('body').on('click', function (e) {
    $('[rel="popover"]').each(function () {
        // hide any open popovers when the anywhere else in the body is clicked
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            $(this).popover('hide');
        }
    });
});


$(document).ready(function () {

    $('.order-select').select2();
});


$(document).on('click', '.fetch_return_time_line', function (e) {
    var url = base_url + '/order/get-return-timeline';
    var id = $(this).data("id");
    var data = {id: id};
    $.ajax({
        type: 'get',
        url: url,
        data: data,
        beforeSend: function () {
            $("#if_loading").html("loading");
        },
        success: function (data) {
                $("#vi-return-timeline").html(data);

        },
        complete: function () {

        }
    });

});


$(function () {
    $('body').on('click', '.pagination a', function (e) {
        e.preventDefault();
        var url = $(this).attr('href');
        var id = $('.getreturn_id').val();
        var data = {id: id};

        $.ajax({
            url: url, data: data,
        }).done(function (data) {
            $('#vi-return-timeline').html(data);
        }).fail(function () {
            alert('log could not be loaded.');
        });


    });


});




