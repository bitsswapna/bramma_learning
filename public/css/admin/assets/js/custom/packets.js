$(document).ready(function(){
    var table = $('#vi-packets-table').DataTable({
        "aaSorting": [],
        'order': [[1, 'desc']],
        "fnDrawCallback": function (oSettings) {
            if (oSettings.bSorted || oSettings.bFiltered || oSettings._iDisplayLength) {
                j = 0;
                for (var i = oSettings._iDisplayStart; i < oSettings.aiDisplay.length + oSettings._iDisplayStart; i++) {
                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[j]].nTr).find('span').text(i + 1);
                    j++;
                }
            }
        },
        serverSide: true,
        scrollX: true,
        oLanguage: {
            sProcessing: '<svg width="30px"  height="30px"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-rolling" style="background: transparent;"><circle cx="50" cy="50" fill="none" stroke="#0fb963" stroke-width="10" r="35" stroke-dasharray="164.93361431346415 56.97787143782138" transform="rotate(228 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform></circle></svg>'
        },
        processing: true,
        "pageLength": 10,
        "columnDefs": [{'orderable': false, 'targets': [0, 5, 5]}],
        mark: true,
        fixedColumns: {},
        ajax: {
            url: base_url + '/get-packets',
        },
        columns: [
            {data: 'sl#', name: 'sl#'},
            {data: 'user_id', name: 'user_id'},
            {data: 'total_saliva', name: 'total_saliva'},
            {data: 'total_boold', name: 'total_boold'},
            {data: 'billed_saliva', name: 'billed_saliva'},
            {data: 'billed_blood', name: 'billed_blood'},
        ]
    });




    $(function () {
        $('.wellness-advisor-list-main').selectize({
            valueField: 'id',
            labelField: 'text',
            load: function (query, callback) {
                $.ajax({
                    url:base_url+'/packet/getselectedusers?q=' + encodeURIComponent(query),
                    success: function (response) {
                        $('#wellness-advisor-list-main').selectize({
                            options : response
                        });
                        callback(response);
                    }
                });
            }
        });

        // var selectize = $select[0].selectize;

    });
    function addPacket(waid){
        $(".wellness-advisor-list-input").val(waid);
        $(".loader").show();
        $(".varification-wa").html('');
        $(".address-form").html('');
        $(".payment-form").html('');
        var url = base_url + '/packet/get-wellness-advisor-detail/' + waid;
        $.ajax({
            url: url,
            type: 'GET',
            beforeSend: function () {
            },
            success: function (data) {
                $(".varification-wa").html(data);
            },
            complete: function () {
                $(".loader").hide();

            }
        });
    }
    $('.wellness-advisor-list-main').on('change', function() {
        var waid = this.value;
        $("#wellness-advisor-list-input").val(this.val)
        if (waid) {
            addPacket(waid);
        }
    });


    $('input[name="admin_check"]').click(function(){
        if($(this).prop("checked") == true){
            $("#wellness-advisor-list-input").val(100)
            $('.varification-wa').html('');
            addPacket('100');

            $('#vi-wellness-advisor').hide();

        }
        else if($(this).prop("checked") == false){
            $('.varification-wa').html('');
            $('#vi-wellness-advisor').show();
        }
    });


    var productarray = {};

    $('body').on('click', '.add-to-cart', function (e) {
        var product_id = $(this).data("id");
        var product_name = "#vi-product-product_name"+product_id;
        var product_name_val = $(product_name).val();
        $(".product_selected").val(product_id);

        var quantity_ = "#vi-product-quantity" + product_id;
        var quantity = $(quantity_).val();

        var total = "#vi-product-total" + product_id;
        var price_ = "#vi-product-price" + product_id;
        var price = $(price_).val();
        var slug = "#vi-product-slug" + product_id;
        var slug = $(slug).val();

        var productarray_ = {};
        productarray_['product_id'] = product_id;
        productarray_['qty'] = quantity;
        productarray_['price'] = price;
        productarray[slug] = productarray_;

        if (parseInt(quantity) > 0) {
            var url = base_url + '/cart/get-stock';
            $.ajax({
                url: url,
                type: 'POST',
                data: {
                    'quantity': quantity,
                    'product_id': product_id
                },
                beforeSend: function () {
                    $("#if_loading").html("loading");
                },
                success: function (result) {
                    if (result > 0) {
                        var i = parseInt(quantity);
                        /*Todo: Set max limit dynamic*/
                        if (parseInt(i) <= result) {
                            var deleteItem = "#delete-cart-item" + product_id;
                            $(deleteItem).addClass('chip');
                            $(deleteItem).html('<div class="icon"><i class="fa fa-check"> </i> </div>Item Selected')
                            // $(deleteItem).html('<button type="button" class="btn btn-link remove-from-cart br-0 btn-sm btn-btn-rm text-danger" id="vi-add-cart' + product_id + '" data-id="' + product_id + '"><i class="fa fa-trash" aria-hidden="true"></i> Remove Item(s)</button>\n')
                            var total_price = parseFloat(price) * parseFloat(quantity);
                            $(quantity).val(i);
                            $(total).html('Total Price : <i class="fa fa-inr" aria-hidden="true">' + total_price);
                            $(".purchase").show()

                        } else {
                            swal("Available stock quantity " + result);
                            $(quantity_).val(result);
                        }
                    } else {
                        swal("Stock not available");
                    }
                },
                complete: function () {
                    $('#if_loading').html("");
                }
            });
        } else {
            swal("Please enter quantity");
            $(".purchase").hide()
        }


    });

    $('body').on('click', '#allocate_packet', function (e) {
        // alert($("#tube_quantity").val())
        // alert($("#wellness-advisor-list-main").val())
        if($("#tube_quantity_saliva").val() == '' && $("#tube_quantity_blood").val() == ''  )
        {
            swal("Please enter the value!");
        }else if($("#tube_quantity_saliva").val() < 0 || $("#tube_quantity_blood").val() < 0 ){
            swal("Please enter the valid data!");
        }else{
            var url = base_url + '/packet/create/' + $("#wellness-advisor-list-input").val();
            $.ajax({
                url: url,
                type: 'POST',
                data: {
                    tube_quantity_saliva: $("#tube_quantity_saliva").val(),
                    tube_quantity_blood: $("#tube_quantity_blood").val(),
                    // wid: $("#wellness-advisor-list-main").val(),
                },
                beforeSend: function () {
                    $("#allocate_packet").prop("disabled", true);
                    // $(".loader").show();

                },
                success: function (data) {
                    $(".varification-wa").html("");
                    //swal("Success")
                    var w = window.open('about:blank');
                    w.document.open();
                    w.document.write(data.html);
                    w.document.close();

                },
                complete: function () {
                    // location.reload();
                    $("#allocate_packet").prop("disabled", false);

                }
            });
        }
    });

    $('body').on('click', '.purchase', function (e) {

        var url = base_url + '/packet/create/' + $(".wellness-advisor-list-input").val();
        $.ajax({
            url: url,
            type: 'POST',
            data: {
                productarray: productarray,
            },
            beforeSend: function () {
                // $(".loader").show();

            },
            success: function (data) {

            },
            complete: function () {
                // location.reload();
            }
        });
    });
});