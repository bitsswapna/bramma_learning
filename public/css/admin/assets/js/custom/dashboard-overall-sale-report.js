$(document).ready(function(){
    /*Function to display count*/
    function getSaleMonthlyReport(month) {
        $.ajax({
            url: base_url + "/dashboard/get-dashboard-sales-report",
            type: 'POST',
            data: {month: month},
            beforeSend: function () {

            },
            success: function (data) {
                $(".sales-count").html(data.sales_count);
                $(".sales-total-price").html(data.sales_total_price);
                $(".sales-total-pv").html(data.sales_total_pv);
                $(".sales-wa").html(data.wa_sales);
                $(".sales-customer").html(data.customer_sale);
                $(".wa-join").html(data.wa_join);
                $(".customer-count").html(data.customer_join);
                $(".staff-count").html(data.staff_join);
                $(".referral-link-purchase").html(data.referral_link_count);
                $(".billing-purchase").html(data.billing_count);
            },
            complete: function () {
            },
            error: function () {
                // alert("Something went wrong ..!");
                // location.reload();
            }
        });
    }

    /*Call monthly report function*/
    getSaleMonthlyReport(13);

    /*Month change event*/
    $('body').on('click', '.sales-tab', function (e) {
        getSaleMonthlyReport($(this).data("id"));
    });
});
