var table_sale_report = $('#admin-sales-report-table').DataTable({
    dom: 'lBfrtip',
    "aaSorting": [],
    'order': [[10, 'desc']],
    buttons: [
        {extend:"copy",footer: true},
        {extend:"csv",footer: true} ,
        {extend:"excel",footer: true},
        {extend:"pdf",footer: true,exportOptions: {
                columns: ':visible',
                stripHtml: true
            },
            pageSize: 'LETTER'  ,customize: function(doc, config) {
                var tableNode;
                for (i = 0; i < doc.content.length; ++i) {
                    if(doc.content[i].table !== undefined){
                        tableNode = doc.content[i];
                        break;
                    }
                }

                var rowIndex = 0;
                var tableColumnCount = tableNode.table.body[rowIndex].length;

                if(tableColumnCount > 5){
                    doc.pageOrientation = 'landscape';
                }
            }},
        {extend:"print",footer: true, orientation: 'landscape',exportOptions: { columns: ':visible' }},
        {extend:'colvis',
            columnText: function (dt, idx, title) {
                return (idx + 1) + ': ' + title;
            }
        }

    ],

    "fnDrawCallback": function (oSettings) {
        if (oSettings.bSorted || oSettings.bFiltered || oSettings._iDisplayLength) {
            j = 0;
            for (var i = oSettings._iDisplayStart; i < oSettings.aiDisplay.length + oSettings._iDisplayStart; i++) {
                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[j]].nTr).find('span').text(i + 1);
                j++;
            }
        }
    },
    serverSide: true,
    scrollX: true,
    oLanguage: {
        sProcessing: '<svg width="30px"  height="30px"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-rolling" style="background: transparent;"><circle cx="50" cy="50" fill="none" stroke="#0fb963" stroke-width="10" r="35" stroke-dasharray="164.93361431346415 56.97787143782138" transform="rotate(228 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform></circle></svg>'
    },
    processing: true,
    "pageLength": 10,
    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
    "columnDefs": [{'orderable': false, 'targets': [0, 16, 16]}],
    mark: true,
    fixedColumns: {},
    "footerCallback": function(row, data, start, end, display) {
        var api = this.api(), data;

        // converting to interger to find total
        var intVal = function ( i ) {
            return typeof i === 'string' ?
                i.replace(/[\$,]/g, '')*1 :
                typeof i === 'number' ?
                    i : 0;
        };

        $( api.column( 0 ).footer() ).html('Total');
        $(api.column(1).footer()).html("--");
        $(api.column(2).footer()).html("--");
        $(api.column(3).footer()).html("--");
        $(api.column(4).footer()).html("--");
        $(api.column(5).footer()).html("--");
        $(api.column(6).footer()).html("--");
        $(api.column(7).footer()).html("--");
        $(api.column(8).footer()).html("--");
        $(api.column(9).footer()).html("--");
        $(api.column(10).footer()).html("--");
        $(api.column(11).footer()).html("--");
        $(api.column(12).footer()).html( parseFloat(api
            .column( 12 )
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 )).toFixed(2));

        $(api.column(13).footer()).html( parseFloat(api
            .column( 13 )
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 )).toFixed(2));

        $(api.column(14).footer()).html( parseFloat(api
            .column( 14 )
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 )).toFixed(2));
        $(api.column(15).footer()).html( parseFloat(api
            .column( 15 )
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 )).toFixed(2));
        $(api.column(16).footer()).html("--")


    },
    ajax: {
        url: base_url  + '/report/get-admin-sales-report',
        data: function (d) {
            d.report_month =  $('#report_month').val();
            d.purchase_type =  $('#purchase_type').val();
            d.order_type =  $('#user_type').val();
            d.user_id =  $('#wellness-advisor-list-main').val();
            d.order_id =  $('#order_id_filter').val();
            d.order_status =  $('#order_status').val();
        }
    },
    columns: [
        {data: 'DT_Row_Index', name: 'DT_Row_Index'},
        {data: 'date', name: 'date'},
        {data: 'billed_user_id', name: 'billed_user_id'},
        {data: 'billed_user_name', name: 'billed_user_name'},
        {data: 'billed_user_type', name: 'billed_user_type'},
        {data: 'purchase_type', name: 'purchase_type'},
        {data: 'parent_id', name: 'parent_id'},
        {data: 'parent_name', name: 'parent_name'},
        {data: 'purchased_user_id', name: 'purchased_user_id'},
        {data: 'purchased_user_name', name: 'purchased_user_name'},
        {data: 'order_id', name: 'order_id'},
        {data: 'product', name: 'product'},
        {data: 'price', name: 'price',render:((value,data,num)=>{
                return isNaN(parseFloat(value).toFixed(2))?0:parseFloat(value).toFixed(2);
})},

{data: 'tax_amount', name: 'tax_amount',render:((value,data,num)=>{
    return isNaN(parseFloat(value).toFixed(2))?0:parseFloat(value).toFixed(2);
})},

{data: 'mrp', name: 'mrp',render:((value,data,num)=>{
    return isNaN(parseFloat(value).toFixed(2))?0:parseFloat(value).toFixed(2);
})},

{data: 'pv', name: 'pv',render:((value,data,num)=>{
    return isNaN(parseFloat(value).toFixed(2))?0:parseFloat(value).toFixed(2);
})},
{data: 'status', name: 'status'}
]
});
$.fn.dataTableExt.sErrMode = 'throw';

$('#admin-sales-report-table').on( 'error.dt', function ( e, settings, techNote, message ) {
    // console.log( 'An error has been reported by DataTables: ', message );
    swal("Hey!", "Please Wait ! We are loading your datas!", "info")
        .then(function(willDelete){
            if (willDelete) {
                location.reload(true);
            }
        });
} ) ;
$('body').on('change', '#report_month', function (e) {
    table_sale_report.draw();
});
$('body').on('change', '#purchase_type', function (e) {
    table_sale_report.draw();
});
$('body').on('change', '#user_type', function (e) {
    if($("#user_type").val()) {
        $('#wellness-advisor-list-main')[0].selectize.enable();
        if($("#wellness-advisor-list-main").val()) {
            table_sale_report.draw();
        }
    }
});
$('body').on('change', '#wellness-advisor-list-main', function (e) {
    if($("#wellness-advisor-list-main").val()) {
        table_sale_report.draw();
    }
});
$('body').on('change', '#order_status', function (e) {
    // if($("#order_status").val()) {
        table_sale_report.draw();
    // }
});

$('body').on('click', '#order_id_btn', function (e) {

    if($("#order_id_filter").val()) {
        table_sale_report.draw();
    }
});
// $('.wellness-advisor-list').select2();
$(function () {
    var select = $('.wellness-advisor-list-main').selectize({
        valueField: 'id',
        labelField: 'text',
        load: function (query, callback) {
            $.ajax({
                url:base_url+'/billing/getselectedusers?q=' + encodeURIComponent(query),
                success: function (response) {
                    $('#wellness-advisor-list-main').selectize({
                        options : response
                    });
                    callback(response);
                }
            });
        }
    });
    // var selectize = $select[0].selectize;
    try {
        select[0].selectize.disable();

    }catch (e) {
        //
    }
});
$(function () {
    $('.rank-wellness-advisor-list-main').selectize({
        valueField: 'id',
        labelField: 'text',
        load: function (query, callback) {
            $.ajax({
                url:base_url+'/billing/getselectedusers?q=' + encodeURIComponent(query),
                success: function (response) {
                    $('#rank_wellness-advisor-list-main').selectize({
                        options : response
                    });
                    callback(response);
                }
            });
        }
    });


});
$(function () {
    $('.payout_wellness-advisor-list-main').selectize({
        valueField: 'id',
        labelField: 'text',
        load: function (query, callback) {
            $.ajax({
                url:base_url+'/billing/getselectedusers?q=' + encodeURIComponent(query),
                success: function (response) {
                    $('#rpayout_wellness-advisor-list-main').selectize({
                        options : response
                    });
                    callback(response);
                }
            });
        }
    });


});



$(function () {
    $('#wa-down-wellness-advisor-list-main').selectize({
        valueField: 'id',
        labelField: 'text',
        load: function (query, callback) {
            $.ajax({
                url:base_url+'/admin_wa/getselectedusers?q=' + encodeURIComponent(query),
                success: function (response) {
                    $('#rpayout_wellness-advisor-list-main').selectize({
                        options : response
                    });
                    callback(response);
                }
            });
        }
    });


});



var table_wa_downlink_report = $('#admin-wa-downlink-report-table').DataTable({
    dom: 'lBfrtip',
    buttons: [
        {extend:"copy",footer: true}, {extend:"csv",footer: true} , {extend:"excel",footer: true}, {extend:"pdf",footer: true,exportOptions: {
                columns: ':visible',
                stripHtml: true
            },
            pageSize: 'LETTER'  ,customize: function(doc, config) {
                var tableNode;
                for (i = 0; i < doc.content.length; ++i) {
                    if(doc.content[i].table !== undefined){
                        tableNode = doc.content[i];
                        break;
                    }
                }

                var rowIndex = 0;
                var tableColumnCount = tableNode.table.body[rowIndex].length;

                if(tableColumnCount > 5){
                    doc.pageOrientation = 'landscape';
                }
            }}, {extend:"print",footer: true, orientation: 'landscape'}
    ],
    "aaSorting": [],
    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
    'order': [[3, 'asc']],
    "fnDrawCallback": function (oSettings) {
        if (oSettings.bSorted || oSettings.bFiltered || oSettings._iDisplayLength) {
            j = 0;
            for (var i = oSettings._iDisplayStart; i < oSettings.aiDisplay.length + oSettings._iDisplayStart; i++) {
                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[j]].nTr).find('span').text(i + 1);
                j++;
            }
        }
    },
    "footerCallback": function(row, data, start, end, display) {
        var api = this.api(), data;

        // converting to interger to find total
        var intVal = function ( i ) {
            return typeof i === 'string' ?
                i.replace(/[\$,]/g, '')*1 :
                typeof i === 'number' ?
                    i : 0;
        };
        $( api.column( 0 ).footer() ).html('Total');
        $(api.column(1).footer()).html("--")
        $(api.column(2).footer()).html("--")
        $(api.column(3).footer()).html("--")
        $(api.column(4).footer()).html( parseFloat(api
            .column( 4 )
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 )).toFixed(2));
        $(api.column(5).footer()).html( parseFloat(api
            .column( 5 )
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 )).toFixed(2));
        $(api.column(6).footer()).html( parseFloat(api
            .column( 6)
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 )).toFixed(2));



    },
    serverSide: true,
    scrollX: true,
    oLanguage: {
        sProcessing: '<svg width="30px"  height="30px"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-rolling" style="background: transparent;"><circle cx="50" cy="50" fill="none" stroke="#0fb963" stroke-width="10" r="35" stroke-dasharray="164.93361431346415 56.97787143782138" transform="rotate(228 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform></circle></svg>'
    },
    processing: true,
    "pageLength": 10,
    "columnDefs": [{'orderable': false, 'targets': [0, 5, 5]}],
    mark: true,
    fixedColumns: {},
    ajax: {
        url: base_url + '/report/get-wa-downlink-report',
        data: function (d) {
            d.user_id =  $('#wa-down-wellness-advisor-list-main').val();
            d.report_month =  $('#wa_downlink_report_month').val();
        }
    },
    columns: [
        {data: 'DT_Row_Index', name: 'DT_Row_Index'},
        {data: 'month',name:'month'},
        {data: 'designation', name: 'designation'},
        {data: 'name', name: 'name'},
        {data: 'pv', name: 'pv'},
        // {data: 'pgv', name: 'pgv'},
        // {data: 'bv', name: 'bv'},
        {data: 'total_price', name: 'total_price'},
        {data: 'total_volume', name: 'total_volume'}
    ]
});

$('body').on('change', '#wa-down-wellness-advisor-list-main', function (e) {
    table_wa_downlink_report.draw();
});
$('body').on('change', '#wa_downlink_report_month', function (e) {
    table_wa_downlink_report.draw();
});



var table_process_tracker = $('#admin-process-tracker-report-table').DataTable({
    dom: 'lBfrtip',
    buttons: [
        'csv', 'excel','print'
    ],
    "aaSorting": [],
    'order': [[7, 'desc']],
    "fnDrawCallback": function (oSettings) {
        if (oSettings.bSorted || oSettings.bFiltered || oSettings._iDisplayLength) {
            j = 0;
            for (var i = oSettings._iDisplayStart; i < oSettings.aiDisplay.length + oSettings._iDisplayStart; i++) {
                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[j]].nTr).find('span').text(i + 1);
                j++;
            }
        }
    },
    serverSide: true,
    scrollX: true,
    oLanguage: {
        sProcessing: '<svg width="30px"  height="30px"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-rolling" style="background: transparent;"><circle cx="50" cy="50" fill="none" stroke="#0fb963" stroke-width="10" r="35" stroke-dasharray="164.93361431346415 56.97787143782138" transform="rotate(228 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform></circle></svg>'
    },
    processing: true,
    "pageLength": 10,
    "columnDefs": [{'orderable': false, 'targets': [0, 14, 14]}],
    mark: true,
    fixedColumns: {},
    ajax: {
        url: base_url  + '/report/get-admin-process-tracker-report',
        data: function (d) {
            d.report_month =  $('#tracker_report_month').val();
            d.purchase_type =  $('#tracker_purchase_type').val();
            d.order_type =  $('#tracker_user_type').val();
            d.user_id =  $('#tracker_wellness-advisor-list-main').val();
            d.order_id =  $('#tracker_order_id_filter').val();
        }
    },
    columns: [
        {data: 'DT_Row_Index', name: 'DT_Row_Index'},
        {data: 'name', name: 'name'},
        {data: 'test', name: 'test'},
        {data: 'sample_type', name: 'sample_type'},
        {data: 'payment_mode', name: 'payment_mode'},
        {data: 'amount', name: 'amount'},
        {data: 'bank_confirmation_number', name: 'bank_confirmation_number'},
        {data: 'order_id', name: 'order_id'},
        {data: 'sample_received_date', name: 'sample_received_date'},
        {data: 'sent_to_gt_date', name: 'sent_to_gt_date'},
        {data: 'rec_at_gt_date', name: 'rec_at_gt_date'},
        {data: 'report_given_date', name: 'report_given_date'},
        {data: 'counselling_scheduled_date', name: 'counselling_scheduled_date'},
        {data: 'counselling_done_date', name: 'counselling_done_date'},
        {data: 'remarks', name: 'remarks'},
    ]
});
$.fn.dataTableExt.sErrMode = 'throw';

$('#admin-process-tracker-report-table').on( 'error.dt', function ( e, settings, techNote, message ) {
    // console.log( 'An error has been reported by DataTables: ', message );
    swal("Hey!", "Please Wait ! We are loading your datas!", "info")
        .then(function(willDelete){
            if (willDelete) {
                location.reload(true);
            }
        });
} ) ;
$('body').on('change', '#tracker_report_month', function (e) {
    table_process_tracker.draw();
});
$('body').on('change', '#tracker_purchase_type', function (e) {
    table_process_tracker.draw();
});
$('body').on('change', '#tracker_wellness-advisor-list-main', function (e) {
    table_process_tracker.draw();
});
$('body').on('change', '#tracker_user_type', function (e) {
    if($("#tracker_user_type").val()) {
        $('#tracker_wellness-advisor-list-main')[0].selectize.enable();
        if($("#tracker_wellness-advisor-list-main").val()) {
            table_process_tracker.draw();
        }
    }
});

$('body').on('click', '#tracker_order_id_btn', function (e) {

    if($("#tracker_order_id_filter").val()) {
        table_process_tracker.draw();
    }
});

//Admin payout datatable


var table_process_payout = $('#admin-process-payout_main-report-table').DataTable({
    dom: 'lBfrtip',
    buttons: [
        {extend:"copy",footer: true}, {extend:"csv",footer: true} , {extend:"excel",footer: true}, {extend:"pdf",footer: true,exportOptions: {
                columns: ':visible',
                stripHtml: true
            },
            pageSize: 'LETTER'  ,customize: function(doc, config) {
                var tableNode;
                for (i = 0; i < doc.content.length; ++i) {
                    if(doc.content[i].table !== undefined){
                        tableNode = doc.content[i];
                        break;
                    }
                }

                var rowIndex = 0;
                var tableColumnCount = tableNode.table.body[rowIndex].length;

                if(tableColumnCount > 5){
                    doc.pageOrientation = 'landscape';
                }
            }}, {extend:"print",footer: true, orientation: 'landscape'}
    ],
    "aaSorting": [],
    'order_id': [[1, 'desc']],
    "fnDrawCallback": function (oSettings) {
        if (oSettings.bSorted || oSettings.bFiltered || oSettings._iDisplayLength) {
            j = 0;
            for (var i = oSettings._iDisplayStart; i < oSettings.aiDisplay.length + oSettings._iDisplayStart; i++) {
                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[j]].nTr).find('span').text(i + 1);
                j++;
            }
        }
    },
    serverSide: true,
    scrollX: true,
    oLanguage: {
        sProcessing: '<svg width="30px"  height="30px"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-rolling" style="background: transparent;"><circle cx="50" cy="50" fill="none" stroke="#0fb963" stroke-width="10" r="35" stroke-dasharray="164.93361431346415 56.97787143782138" transform="rotate(228 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform></circle></svg>'
    },
    processing: true,
    "pageLength": 10,
    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
    "columnDefs": [{'orderable': false, 'targets': [0, 16, 16]}],
    mark: true,
    fixedColumns: {},
    "footerCallback": function(row, data, start, end, display) {
        var api = this.api(), data;

        // converting to interger to find total
        var intVal = function ( i ) {
            return typeof i === 'string' ?
                i.replace(/[\$,]/g, '')*1 :
                typeof i === 'number' ?
                    i : 0;
        };

        $( api.column( 0 ).footer() ).html('Total');
        $(api.column(1).footer()).html("--")
        $(api.column(2).footer()).html("--")
        $(api.column(3).footer()).html( parseFloat(api
            .column( 3 )
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 )).toFixed(2));
        $(api.column(4).footer()).html( parseFloat(api
            .column( 4 )
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 )).toFixed(2));
        $(api.column(5).footer()).html( parseFloat(api
            .column( 5)
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 )).toFixed(2));
        $(api.column(6).footer()).html( parseFloat(api
            .column( 6)
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 )).toFixed(2));
        $(api.column(7).footer()).html( parseFloat(api
            .column( 7)
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 )).toFixed(2));
        $(api.column(8).footer()).html( parseFloat(api
            .column( 8)
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 )).toFixed(2));
        $(api.column(9).footer()).html(parseFloat( api
            .column( 9)
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 )).toFixed(2));
        $(api.column(10).footer()).html(parseFloat( api
            .column( 10)
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 )).toFixed(2));
        $(api.column(11).footer()).html(parseFloat( api
            .column( 11)
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 )).toFixed(2));
        $(api.column(12).footer()).html("--")
        // $(api.column(13).footer()).html("--")
        $(api.column(13).footer()).html("--")
        $(api.column(14).footer()).html("--")
        $(api.column(15).footer()).html("--")
        $(api.column(16).footer()).html("--")


    },
    ajax: {
        url: base_url  + '/report/get-admin-payout-report-dashboard',
        data: function (d) {
            d.report_month =  $('#payout_report_month').val();
            d.user_id =  $('#payout_wellness-advisor-list-main').val();
        }
    },
    columns: [
        {data: 'month', name: 'month'},
        {data: 'user_id', name: 'user_id'},
        {data: 'name', name: 'name'},
        {data: 'psi', name: 'psi',render:((value,data,num)=>{
                return parseFloat(value).toFixed(2);
})},
{data: 'fsi', name: 'fsi',render:((value,data,num)=>{
    return parseFloat(value).toFixed(2);
})},
{data: 'gsi', name: 'gsi',render:((value,data,num)=>{
    return parseFloat(value).toFixed(2);
})},
{data: 'ucr', name: 'ucr',render:((value,data,num)=>{
    return parseFloat(value).toFixed(2);
})},
{data: 'gcr', name: 'gcr',render:((value,data,num)=>{
    return parseFloat(value).toFixed(2);
})},
{data: 'WAmbR', name: 'WAmbR',render:((value,data,num)=>{
    return parseFloat(value).toFixed(2);
})},
{data: 'total', name: 'total',render:((value,data,num)=>{
    return parseFloat(value).toFixed(2);
})},
{data: 'tds', name: 'tds',render:((value,data,num)=>{
    return parseFloat(value).toFixed(2);
})},
{data: 'net_pay', name: 'net_pay',render:((value,data,num)=>{
    return parseFloat(value).toFixed(2);
})},
        {data: 'bank_name', name: 'bank_name'},
        {data: 'acc_no', name: 'acc_no'},
        {data: 'ifsc', name: 'ifsc'},
{data: 'utr_number', name: 'utr_number'},
// {data: 'payment_details', name: 'payment_details'},
{data: 'status', name: 'status'},
]
});
$.fn.dataTableExt.sErrMode = 'throw';

$('#admin-process-payout_main-report-table').on( 'error.dt', function ( e, settings, techNote, message ) {
    // console.log( 'An error has been reported by DataTables: ', message );
    swal("Hey!", "Please Wait ! We are loading your datas!", "info")
        .then(function(willDelete){
            if (willDelete) {
                location.reload(true);
            }
        });
} ) ;
$('body').on('change', '#payout_report_month', function (e) {
    table_process_payout.draw();
});

$('body').on('change', '#payout_wellness-advisor-list-main', function (e) {
    table_process_payout.draw();
});


//Wallness Advisor rank report

var table_rank_report = $('#admin-rank-report-table').DataTable({
    dom: 'lBfrtip',
    buttons: [
        'csv', 'excel','print'
    ],
    "aaSorting": [],
    "order": [[ 1, 'asc' ]],
    serverSide: true,
    scrollX: true,
    oLanguage: {
        sProcessing: '<svg width="30px"  height="30px"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-rolling" style="background: transparent;"><circle cx="50" cy="50" fill="none" stroke="#0fb963" stroke-width="10" r="35" stroke-dasharray="164.93361431346415 56.97787143782138" transform="rotate(228 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform></circle></svg>'
    },
    processing: true,
    "pageLength": 10,
    "columnDefs": [{'orderable': false, 'targets': [0, 5, 5]}],
    mark: true,
    fixedColumns: {},
    ajax: {
        url: base_url  + '/report/get-admin-rank-report',
        data: function (d) {
            d.report_month =  $('#rank_report_month').val();
            d.user_id =  $('#rank_wellness-advisor-list-main').val();
            d.rank_type =  $('#rank_type').val();
            d.position =  $('#position').val();
        }
    },
    columns: [
        { data: 'DT_Row_Index', name: 'DT_Row_Index' },
        {data: 'month', name: 'month'},
        {data: 'name', name: 'name'},
        {data: 'user_id', name: 'user_id'},
        {data: 'current', name: 'current'},
        {data: 'highest', name: 'highest'},

    ]
});
$.fn.dataTableExt.sErrMode = 'throw';

$('#admin-rank-report-table').on( 'error.dt', function ( e, settings, techNote, message ) {
    // console.log( 'An error has been reported by DataTables: ', message );
    swal("Hey!", "Please Wait ! We are loading your datas!", "info")
        .then(function(willDelete){
            if (willDelete) {
                location.reload(true);
            }
        });
} ) ;

$('body').on('change', '#rank_report_month', function (e) {
    table_rank_report.draw();
});
$('body').on('change', '#rank_wellness-advisor-list-main', function (e) {
    table_rank_report.draw();
});
$('body').on('change', '#rank_type', function (e) {
    if($("#position").val()) {
        table_rank_report.draw();
    }
});
$('body').on('change', '#position', function (e) {
    if($("#rank_type").val()) {
        if($("#position").val()) {
            table_rank_report.draw();
        }
    }
});


/*Payment details*/

$('body').on('click', '.payment-details', function (e) {
    var u_id = $(this).data("id");
    $.ajax({
        url: base_url+'/report/get-payment-details',
        type: 'POST',
        data: {
            'u_id': u_id
        },
        beforeSend: function() {
        },
        success: function(result) {
            $(".payment_detail_body").html(result);
        },
        complete:function () {

        },

        error:function f() {

        }

    });
});



