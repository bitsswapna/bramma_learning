
//index page
$(document).ready(function () {
    var table = $('#offer').DataTable({
        "aaSorting": [],
        'order': [[2, 'desc']],
        "fnDrawCallback": function (oSettings) {
            if (oSettings.bSorted || oSettings.bFiltered || oSettings._iDisplayLength) {
                j = 0;
                for (var i = oSettings._iDisplayStart; i < oSettings.aiDisplay.length + oSettings._iDisplayStart; i++) {
                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[j]].nTr).find('span').text(i + 1);
                    j++;
                }
            }
        },
        serverSide: true,
        scrollX: true,
        oLanguage: {
            sProcessing: '<svg width="30px"  height="30px"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-rolling" style="background: transparent;"><circle cx="50" cy="50" fill="none" stroke="#0fb963" stroke-width="10" r="35" stroke-dasharray="164.93361431346415 56.97787143782138" transform="rotate(228 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform></circle></svg>'
        },
        processing: true,
        "pageLength": 10,
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "columnDefs": [{ 'orderable': false, 'targets': [0, 9, 8] }],
        mark: true,
        fixedColumns: {},
        ajax: {
            url: base_url + '/get-offers',
        },
        columns: [
            { data: 'sl#', name: 'sl#' },
            { data: 'name', name: 'name' },
            { data: 'percentage', name: 'percentage' },
            { data: 'type', name: 'type' },
            { data: 'total_purchase', name: 'total_purchase' },
            { data: 'purchase', name: 'purchase' },
            { data: 'offer', name: 'offer' },
            { data: 'description', name: 'description' },
            { data: 'status', name: 'status' },
            { data: 'action', name: 'action' }
        ]
    });

});

//AJAX EXCEPTION ERROR
$(function () {
    $.fn.dataTableExt.sErrMode = 'throw';

    $('#offer').on('error.dt', function (e, settings, techNote, message) {
        console.log('An error has been reported by DataTables: ', message);
        swal("Hey!", "Please Wait ! We are loading your data!", "info")
            .then(function (willDelete) {
                if (willDelete) {
                    location.reload(true);
                }
            });
    });
});


$(document).on('click', '.change-status', function (e) {
    var token = $('meta[name="csrf-token"]').attr("content");
    var id = $(this).data("id");
    var status = $(this).data('status');

    var data = { id: id, status: status, _token: token };


    var url = base_url + '/offers/change_status';

    $.ajax({
        url: url, type: 'POST', data: data,
        beforeSend: function () {

        },
        success: function (data) {
            successMsg(data.msg);
        },
        error: function (data) {
            errorMsg(data.responseJSON.msg);
        },
        complete: function () {

        }
    });
});

$(document).on('click', '.delete-offers', function (e) {

    e.preventDefault();
    var token = $('meta[name="csrf-token"]').attr("content");
    var id = $(this).data("id");

    var data = { id: id, _token: token };
    var url = base_url + '/offers/delete';

    swal({
        title: "Are you sure you want to delete this offer?",

        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((willDelete) => {
            if (willDelete) {


                $.ajax({
                    url: url,
                    type: 'POST',
                    data: data,
                    beforeSend: function () {

                    },
                    success: function (data) {
                        location.reload();
                        table.draw();
                        successMsg(data.msg);
                    },
                    error: function (data) {
                        table.draw();
                        errorMsg(data.responseJSON.msg);
                    },
                    complete: function () {

                    }
                });

                swal("Poof! Offer is deleted successfully!", {
                    icon: "success",
                });
            } else {
                swal("Offer is not deleted!");
            }
        });


});







