$('.change-status').change(function(){
    var $this = this;
    var $status =  $(this).is(":checked");
    var $token = $('meta[name="csrf-token"]').attr("content");
    var $data ={ id:$(this).data('id'), _token:$token};
    var $url =  base_url + '/team/change_status';

    $.ajax({
        url: $url,
        type: 'POST',
        data: $data,
        beforeSend: function() {

        },
        success: function (data) {
            successMsg(data.msg);
        },
        error: function (data) {
            if($status){
                $($this).prop('checked', false);
            }else{
                $($this).prop('checked', true);
            }
            var data = $.parseJSON(data.responseText);
            errorMsg(data.msg);
        },
        complete: function() {

        }
    });
});


$(document).ready(function () {
    $('#external').change(function() {
        page_change();
    });
    page_change();

    function page_change() {
        if($('#external').is(':checked')){
            $('.not-external').hide(500);
            $('.is-external').show(500);
        }else{
            $('.not-external').show(500);
            $('.is-external').hide(500);
        }
    }

});