// var product_status = null;
// var table = $('#salesReport').DataTable({
//     responsive: false,
//     dom: 'lBfrtip',
//     buttons: [
//         'copy', 'csv', 'excel', 'pdf', 'print'
//     ],
//     // "columnDefs": [
//     //     { className: "text-right", "targets": [ 1 ] }
//     // ],
//
//     "aaSorting": [],
//     'order': [[1, 'desc']],
//     "fnDrawCallback": function (oSettings) {
//         if (oSettings.bSorted || oSettings.bFiltered || oSettings._iDisplayLength) {
//             j = 0;
//             for (var i = oSettings._iDisplayStart; i < oSettings.aiDisplay.length + oSettings._iDisplayStart; i++) {
//                 $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[j]].nTr).find('span').text(i + 1);
//                 j++;
//             }
//         }
//     },
//     serverSide: true,
//     scrollX: true,
//     oLanguage: {
//         sProcessing: '<svg width="30px"  height="30px"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-rolling" style="background: transparent;"><circle cx="50" cy="50" fill="none" stroke="#0fb963" stroke-width="10" r="35" stroke-dasharray="164.93361431346415 56.97787143782138" transform="rotate(228 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform></circle></svg>'
//     },
//     processing: true,
//     "pageLength": 10,
//     "columnDefs": [{'orderable': false, 'targets': [0, 4, 4]}
//
//     ],
//     mark: true,
//     fixedColumns: {},
//     ajax: {
//         url: base_url + '/sales/get-overall-sales-report',
//     },
//     columns: [
//         {data: 'DT_Row_Index', name: 'DT_Row_Index'},
//         {data: 'user_id', name: 'user_id'},
//         {data: 'name', name: 'name'},
//         {data: 'total_sales_volume', name: 'total_sales_volume'},
//         {data: 'total_personal_volume', name: 'total_personal_volume'},
//
//     ]
// });

var table = $('#salesReport').DataTable({
    "aaSorting": [],
    'order': [[1, 'desc']],
    "fnDrawCallback": function (oSettings) {
        if (oSettings.bSorted || oSettings.bFiltered || oSettings._iDisplayLength) {
            j = 0;
            for (var i = oSettings._iDisplayStart; i < oSettings.aiDisplay.length + oSettings._iDisplayStart; i++) {
                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[j]].nTr).find('span').text(i + 1);
                j++;
            }
        }
    },
    serverSide: true,
    scrollX: true,
    oLanguage: {
        sProcessing: '<svg width="30px"  height="30px"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-rolling" style="background: transparent;"><circle cx="50" cy="50" fill="none" stroke="#0fb963" stroke-width="10" r="35" stroke-dasharray="164.93361431346415 56.97787143782138" transform="rotate(228 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform></circle></svg>'
    },
    processing: true,
    "pageLength": 10,
    "columnDefs": [{'orderable': false, 'targets': [0, 5, 5]}],
    mark: true,
    fixedColumns: {},
    ajax: {
        url: base_url + '/sales/get-overall-sales-report',
    },
    columns: [
        {data: 'DT_Row_Index', name: 'DT_Row_Index'},
        {data: 'date', name: 'date'},
        {data: 'user_id', name: 'user_id'},
        {data: 'name', name: 'name'},
        {data: 'total_sales_volume', name: 'total_sales_volume'},
        {data: 'total_personal_volume', name: 'total_personal_volume'},
    ]
});

$.fn.dataTableExt.sErrMode = 'throw';

$('#user_customer').on( 'error.dt', function ( e, settings, techNote, message ) {
    // console.log( 'An error has been reported by DataTables: ', message );
    swal("Hey!", "Please Wait ! We are loading your datas!", "info")
        .then(function(willDelete){
            if (willDelete) {
                location.reload(true);
            }
        });
} ) ;


// $(document).ready(function () {
//     $.fn.dataTableExt.sErrMode = 'throw';
//
//     $('#salesReport').on( 'error.dt', function ( e, settings, techNote, message ) {
//        // console.log( 'An error has been reported by DataTables: ', message );
//         swal("Hey!", "Please Wait ! We are loading your datas!", "info")
//             .then((willDelete) => {
//                 if (willDelete) {
//                     location.reload(true);
//                 }
//             });
//     } ) ;
// });





