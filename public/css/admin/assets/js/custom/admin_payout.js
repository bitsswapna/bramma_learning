var table_sale_report = $('#admin-payout-dashbord-report').DataTable({
    "aaSorting": [],
    'order_id': [[1, 'desc']],
    "fnDrawCallback": function (oSettings) {
        if (oSettings.bSorted || oSettings.bFiltered || oSettings._iDisplayLength) {
            j = 0;
            for (var i = oSettings._iDisplayStart; i < oSettings.aiDisplay.length + oSettings._iDisplayStart; i++) {
                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[j]].nTr).find('span').text(i + 1);
                j++;
            }
        }
    },
    serverSide: true,
    scrollX: true,
    oLanguage: {
        sProcessing: '<svg width="30px"  height="30px"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-rolling" style="background: transparent;"><circle cx="50" cy="50" fill="none" stroke="#0fb963" stroke-width="10" r="35" stroke-dasharray="164.93361431346415 56.97787143782138" transform="rotate(228 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform></circle></svg>'
    },
    processing: true,
    "pageLength": 10,
    "columnDefs": [{'orderable': false, 'targets': [0, 12, 12]}],
    mark: true,
    fixedColumns: {},
    ajax: {
        url: base_url  + '/report/get-admin-payout-report-dashboard',
        data: function (d) {
            d.report_month =  $('#report_month').val();
            d.purchase_type =  $('#purchase_type').val();
            d.order_type =  $('#user_type').val();
            d.user_id =  $('#wellness-advisor-list-main').val();
        }
    },
    columns: [
        {data: 'month', name: 'month'},
        {data: 'user_id', name: 'user_id'},
        {data: 'name', name: 'name'},
        {data: 'psi', name: 'psi',render:((value,data,num)=>{
          return parseFloat(value).toFixed(2);
        })},
        {data: 'fsi', name: 'fsi',render:((value,data,num)=>{
            return parseFloat(value).toFixed(2);
          })},
        {data: 'gsi', name: 'gsi',render:((value,data,num)=>{
            return parseFloat(value).toFixed(2);
          })},
        {data: 'ucr', name: 'ucr',render:((value,data,num)=>{
            return parseFloat(value).toFixed(2);
          })},
        {data: 'gcr', name: 'gcr',render:((value,data,num)=>{
            return parseFloat(value).toFixed(2);
          })},
        {data: 'WAmbR', name: 'WAmbR',render:((value,data,num)=>{
            return parseFloat(value).toFixed(2);
          })},
        {data: 'total', name: 'total',render:((value,data,num)=>{
            return parseFloat(value).toFixed(2);
          })},
        {data: 'tds', name: 'tds',render:((value,data,num)=>{
            return parseFloat(value).toFixed(2);
          })},
        {data: 'net_pay', name: 'net_pay',render:((value,data,num)=>{
            return parseFloat(value).toFixed(2);
          })},

        {data: 'utr_number', name: 'utr_number'},
       
    ]
});
$.fn.dataTableExt.sErrMode = 'throw';

$('#admin-sales-report-table').on( 'error.dt', function ( e, settings, techNote, message ) {
    // console.log( 'An error has been reported by DataTables: ', message );
    swal("Hey!", "Please Wait ! We are loading your datas!", "info")
        .then(function(willDelete){
            if (willDelete) {
                location.reload(true);
            }
        });
} ) ;