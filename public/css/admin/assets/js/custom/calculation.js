//    bh values
$(document).on('click', '.edit_row', function (e) {
    var id = $(this).data("id");
    var generation = document.getElementById("generation" + id).innerHTML;
    var executive_count = document.getElementById("executive_count" + id).innerHTML;
    var percentage = document.getElementById("percentage" + id).innerHTML;
    var side_volume = document.getElementById("side_volume" + id).innerHTML;
    document.getElementById("generation" + id).innerHTML = "<input class='td-res form-control' tro type='text' id='generation_text" + id + "' value='"+generation+"'>";
    document.getElementById("executive_count" + id).innerHTML = "<input class='td-res form-control' type='text' id='executive_count_text" + id + "' value='"+executive_count+"'>";
    document.getElementById("percentage" + id).innerHTML = "<input class='td-res form-control' type='text' id='percentage_text" + id + "' value='"+percentage+"'>";
    document.getElementById("side_volume" + id).innerHTML = "<input class='td-res form-control' type='text' id='side_volume_text" + id + "' value='"+side_volume+"'>";

    document.getElementById("edit_button" + id).style.display = "none";
    document.getElementById("delete_button" + id).style.display = "none";
    document.getElementById("save_button" + id).style.display = "block";
});

$(document).on('click', '.save_row', function (e) {
    var id = $(this).data("id");
    var generation_text = document.getElementById("generation_text" + id).value;
    var executive_count_text = document.getElementById("executive_count_text" + id).value;
    var percentage_text = document.getElementById("percentage_text" + id).value;
    var side_volume_text = document.getElementById("side_volume_text" + id).value;

    swal({
        title: "Are you sure?",
        text: "The changes will affect to the entire Website",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })

        .then((willDelete) => {
            if (willDelete) {
                $.ajax
                ({
                    type: 'post',
                    url: base_url + '/calculation_setting/update_bh_calculation',
                    data: {
                        row_id: id,
                        generation_text: generation_text,
                        executive_count_text: executive_count_text,
                        percentage_text: percentage_text,
                        side_volume_text: side_volume_text
                    },
                    beforeSend: function () {
                    },
                    success: function (response) {
                        if (response == "success") {
                            document.getElementById("generation_text" + id).innerHTML = generation_text;
                            document.getElementById("executive_count_text" + id).innerHTML = executive_count_text;
                            document.getElementById("percentage_text" + id).innerHTML = percentage_text;
                            document.getElementById("side_volume_text" + id).innerHTML = side_volume_text;
                            document.getElementById("edit_button" + id).style.display = "block";
                            document.getElementById("save_button" + id).style.display = "none";
                            location.reload();
                        }
                    }
                });
            } else {
                swal("Not saved");
                location.reload();
            }
        });
});


$(document).on('click', '.delete_row', function (e) {
    var id = $(this).data("id");
    swal({
        title: "Are you sure ?",
        text: "The changes will affect to the entire Website",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })

        .then((willDelete) => {
            if (willDelete) {

                $.ajax
                ({
                    type: 'post',
                    url: base_url + '/calculation_setting/delete_bh_calculation',
                    data: {
                        row_id: id,
                    },
                    success: function (response) {
                        if (response == "success") {
                            var row = document.getElementById("row" + id);
                            row.parentNode.removeChild(row);
                        }
                    }
                });
            } else {
                swal("Not Deleted!");
                location.reload();
            }
        });
});


$(document).on('click', '.insert_row', function (e) {

    var new_generation = document.getElementById("new_generation").value;
    var new_executive_count = document.getElementById("new_executive_count").value;
    var new_percentage = document.getElementById("new_percentage").value;
    var new_side_volume = document.getElementById("new_side_volume").value;
    // if(generation_text == ''){
    //     //    //swal('Please Input Values');
    //     // }else {
    //     //
    //     // }

        swal({
            title: "Are you sure ?",
            text: "The changes will affect to the entire Website",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })

            .then((willDelete) => {
                if (willDelete) {


                    $.ajax
                    ({
                        type: 'post',
                        url: base_url + '/calculation_setting/add_bh_calculation',
                        data: {
                            new_generation: new_generation,
                            new_executive_count: new_executive_count,
                            new_percentage: new_percentage,
                            new_side_volume: new_side_volume
                        },
                        success: function (response) {
                            if (response != "") {
                                var id = response;
                                var table = document.getElementById("user_table");
                                var table_len = (table.rows.length) - 1;
                                var row = table.insertRow(table_len).outerHTML = "<tr id='row" + id + "'><td id='generation" + id + "'>" + new_generation + "</td><td id='executive_count" + id + "'>" + new_executive_count + "</td><td id='percentage" + id + "'>" + new_percentage + "</td><td id='side_volume" + id + "'>" + new_side_volume + "</td></tr>";

                                document.getElementById("new_generation").value = "";
                                document.getElementById("new_executive_count").value = "";
                                document.getElementById("new_percentage").value = "";
                                document.getElementById("new_side_volume").value = "";
                                location.reload(true);
                            }
                        }
                    });
                } else {
                    swal("Not saved");
                    location.reload();
                }
            });

});




/////////////bm value  ///////////

$(document).on('click', '.bm_edit_row', function (e) {
    var id = $(this).data("id");
    var bm_generation = document.getElementById("bm_generation" + id).innerHTML;
    var bm_bh_count = document.getElementById("bm_bh_count" + id).innerHTML;
    var bm_percentage = document.getElementById("bm_percentage" + id).innerHTML;
    var bm_side_volume = document.getElementById("bm_side_volume" + id).innerHTML;

    document.getElementById("bm_generation"+id).innerHTML="<input class='td-res form-control' tro type='text' id='bm_generation_text"+id+"' value='"+bm_generation+"'>";
    document.getElementById("bm_bh_count"+id).innerHTML="<input class='td-res form-control' type='text' id='bm_bh_count_text"+id+"' value='"+bm_bh_count+"'>";
    document.getElementById("bm_percentage"+id).innerHTML="<input class='td-res form-control' type='text' id='bm_percentage_text"+id+"' value='"+bm_percentage+"'>";
    document.getElementById("bm_side_volume"+id).innerHTML="<input class='td-res form-control' type='text' id='bm_side_volume_text"+id+"' value='"+bm_side_volume+"'>";


    document.getElementById("bm_edit_button" + id).style.display = "none";
    document.getElementById("bm_delete_button" + id).style.display = "none";
    document.getElementById("bm_save_button" + id).style.display = "block";
});

$(document).on('click', '.bm_save_row', function (e) {
    var id = $(this).data("id");

    var bm_generation_text = document.getElementById("bm_generation_text" + id).value;
    var bm_bh_count_text = document.getElementById("bm_bh_count_text" + id).value;
    var bm_percentage_text = document.getElementById("bm_percentage_text" + id).value;
    var bm_side_volume_text = document.getElementById("bm_side_volume_text" + id).value;
    swal({
        title: "Are you sure?",
        text: "The changes will affect to the entire Website",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })

        .then((willDelete) => {
            if (willDelete) {
                $.ajax
                ({
                    type: 'post',
                    url: base_url + '/calculation_setting/update_bm_calculation',
                    data: {
                        row_id: id,
                        bm_generation_text: bm_generation_text,
                        bm_bh_count_text: bm_bh_count_text,
                        bm_percentage_text: bm_percentage_text,
                        bm_side_volume_text: bm_side_volume_text
                    },
                    beforeSend: function () {
                    },
                    success: function (response) {
                        if (response == "success") {
                            document.getElementById("bm_generation_text" + id).innerHTML = bm_generation_text;
                            document.getElementById("bm_bh_count_text" + id).innerHTML = bm_bh_count_text;
                            document.getElementById("bm_percentage_text" + id).innerHTML = bm_percentage_text;
                            document.getElementById("bm_side_volume_text" + id).innerHTML = bm_side_volume_text;
                            document.getElementById("bm_edit_button" + id).style.display = "block";
                            document.getElementById("bm_save_button" + id).style.display = "none";
                            location.reload();
                        }
                    }
                });
            } else {
                swal("Not saved");
                location.reload();
            }
        });
});


$(document).on('click', '.bm_delete_row', function (e) {
    var id = $(this).data("id");
    swal({
        title: "Are you sure ?",
        text: "The changes will affect to the entire Website",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })

        .then((willDelete) => {
            if (willDelete) {

                $.ajax
                ({
                    type: 'post',
                    url: base_url + '/calculation_setting/delete_bm_calculation',
                    data: {
                        row_id: id,
                    },
                    success: function (response) {
                        if (response == "success") {
                            var row = document.getElementById("row" + id);
                            row.parentNode.removeChild(row);
                        }
                    }
                });
            } else {
                swal("Not Deleted!");
                location.reload();
            }
        });
});


$(document).on('click', '.bm_insert_row', function (e) {
    var new_bm_generation = document.getElementById("new_bm_generation").value;
    var new_bm_bh_count = document.getElementById("new_bm_bh_count").value;
    var new_bm_percentage = document.getElementById("new_bm_percentage").value;
    var new_bm_side_volume = document.getElementById("new_bm_side_volume").value;
    swal({
        title: "Are you sure ?",
        text: "The changes will affect to the entire Website",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })

        .then((willDelete) => {
            if (willDelete) {


                $.ajax
                ({
                    type: 'post',
                    url: base_url + '/calculation_setting/add_bm_calculation',
                    data: {
                        new_bm_generation: new_bm_generation,
                        new_bm_bh_count: new_bm_bh_count,
                        new_bm_percentage: new_bm_percentage,
                        new_bm_side_volume: new_bm_side_volume
                    },
                    success: function (response) {
                        if (response != "") {
                            var id = response;
                            var table = document.getElementById("user_bm_table");
                            var table_len = (table.rows.length) - 1;
                            var row = table.insertRow(table_len).outerHTML = "<tr id='row" + id + "'><td id='bm_generation" + id + "'>" + new_bm_generation + "</td><td id='bm_bh_count" + id + "'>" + new_bm_bh_count + "</td><td id='bm_percentage" + id + "'>" + new_bm_percentage + "</td><td id='bm_side_volume" + id + "'>" + new_bm_side_volume + "</td></tr>";

                            document.getElementById("new_bm_generation").value = "";
                            document.getElementById("new_bm_bh_count").value = "";
                            document.getElementById("new_bm_percentage").value = "";
                            document.getElementById("new_bm_side_volume").value = "";
                            location.reload(true);
                        }
                    }
                });
            } else {
                swal("Not saved");
                location.reload();
            }
        });
});



//////////////gm setting

$(document).on('click', '.gm_edit_row', function (e) {
    var id = $(this).data("id");
    var gm_generation = document.getElementById("gm_generation" + id).innerHTML;
    var gm_bh_count = document.getElementById("gm_bh_count" + id).innerHTML;
    var gm_percentage = document.getElementById("gm_percentage" + id).innerHTML;
    var gm_side_volume = document.getElementById("gm_side_volume" + id).innerHTML;

    document.getElementById("gm_generation"+id).innerHTML="<input class='td-res form-control' tro type='text' id='gm_generation_text"+id+"' value='"+gm_generation+"'>";
    document.getElementById("gm_bh_count"+id).innerHTML="<input class='td-res form-control' type='text' id='gm_bh_count_text"+id+"' value='"+gm_bh_count+"'>";
    document.getElementById("gm_percentage"+id).innerHTML="<input class='td-res form-control' type='text' id='gm_percentage_text"+id+"' value='"+gm_percentage+"'>";
    document.getElementById("gm_side_volume"+id).innerHTML="<input class='td-res form-control' type='text' id='gm_side_volume_text"+id+"' value='"+gm_side_volume+"'>";


    document.getElementById("gm_edit_button" + id).style.display = "none";
    document.getElementById("gm_delete_button" + id).style.display = "none";
    document.getElementById("gm_save_button" + id).style.display = "block";
});

$(document).on('click', '.gm_save_row', function (e) {
    var id = $(this).data("id");

    var gm_generation_text = document.getElementById("gm_generation_text" + id).value;
    var gm_bh_count_text = document.getElementById("gm_bh_count_text" + id).value;
    var gm_percentage_text = document.getElementById("gm_percentage_text" + id).value;
    var gm_side_volume_text = document.getElementById("gm_side_volume_text" + id).value;
    swal({
        title: "Are you sure?",
        text: "The changes will affect to the entire Website",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })

        .then((willDelete) => {
            if (willDelete) {
                $.ajax
                ({
                    type: 'post',
                    url: base_url + '/calculation_setting/update_gm_calculation',
                    data: {
                        row_id: id,
                        gm_generation_text: gm_generation_text,
                        gm_bh_count_text: gm_bh_count_text,
                        gm_percentage_text: gm_percentage_text,
                        gm_side_volume_text: gm_side_volume_text
                    },
                    beforeSend: function () {
                    },
                    success: function (response) {
                        if (response == "success") {
                            document.getElementById("gm_generation_text" + id).innerHTML = gm_generation_text;
                            document.getElementById("gm_bh_count_text" + id).innerHTML = gm_bh_count_text;
                            document.getElementById("gm_percentage_text" + id).innerHTML = gm_percentage_text;
                            document.getElementById("gm_side_volume_text" + id).innerHTML = gm_side_volume_text;
                            document.getElementById("gm_edit_button" + id).style.display = "block";
                            document.getElementById("gm_save_button" + id).style.display = "none";
                            location.reload();
                        }
                    }
                });
            } else {
                swal("Not saved");
                location.reload();
            }
        });
});


$(document).on('click', '.gm_delete_row', function (e) {
    var id = $(this).data("id");
    swal({
        title: "Are you sure ?",
        text: "The changes will affect to the entire Website",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })

        .then((willDelete) => {
            if (willDelete) {

                $.ajax
                ({
                    type: 'post',
                    url: base_url + '/calculation_setting/delete_gm_calculation',
                    data: {
                        row_id: id,
                    },
                    success: function (response) {
                        if (response == "success") {
                            var row = document.getElementById("row" + id);
                            row.parentNode.removeChild(row);
                        }
                    }
                });
            } else {
                swal("Not Deleted!");
                location.reload();
            }
        });
});


$(document).on('click', '.gm_insert_row', function (e) {
    var new_gm_generation = document.getElementById("new_gm_generation").value;
    var new_gm_bh_count = document.getElementById("new_gm_bh_count").value;
    var new_gm_percentage = document.getElementById("new_gm_percentage").value;
    var new_gm_side_volume = document.getElementById("new_gm_side_volume").value;
    swal({
        title: "Are you sure ?",
        text: "The changes will affect to the entire Website",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })

        .then((willDelete) => {
            if (willDelete) {


                $.ajax
                ({
                    type: 'post',
                    url: base_url + '/calculation_setting/add_gm_calculation',
                    data: {
                        new_gm_generation: new_gm_generation,
                        new_gm_bh_count: new_gm_bh_count,
                        new_gm_percentage: new_gm_percentage,
                        new_gm_side_volume: new_gm_side_volume
                    },
                    success: function (response) {
                        if (response != "") {
                            var id = response;
                            var table = document.getElementById("user_gm_table");
                            var table_len = (table.rows.length) - 1;
                            var row = table.insertRow(table_len).outerHTML = "<tr id='row" + id + "'><td id='gm_generation" + id + "'>" + new_gm_generation + "</td><td id='gm_bh_count" + id + "'>" + new_gm_bh_count + "</td><td id='gm_percentage" + id + "'>" + new_gm_percentage + "</td><td id='gm_side_volume" + id + "'>" + new_gm_side_volume + "</td></tr>";

                            document.getElementById("new_gm_generation").value = "";
                            document.getElementById("new_gm_bh_count").value = "";
                            document.getElementById("new_gm_percentage").value = "";
                            document.getElementById("new_gm_side_volume").value = "";
                            location.reload(true);
                        }
                    }
                });
            } else {
                swal("Not saved");
                location.reload();
            }
        });
});



//////////////Slab setting

$(document).on('click', '.slab_edit_row', function (e) {
    var id = $(this).data("id");
    var range_to = document.getElementById("range_to" + id).innerHTML;
    var range_from = document.getElementById("range_from" + id).innerHTML;
    var slab_percentage = document.getElementById("slab_percentage" + id).innerHTML;

    document.getElementById("range_to"+id).innerHTML="<input class='td-res form-control' tro type='text' id='range_to_text"+id+"' value='"+range_to+"'>";
    document.getElementById("range_from"+id).innerHTML="<input class='td-res form-control' type='text' id='range_from_text"+id+"' value='"+range_from+"'>";
    document.getElementById("slab_percentage"+id).innerHTML="<input class='td-res form-control' type='text' id='slab_percentage_text"+id+"' value='"+slab_percentage+"'>";


    document.getElementById("slab_edit_button" + id).style.display = "none";
    document.getElementById("slab_delete_button" + id).style.display = "none";
    document.getElementById("slab_save_button" + id).style.display = "block";
});

$(document).on('click', '.slab_save_row', function (e) {
    var id = $(this).data("id");

    var range_to_text = document.getElementById("range_to_text" + id).value;
    var range_from_text = document.getElementById("range_from_text" + id).value;
    var slab_percentage_text = document.getElementById("slab_percentage_text" + id).value;
    swal({
        title: "Are you sure?",
        text: "The changes will affect to the entire Website",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })

        .then((willDelete) => {
            if (willDelete) {
                $.ajax
                ({
                    type: 'post',
                    url: base_url + '/calculation_setting/update_slab_calculation',
                    data: {
                        row_id: id,
                        range_to_text: range_to_text,
                        range_from_text: range_from_text,
                        slab_percentage_text: slab_percentage_text,
                    },
                    beforeSend: function () {
                    },
                    success: function (response) {
                        if (response == "success") {
                            document.getElementById("range_to_text" + id).innerHTML = range_to_text;
                            document.getElementById("range_from_text" + id).innerHTML = range_from_text;
                            document.getElementById("slab_percentage_text" + id).innerHTML = slab_percentage_text;
                            document.getElementById("slab_edit_button" + id).style.display = "block";
                            document.getElementById("slab_save_button" + id).style.display = "none";
                            location.reload();
                        }
                    }
                });
            } else {
                swal("Not saved");
                location.reload();
            }
        });
});


$(document).on('click', '.slab_delete_row', function (e) {
    var id = $(this).data("id");
    //alert(id);
    swal({
        title: "Are you sure ?",
        text: "The changes will affect to the entire Website",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })

        .then((willDelete) => {
            if (willDelete) {

                $.ajax
                ({
                    type: 'post',
                    url: base_url + '/calculation_setting/delete_slab_calculation',
                    data: {
                        row_id: id,
                    },
                    success: function (response) {
                        if (response == "success") {
                            var row = document.getElementById("row" + id);
                            row.parentNode.removeChild(row);
                        }
                    }
                });
            } else {
                swal("Not Deleted!");
                location.reload(true);
            }
        });
});


$(document).on('click', '.slab_insert_row', function (e) {
    var new_range_to = document.getElementById("new_range_to").value;
    var new_range_from = document.getElementById("new_range_from").value;
    var new_slab_percentage = document.getElementById("new_slab_percentage").value;
    swal({
        title: "Are you sure ?",
        text: "The changes will affect to the entire Website",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })

        .then((willDelete) => {
            if (willDelete) {


                $.ajax
                ({
                    type: 'post',
                    url: base_url + '/calculation_setting/add_slab_calculation',
                    data: {
                        new_range_to: new_range_to,
                        new_range_from: new_range_from,
                        new_slab_percentage: new_slab_percentage,
                    },
                    success: function (response) {
                        if (response != "") {
                            var id = response;
                            var table = document.getElementById("user_slab_table");
                            var table_len = (table.rows.length) - 1;
                            var row = table.insertRow(table_len).outerHTML = "<tr id='row" + id + "'><td id='range_to" + id + "'>" + new_range_to + "</td><td id='range_from " + id + "'>" + new_range_from + "</td><td id='slab_percentage" + id + "'>" + new_slab_percentage + "</td></tr>";

                            document.getElementById("new_range_to").value = "";
                            document.getElementById("new_range_from").value = "";
                            document.getElementById("new_slab_percentage").value = "";
                            location.reload(true);
                        }
                    }
                });
            } else {
                swal("Not saved");
                location.reload();
            }
        });
});


//////////////Retail  setting

$(document).on('click', '.rt_edit_row', function (e) {
    var id = $(this).data("id");
    var rt_range_to = document.getElementById("rt_range_to" + id).innerHTML;
    var rt_range_from = document.getElementById("rt_range_from" + id).innerHTML;
    var rt_percentage = document.getElementById("rt_percentage" + id).innerHTML;

    document.getElementById("rt_range_to"+id).innerHTML="<input class='td-res form-control' tro type='text' id='rt_range_to_text"+id+"' value='"+rt_range_to+"'>";
    document.getElementById("rt_range_from"+id).innerHTML="<input class='td-res form-control' type='text' id='rt_range_from_text"+id+"' value='"+rt_range_from+"'>";
    document.getElementById("rt_percentage"+id).innerHTML="<input class='td-res form-control' type='text' id='rt_percentage_text"+id+"' value='"+rt_percentage+"'>";


    document.getElementById("rt_edit_button" + id).style.display = "none";
    document.getElementById("rt_delete_button" + id).style.display = "none";
    document.getElementById("rt_save_button" + id).style.display = "block";
});

$(document).on('click', '.rt_save_row', function (e) {
    var id = $(this).data("id");

    var rt_range_to_text = document.getElementById("rt_range_to_text" + id).value;
    var rt_range_from_text = document.getElementById("rt_range_from_text" + id).value;
    var rt_percentage_text = document.getElementById("rt_percentage_text" + id).value;
    swal({
        title: "Are you sure?",
        text: "The changes will affect to the entire Website",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })

        .then((willDelete) => {
            if (willDelete) {
                $.ajax
                ({
                    type: 'post',
                    url: base_url + '/calculation_setting/update_rt_calculation',
                    data: {
                        row_id: id,
                        rt_range_to_text: rt_range_to_text,
                        rt_range_from_text: rt_range_from_text,
                        rt_percentage_text: rt_percentage_text,
                    },
                    beforeSend: function () {
                    },
                    success: function (response) {
                        if (response == "success") {
                            document.getElementById("rt_range_to_text" + id).innerHTML = rt_range_to_text;
                            document.getElementById("rt_range_from_text" + id).innerHTML = rt_range_from_text;
                            document.getElementById("rt_percentage_text" + id).innerHTML = rt_percentage_text;
                            document.getElementById("rt_edit_button" + id).style.display = "block";
                            document.getElementById("rt_save_button" + id).style.display = "none";
                            location.reload();
                        }
                    }
                });
            } else {
                swal("Not saved");
                location.reload();
            }
        });
});


$(document).on('click', '.rt_delete_row', function (e) {
    var id = $(this).data("id");
    //alert(id);
    swal({
        title: "Are you sure ?",
        text: "The changes will affect to the entire Website",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })

        .then((willDelete) => {
            if (willDelete) {

                $.ajax
                ({
                    type: 'post',
                    url: base_url + '/calculation_setting/delete_rt_calculation',
                    data: {
                        row_id: id,
                    },
                    success: function (response) {
                        if (response == "success") {
                            var row = document.getElementById("row" + id);
                            row.parentNode.removeChild(row);
                        }
                    }
                });
            } else {
                swal("Not Deleted!");
                location.reload(true);
            }
        });
});


$(document).on('click', '.rt_insert_row', function (e) {
    var new_rt_range_to = document.getElementById("new_rt_range_to").value;
    var new_rt_range_from = document.getElementById("new_rt_range_from").value;
    var new_rt_percentage = document.getElementById("new_rt_percentage").value;
    swal({
        title: "Are you sure ?",
        text: "The changes will affect to the entire Website",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })

        .then((willDelete) => {
            if (willDelete) {


                $.ajax
                ({
                    type: 'post',
                    url: base_url + '/calculation_setting/add_rt_calculation',
                    data: {
                        new_rt_range_to: new_rt_range_to,
                        new_rt_range_from: new_rt_range_from,
                        new_rt_percentage: new_rt_percentage,
                    },
                    success: function (response) {
                        if (response != "") {
                            var id = response;
                            var table = document.getElementById("user_rt_table");
                            var table_len = (table.rows.length) - 1;
                            var row = table.insertRow(table_len).outerHTML = "<tr id='row" + id + "'><td id='rt_range_to" + id + "'>" + new_rt_range_to + "</td><td id='rt_range_from" + id + "'>" + new_rt_range_from + "</td><td id='rt_percentage" + id + "'>" + new_rt_percentage + "</td></tr>";

                            document.getElementById("new_rt_range_to").value = "";
                            document.getElementById("new_rt_range_from").value = "";
                            document.getElementById("new_rt_percentage").value = "";
                            location.reload(true);
                        }
                    }
                });
            } else {
                swal("Not saved");
                location.reload();
            }
        });
});


