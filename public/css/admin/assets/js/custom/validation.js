function checkSampleValidation(formfield, form_value,field) {
    var result = "";
    switch (formfield) {
        case 'email':
            if (form_value == "") {
                result = "Please enter an email";
            } else if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(form_value)) {
                result = "Please enter a valid email";
            }
            break;
        case 'mobile':
            form_value = form_value.replace(/[\s\n\t]/igm, "")
            if (form_value == "") {
                result = "Please enter a mobile number";
            }
            else if (!(/^[0-9]+$/.test(form_value))) {
                result = " Please enter a valid mobile number";
            }

            break;
        case 'first_name':
            if (form_value.length < 3 || form_value.length > 20) {
                result = "Please enter valid first name";
            }
            break;
        // case 'last_name':
        //     if (form_value.length < 1 || form_value.length > 20) {
        //         result = "Please enter valid last name";
        //     }
        //     break;
        case 'location':
            if (form_value.length < 3 || form_value.length > 50) {
                result = "Please enter valid location";
            }
            break;
        case 'aadhar_card_no':
            if (!form_value) {
                result = "Please enter valid aadhar card number";
            } else if (form_value.length == 12) {
                result = "";
            } else {
                result = "Please enter valid aadhar card number";
            }
            break;
        case 'pan_card_no':
            var regpan = /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/;
            if (!form_value) {
                result = "Please enter valid pan card number";
            } else if (regpan.test(form_value)) {
                result = "";
            } else {
                result = "Please enter valid pan card number";
            }
            break;
        case 'password':
            if (!form_value || form_value.length > 20) {
                result = "Please enter valid password";
            }else if( form_value.length < 6) {
                result = "Enter atleast 6 characters";
            }
            break;
        case 'password_confirmation':
            if (!form_value || form_value.length > 20 || $("#password_confirmation").val()!=$("#password").val()) {
                result = "Password doesn't match";
            }
            break;
        case 'postcode':
            if (form_value == "") {
                result = "Enter postcode number";
            }
            else if (!/^[1-9][0-9]{5}$/.test(form_value)) {
                result = "Please enter a valid postcode number";
            }

            break;
        case 'pincode':
            if (form_value == "") {
                result = "Enter pincode number";
            }
            else if (!/^[1-9][0-9]{5}$/.test(form_value)) {
                result = "Please enter a valid pincode number";
            }

            break;
        case 'first_name':
            if (form_value == "") {
                result = "Please enter a first name";
            }
            break;
        case 'address1':
            if (form_value == "") {
                result = "Please enter address Line 1";
            }
            break;
        case 'address2':
            if (form_value == "") {
                result = "Please enter address line 2";
            }
            break;
        case 'date_of_birth':
            if(!form_value)
            {
                result="Please enter a date of birth"
            }
            else if((new Date(form_value)).getTime()>(new Date).getTime())
            {
                result="Please enter a valid date of birth";
            }
            break;
        case "set_damin":{
            if((!$("#vi-admin-as-parent").prop("checked"))&&($("#wellness-advisor-list").val().trim()==""))
            {
                result="Please choose the parent";
            }
            
            break;
        }
        case "empty":{
            if(form_value=="")
            {
                result=field;
            }
            break;
        }
        // case 'branch_code':
        //     if (form_value == "") {
        //         result = "Please enter branch code";
        //     }
        //     break;
        case 'name_of_banker':
            if (form_value == "") {
                result = "Please enter name of banker";
            }
            break;
        case 'branch':
            if (form_value == "") {
                result = "Please enter branch";
            }
            break;
        case 'ac_number':
            if (form_value == "") {
                result = "Please enter A/C number";
            }
            break;
        case 'ifsc':
            if (form_value == "") {
                result = "Please enter IFSC";
            }
            break;

        case 'utr_number':
            if (form_value == "") {
                result = "Please enter UTR number";
            }
            break;

        case 'payment_date':
            if (form_value == "") {
                result = "Please enter payment date";
            }
            break;

        case 'pay_status':
            if (form_value == "") {
                result = "Please enter pay status";
            }
            break;

        default:
            result ="";
            break;
    }
    return result;
}
var validation=function()
{
    var url = base_url + '/get-address-form-validation';
    var id = $(this).data("id");
    var formfield = $(this).data("formfield");
    var field=$(this).attr("message")?$(this).attr("message"):"Please enter the value";
    var formfield_val = checkSampleValidation(formfield, $(this).val(),field);



    // var error_inst = ".reg-form-" + formfield;
    // $(error_inst).html('');
    // $('.error').html('');
    if ((formfield_val == "" &&$(id).attr("name")=="mobile") || (formfield_val == "" && $(id).attr("name")=="email") ) {

        // $(error_inst).html('<i class="fa fa-spinner fa-spin "></i>\n' +
        //     '                        <span class="sr-only">Loading...</span>')
        $.ajax({
            url: url,
            type: 'post',
            data: {
                form_field: $(id).attr("name"),
                form_value: $(this).val(),
            },
            beforeSend: function () {
            },
            success: function (result) {

             //   $(error_inst).remove();

                if (result.status == false) {
                    if (result.errors) {
                        $.each($.parseJSON(result.errors), function (i, item) {
                            if (item) {
                                $(id).parent("div").children("span.error").html( item[0])
                                //$(id).after("<p class='text-danger small reg-form-" + formfield + "'>" + item[0] + "</p>");
                            }
                        });
                    }

                }
            },
            complete: function () {
                //
            }
        });
    } else {
        if((id=="#wellness-advisor-list" || id=="#vi-admin-as-parent" ) &&formfield!="empty")
        {
            $("#wellness-advisor-list").parent("div").parent("div").children("span.error").html(formfield_val)
         
        }
        else
        $(id).parent("div").children("span.error").html(formfield_val)
        // $(id).after("<p class='text-danger small reg-form-" + formfield + "'>" +  + "</p>");
    }
}
var removeError=function(){
    var id = $(this).data("id");
    if(id=="#wellness-advisor-list" || id=="#vi-admin-as-parent" )
        {
            $("#wellness-advisor-list").parent("div").parent("div").children("span.error").html("")
         
        }
    $(this).parent("div").children("span.error").html("")
}
/*Registration form validation*/
$('body').on('blur change', '.validate-field ', validation);
$('body').on('blur change', '.wellness-advisor-list ', validation);
$('body').on('blur change', '.wellness-advisor-list-offer ', validation);
// $("#wellness-advisor-list").serialize({onBlur:validation,onChange:validation,onFocus:removeError})
$("body").on("focus",".validate-field",removeError)
$("body").on("focus",".wellness-advisor-list",removeError)
$("body").on("focus",".wellness-advisor-list-offer",removeError)
