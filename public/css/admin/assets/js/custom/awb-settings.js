$("#awd-form").on("submit",function (e) {
    e.preventDefault();

    $.ajax({
        type:"POST",
        url:window.location.href,
        data:$(this).serialize(),
        success:function (data) {
            if(data&&data.status==false&&data.error)
            {

                    var error =data.error;
                    Object.keys(error).forEach(e=>{
                        $("#"+e).parent().find(".error").remove();
                        $("#"+e).addClass("error-field");

                        $("#"+e).parent("div").append(" <span class=\"  error\" style='color: red'>"+error[e][0]+"</span>");
                    })

            }
            else if(data && data.status)
            {
                $.notify({message:data.message,type:"success",z_index:5000});
            }

        },
        error:function (err) {
            console.log(err);
        }
    })
})
$("input.validate").on("focus",function (e) {

    $(this).removeClass(".error-field");
    $(this).parent().find(".error").remove();
})
$("input.validate").on("blur",function (e) {
    if($(this).val()=="")
    {
        $(this).addClass(".error-field");
        $(this).parent().append(" <span class=\"  error\" style='color: red'>The  field is required.</span>")
    }

})