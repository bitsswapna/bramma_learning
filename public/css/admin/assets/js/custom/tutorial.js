$(document).ready(function(){

    $(document).on('click', '.delete-tutorial', function (e) {

        e.preventDefault();
        var id = $(this).data("id") ;
        var url = base_url+'/tutorial/delete';


        swal({
            title: "Are you sure ?",
            text: "Once deleted, it will be removed from tutorial!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: url,
                        type: 'POST',  // user.destroy
                        data:{
                            'id':id,
                        },
                        beforeSend: function(){
                            $("#if_loading").html("loading");
                        },
                        success: function(result) {
                            var divId = '#tr'+ id;
                            $(divId).remove();

                            // Do something with the result
                        },
                        complete: function(){
                            $('#if_loading').html("");
                        }
                    });

                } else {
                    //
                }
            });

    });

    /*Create slug for tutorial*/
    var createSlug = function(str) {
        str = str.replace(/^\s+|\s+$/g, ''); // trim
        str = str.toLowerCase();

        // remove accents, swap ñ for n, etc
        var from = "ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";
        var to = "aaaaaeeeeeiiiiooooouuuunc------";
        for (var i = 0, l = from.length; i < l; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
        }

        str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '-') // collapse whitespace and replace by -
            .replace(/-+/g, '-'); // collapse dashes

        return str;
    };

    /*Create slug on change in title*/
    $(document).on('change', '#title', function() {
        $slug = createSlug($(this).val());
        $('#slug').val($slug);
    });

    /*Append file acceptance on type change*/
    // $('#vi-media-type').on('change', function() {
    //     if (this.value == 'image'){
    //         $("#vi-media").attr('accept', 'image/*');;
    //
    //     } else if(this.value == 'document'){
    //         $("#vi-media").attr('accept', 'application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint,text/plain, application/pdf');;
    //
    //     }
    // });
    // accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint,text/plain, application/pdf, image/*"

});

$(document).ready(function(){
    var table = $('#vi-tutorial-table').DataTable({
        "aaSorting": [],
        'order': [[1, 'desc']],
        "fnDrawCallback": function (oSettings) {
            if (oSettings.bSorted || oSettings.bFiltered || oSettings._iDisplayLength) {
                j = 0;
                for (var i = oSettings._iDisplayStart; i < oSettings.aiDisplay.length + oSettings._iDisplayStart; i++) {
                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[j]].nTr).find('span').text(i + 1);
                    j++;
                }
            }
        },
        serverSide: true,
        scrollX: true,
        oLanguage: {
            sProcessing: '<svg width="30px"  height="30px"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-rolling" style="background: transparent;"><circle cx="50" cy="50" fill="none" stroke="#0fb963" stroke-width="10" r="35" stroke-dasharray="164.93361431346415 56.97787143782138" transform="rotate(228 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform></circle></svg>'
        },
        processing: true,
        "pageLength": 10,
        "columnDefs": [{'orderable': false, 'targets': [0, 5, 5]}],
        mark: true,
        fixedColumns: {},
        ajax: {
            url: base_url + '/get-tutorial',
        },
        columns: [
            {data: 'title', name: 'title'},
            {data: 'max_score', name: 'max_score'},
            {data: 'min_score', name: 'min_score'},
            {data: 'description', name: 'description'},
            {data: 'type', name: 'type'},
            {data: 'path', name: 'path'},
            {data: 'action', name: 'action'}
        ]
    });

})
