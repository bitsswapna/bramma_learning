/**
    ============================================================================
    Number Counter
    ============================================================================
**/

$('.counter').each(function() {
    var $this = $(this),
        countTo = $this.attr('data-count');

    $({
        countNum: $this.text()
    }).animate({
            countNum: countTo
        },

        {

            duration: 3000,
            easing: 'linear',
            step: function() {
                $this.text(Math.floor(this.countNum));
            },
            complete: function() {
                $this.text(this.countNum);
            }

        });



});


/**
    ============================================================================
    Animated PIE Charts
    ============================================================================
**/

$(function() {
    $('.chart').easyPieChart({
        size: 150,
        barColor: "#28D094",
        scaleLength: 0,
        lineWidth: 5,
        trackColor: "#eee",
        lineCap: "circle",
        animate: 2000,
    });
});

/**
    ============================================================================
    Slick Slider -Order Details
    ============================================================================
**/

$('.order-info-slider').slick({
    dots: false,
    speed: 1000,
    autoplay: true,
    slidesToShow: 2,
    slidesToScroll: 1,
    infinite: true,
    arrows: true,
    vertical: true,
    responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
});

/**
    ============================================================================
    Slick Slider -Expiry Details
    ============================================================================
**/

$('.expiring-slider').slick({
    dots: false,
    infinite: true,
    speed: 3000,
    autoplay: true,
    slidesToShow: 4,
    slidesToScroll: 4,
    responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: true
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
});



/**
    ============================================================================
    Pop Over
    ============================================================================
**/


$(document).ready(function() {
    $('[data-toggle="popover"]').popover({
        placement: 'top',
        trigger: 'hover'
    });
});


/**
    ============================================================================
    CHART Pages
    ============================================================================
**/

//creates Donut Chart
! function($) {
    "use strict";

    var FlotChart = function() {
        this.$body = $("body")
        this.$realData = []
    };
    FlotChart.prototype.createDonutGraph = function(selector, labels, datas, colors) {
            var data = [{
                label: labels[0],
                data: datas[0]
            }, {
                label: labels[1],
                data: datas[1]
            }, {
                label: labels[2],
                data: datas[2]
            }, {
                label: labels[3],
                data: datas[3]
            }];
            var options = {
                series: {
                    pie: {
                        show: true,
                        innerRadius: 0.7
                    }
                },
                legend: {
                    show: true,
                    labelFormatter: function(label, series) {
                        return '<div style="font-size:14px;">&nbsp;' + label + '</div>'
                    },
                    labelBoxBorderColor: null,
                    margin: 50,
                    width: 20,
                    padding: 1
                },
                grid: {
                    hoverable: true,
                    clickable: true
                },
                colors: colors,
                tooltip: true,
                tooltipOpts: {
                    content: "%s, %p.0%"
                }
            };

            // $.plot($(selector), data, options);
        },

        //initializing various charts and components
        FlotChart.prototype.init = function() {
            //Donut pie graph data
            var donutlabels = ["Series 1", "Series 2", "Series 3", "Series 4"];
            var donutdatas = [35, 20, 10, 20];
            var donutcolors = ['#ff9800', '#8d6e63', "#26a69a", "#7fc1fc"];
            this.createDonutGraph("#donut-chart #donut-chart-container", donutlabels, donutdatas, donutcolors);
        },

        //init flotchart
        $.FlotChart = new FlotChart, $.FlotChart.Constructor =
        FlotChart

}
(window.jQuery),

//initializing flotchart
function($) {
    "use strict";
    $.FlotChart.init()
}(window.jQuery);



! function($) {
    "use strict";

    var Dashboard1 = function() {
        this.$realData = []
    };

    //creates Bar chart
    Dashboard1.prototype.createBarChart = function(element, data, xkey, ykeys, labels, lineColors) {
            // Morris.Bar({
            //     element: element,
            //     data: data,
            //     xkey: xkey,
            //     ykeys: ykeys,
            //     labels: labels,
            //     hideHover: 'auto',
            //     resize: true, //defaulted to true
            //     gridLineColor: '#eeeeee',
            //     barSizeRatio: 0.2,
            //     barColors: lineColors,
            //     postUnits: 'k'
            // });
        },

        //creates line chart
        Dashboard1.prototype.createLineChart = function(element, data, xkey, ykeys, labels, opacity, Pfillcolor, Pstockcolor, lineColors) {
            // Morris.Line({
            //   element: element,
            //   data: data,
            //   xkey: xkey,
            //   ykeys: ykeys,
            //   labels: labels,
            //   fillOpacity: opacity,
            //   pointFillColors: Pfillcolor,
            //   pointStrokeColors: Pstockcolor,
            //   behaveLikeLine: true,
            //   gridLineColor: '#eef0f2',
            //   hideHover: 'auto',
            //   resize: true, //defaulted to true
            //   pointSize: 0,
            //   lineColors: lineColors,
            //     postUnits: 'k'
            // });
        },

        //creates Donut chart
        Dashboard1.prototype.createDonutChart = function(element, data, colors) {
            // Morris.Donut({
            //     element: element,
            //     data: data,
            //     resize: true, //defaulted to true
            //     colors: colors
            // });
        },


        Dashboard1.prototype.init = function() {

            //creating bar chart
            var $barData = [
                { y: '01/16', a: 42 },
                { y: '02/16', a: 75 },
                { y: '03/16', a: 38 },
                { y: '04/16', a: 19 },
                { y: '05/16', a: 93 }
            ];
            this.createBarChart('morris-bar-example', $barData, 'y', ['a'], ['Statistics'], ['#3bafda']);

            //create line chart
            var $data = [
                { y: '2008', a: 50, b: 0 },
                { y: '2009', a: 75, b: 50 },
                { y: '2010', a: 30, b: 80 },
                { y: '2011', a: 50, b: 50 },
                { y: '2012', a: 75, b: 10 },
                { y: '2013', a: 50, b: 40 },
                { y: '2014', a: 75, b: 50 },
                { y: '2015', a: 100, b: 70 }
            ];
            this.createLineChart('morris-line-example', $data, 'y', ['a', 'b'], ['Series A', 'Series B'], ['0.9'], ['#ffffff'], ['#999999'], ['#10c469', '#188ae2']);

            //creating donut chart
            var $donutData = [
                { label: "Download Sales", value: 12 },
                { label: "In-Store Sales", value: 30 },
                { label: "Mail-Order Sales", value: 20 }
            ];
            this.createDonutChart('morris-donut-example', $donutData, ['#3ac9d6', '#f5707a', "#4bd396"]);
        },
        //init
        $.Dashboard1 = new Dashboard1, $.Dashboard1.Constructor = Dashboard1
}(window.jQuery),

//initializing 
function($) {
    "use strict";
    $.Dashboard1.init();
}(window.jQuery);