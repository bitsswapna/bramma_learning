$(document).ready(function () {
    var table = $('#vi-complaints-table').DataTable({
        "aaSorting": [],
        'order': [[1, 'desc']],
        "fnDrawCallback": function (oSettings) {
            if (oSettings.bSorted || oSettings.bFiltered || oSettings._iDisplayLength) {
                j = 0;
                for (var i = oSettings._iDisplayStart; i < oSettings.aiDisplay.length + oSettings._iDisplayStart; i++) {
                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[j]].nTr).find('span').text(i + 1);
                    j++;
                }
            }
        },
        serverSide: true,
        scrollX: true,
        oLanguage: {
            sProcessing: '<svg width="30px"  height="30px"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-rolling" style="background: transparent;"><circle cx="50" cy="50" fill="none" stroke="#0fb963" stroke-width="10" r="35" stroke-dasharray="164.93361431346415 56.97787143782138" transform="rotate(228 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform></circle></svg>'
        },
        processing: true,
        "pageLength": 10,
        "columnDefs": [{'orderable': false, 'targets': [0, 5, 5]}],
        mark: true,
        fixedColumns: {},

        ajax: {
            url: base_url + '/get-complaints',
            data: function (d) {
                d.id = $('#complaint-filter').val();
            }
        },
        columns: [
            {data: 'sl#', name: 'sl#'},
            {data: 'complaint_id', name: 'complaint_id'},
            {data: 'order_id', name: 'order_id'},
            {data: 'user', name: 'user'},
            {data: 'category', name: 'category'},
            {data: 'assigned_to', name: 'assigned_to'},
            {data: 'status', name: 'status'},
            {data: 'action', name: 'action'}
        ]
    });


    $('body').on('click', '.confirm-complaint', function (e) {

        var id = $(this).data("id");
        var actionurl = base_url + '/complaint-confirm';
        var url = base_url + '/complaint-details/' + id;
        $("#active_complaint_id").val(id);

        $.ajax({
            type: 'post',
            data: {
                id: id,
            },
            url: actionurl,
            beforeSend: function () {
                $("#if_loading").html("loading");
            },
            success: function (result) {
                table.draw();
                $.ajax({
                    type: 'get',
                    url: url,
                    beforeSend: function () {
                        $("#if_loading").html("loading");
                    },
                    success: function (data) {
                        console.log(data)
                        $("#complaint-details").html(data.html)
                        $('.vi-member-select-multiple').selectize({
                            options: data.customer_service_executive,
                            optgroups: data.departuments,
                            labelField: 'model',
                            valueField: 'id',
                            optgroupField: 'make',
                            optgroupLabelField: 'name',
                            optgroupValueField: 'id',
                            optgroupOrder: data.optgroupOrder,
                            searchField: ['model'],
                            plugins: ['remove_button'],

                        });

                    },
                    complete: function () {
                        // $('.js-example-basic-multiple').select2();

                    }
                });

            }
        });

    });
    $('body').on('click', '.confirm-complaint-view', function (e) {

        var id = $(this).data("id");
        var actionurl = base_url + '/complaint-details/' + id;
        $("#active_complaint_id").val(id);

        $.ajax({
            type: 'get',
            url: actionurl,
            beforeSend: function () {
                $("#if_loading").html("loading");
            },
            success: function (data) {
                $("#complaint-details-view").html(data.html)
                $('.vi-member-select-multiple').selectize({
                    options: data.customer_service_executive,
                    optgroups: data.departuments,
                    labelField: 'model',
                    valueField: 'id',
                    optgroupField: 'make',
                    optgroupLabelField: 'name',
                    optgroupValueField: 'id',
                    optgroupOrder: data.optgroupOrder,
                    searchField: ['model'],
                    plugins: ['remove_button'],

                });
            },
            complete: function () {
                // $('.js-example-basic-multiple').select2();

            }
        });
    });
    $('body').on('click', '.complaint_assign_user_view', function (e) {

        var id = $(this).data("id");
        var actionurl = base_url + '/get-complaint-assign-user/' + id;

        $.ajax({
            type: 'get',
            url: actionurl,
            beforeSend: function () {
                $("#if_loading").html("loading");
            },
            success: function (data) {
                $("#vi-complaint-assign-user-view").html(data.html)
            },
            complete: function () {
                //
            }
        });
    });
    $(document).on('click', '.vi-complaint-assign-to', function (e) {
        var active_complaint_id = $("#active_complaint_id").val()
        var actionurl = base_url + '/complaint-assign/' + active_complaint_id;


        $.ajax({
            type: 'post',
            url: actionurl,
            data: $("#vi-complaint-assign-form").serialize(),
            beforeSend: function () {
                $("#if_loading").html("loading");
            },
            success: function (result) {

                if (result.status == false) {
                    $(".error").html("");
                    $("#submit-btn").prop("disabled", false);
                    $.each($.parseJSON(result.errors), function (i, item) {
                        err = item;
                        field = $('[name="' + i + '"]');
                        field.next('span.error').html(item);
                        return;
                    });

                } else if (result.status == true) {
                    $("#view-complaint-modal .close").click()
                    $("#confirm-complaint-modal .close").click()
                }
            },
            complete: function () {

            }
        });
    });

    $('#complaint-filter').on('change', function () {
        table.draw();
    });

    $(document).on('click', '.change_complaint_status', function (e) {
        var id = $(this).data("id");
        $('.vi-change-complaint-status').data('id', id);
    });

    $(document).on('click', '.vi-change-complaint-status', function (e) {
        var id = $(this).data("id");
        var complaint_status_description = $(".complaint_status_description").val();
        var actionurl = base_url + '/update-complaint-status';
        $.ajax({
            type: 'post',
            url: actionurl,
            data: {
                id: id,
                complaint_status_description: complaint_status_description
            },
            beforeSend: function () {
                $("#if_loading").html("loading");
            },
            success: function (result) {

                if (result.status == false) {
                    $(".error").html("");
                    $.each($.parseJSON(result.errors), function (i, item) {
                        err = item;
                        field = $('[name="' + i + '"]');
                        field.next('span.error').html(item);
                        return;
                    });

                } else if (result.status == true) {
                    table.draw();
                    $("#change-complaint-status .close").click()
                }
            },
            complete: function () {

            }
        });
    });

    $(document).on('click', '.fetch_complaint_time_line', function (e) {
        var url = base_url + '/get-complaint-timeline/' + $(this).data("id");
        $.ajax({
            type: 'get',
            url: url,
            beforeSend: function () {
                $("#if_loading").html("loading");
            },
            success: function (data) {
                $("#vi-complaint-timeline").html(data)
            },
            complete: function () {

            }
        });

    });


        $(function () {
            $('body').on('click', '.pagination a', function (e) {
                e.preventDefault();
                var url = $(this).attr('href');
                var id = $('.getcomplaint_id').val();
                var data = {id: id};

                $.ajax({
                    url: url, data: data,
                }).done(function (data) {
                    $('#vi-complaint-timeline').html(data);
                }).fail(function () {
                    alert('log could not be loaded.');
                });
            });

        });

});