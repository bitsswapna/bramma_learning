
var product_status = null;

var table = $('#productReport').DataTable({
    dom: 'lBfrtip',
    buttons: [
        'copy', 'csv', 'excel', 'pdf', 'print'
    ],
    "aaSorting": [],
    'order': [[1, 'desc']],
    "fnDrawCallback": function (oSettings) {
        if (oSettings.bSorted || oSettings.bFiltered || oSettings._iDisplayLength) {
            j = 0;
            for (var i = oSettings._iDisplayStart; i < oSettings.aiDisplay.length + oSettings._iDisplayStart; i++) {
                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[j]].nTr).find('span').text(i + 1);
                j++;
            }
        }
    },
    serverSide: true,
    scrollX: true,
    oLanguage: {
        sProcessing: '<svg width="30px"  height="30px"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-rolling" style="background: transparent;"><circle cx="50" cy="50" fill="none" stroke="#0fb963" stroke-width="10" r="35" stroke-dasharray="164.93361431346415 56.97787143782138" transform="rotate(228 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform></circle></svg>'
    },
    processing: true,
    "pageLength": 25,
    "columnDefs": [{'orderable': false, 'targets': [0,6,6]}],
    mark: true,
    fixedColumns: {},
    ajax: {
        url: base_url  + '/product-report/getProductReport',
        data: function (d) {
            x = document.getElementById("product_status").selectedIndex;
            d.product_status = document.getElementsByTagName("option")[x].value;
            d.start_date = $('#start_date').val(); 
            d.end_date =  $('#end_date').val();
            d.product_code = $('#product_code').val();
            d.product_name =  $('#product_name').val();
        }

    },
    columns: [
        {data: 'DT_Row_Index', name: 'DT_Row_Index'},
        {data: 'product_id', name: 'product_id'},
        {data: 'product_name', name: 'product_name'},
        {data: 'product_batch', name: 'product_batch'},
        {data: 'sold_count', name: 'sold_count'},
        {data: 'return_count', name: 'return_count'},
        {data: 'complaint', name: 'complaint'},
    ]
});




    $('#product_status').change(function (e) {
    //  alert("hai");
    table.draw();
    e.preventDefault();
    });

$('.filter_button').click(function (e) {
    table.draw();
    e.preventDefault();
});
    $(function() {
        $('input[name="daterange"]').daterangepicker({
            opens: 'left'
        }, function(start, end, label) {
            var startDate = start.format('YYYY-MM-DD');
            var endDate = end.format('YYYY-MM-DD');
           // alert(startDate);
            $('#start_date').val(startDate);
            $('#end_date').val(endDate);
            $('.showDate_field').show();
        table.draw();

        });
    });



$(document).ready(function () {
    $.fn.dataTableExt.sErrMode = 'throw';

    $('#productReport').on( 'error.dt', function ( e, settings, techNote, message ) {
        // console.log( 'An error has been reported by DataTables: ', message );
        swal("Hey!", "Please Wait ! We are loading your datas!", "info")
            .then(function(willDelete){
                if (willDelete) {
                    location.reload(true);
                }
            });
    } ) ;
});
