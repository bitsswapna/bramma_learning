
var product_status = null;
var table = $('#orderTable').DataTable({
    "aaSorting": [],
    'order': [[2, 'desc']],
    "fnDrawCallback": function (oSettings) {
        if (oSettings.bSorted || oSettings.bFiltered || oSettings._iDisplayLength) {
            j = 0;
            for (var i = oSettings._iDisplayStart; i < oSettings.aiDisplay.length + oSettings._iDisplayStart; i++) {
                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[j]].nTr).find('span').text(i + 1);
                j++;
            }
        }
    }, 
    serverSide: true,
    scrollX: true,
    oLanguage: {
        sProcessing: '<svg width="30px"  height="30px"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-rolling" style="background: transparent;"><circle cx="50" cy="50" fill="none" stroke="#0fb963" stroke-width="10" r="35" stroke-dasharray="164.93361431346415 56.97787143782138" transform="rotate(228 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform></circle></svg>'
    },
    processing: true,
    "pageLength": 10,
    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
    "columnDefs": [{'orderable': false, 'targets': [0, 5,7]}],
    mark: true,
    fixedColumns: {},
    ajax: {
        url: base_url  + '/order/get-orderDetails',
        data: function (d) {
            // x = document.getElementById("product_status").selectedIndex;
            // d.product_status = document.getElementsByTagName("option")[x].value;
            d.product_status = $("#product_status").val();
            // d.unique_code = $('#unique_code_list').val();
            d.order_id = $('#order_id_filter').val();
            d.start_date = $('#start_date').val();
            d.end_date =  $('#end_date').val();
        }
    },
    columns: [
        {data: 'sl#', name: 'Sl#'},
        {data: 'date', name: 'date'},
        {data: 'order_id', name: 'order_id'},
        {data: 'sold_by', name: 'sold_by'},
        {data: 'Purchase_by', name: 'Purchase_by'},
        {data: 'product', name: 'product'},
        {data: 'shipping_address', name: 'shipping_address'},
        {data: 'order_price', name: 'order_price'},
        {data: 'purchase_price', name: 'purchase_price'},
        {data: 'status', name: 'status'},
        {data: 'awb_number', name: 'awb_number'},
        {data: 'payment', name: 'payment'},
        {data: 'action', name: 'action'}
    ]
});


$(document).ready(function() {
    $('#unique_code_list').select2({
        placeholder: "Search Unique Code ...",
        minimumInputLength: 1,

        ajax: {
            url:base_url+'/order/getOrderAsBarcode',
            dataType: 'json',
            data: function (params) {
                return {
                    q: $.trim(params.term)
                };
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }

    });
});

$('#bt_remove').click(function () {
    $('#order_id_filter').val('').trigger("change");
    table.draw();
    $("#toggle").trigger("click");
});

$('.filter_button').click(function (e) {
    table.draw();
    e.preventDefault();
});


$('#product_status').change(function (e) {
    table.draw();
    e.preventDefault();
});

$(function() {
    $.fn.dataTableExt.sErrMode = 'throw';

    $('#orderTable').on( 'error.dt', function ( e, settings, techNote, message ) {
        console.log( 'An error has been reported by DataTables: ', message );
        swal("Hey!", "Please Wait ! We are loading your data!", "info")
            .then(function(willDelete){
                if (willDelete) {
                    location.reload(true);
                }
            });
    }) ;
}) ;
$(function() {
    $('input[name="daterange"]').daterangepicker({
        opens: 'left'
    }, function(start, end, label) {
        var startDate = start.format('YYYY-MM-DD');
        var endDate = end.format('YYYY-MM-DD');
        // alert(startDate);
        $('#start_date').val(startDate);
        $('#end_date').val(endDate);
        $('.showDate_field').show();
        table.draw();

    });
});


$(document).on('click','.order_products',function (e) {
    $con = $(this);
    var $data ={data:$(this).data('value')};
    var id = $(this).data('value');
    var actionurl = base_url + '/order-products'  ;
    var title = "/Order Details";
// console.log($data);
    $.ajax({
        type: 'get',
        dataType: "html",
        url: actionurl,
        data: $data,
        beforeSend: function () {

        },
        success: function (data) {
            $('.product-details-modal').html(data);
            $('#myModal').modal()

        },
        error: function (data) {

        },
    });
});

function changeOrderStatus(id, value){
    var url = base_url + '/order/change_status';
    swal({
        title: "Are you sure?",
        text: "Please Confirm The Status Before Change!",
        icon: "warning",
        buttons: true,
        dangerMode: true,

        content: {
            element: "textarea",
            attributes: {
                placeholder: "Type your comment",
                id: "comment",
                class:"form-control",
            },
        },
    })
        .then(function (willDelete) {
            if (willDelete) {
                var comment = document.getElementById('comment').value;
                var data = {id: id , value:value,comment:comment };

                if(comment != ''){
                    $.ajax({
                        url: url, type: 'get', data: data,
                        beforeSend: function () {
                            //
                        },
                        success: function (data) {
                            table.draw();
                            swal(data.msg, {
                                icon: data.icon,
                            });
                        },
                        error: function (data) {
                            // errorMsg(data.responseJSON.msg);
                        },
                        complete: function () {
                            //
                        }
                    });
                    // swal("OK! Successfully Changed!", {
                    //     icon: "success",
                    // });
                }else{
                    table.draw( swal("please input comment before submitting", {
                        icon: "warning",
                    }));


                }
            } else {
                swal("Status Not Changed!");
                table.draw();
            }
        });
}
function addBracodeTestTube(id, value){
    var url_add_test_tube = base_url + '/order/update-test-tube-code';
    swal({
        title: "Test tube Barcode ",
        text: "Please enter barcode!",
        icon: "warning",
        buttons: true,
        dangerMode: true,

        content: {
            element: "textarea",
            attributes: {
                placeholder: "Type your test tube bar-code",
                id: "comment",
                class:"form-control",
            },
        },
    })
        .then(function (willDelete) {
            if (willDelete) {
                var comment = document.getElementById('comment').value;
                var data_test_tube_code = {id: id, comment:comment };

                if(comment != ''){
                    $.ajax({
                        url: url_add_test_tube, type: 'get', data: data_test_tube_code,
                        beforeSend: function () {

                        },
                        success: function (data) {
                            if (data.status == true) {
                                changeOrderStatus(id, value);
                            }else{
                                swal("Oops...! Please provide valid test tube Bar-code");
                                table.draw();
                                // $("#comment").after('<span>Data not found !. Please enter valid barcode</span>')
                            }
                        },
                        error: function (data) {
                            // errorMsg(data.responseJSON.msg);
                        },
                        complete: function () {

                        }
                    });
                    // swal("OK! Successfully Changed!", {
                    //     icon: "success",
                    // });
                }else{
                    table.draw( swal("please input test tube barcode before submitting", {
                        icon: "warning",
                    }));
                }
            } else {
                swal("Test tube Barcode not updated!");
                table.draw();
            }
        });
}
$("input.validate").on("blur",function (e) {
    if($(this).val()=="")
    {
        $(this).addClass(".error-field");
        $(this).parent().append(" <span class=\"  error\" style='color: red'>The  field is required.</span>")
    }
})
$("input.validate-custom").on("blur",function (e) {
    if(($(this).val()=="")&& ($("#mrp").val() > 50000))
    {
        $(this).addClass(".error-field");
        $(this).parent().append(" <span class=\"  error\" style='color: red'>The  field is required.</span>")
    }
})
$("input.validate-custom").on("focus",function (e) {
    $(this).removeClass(".error-field");
    $(this).parent().find(".error").remove();
})

$(".validate").on("focus",function (e) {

    $(this).removeClass(".error-field");
    $(this).parent().find(".error").remove();
})
$('#myModal').on('hidden.bs.modal', function () {

    $('a[href="#gst_details"]').click();
    document.getElementById("shipment").reset();

   table.draw();
})
$("#shipment").on("submit",function (e) {

    e.preventDefault();
    $("#btn-submit").prop("disbaled",true);
    $(".loading-save").css("display","block");
    $(".loading-save-btn").css("display","none");
    $.ajax({
        type:"POST",
        url:'/init-shipment',
        data:$(this).serialize(),
        success:function (data) {
            $(".loading-save").css("display","none");
            $(".loading-save-btn").css("display","block");
            if(data&&data.status==false&&data.error)
            {

                var error =data.error;
                Object.keys(error).forEach(e=>{
                    $("#"+e).parent().find(".error").remove();
                    $("#"+e).addClass("error-field");

                    $("#"+e).parent("div").append(" <span class=\"  error\" style='color: red'>"+error[e][0]+"</span>");
                })
                var error_tab=$("span.error:first").parent("div").parent("div").attr("id");
                $('a[href="#'+error_tab+'"]').click();

            }
            else if(data && data.status)
            {
                $("#myModal").modal('toggle');
                table.draw();
                //$(this)[0].reset();
                $.notify({message:data.message},{type:"success",z_index:5000});

            }
            else
            {
                $.notify({message:data.message},{type:"danger",z_index:5000});
            }
            $("#btn-submit").prop("disbaled",false);

        },
        error:function (err) {
            $(".loading-save").css("display","none");
            $(".loading-save-btn").css("display","block");
            $.notify({message:"Some error occured please try again"},{type:"danger",z_index:5000});
            $("#btn-submit").prop("disbaled",true);
        }
    })
})

    $(document).on('change', '.change-order-status', function (e) {


        var id = $(this).data("id");
        var value = this.value;
        $(".order_id").val($(this).data("id"));
        $("#mrp").val($(this).data("mrp"));
        var url_check_tube = base_url + '/order/check-test-tube';
        var data_check = {id: id, value: value};
        $.ajax({
            url: url_check_tube, type: 'get', data: data_check,
            beforeSend: function () {
                //
            },
            success: function (result) {
                if (result.status == true) {/*Billing or test tube bar code entered*/
                    if (value == 4 || value == 6) {
                        $("#status").val(value);
                        $(".product_name_ship").html(result['product']['name']);
                        $(".product_id_ship").html(result['product']['id']);
                        $('#myModal').modal('show');

                    } else {
                        changeOrderStatus(id, value);
                    }

                } else {
                    addBracodeTestTube(id, value);
                }
            },
            error: function (data) {
                // errorMsg(data.responseJSON.msg);
            },
            complete: function () {
                //
            }
        });


    });


$(document).on('click mouseover', '.shipping-add',function(){

    $('[rel="popover"]').popover({
        container: 'body',
        html: true,
        content: function () {
            var clone = $($(this).data('popover-content')).clone(true).removeClass('hide');
            return clone;
        }
    }).click(function(e) {
        e.preventDefault();
    });
});


$('body').on('click', function (e) {
    $('[rel="popover"]').each(function () {
        // hide any open popovers when the anywhere else in the body is clicked
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            $(this).popover('hide');
        }
    });
});


$(document).ready(function() {

    $('.order-select').select2();

});


$('body').on('click', '.order-offer-details', function(e) {
    var order_id = $(this).data("offer");
    $.ajax({
        url: base_url + "/order-details/get-order/"+order_id,
        type: 'get',
        beforeSend: function() {
        },
        success: function(result) {
            // console.log(related_order)
            var related_order_html = "";
            var data = result['order'];
            var related_order = result['related_order'];
            // $.each($.parseJSON(related_order), function(i, item) {
            //     related_order_html = related_order_html + "<span>"+item['id']+"</span>";
            // });
            if (data['offer_type'] == 1) {
                $(".offer-details-body").html('   <p>Offer key : <strong>' + data["offer_key"] + '</strong></p>\n' +
                    '                        <p>Offer purchase quantity : <strong>' + data["offer_purchase_quantity"] + '</strong></p>\n' +
                    '                        <p>Offer quantity : <strong>' + data["offer_offer_quantity"] + '</strong></p>\n' +
                    '                        <p>Offer : <strong> ' + data["offer"] + ' % </strong></p>');
            }else {
                $(".offer-details-body").html('   <p>Offer key : <strong>' + data["offer_key"] + '</strong></p>\n' +
                    '                        <p>Offer purchase quantity : <strong>' + data["offer_purchase_quantity"] + '</strong></p>\n' +
                    '                        <p>Offer quantity : <strong>' + data["offer_offer_quantity"] + '</strong></p>\n' +
                    '                        <p>Offer : <strong> ₹' + data["offer"] + '</strong></p>');
            }

        },
        complete: function() {
        },
        error: function() {
        }
    });
});
$('body').on('click', '.order-payment-details-btn', function(e) {
    var id = $(this).data("id");
    $.ajax({
        url: base_url + "/order-details/payment-details",
        type: 'POST',
        data: {order_id: id },
        beforeSend: function() {
        },
        success: function(data) {
            $(".order-payment-details-body").html(data);
        },
        complete: function() {
        },
        error: function() {
        }
    });
});
$('body').on('click', '.awb_order_timeline', function(e) {
    var url = base_url + '/order/track_order';
    var awb_number = $(this).data("id");
    var awb_type = $(this).data("content");
    var ajax_data = { awb_number: awb_number ,awb_type:awb_type};
    $.ajax({
        type: 'post',
        url: url,
        data: ajax_data,
        beforeSend: function () {
            $(".awb-details-body").html('<div style="margin-right: 50%; margin-left: 50%;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>\n' +
                '<span class="sr-only">Loading...</span></div>');

        },
        success: function (data) {
            var html = "";

            if (data.status && data.process) {
                var feild = data.data.object.field.map(function (e) {
                    if ((typeof e) == "object") {
                        return e
                    }
                });
                feild = feild.filter(function (e) {
                    return e != undefined
                });
                feild = feild.find(function (e) {
                    if (((typeof e) == "object") && e['@attributes'].name == "scans") {
                        return e
                    }
                });
                var scans = feild.object;

                if (scans.constructor.name == "Array") {
                    scans.reverse();
                    scans.forEach(e => {

                        html += "<li>";

                    html += "<h3>" + e.field[1] + "<h3>";
                    html += "<p>" + e.field[0] + "</p>";
                    html += "<p>" + e.field[7] + "</p>";
                    html += "<p>" + e.field[6] + "</p>";

                    html += "</li>";
                })
                } else {
                    html += "<li>";

                    html += "<h3>" + scans.field[1] + "<h3>";
                    html += "<p>" + scans.field[0] + "</p>";
                    html += "<p>" + scans.field[7] + "</p>";
                    html += "<p>" + scans.field[6] + "</p>";

                    html += "</li>";
                }
            }
            else if (data.status && data.process == false) {
                html += "<li>";

                html += "<h3>" + "Order not shiped" + "<h3>";

                html += "</li>";
            }
            else {
                html = "No Order found";
            }



            $(".awb-details-body").html(html);

        },
        complete: function () {

        },
        error: function () {
            swal("Something went wrong.Please try again.")
        }
    });

});

