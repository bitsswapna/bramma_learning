
// $('.wellness-advisor-list').select2();
$(function () {
    $('.wellness-advisor-list-main').selectize({
        valueField: 'id',
        labelField: 'text',
        load: function (query, callback) {
            $.ajax({
                url:base_url+'/billing/getselectedusers?q=' + encodeURIComponent(query),
                success: function (response) {
                    $('#wellness-advisor-list-main').selectize({
                        options : response
                    });
                    callback(response);
                }
            });
        }
    });

    // var selectize = $select[0].selectize;

});


//Admin payout datatable


var table_process_payout = $('#admin-process-payout_main-report-table').DataTable({
    dom: 'lBfrtip',
    buttons: [
        {extend:"copy",footer: true}, {extend:"csv",footer: true} , {extend:"excel",footer: true}, {extend:"pdf",footer: true,exportOptions: {
                columns: ':visible',
                stripHtml: true
            },
            pageSize: 'LETTER'  ,customize: function(doc, config) {
                var tableNode;
                for (i = 0; i < doc.content.length; ++i) {
                    if(doc.content[i].table !== undefined){
                        tableNode = doc.content[i];
                        break;
                    }
                }

                var rowIndex = 0;
                var tableColumnCount = tableNode.table.body[rowIndex].length;

                if(tableColumnCount > 5){
                    doc.pageOrientation = 'landscape';
                }
            }}, {extend:"print",footer: true, orientation: 'landscape'}
    ],
    "aaSorting": [],
    'order_id': [[1, 'desc']],
    "fnDrawCallback": function (oSettings) {
        if (oSettings.bSorted || oSettings.bFiltered || oSettings._iDisplayLength) {
            j = 0;
            for (var i = oSettings._iDisplayStart; i < oSettings.aiDisplay.length + oSettings._iDisplayStart; i++) {
                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[j]].nTr).find('span').text(i + 1);
                j++;
            }
        }
    },
    serverSide: true,
    scrollX: true,
    oLanguage: {
        sProcessing: '<svg width="30px"  height="30px"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-rolling" style="background: transparent;"><circle cx="50" cy="50" fill="none" stroke="#0fb963" stroke-width="10" r="35" stroke-dasharray="164.93361431346415 56.97787143782138" transform="rotate(228 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform></circle></svg>'
    },
    processing: true,
    "pageLength": 10,
    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
    "columnDefs": [{'orderable': false, 'targets': [0, 17, 17]}],
    mark: true,
    fixedColumns: {},
    "footerCallback": function(row, data, start, end, display) {
        var api = this.api(), data;

        // converting to interger to find total
        var intVal = function ( i ) {
            return typeof i === 'string' ?
                i.replace(/[\$,]/g, '')*1 :
                typeof i === 'number' ?
                    i : 0;
        };

        $( api.column( 0 ).footer() ).html('Total');
        $(api.column(1).footer()).html("--")
        $(api.column(2).footer()).html("--")
        $(api.column(3).footer()).html( parseFloat(api
            .column( 3 )
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 )).toFixed(2));
        $(api.column(4).footer()).html( parseFloat(api
            .column( 4 )
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 )).toFixed(2));
        $(api.column(5).footer()).html( parseFloat(api
            .column( 5)
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 )).toFixed(2));
        $(api.column(6).footer()).html( parseFloat(api
            .column( 6)
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 )).toFixed(2));
        $(api.column(7).footer()).html( parseFloat(api
            .column( 7)
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 )).toFixed(2));
        $(api.column(8).footer()).html( parseFloat(api
            .column( 8)
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 )).toFixed(2));
        $(api.column(9).footer()).html(parseFloat( api
            .column( 9)
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 )).toFixed(2));
        $(api.column(10).footer()).html(parseFloat( api
            .column( 10)
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 )).toFixed(2));
        $(api.column(11).footer()).html(parseFloat( api
            .column( 11)
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 )).toFixed(2));
        $(api.column(12).footer()).html('--');
        $(api.column(13).footer()).html("--")
        $(api.column(14).footer()).html("--")
        $(api.column(15).footer()).html("--")
        $(api.column(16).footer()).html("--")
        $(api.column(17).footer()).html("--")



    },
    ajax: {
        url: base_url  + '/order/get-admin-payout',
        data: function (d) {
            d.report_month =  $('#payout_report_month').val();
            d.user_id =  $('#payout_wellness-advisor-list-main').val();
        }
    },
    columns: [
        {data: 'month', name: 'month'},
        {data: 'user_id', name: 'user_id'},
        {data: 'name', name: 'name'},
        {data: 'psi', name: 'psi',render:((value,data,num)=>{
                return parseFloat(value).toFixed(2);
})},
{data: 'fsi', name: 'fsi',render:((value,data,num)=>{
    return parseFloat(value).toFixed(2);
})},
{data: 'gsi', name: 'gsi',render:((value,data,num)=>{
    return parseFloat(value).toFixed(2);
})},
{data: 'ucr', name: 'ucr',render:((value,data,num)=>{
    return parseFloat(value).toFixed(2);
})},
{data: 'gcr', name: 'gcr',render:((value,data,num)=>{
    return parseFloat(value).toFixed(2);
})},
{data: 'WAmbR', name: 'WAmbR',render:((value,data,num)=>{
    return parseFloat(value).toFixed(2);
})},
{data: 'total', name: 'total',render:((value,data,num)=>{
    return parseFloat(value).toFixed(2);
})},
{data: 'tds', name: 'tds',render:((value,data,num)=>{
    return parseFloat(value).toFixed(2);
})},
{data: 'net_pay', name: 'net_pay',render:((value,data,num)=>{
    return parseFloat(value).toFixed(2);
})},
        {data: 'bank_name', name: 'bank_name'},
        {data: 'acc_no', name: 'acc_no'},
        {data: 'ifsc', name: 'ifsc'},

{data: 'utr_number', name: 'utr_number'},
        {data: 'status', name: 'stauts'},
{data: 'payment_details', name: 'payment_details'},
]
});
$.fn.dataTableExt.sErrMode = 'throw';

$('#admin-process-payout_main-report-table').on( 'error.dt', function ( e, settings, techNote, message ) {
    // console.log( 'An error has been reported by DataTables: ', message );
    swal("Hey!", "Please Wait ! We are loading your datas!", "info")
        .then(function(willDelete){
            if (willDelete) {
                location.reload(true);
            }
        });
} ) ;
$('body').on('change', '#payout_report_month', function (e) {
    table_process_payout.draw();
});

$('body').on('change', '#payout_wellness-advisor-list-main', function (e) {
    table_process_payout.draw();
});



/*Payment details*/

$('body').on('click', '.edit-bank-detail_hide', function (e) {
    $(".bank-details-block").show();
    $(".bank-detail-edit-form").html('');
    $("#payout_form").show();

});

$('body').on('click', '.edit-bank-detail_save', function (e) {
    $.ajax({
        url: base_url+'/edit-user-bank-detail',
        type: 'POST',
        data: $("#edit_bank_datil_form").serialize(),
        beforeSend: function() {

        },
        success: function(result) {
            $(".save_payout_btn").prop("edit-bank-detail_save", false);
            if (result.status == false) {

                $.each($.parseJSON(result.errors), function(i, item) {
                    err = item;
                    field = $('[name="' + i + '"]');
                    field.next('span.error').html(item);
                    return;
                });

            } else if (result.status == true) {
                $(".bank-detail-edit-form").html('');
                bankDetail(result.user_id)
                $("#payout_form").show();
            }
        },
        complete:function () {

        },

        error:function f() {

        }

    });

});
$('body').on('click', 'input[type=radio][name=bank_detail_check]', function(e) {

    if (this.value) {
        $('#user_id_form').val($(this).data("id"));
        $('#user_bank_id_form').val(this.value)
    }
});
$('body').on('click', '.save_payout_btn', function (e) {
    $('#user_id_form').val($("#user_id_form_bank").val());
    $('#user_bank_id_form').val($("#user_bank_id_form_bank").val());
    var notify;
    $(".save_payout_btn").prop("disabled", true);
    // var radioValue = $("input[name='bank_detail_check']:checked").val();
    // if (radioValue) {
    $(".error_bank_detail_check").html("");
    $.ajax({
        url: base_url + '/order/add-payout',
        type: 'POST',
        data: $("#payout_form").serialize(),
        beforeSend: function () {

        },
        success: function (result) {
            $(".save_payout_btn").prop("disabled", false);

            if (result.status == false) {
                $.each($.parseJSON(result.errors), function(i, item) {
                    err = item;
                    field = $('[name="' + i + '"]');
                    field.next('span.error').html(item);
                    return;
                });

            } else if (result.status == true) {

                $.notify('<strong>Success</strong> Payment details updated!', {type:"success",z_index:5000 });
                setTimeout(function () {


                    $('.close').trigger('click');
                    $(".save_payout_btn").prop("disabled", false);
                }, 1000);

            }
        },
        complete: function () {

        },

        error: function f() {

        }

    });
    // }else{
    //     $(".error_bank_detail_check").html("Please choose bank details");
    //     $(".save_payout_btn").prop("disabled", false);
    //
    // }

});

$('body').on('click', '.edit-bank-detail', function (e) {
    var id = $(this).data("id");
    $.ajax({
        url: base_url+'/get-payment-details-edit-form',
        type: 'POST',
        data: {
            'id': id
        },
        beforeSend: function() {
            $(".bank-details-block").hide();
            $("#payout_form").hide();

        },
        success: function(result) {
            $(".bank-detail-edit-form").html(result);
        },
        complete:function () {

        },

        error:function f() {

        }

    });
});
function bankDetail(u_id){


    $.ajax({
        url: base_url+'/order/get-payment-details',
        type: 'POST',
        data: {
            'u_id': u_id,
            'month':$("#payout_report_month").val()
        },
        beforeSend: function() {
        },
        success: function(result) {
            $(".payment_detail_body").html(result);
        },
        complete:function () {
            $('.form-control.date-pick').datepicker({
                format: "yyyy-mm-dd",
                todayHighlight:true,
                defaultViewDate:'today',
                autoclose:true,
            });
            var now=new Date();
            now.setMonth(now.getMonth() - 1);
            $(function() {
                $(".month-pick").datepicker( {
                    format: "mm-yyyy",
                    viewMode: "months",
                    minViewMode: "months",
                    orientation: 'auto bottom',
                    autoclose:true,
                    endDate: "today",
                    maxDate: "today",
                    // defaultDate:("0"+(now.getMonth()+1)).slice(-2)+"-"+now.getFullYear()
                });




            });
            var click_id = '#payment-details'+u_id;
            var net_amount = $(click_id).data("content");
            now=new Date("2019-04-30");

            $(".month-pick").datepicker("setDate",$("#payout_report_month").val());

            $(".month_year").prop("disabled",true);
            $('#net_pay').val(net_amount);
            $('.net-pay-amount').html(net_amount);
        },

        error:function f() {

        }

    });

}
$('body').on('click', '.payment-details', function (e) {
    var u_id = $(this).data("id");

    bankDetail(u_id)
});