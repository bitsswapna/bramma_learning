// $('.wellness-advisor-list').select2();
$(function() {
    $('.wellness-advisor-list-main').selectize({
        valueField: 'id',
        labelField: 'text',
        load: function(query, callback) {
            $.ajax({
                url: base_url + '/billing/getselectedusers?q=' + encodeURIComponent(query),
                success: function(response) {
                    $('#wellness-advisor-list-main').selectize({
                        options: response
                    });
                    callback(response);
                }
            });
        }
    });

    // var selectize = $select[0].selectize;..

});
$('.wellness-advisor-list-main').on('change', function() {
    $(".error").html('');
    var waid = this.value;
    if (waid) {
        $(".wellness-advisor-list-input").val(waid);
        $(".loader").show();
        $(".varification-wa").html('');
        $(".address-form").html('');
        $(".payment-form").html('');
        $(".barcode-form").html('');
        var url = base_url + '/get-wellness-advisor-detail-tab/' + waid;
        $.ajax({
            url: url,
            type: 'GET',
            beforeSend: function() {
                $(".varification-wa").html('<div style="margin-right: 50%; margin-left: 50%;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>\n' +
                    '<span class="sr-only">Loading...</span></div>');
            },
            success: function(data) {
                $(".varification-wa").html(data);
            },
            complete: function() {
                $(".loader").hide();
                $(".profile-tab").trigger("click");
            }
        });
    }
});

$(document).ready(function() {

    $('body').on('click', '.purchase', function(e) {
        $(".verify-barcode-tab").trigger("click");
        // var url = base_url+'/get-barcode-form/'+$(".wellness-advisor-list-input").val();
        // $.ajax({
        //     url: url,
        //     type: 'GET',
        //
        //     beforeSend:function(){
        //         $(".address-form").html('');
        //         $(".payment-form").html('');
        //         // $(".loader").show();
        //         $(".barcode-form").html('<div style="margin-right: 50%; margin-left: 50%;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>\n' +
        //             '<span class="sr-only">Loading...</span></div>');
        //     },
        //     success: function(data) {
        //         $('html, body').animate({
        //             scrollTop: $(".barcode-form").offset().top
        //         }, 2000);
        //         $(".barcode-form").html(data.html);
        //
        //     },
        //     complete:function () {
        //         // $(".loader").hide();
        //         //
        //         // var scrollPos =  $(".purchase").offset().top;
        //         // $(window).scrollTop(scrollPos);
        //     }
        // });
    });
    $('body').on('click', '.barcode_validation', function(e) {

        var url = base_url + '/get-address-form-tab/' + $(".wellness-advisor-list-input").val();
        $.ajax({
            url: url,
            type: 'POST',
            data: {
                barcode: $("#barcode_number").val(),
                productarray: productarray,
            },
            beforeSend: function() {
                // $(".loader").show();
                $(".address-form").html('<div style="margin-right: 50%; margin-left: 50%;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>\n' +
                    '<span class="sr-only">Loading...</span></div>');
            },
            success: function(data) {

                if (data.status == true) {
                    $(".address-form").html(data.html);
                    $(".payment-form").html("");
                    $(".verify-alert-success").html(data.message);
                    $(".verify-alert-error").html("");
                    $(".delivery-address-tab").trigger("click");
                } else {
                    $(".verify-alert-error").html(data.message);
                    $(".verify-alert-success").html("");
                    $(".payment-form").html("");
                    $(".address-form").html("");
                }
                // if (data.address_first != 0){
                //
                // }

            },
            complete: function(data) {

                // $(".loader").hide();
                //
                // var scrollPos =  $(".purchase").offset().top;
                // $(window).scrollTop(scrollPos);
            },
            error: function() {
                swal("Something went wrong ! Please try again");
            }
        });
    });
    $(function() {

        $('body').on('click', '.save-customer-staff-form', function(e) {
            var actionurl = base_url + '/admin_user/create-customer';
            $(".save-customer-staff-form").prop("disabled", true);
            var formdata = new FormData($("#createAdminUser").get(0))
            formdata.append("role_id", $(".image-radio-checked").children("input").val())
            $.ajax({
                type: 'POST',
                url: actionurl,
                data: formdata,
                contentType: false,
                processData: false,
                success: function(result) {

                    if (result.permission == true) {
                        if (result.status == false) {

                            $(".error").html("");

                            $.each($.parseJSON(result.errors), function(i, item) {
                                err = item;
                                if (i == "parent_id") {
                                    field = $('[name="' + i + '"]').parent("div").parent("div");
                                    field.children('span.error').html(item);
                                    return;
                                } else {
                                    field = $('[name="' + i + '"]');
                                    field.next('span.error').html(item);
                                    return;
                                }
                            });
                            $(".save-customer-staff-form").prop("disabled", false);
                            $("li[data-number=" + $("span.error:not(:empty)").first().data("number") + "]").children("a").trigger("click")
                            $(".save-customer-staff-form").prop("disabled", false);

                        } else if (result.status == true) {
                            var notify = $.notify('<strong>Saving</strong> Do not close this page...', {
                                allow_dismiss: false,
                                showProgressbar: true,
                                z_index: 15000,
                            });
                            setTimeout(function() {
                                $('#exampleModalScrollable').modal('hide');
                                $('#createAdminUser').trigger("reset");

                                notify.update({ 'type': 'success', 'message': '<strong>Success</strong> Your page has been saved!', 'progress': 25 });
                                // $('#prof_img').find('img').attr('src', no_image_url);
                                $(".error").html("");
                                var waid = result.user_id;
                                $(".wellness-advisor-list-input").val(waid)
                                $(".wellness-advisor-list-main").val(waid)
                                $(".loader").show();
                                $(".varification-wa").html('');
                                $(".address-form").html('');
                                $(".payment-form").html('');
                                var url = base_url + '/get-wellness-advisor-detail-tab/' + waid;
                                $.ajax({
                                    url: url,
                                    type: 'GET',
                                    beforeSend: function() {},
                                    success: function(data) {
                                        $(".varification-wa").html(data);
                                    },
                                    complete: function() {
                                        $(".loader").hide();
                                        $(".profile-tab").trigger("click");
                                    }
                                });
                            }, 1000);
                        }
                        $(".save-customer-staff-form").prop("disabled", false);
                        //$(".lst_data ").removeClass("save-customer-staff-form");
                    } else {
                        swal("You have no permission")
                        $(".save-customer-staff-form").prop("disabled", false);

                    }
                },
                error: function() {
                    swal("Something went wrong ! Please try again");
                    $(".save-customer-staff-form").prop("disabled", false);
                }
            });
        });

        // $('#myform-edit-address').bind('submit', function () {

        $('body').on('submit', '#myform-edit-address', function(e) {
            var actionurl = base_url + '/billing/address-edit';

            $.ajax({
                type: 'post',
                url: actionurl,
                data: $('#myform-edit-address').serialize(),
                beforeSend: function() {
                    $("#submit-btn").prop("disabled", true);

                },
                success: function(result) {
                    if (result.status == false) {
                        $("#submit-btn").prop("disabled", false);
                        $.each($.parseJSON(result.errors), function(i, item) {
                            err = item;
                            field = $('[name="' + i + '"]');
                            field.next('span.error').html(item);
                            return;
                        });

                    } else if (result.status == true) {
                        $(".new-delivery-address").html('');
                        $(".edit-delivery-address").html('');
                        $(".address-form").html('');
                        fetchDeliveryAddress();
                    }
                },
                error: function() {
                    swal("Something went wrong ! Please try again");
                    $("#submit-btn").prop("disabled", false);
                }

            });
            return false;
        });
    });
    $(function() {

        $('body').on('click', '.save-wa-form', function(e) {
            e.preventDefault();
            var actionurl = base_url + '/admin_user/create';
            var formdata = new FormData($("#createAdminUser").get(0))
            formdata.append("role_id", $(".image-radio-checked").children("input").val())
            $(".save-wa-form").prop("disabled", true);
            $.ajax({
                type: 'POST',
                url: actionurl,
                contentType: false,
                processData: false,
                data: formdata,
                // data:$("#createAdminUser").serialize(),
                success: function(result) {
                    // console.log(result)

                    if (result.permission == true) {
                        if (result.status == false) {
                            $(".error").html("");
                            $(".save-wa-form").prop("disabled", false);
                            $.each($.parseJSON(result.errors), function(i, item) {
                                err = item;
                                if (i == "parent_id") {
                                    field = $('[name="' + i + '"]').parent("div").parent("div");
                                    field.children('span.error').html(item);
                                    return;
                                } else {
                                    field = $('[name="' + i + '"]');
                                    field.next('span.error').html(item);
                                    return;
                                }

                            });
                            console.log($("li[data-number=" + $("span.error:not(:empty)").first().data("number") + "]").children("a"));
                            $("li[data-number=" + $("span.error:not(:empty)").first().data("number") + "]").children("a").trigger("click")
                        } else if (result.status == true) {
                            var notify = $.notify('<strong>Saving</strong> Do not close this page...', {
                                allow_dismiss: false,
                                showProgressbar: true,
                                z_index: 15000,
                            });

                            // setTimeout(function () {
                            $('#exampleModalScrollable').modal('hide');
                            $('#createAdminUser').trigger("reset");

                            // notify.update({ 'type': 'success', 'message': '<strong>Success</strong> Your page has been saved!', 'progress': 25 });
                            // $('#prof_img').find('img').attr('src', no_image_url);
                            $(".error").html("");
                            var waid = result.user_id;
                            $(".wellness-advisor-list-input").val(waid)
                            $(".wellness-advisor-list-main").val(waid)
                            $(".loader").show();
                            $(".varification-wa").html('');
                            $(".address-form").html('');
                            $(".payment-form").html('');
                            var url = base_url + '/get-wellness-advisor-detail-tab/' + waid;
                            $.ajax({
                                url: url,
                                type: 'GET',
                                beforeSend: function() {},
                                success: function(data) {
                                    $(".varification-wa").html(data);

                                },
                                complete: function() {
                                    $(".loader").hide();
                                    $(".profile-tab").trigger("click");
                                }
                            });
                            // }, 4500);
                        }

                    } else {
                        swal("You have no permission")
                    }




                },
                error: function() {
                    swal("Something went wrong ! Please try again");
                    $(".save-wa-form").prop("disabled", false);
                }
            });
        });

        // $('#myform-edit-address').bind('submit', function () {

        $('body').on('submit', '#myform-edit-address', function(e) {
            var actionurl = base_url + '/billing/address-edit';

            $.ajax({
                type: 'post',
                url: actionurl,
                data: $('#myform-edit-address').serialize(),
                beforeSend: function() {
                    $("#submit-btn").prop("disabled", true);

                },
                success: function(result) {
                    if (result.status == false) {
                        $("#submit-btn").prop("disabled", false);
                        $.each($.parseJSON(result.errors), function(i, item) {
                            err = item;
                            field = $('[name="' + i + '"]');
                            field.next('span.error').html(item);
                            return;
                        });

                    } else if (result.status == true) {
                        $(".new-delivery-address").html('');
                        $(".edit-delivery-address").html('');
                        $(".address-form").html('');
                        fetchDeliveryAddress();
                    }
                },
                error: function() {
                    swal("Something went wrong ! Please try again");
                    $("#submit-btn").prop("disabled", false);
                }
            });
            return false;
        });
    });

    function fetchDeliveryAddress() {
        var url = base_url + '/get-address-form-tab/' + $(".wellness-advisor-list-input").val();
        $.ajax({
            url: url,
            type: 'POST',
            data: {
                barcode: $("#barcode_number").val(),
                productarray: productarray,
            },
            beforeSend: function() {
                // $(".loader").show();
                $(".address-form").html('<div style="margin-right: 50%; margin-left: 50%;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>\n' +
                    '<span class="sr-only">Loading...</span></div>');
            },
            success: function(data) {
                $('html, body').animate({
                    scrollTop: $(".address-form").offset().top
                }, 2000);

                if (data.status == true) {
                    $(".address-form").html(data.html);
                } else {
                    $(".address-form").html(data.message);
                }


            },
            complete: function() {
                // $(".loader").hide();
                //
                // var scrollPos =  $(".purchase").offset().top;
                // $(window).scrollTop(scrollPos);
            }
        });
    }
    var productarray = {};

    $('body').on('click', '.add-to-cart', function(e) {
        productarray = {};
        var product_id = $(this).data("id");
        var box = "#product-box--content-" + product_id;
        $(".product-box--content").removeClass("intro");
        $(box).addClass("intro");

        var url = base_url + '/cart/get-stock';
        $('.product_selected').val(product_id);
        $.ajax({
            url: url,
            type: 'POST',
            data: {
                'quantity': 1,
                'product_id': product_id
            },
            beforeSend: function() {
                $(".address-form").html('');
                $(".payment-form").html('');
                $(".barcode-form").html('');
                $(".purchase").hide();
                $(".purchase-btn-div").html('<div style="margin-right: 50%; margin-left: 50%;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>\n' +
                    '<span class="sr-only">Loading...</span></div>');
            },
            success: function(result) {

                $(".purchase-btn-div").html("");
                $(".product-price-details").hide();

                if (result > 0) {

                    var quantity_ = "#vi-product-quantity" + product_id;
                    var quantity = $(quantity_).val();

                    var total = "#vi-product-total" + product_id;
                    var price_ = "#vi-product-price" + product_id;
                    var price = $(price_).val();
                    var mrp_ = "#vi-product-mrp" + product_id;
                    var mrp = $(mrp_).val();
                    var slug = "#vi-product-slug" + product_id;
                    slug = $(slug).val();
                    var product_ = "#vi-product-product_name" + product_id;
                    var product_name = $(product_).val();
                    var product_img = "#product-img-path" + product_id;
                    var product_image = $(product_img).val();

                    var productarray_ = {};
                    productarray_['product_id'] = product_id;
                    productarray_['qty'] = quantity;
                    productarray_['price'] = price;
                    productarray[slug] = productarray_;


                    var i = parseInt(quantity);
                    /*Todo: Set max limit dynamic*/
                    if (parseInt(i) <= result) {

                        var total_price = parseFloat(price) * parseFloat(quantity);
                        $(quantity).val(i);
                        $(product_).html('Total Price : <i class="fa fa-inr" aria-hidden="true">' + total_price);
                        $('.test-product-image').html('<div class="side-pro-view"><p><span class="p-left test-product-name">' + product_name + '</span></p><p><i class="fa fa-inr"></i>' + mrp + '<span class="product-tax">Inclusive of taxes</span><span class="side-added">Added to cart</span></p></div>');
                        var url_purchase = base_url + '/get-barcode-form-tab/' + $(".wellness-advisor-list-input").val();
                        $.ajax({
                            url: url_purchase,
                            type: 'GET',

                            beforeSend: function() {
                                $(".address-form").html('');
                                $(".payment-form").html('');
                                // $(".loader").show();
                                $(".barcode-form").html('<div style="margin-right: 50%; margin-left: 50%;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>\n' +
                                    '<span class="sr-only">Loading...</span></div>');
                            },
                            success: function(data) {
                                $(".barcode-form").html(data.html);
                            },
                            complete: function() {
                                $(".purchase").show();
                                $('html, body').animate({
                                    scrollTop: $(".purchase").offset().top
                                }, 2000);


                            },
                            error: function() {
                                swal("Somethimg went worng !. Please try again.")
                            }
                        });

                    } else {
                        swal("Available stock quantity " + result);
                        $(quantity_).val(result);
                    }
                } else {
                    swal("Stock not available");
                }
            },
            complete: function() {
                $('#if_loading').html("");
            },
            error: function() {
                swal("Somethimg went wrong !. Please try again.");
            }
        });

    });


    $('body').on('click', '.proceed', function(e) {
        $(".product-taxes-tax_per").html('');
        var address_id = $(this).data("id");
        $(".proceed").html('Deliver Here');
        $(this).html('<span class="badge badge-light"><i class="fa fa-check"></i></span>Deliver Here');
        $("#form_address_id").val(address_id);
        var url = base_url + '/get-payment-form-tab';
        var taxname = '';
        $.ajax({
            url: url,
            type: 'GET',
            data: {
                address_id: address_id,
                product_id: $(".product_selected").val()
            },
            beforeSend: function() {
                $(".proceed").attr("disabled", true);
                $(".payment-form").html('<div style="margin-right: 50%; margin-left: 50%;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>\n' +
                    '<span class="sr-only">Loading...</span></div>');
            },
            success: function(data) {
                // for (i=0; i< data.product_taxes.length;i++) {
                //    taxname = data.product_taxes[i]['tax_name'] +"-"+ parseInt(data.product_taxes[i]['tax_percentage'])
                // }
                if(data.status) {
                    for (i = 0; i < data.product_taxes.length; i++) {
                        $(".product-taxes-tax_per").append(' ' +
                            data.product_taxes[i]['tax_name'] + "-" + parseInt(data.product_taxes[i]['tax_percentage']) + "%" +
                            ' ');
                    }
                    $(".product-price-details").show();
                    // $(".product-taxes-tax_per").html(taxname);
                    var price_ = "#vi-product-price" + $(".product_selected").val();
                    var price = $(price_).val();
                    var mrp_ = "#vi-product-mrp" + $(".product_selected").val();
                    var mrp = $(mrp_).val();
                    $(".product-selected-mrp").html(mrp);
                    $(".product-selected-price").html(price);

                    $(".discount-show").hide();
                    $(".product-selected-mrp").removeAttr('style');
                    $(".discount-form-body").html('');
                    $(".product-selected-discount-off-per").html('');
                    $(".product-selected-discount").html('');
                    $(".payment-form").html(data.html);

                    $(".payment-option-tab").trigger("click");
                }
                else{
                    $.notify({message:data.message},{type: 'danger'})
                }

            },
            complete: function() {
                $(".proceed").attr("disabled", false);
            }
        });

    });

    function phonenumber(inputtxt) {
        if (inputtxt.length != 10) {
            return false
        } else {
            return true
        }
    }
    $('body').on('click', 'input[type=radio][name=method_payment]', function(e) {

        $(".image-radioP").removeClass('image-radio-checkedP');
        $(this).parent().addClass('image-radio-checkedP');
        // $('body').on('click', '.payment_method_cls', function (e) {
        // $('.confirm-message').html('Rs '+$(".total-amount").val()+' will collected from customer.');
        // var radioValue = $(this).data("id");
        var radioValue = $("input[name='method_payment']:checked").val();

        $(".payment_method").val(radioValue);

        $.ajax({
            url: base_url + '/billing/get-payment-form/' + radioValue,
            type: 'GET',
            data: { amount: $(".total-amount").val() },
            beforeSend: function() {
                //
            },
            success: function(data) {
                $('.confirm-message').html(data.html);
                if (data.pay_permission) {
                    $('.payment-proceed').html('<button type="button" class="btn btn-primary checkout btn-block br-0">Procced to Pay</button>');
                }
                $('.confirm-order').bPopup({
                    modalClose: false
                });
            },
            complete: function() {


            },
            error: function() {
                swal("Something went wrong .! Please try again.")
            }
        });
    });

    $('body').on('click', '.delivery-edit-btn', function() {

        $(".new-delivery-address").html('');
        $(".edit-delivery-address").html('');
        $(".show-delivery-address").hide();
        var id = $(this).data("id");
        $.ajax({
            type: 'GET',
            url: base_url + '/billing/get-shipping-address-form/' + id + '/' + $(".wellness-advisor-list-main").val(),
            beforeSend: function() {

            },
            success: function(result) {
                $(".edit-delivery-address").html(result);
            }

        });
    });

    $('body').on('click', '.delivery-cancel-btn', function() {

        $(".new-delivery-address").html('');
        $(".edit-delivery-address").html('');
        $(".show-delivery-address").show();

    });
    $(function() {
        $('body').on('click', '.delivery-add-btn', function() {
            $(".new-delivery-address").html('');
            $(".edit-delivery-address").html('');
            $(".show-delivery-address").hide();
            $.ajax({
                type: 'GET',
                url: base_url + '/billing/get-shipping-address-form/new/' + $(".wellness-advisor-list-main").val(),
                beforeSend: function() {
                    //
                },
                success: function(result) {
                    $(".new-delivery-address").html(result);
                }
            });
        });
    });

    $(function() {
        // $('#myform-edit-address').bind('submit', function () {
        $('body').on('submit', '#myform-new-address', function(e) {
            $(".payment-form").html('')
            var actionurl = base_url + '/billing/address-add';

            $.ajax({
                type: 'post',
                url: actionurl,
                data: $('#myform-new-address').serialize(),
                beforeSend: function() {
                    $("#submit-btn").prop("disabled", true);

                },
                success: function(result) {
                    $("#submit-btn").prop("disabled", false);
                    if (result.status == false) {

                        $.each($.parseJSON(result.errors), function(i, item) {
                            err = item;
                            field = $('[name="' + i + '"]');
                            field.next('span.error').html(item);
                            return;
                        });

                    } else if (result.status == true) {
                        $(".new-delivery-address").html('');
                        $(".edit-delivery-address").html('');
                        $(".address-form").html('');
                        fetchDeliveryAddress();
                    }
                },
                error: function() {
                    swal("Something went wrong ! Please try again");
                    $("#submit-btn").prop("disabled", false);
                }

            });
            return false;
        });
    });


    $('body').on('click', '.checkout', function(e) {
        if($(".error").text() == "") {
            $(".checkout").prop("disabled", true);
            if (!$(".payment_method").val()) {
                // if (!$("input[name='payment_method']:checked").val()) {
                swal('Choose Payment method.');
                return false;
            } else {
                var url = base_url + '/checkout';

                var form_address_id = $("#form_address_id").val();
                var discount_id = $("input[name='discount_id']:checked").val();
                if(discount_id == undefined){
                    discount_id = '';
                }
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: {
                        'cart': productarray,
                        'user_id': $(".wellness-advisor-list-input").val(),
                        'form_address_id': form_address_id,
                        'payment_type': $(".payment_method").val(),
                        // 'sample_type':$("input[name='sample_type']:checked").val(),
                        'barcode': $("#barcode_number").val(),
                        'id_who_collected_billing': $("#id_who_collected_billing").val(),
                        'name_who_collected_billing': $("#name_who_collected_billing").val(),
                        'amount_billing': $("#amount_billing").val(),
                        'cheque_number_billing': $("#cheque_number_billing").val(),
                        'number_billing': $("#number_billing").val(),
                        'date_billing': $("#date_billing").val(),

                        'discount_id': discount_id,     /*Discount ID*/
                        'card_tid': $("#card_tid").val(),   /*Card swipe*/

                        'id_type_who_collected_billing': $("#id_type_who_collected_billing").val(),
                        'cheque_date_billing': $("#cheque_date_billing").val(),
                        'cheque_bank_billing': $("#cheque_bank_billing").val()

                    },

                    beforeSend: function () {
                        $(".invoice").html("<p>Loading</p>");
                    },
                    success: function (data) {
                        $(".checkout").prop("disabled", false);
                        if (data.status == false) {
                            $(".error").html("");
                            $.each($.parseJSON(data.errors), function (i, item) {
                                err = item;
                                field = $('[name="' + i + '"]');
                                field.next('span.error').html(item);
                                return;
                            });

                        } else if (data.status == true) {
                            // $(".invoice").html(data);
                            if (data.pay == '3') {
                                var form = data.form;
                                $('body').append(form);
                                $("#admin-payment-form").submit();
                            } else {
                                window.location.href = base_url + "/invoice/" + data.invoice_id;
                            }
                        }
                    },

                    complete: function () {
                    },
                    error: function () {
                        swal("Something went wrong ! Please try again");
                        $(".checkout").prop("disabled", false);
                    }
                });
            }
        }
    });


});
$(document).ready(function() {
    var table = $('#vi-billings-table').DataTable({
        "aaSorting": [],
        'order': [
            [2, 'desc']
        ],
        "fnDrawCallback": function(oSettings) {
            if (oSettings.bSorted || oSettings.bFiltered || oSettings._iDisplayLength) {
                j = 0;
                for (var i = oSettings._iDisplayStart; i < oSettings.aiDisplay.length + oSettings._iDisplayStart; i++) {
                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[j]].nTr).find('span').text(i + 1);
                    j++;
                }
            }
        },
        serverSide: true,
        scrollX: true,
        oLanguage: {
            sProcessing: '<svg width="30px"  height="30px"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-rolling" style="background: transparent;"><circle cx="50" cy="50" fill="none" stroke="#0fb963" stroke-width="10" r="35" stroke-dasharray="164.93361431346415 56.97787143782138" transform="rotate(228 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform></circle></svg>'
        },
        processing: true,
        "pageLength": 10,
        "columnDefs": [{ 'orderable': false, 'targets': [0, 5, 5] }],
        mark: true,
        fixedColumns: {},
        ajax: {
            url: base_url + '/get-billings',
            data: function (d) {
                d.order_type =  $('#order_type').val();
                d.user_id =  $('#wellness-advisor-filter').val();
                d.order_id =  $('#order_id').val();
            }
        },
        columns: [
            { data: 'sl#', name: 'sl#' },
            { data: 'date', name: 'date' },
            { data: 'orderid', name: 'orderid' },
            { data: 'billed_by', name: 'billed_by' },
            { data: 'user_id', name: 'user_id' },
            { data: 'order_price', name: 'order_price' },
            { data: 'invoice', name: 'invoice' },
            { data: 'status', name: 'status' }
        ]
    });
    $('body').on('change', '#order_type', function (e) {
        if ($('#wellness-advisor-filter')){
            table.draw();
        }
    });
    $('body').on('change', '#wellness-advisor-filter', function (e) {
        if ($('#wellness-advisor-filter').val()) {
            table.draw();
        }
    });
    $('body').on('click', '#order_btn', function (e) {
        if ($('#order_id').val()){
            table.draw();
        }
    });

    $('body').on('click', '.resend-invoice-btn', function(e) {
        var id = $(this).data("id");
        var email = $(this).data("email");
        $(".resend-invoice-body").html('<label for="">Email Id</label><input name="emil_id" type="text" value="' + email + '" class="form-control br-0" id="resend-invoice' + id + '">' +
            '<span class="small text-danger" id="resend-invoice-span' + id + '"></span><br>' +
            '<span class="text-right"><button class="btn btn-primary resend-invoice m-t-10 btn-xs" data-id="' + id + '" type="button"><i class="fa fa-paper-plane"></i> Send</button></span>')

    });
    $('body').on('click', '.resend-invoice', function(e) {
        var id = $(this).data("id");
        var email_val = "#resend-invoice"+ id;
        var email_span = "#resend-invoice-span" + id;
        var email = $(email_val).val();
        var validate_email = '';
        if (email == "") {
            validate_email = "Please enter an email";
        } else if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
            validate_email = "Please enter a valid email";
        }
        if (validate_email){
            $(email_span).addClass('error');
            $(email_span).html(validate_email);
        } else {
            $.ajax({
                url: base_url + "/invoice-resend",
                type: 'POST',
                data: {email: email, order_id: id},
                beforeSend: function () {
                    $(email_span).removeClass('error');
                    $(email_span).html("Sending...");
                },
                success: function (data) {
                    if (data.status) {
                        $(email_span).html("Success");
                    } else {
                        $(email_span).addClass('error');
                        $(email_span).html("Please enter valid email");
                    }
                },
                complete: function () {
                    $(email_span).removeClass('error');
                    $(email_span).html("Success");
                    // $('#exampleModalScrollable').modal('hide');

                },
                error: function () {

                }

            });
        }

    });
    $('body').on('click', '.order-log-btn', function(e) {
        var id = $(this).data("id");
        $.ajax({
            url: base_url + "/billing/order-status-log",
            type: 'POST',
            data: {order_id: id },
            beforeSend: function() {
            },
            success: function(data) {
                $(".order-status-log-body").html(data);
            },
            complete: function() {
            },
            error: function() {
            }
        });
    });
   $('body').on('click', '.apply_discount', function(e) {
        var id = $(this).data("id");
        $.ajax({
            url: base_url + "/billing/get-discount-form",
            type: 'POST',
            data: {product_id: id },
            beforeSend: function() {
            },
            success: function(data) {
                $(".discount-form-body").html(data.html);
            },
            complete: function() {
            },
            error: function() {
                swal("Something went wrong.Please try again...!")
            }
        });
    });

    $('body').on('click', '.remove-discount', function(e) {
        $("input[name='discount_id']:checked").prop("checked",false);
        $(".discount-show").hide();
        $(".product-selected-mrp").css('text-decoration', 'none');
        $(".product-selected-discount-off-per").html('');
        $(".product-selected-discount").html('');
        $(".remove-discount").hide();
    });

    $('body').on('click', 'input[type=radio][name=discount_id]', function(e) {
        var discount_id = $("input[name='discount_id']:checked").val();
        var mrp = $(this).data("mrp");
        var discount_percentage = $(this).data("discount_percentage");
        $(".discount-show").show();
        $(".product-selected-mrp").css('text-decoration', 'line-through');
        $(".product-selected-discount-off-per").html(discount_percentage);
        $(".product-selected-discount").html(mrp);
        $(".remove-discount").show();
        $('#discount_modal').modal('hide');
    });
});