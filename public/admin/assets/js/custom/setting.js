
$(function(){

    $('#formula-symbol').keyup(function()
    {
        var yourInput = $(this).val();
        re = /[`~!@#$%^&_|\?;:'",<>\{\}\[\]]/gi;
        var isSplChar = re.test(yourInput);
        if(isSplChar)
        {
            var no_spl_char = yourInput.replace(/[`~!@#$%^&_|\?;:'",<>\{\}\[\]]/gi, '');
            $(this).val(no_spl_char);
        }
    });

});
/*Formula*/
$(document).ready(function(){
    $('#createSettingFormula').submit(function(evt) {
        evt.preventDefault();

        var formData = new FormData(this);
        var actionurl = base_url+'/setting/formula';
        $.ajax({
            type: 'POST',
            url: actionurl,
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            success: function (result) {
                if (result.status == false) {
                    $("#error").html("");
                    $("#submit-btn").prop("disabled",false);
                    $.each($.parseJSON(result.errors), function (i, item) {
                        err = item;
                        field = $('[name="' + i + '"]');
                        field.next('span.error').html(item);
                        return;
                    });

                }else if(result.status == true){
                    $( "#formula-modal").modal("hide");

                    var notify = $.notify('<strong>Saving</strong> Do not close this page...', {
                        allow_dismiss: false,
                        showProgressbar: true
                    });

                    setTimeout(function() {
                        notify.update({'type': 'success', 'message': '<strong>Success</strong> Your page has been saved!', 'progress': 25});
                        $('#createSettingFormula').trigger("reset");
                        $("#error").html("");

                        $("#formula-tbody").append('<tr id="tr-formula'+result.data['id']+'">\n' +
                            '                                <td>'+result.data['formula_name']+'</td>\n' +
                            '                                <td>'+result.data['formula']+'</td>\n' +
                            '                                <td>\n' +
                            '                                    <i class="fa fa-edit edit-setting-lookup" data-toggle="modal" data-target="#lookup-modal-edit" data-id="'+result.data['id']+'"></i>&nbsp;&nbsp;\n' +
                            '                                    <i class="fa fa-trash delete-setting-lookup" data-id="'+result.data['id']+'"></i>\n' +
                            '                                </td>\n' +
                            '                            </tr>')
                    }, 4500);



                }
            }
        });
    });
    $('#editSettingFormula').submit(function(evt) {
        evt.preventDefault();

        var formData = new FormData(this);
        var actionurl = base_url+'/setting/formula/edit/'+$('#user_id').val();
        $.ajax({
            type: 'POST',
            url: actionurl,
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            success: function (result) {
                if (result.status == false) {
                    $("#error").html("");
                    $("#submit-btn").prop("disabled",false);
                    $.each($.parseJSON(result.errors), function (i, item) {
                        err = item;
                        field = $('[name="' + i + '"]');
                        field.next('span.error').html(item);
                        return;
                    });

                }else if(result.status == true){
                    var notify = $.notify('<strong>Saving</strong> Do not close this page...', {
                        allow_dismiss: false,
                        showProgressbar: true
                    });

                    setTimeout(function() {
                        notify.update({'type': 'success', 'message': '<strong>Success</strong> Your page has been saved!', 'progress': 25});
                        $("#error").html("");
                    }, 4500);

                }
            }
        });
    });
    $(document).on('click', '.delete-setting-formula', function (e) {

        e.preventDefault();
        var id = $(this).data("id") ;
        var url = base_url+'/formula/delete';


        swal({
            title: "Are you sure ?",
            text: "Once deleted, it will be removed from Formula Values!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: url,
                        type: 'POST',  // user.destroy
                        data:{
                            'id':id,
                        },
                        beforeSend: function(){
                            $("#if_loading").html("loading");
                        },
                        success: function(result) {
                            var divId = '#tr-formula'+ id;
                            $(divId).remove();

                            // Do something with the result
                        },
                        complete: function(){
                            $('#if_loading').html("");
                        }
                    });

                } else {
                    //
                }
            });

    });
    $(document).on('click', '.symbol-key', function (e){
        e.preventDefault();
        var id = $(this).data("id") ;
        var formula = $("#formula-symbol").val();
        $("#formula-symbol").val(formula+id);

    });


});

$(document).ready(function(){
    /*Lookup Values*/
    $('#createSettingLookup').submit(function(evt) {
        evt.preventDefault();
        var formData = new FormData(this);
        var actionurl = base_url+'/setting/lookup';
        $.ajax({
            type: 'POST',
            url: actionurl,
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            success: function (result) {
                if (result.status == false) {
                    $("#error").html("");
                    $("#submit-btn").prop("disabled",false);
                    $.each($.parseJSON(result.errors), function (i, item) {
                        err = item;
                        field = $('[name="' + i + '"]');
                        field.next('span.error').html(item);
                        return;
                    });

                }else if(result.status == true){
                    $( "#lookup-modal").modal("hide");
                    var notify = $.notify('<strong>Saving</strong> Do not close this page...', {
                        allow_dismiss: false,
                        showProgressbar: true
                    });

                    setTimeout(function() {
                        notify.update({'type': 'success', 'message': '<strong>Success</strong> Your page has been saved!', 'progress': 25});
                        $('#createSettingLookup').trigger("reset");
                        $("#error").html("");
                        $("#lookup-tbody").append('<tr id="tr-lookup'+result.data['id']+'">\n' +
                            '                                <td>'+result.data['key_name']+'</td>\n' +
                            '                                <td>'+result.data['value']+'</td>\n' +
                            '                                <td>\n' +
                            '                                    <i class="fa fa-edit edit-setting-lookup" data-toggle="modal" data-target="#lookup-modal-edit" data-id="'+result.data['id']+'"></i>&nbsp;&nbsp;\n' +
                            '                                    <i class="fa fa-trash delete-setting-lookup" data-id="'+result.data['id']+'"></i>\n' +
                            '                                </td>\n' +
                            '                            </tr>')


                    }, 4500);
                }
            }
        });
    });

    $(document).on('click', '.edit-setting-lookup', function (e) {

        e.preventDefault();
        var id = $(this).data("id") ;
        var url = base_url+'/get-lookup/edit';
        $.ajax({
            url: url,
            type: 'post',  // user.destroy
            data:{
                'id':id,
            },
            beforeSend: function(){
                $("#if_loading").html("loading");
            },
            success: function(result) {
                $( "#lookup-value-edit-form").html(result);
            },
            complete: function(){
                $('#if_loading').html("");
            }
        });
    });

    $(document).on('click', '.delete-setting-lookup', function (e) {

        e.preventDefault();
        var id = $(this).data("id") ;
        var url = base_url+'/lookup/delete';


        swal({
            title: "Are you sure ?",
            text: "Once deleted, it will be removed from Lookup Values!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: url,
                        type: 'POST',  // user.destroy
                        data:{
                            'id':id,
                        },
                        beforeSend: function(){
                            $("#if_loading").html("loading");
                        },
                        success: function(result) {
                            var divId = '#tr-lookup'+ id;
                            $(divId).remove();

                            // Do something with the result
                        },
                        complete: function(){
                            $('#if_loading').html("");
                        }
                    });

                } else {
                    //
                }
            });

    });

});
$(document).on('click', '.vi-edit-lookup', function (e) {
    var actionurl = base_url+'/setting/lookup/edit/'+$('#lookup_id-edit').val();
    $.ajax({
        type: 'POST',
        url: actionurl,
        data:$("#editSettingLookup").serialize(),

        success: function (result) {
            if (result.status == false) {
                $("#error").html("");
                $("#submit-btn").prop("disabled",false);
                $.each($.parseJSON(result.errors), function (i, item) {
                    err = item;
                    field = $('[name="' + i + '"]');
                    field.next('span.error').html(item);
                    return;
                });

            }else if(result.status == true){
                $( "#lookup-modal-edit").modal("hide");
                var notify = $.notify('<strong>Saving</strong> Do not close this page...', {
                    allow_dismiss: false,
                    showProgressbar: true
                });

                setTimeout(function() {
                    notify.update({'type': 'success', 'message': '<strong>Success</strong> Your page has been saved!', 'progress': 25});
                    $("#error").html("");
                    var keyname = "#tr-lookup-keyname"+$('#lookup_id-edit').val();
                    var value = "#tr-lookup-value"+$('#lookup_id-edit').val();

                    $(keyname).html(result.data['key_name']);
                    $(value).html(result.data['value']);
                }, 4500);
            }
        }
    });
});

/*Formula generation*/
"use strict";

var input = document.getElementById('input'), // input/output button
    number = document.querySelectorAll('.numbers div'), // number buttons
    operator = document.querySelectorAll('.operators div'), // operator buttons
    result = document.getElementById('result'), // equal button
    clear = document.getElementById('clear'), // clear button
    resultDisplayed = false; // flag to keep an eye on what output is displayed

// adding click handlers to number buttons
for (var i = 0; i < number.length; i++) {
    number[i].addEventListener("click", function(e) {

        // storing current input string and its last character in variables - used later
        var currentString = input.innerHTML;
        var lastChar = currentString[currentString.length - 1];

        // if result is not diplayed, just keep adding
        if (resultDisplayed === false) {
            input.innerHTML += e.target.innerHTML;
        } else if (resultDisplayed === true && lastChar === "+" || lastChar === "-" || lastChar === "×" || lastChar === "÷") {
            // if result is currently displayed and user pressed an operator
            // we need to keep on adding to the string for next operation
            resultDisplayed = false;
            input.innerHTML += e.target.innerHTML;
        } else {
            // if result is currently displayed and user pressed a number
            // we need clear the input string and add the new input to start the new opration
            resultDisplayed = false;
            input.innerHTML = "";
            input.innerHTML += e.target.innerHTML;
        }

    });
}

// adding click handlers to number buttons
for (var i = 0; i < operator.length; i++) {
    operator[i].addEventListener("click", function(e) {

        // storing current input string and its last character in variables - used later
        var currentString = input.innerHTML;
        var lastChar = currentString[currentString.length - 1];

        // if last character entered is an operator, replace it with the currently pressed one
        if (lastChar === "+" || lastChar === "-" || lastChar === "×" || lastChar === "÷") {
            var newString = currentString.substring(0, currentString.length - 1) + e.target.innerHTML;
            input.innerHTML = newString;
        } else if (currentString.length == 0) {
            // if first key pressed is an opearator, don't do anything
            console.log("enter a number first");
        } else {
            // else just add the operator pressed to the input
            input.innerHTML += e.target.innerHTML;
        }

    });
}

// on click of 'equal' button
result.addEventListener("click", function() {

    // this is the string that we will be processing eg. -10+26+33-56*34/23
    var inputString = input.innerHTML;

    // forming an array of numbers. eg for above string it will be: numbers = ["10", "26", "33", "56", "34", "23"]
    var numbers = inputString.split(/\+|\-|\×|\÷/g);

    // forming an array of operators. for above string it will be: operators = ["+", "+", "-", "*", "/"]
    // first we replace all the numbers and dot with empty string and then split
    var operators = inputString.replace(/[0-9]|\./g, "").split("");

    console.log(inputString);
    console.log(operators);
    console.log(numbers);
    console.log("----------------------------");

    // now we are looping through the array and doing one operation at a time.
    // first divide, then multiply, then subtraction and then addition
    // as we move we are alterning the original numbers and operators array
    // the final element remaining in the array will be the output

    var divide = operators.indexOf("÷");
    while (divide != -1) {
        numbers.splice(divide, 2, numbers[divide] / numbers[divide + 1]);
        operators.splice(divide, 1);
        divide = operators.indexOf("÷");
    }

    var multiply = operators.indexOf("×");
    while (multiply != -1) {
        numbers.splice(multiply, 2, numbers[multiply] * numbers[multiply + 1]);
        operators.splice(multiply, 1);
        multiply = operators.indexOf("×");
    }

    var subtract = operators.indexOf("-");
    while (subtract != -1) {
        numbers.splice(subtract, 2, numbers[subtract] - numbers[subtract + 1]);
        operators.splice(subtract, 1);
        subtract = operators.indexOf("-");
    }

    var add = operators.indexOf("+");
    while (add != -1) {
        // using parseFloat is necessary, otherwise it will result in string concatenation :)
        numbers.splice(add, 2, parseFloat(numbers[add]) + parseFloat(numbers[add + 1]));
        operators.splice(add, 1);
        add = operators.indexOf("+");
    }

    input.innerHTML = numbers[0]; // displaying the output

    resultDisplayed = true; // turning flag if result is displayed
});

// clearing the input on press of clear
clear.addEventListener("click", function() {
    input.innerHTML = "";
})