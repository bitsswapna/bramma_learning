
/*
Create an accordion and set it's event attribute to False
 */
$("#accordion").accordion({ event: false, active: false, heightStyle: "content" });

/*
Find out how many sections are contained within your accordion.
The sections are identified by the default div HTML Tag.
 */
var noSections = $("#accordion div").length - 1;

/*
Cycle through each div and when clicked, a number of different scenarios may occur.

1. If you are not on the last blade currently and you click the header of the currently open blade then this current blade will close and the next immediate blade will be made active.

2. If you are on the last blade currently and you reclick the header of the final blade, it will close and the blade immediately above it will be made active.

3. If you click on any closed blade header then this blade will become active.
 */
$(".accordionBilling").each(function (index, element) {
    $(element).click(function () {
        if ($(this).hasClass('ui-state-active')) {
            if (index < noSections)
                $("#accordion").accordion('option', 'active', index + 1);
            else
                $("#accordion").accordion('option', 'active', index - 1);
        }
        else {
            $("#accordion").accordion('option', 'active', index);
        }
    });
});


/* accordion for tab 2 */
/*
Create an accordion and set it's event attribute to False
 */
$("#accordionq").accordion({ event: false, active: false, heightStyle: "content" });

/*
Find out how many sections are contained within your accordion.
The sections are identified by the default div HTML Tag.
 */
var noSections = $("#accordionq div").length - 1;

/*
Cycle through each div and when clicked, a number of different scenarios may occur.

1. If you are not on the last blade currently and you click the header of the currently open blade then this current blade will close and the next immediate blade will be made active.

2. If you are on the last blade currently and you reclick the header of the final blade, it will close and the blade immediately above it will be made active.

3. If you click on any closed blade header then this blade will become active.
 */
$(".accordionBillingq").each(function (index, element) {
    $(element).click(function () {
        if ($(this).hasClass('ui-state-active')) {
            if (index < noSections)
                $("#accordionq").accordion('option', 'active', index + 1);
            else
                $("#accordionq").accordion('option', 'active', index - 1);
        }
        else {
            $("#accordionq").accordion('option', 'active', index);
        }
    });
});




$(document).ready(function () {

    // add/remove checked class
    $(".image-radio").each(function () {
        if ($(this).find('input[type="radio"]').first().attr("checked")) {
            $(this).addClass('image-radio-checked');
        } else {
            $(this).removeClass('image-radio-checked');
        }
    });

    // sync the input state
    $(".image-radio").on("click", function (e) {
        $(".image-radio").removeClass('image-radio-checked');
        $(this).addClass('image-radio-checked');
        var $radio = $(this).find('input[type="radio"]');
        $radio.prop("checked", !$radio.prop("checked"));

        e.preventDefault();
    });
});



jQuery(document).ready(function ($) {
    //variables
    var $window = $(window);
    var $container = $("#container");
    var $main = $("#main");
    var window_min = 0;
    var window_max = 0;
    var threshold_offset = 50;
    /*
     set the container's maximum and minimum limits as well as movement thresholds
    */
    function set_limits() {
        //max and min container movements
        var max_move = $main.offset().top + $main.height() - $container.height() - 2 * parseInt($container.css("top"));
        var min_move = $main.offset().top;
        //save them
        $container.attr("data-min", min_move).attr("data-max", max_move);
        //window thresholds so the movement isn't called when its not needed!
        //you may wish to adjust the freshhold offset
        window_min = min_move - threshold_offset;
        window_max = max_move + $container.height() + threshold_offset;
    }
    //sets the limits for the first load
    set_limits();

    function window_scroll() {
        //if the window is within the threshold, begin movements
        if ($window.scrollTop() >= window_min && $window.scrollTop() < window_max) {
            //reset the limits (optional)
            set_limits();
            //move the container
            container_move();
        }
    }
    $window.bind("scroll", window_scroll);

    /**
     * Handles moving the container if needed.
     **/
    function container_move() {
        var wst = $window.scrollTop();
        //if the window scroll is within the min and max (the container will be "sticky";
        if (wst >= $container.attr("data-min") && wst <= $container.attr("data-max")) {
            //work out the margin offset
            var margin_top = $window.scrollTop() - $container.attr("data-min");
            //margin it down!
            $container.css("margin-top", margin_top);
            //if the window scroll is below the minimum
        } else if (wst <= $container.attr("data-min")) {
            //fix the container to the top.
            $container.css("margin-top", 0);
            //if the window scroll is above the maximum
        } else if (wst > $container.attr("data-max")) {
            //fix the container to the top
            $container.css("margin-top", $container.attr("data-max") - $container.attr("data-min") + "px");
        }
    }
    //do one container move on load
    container_move();
});

// $('#wellness-advisor-list-main').select2({
//     placeholder: "Search...",
//     minimumInputLength: 1,
//
//     ajax: {
//         url:base_url+'/billing/getselectedusers',
//         dataType: 'json',
//         data: function (params) {
//             return {
//                 q: $.trim(params.term)
//             };
//         },
//         processResults: function (data) {
//             return {
//                 results: data
//             };
//         },
//         cache: true
//     }
//
// });



// $('input[type=radio][name=user_type]').change(function() {
//     var user_role = this.value;
//     $.ajax({
//         type: 'post',
//         url: base_url+"/billing/get-user-form",
//         data: {
//             role:user_role,
//         },
//         beforeSend: function () {
//             $("#submit-btn").prop("disabled",true);
//
//         },
//         success: function (result) {
//             $("#form-user-customer").html(result)
//         },complete:function () {
//             $(function () {
//                 $('#wellness-advisor-list-main').selectize({
//                     valueField: 'id',
//                     labelField: 'text',
//                     load: function (query, callback) {
//                         $.ajax({
//                             url: base_url+'/billing/getselectedusers?q=' + encodeURIComponent(query),
//                             success: function (response) {
//                                 console.log(response);
//                                 // $select.options = response;
//                                 $('#wellness-advisor-list-main').selectize({
//                                     options : response
//                                 });
//                                 callback(response);
//                             }
//                         });
//                     }
//                 });
//
//                 // var selectize = $select[0].selectize;
//
//             });
//         }
//     });
//
// });

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#imagePreview').css('background-image', 'url(' + e.target.result + ')');
            $('#imagePreview').hide();
            $('#imagePreview').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#imageUpload").change(function () {
    readURL(this);
});
