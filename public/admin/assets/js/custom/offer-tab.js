var productList = [];
var cart={};
var $select;
var selectize_options={
    valueField: 'id',
    maxItems: 1,
    placeholder:"Enter Name / Id",
    labelField: 'text',
    load: function(query, callback) {
        $.ajax({
            url: base_url + '/billing/getselectedusers?q=' + encodeURIComponent(query),
            success: function(response) {
                $('#wellness-advisor-list-main').selectize({
                    options: response
                });
                callback(response);
            }
        });
    }
};
// $('body').on('change', '.product_list_ckeck', function(e) {
//     var product_id = $(this).val();
//
//     if ($(this).is(":checked")) {
//         productList.push(product_id);
//     } else {
//         productList= productList.filter(function(a){return a!=product_id})
//     }
// });
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
$("body").on("click","#cart-add",function (e) {
    var val=$(e.target).parent("div").find("input").val();
    val++;
    $(e.target).parent("div").find("input").val(val);

})
$("body").on("click","#cart-remove",function (e) {
    var val=$(e.target).parent("div").find("input").val();
    val--;
    $(e.target).parent("div").find("input").val(val<=0?0:val);

})
$('body').on('click', '.product-list-proceed', function(e) {
    productList=[];
    productList=$("input[name='product[]']")
        .map(function(){return {["product_id"]:$(this).data("id"),["qty"]:$(this).val()}}).get();

    var singleone=productList.find(function (data) {
        return data.qty >0
    })
    if (singleone) {
        $(".offer-list-tab").trigger('click');

        $.ajax({
            url: base_url + '/billing/get-offer-list',
            type: 'post',
            data:{
                cart:productList
            },
            beforeSend: function () {

                $(".offer-list").html('<div style="margin-right: 50%; margin-left: 50%;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>\n' +
                    '<span class="sr-only">Loading...</span></div>');
            },
            success: function (result) {
                $(".offer-list").html(result.html);
                $(".offer-product-price-details").html(result.priceHtml);
                $(".offer-test-product-image").hide()
            },
            error: function () {

            }
        });
    }else{
        swal("Please choose atleast one product")
    }
});
$('body').on('click', '.apply-offer', function(e) {
    var offer_id = $(this).data("id");
    var data=[];
    var datalist=productList;
    productList.forEach(e=>{if(e.qty>0)data.push.apply(data,new Array(parseInt(e.qty)).fill({product_id:e.product_id,offer_id:offer_id}))})
    productList=data;

    $(".offer-user-tab").trigger('click');
    $.ajax({
        url: base_url+'/billing/get-offer-product-form',
        type: 'post',
        data:{
            products:productList.map(function (data) {
                return data.product_id;
            }),
            productList:datalist,
            productListOffer:productList,
            offer_id:productList[0]["offer_id"]
        },
        beforeSend: function() {

            $(".offer-product").html('<div style="margin-right: 50%; margin-left: 50%;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>\n' +
                '<span class="sr-only">Loading...</span></div>');
        },
        success: function(result) {
            $(".offer-product").html(result.html)
            $(".offer-price-details").show();
            $(".offer-price-details").html(result.priceHtml)
            var data_index=$('[data-index=""]');
            productList.forEach((data,index)=>{
                $(data_index[index]).data("index",index)
        })

        },
        complete:function(){
            $(function() {

                $select=  $('.wellness-advisor-list-offer').selectize(selectize_options);
               $select.map(function (e) {
                   var selectize = $select[e].selectize;
                   selectize.on("blur",function () {
                       var obj = $(this)[0];
                       if(selectize.getValue()=="")
                       {
                           $( obj.$input["0"]).parent().find("span.error").text("Please select the user");
                       }
                       else
                       {
                           $( obj.$input["0"]).parent().find("span.error").text("");
                       }
                   })
                   selectize.on("focus",function () {
                       var obj = $(this)[0];
                       $( obj.$input["0"]).parent().find("span.error").text("");
                   });
               })

                // selectize.onBlur(function () {
                //
                // })

            });
        },
        error:function(){

        }
    });
});
$('body').on("click",'.radio-toolbar input[type="radio"]',function (e) {
    var address_id=$(e.target).val();
    $.ajax({
        url: base_url+'/billing/checkaddress',
        type: 'post',
        data:{
            address_id:address_id,
            productListOffer:productList,
            offer_id:productList[0]['offer_id']
        },
        beforeSend: function() {

            $(".offer-payment-form").html('<div style="margin-right: 50%; margin-left: 50%;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>\n' +
                '<span class="sr-only">Loading...</span></div>');
        },
        success: function(result) {
            $(".offer-payment-form").html('');
            if(result.status)
            {
                $(".offer-price-details").html(result.priceHtml);
                $(e.target).parent().parent().parent().parent().find("li").css("display","none")
                $(e.target).parent("div").find("li").css("display","inline");
                var index=$(e.target).parent().parent().parent().parent().parent().parent().find("select").data('index');
                var address_id=$(e.target).val();
                var offer_id=productList[0].offer_id;
                productList.forEach(function(v,i){ delete productList[i].offer_id });
                cart={...{cart:productList},...{address_id:address_id,offer_id:offer_id}};
                $(".offer-payment-form-tab").trigger("click");
                $.ajax({
                    url: base_url+'/billing/payment-offer',
                    type: 'post',
                    data:{
                    },
                    beforeSend: function() {

                        $(".offer-payment-form").html('<div style="margin-right: 50%; margin-left: 50%;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>\n' +
                            '<span class="sr-only">Loading...</span></div>');
                    },
                    success: function(result) {
                        $(".offer-payment-form").html(result.html)
                    },
                    complete:function(){

                    },
                    error:function(){

                    }
                });
            }
            else
            {
                $.notify({message:result.message},{type: 'danger'});

            }
        },
        complete:function(){

        },
        error:function(){

        }
    });



})
$("body").on("click",".payment_method_cls",function (e) {
    $(".image-radioP").removeClass('image-radio-checkedP');
    $(this).parent().addClass('image-radio-checkedP');
    // $('body').on('click', '.payment_method_cls', function (e) {
    // $('.confirm-message').html('Rs '+$(".total-amount").val()+' will collected from customer.');
    // var radioValue = $(this).data("id");
    var radioValue = $(e.target).data('id');
    cart={...cart,...{payment_type:radioValue}};
    $(".payment_method").val(radioValue);

    $.ajax({
        url: base_url + '/billing/get-payment-form-offer/' + radioValue,
        type: 'GET',
        data: { amount: $(".total-amount").val() },
        beforeSend: function() {
            //
        },
        success: function(data) {
            $('.confirm-message').html(data.html);
            if (data.pay_permission) {
                $('.payment-proceed').html('<button type="button" class="btn btn-primary offer-checkout btn-block br-0"> <i class="fa fa-spinner fa-spin loading-btn"></i> Procced to Pay</span></button>');
            }
            $('.confirm-order').bPopup({
                modalClose: false
            });
        },
        complete: function() {


        },
        error: function() {
            swal("Something went wrong .! Please try again.")
        }
    });
})

$("body").on("click",".image-radio",function (e) {
    if($(".active_add").length!=0)
    {
        $(".save-wa-form").addClass("save-wa-form-offer");
        $(".save-wa-form").removeClass("save-wa-form");
    }
})
//Add user
$("body").on("click",".btn-user-add-btn",function (e) {
    $(e.target).addClass("active_add");
    // $("#exampleModalScrollable").modal("show");
    $(".image-radio").click()
    $(".image-radio")[0].click()
    $('[href="#user-type"]').trigger("click");
    $(".save-wa-form").addClass("save-wa-form-offer");
    $(".save-wa-form").removeClass("save-wa-form");

})
$("body").on("click",".save-wa-form-offer",function (e) {
    e.preventDefault();
    var actionurl = base_url + '/admin_user/create';
    var formdata = new FormData($("#createAdminUser").get(0))
    formdata.append("role_id", $(".image-radio-checked").children("input").val())
    $(".save-wa-form-offer").prop("disabled", true);
    $.ajax({
        type: 'POST',
        url: actionurl,
        contentType: false,
        processData: false,
        data: formdata,
        // data:$("#createAdminUser").serialize(),
        success: function(result) {
            // console.log(result)

            if (result.permission == true) {
                if (result.status == false) {
                    $(".error").html("");
                    $(".save-wa-form-offer").prop("disabled", false);
                    $.each($.parseJSON(result.errors), function(i, item) {
                        err = item;
                        if (i == "parent_id") {
                            field = $('[name="' + i + '"]').parent("div").parent("div");
                            field.children('span.error').html(item);
                            return;
                        } else {
                            field = $('[name="' + i + '"]');
                            field.next('span.error').html(item);
                            return;
                        }

                    });
                    //console.log($("li[data-number=" + $("span.error:not(:empty)").first().data("number") + "]").children("a"));
                    $("li[data-number=" + $("span.error:not(:empty)").first().data("number") + "]").children("a").trigger("click")
                } else if (result.status == true) {
                    var notify = $.notify('<strong>Saving</strong> Do not close this page...', {
                        allow_dismiss: false,
                        showProgressbar: true,
                        z_index: 15000,
                    });

                    // setTimeout(function () {
                    $('#exampleModalScrollable').modal('hide');
                    $('#createAdminUser').trigger("reset");

                    // notify.update({ 'type': 'success', 'message': '<strong>Success</strong> Your page has been saved!', 'progress': 25 });
                    // $('#prof_img').find('img').attr('src', no_image_url);
                    $(".error").html("");
                    var waid = result.user_id;
                    var name=result.name;
                    console.log(waid);
                    console.log(e);
                    if($(".active_add").prop("tagName")=="I")
                    {
                        var $select=$(".active_add").parent().parent().find("select.wellness-advisor-list-offer").selectize()

                    }
                    else
                    {
                        var $select=$(".active_add").parent().find("select.wellness-advisor-list-offer").selectize()

                    }
                    var selectize = $select[0].selectize;
                    selectize.addOption({id:waid,text:name+"--("+waid+")"})
                    selectize.setValue(waid);
                    $(".active_add").removeClass("active_add");
                    $(".save-wa-form-offer").prop("disabled", false);
                    // $(".wellness-advisor-list-input").val(waid)
                    // $(".wellness-advisor-list-main").val(waid)
                    // $(".loader").show();
                    // $(".varification-wa").html('');
                    // $(".address-form").html('');
                    // $(".payment-form").html('');
                    // var url = base_url + '/get-wellness-advisor-detail-tab/' + waid;
                    // $.ajax({
                    //     url: url,
                    //     type: 'GET',
                    //     beforeSend: function() {},
                    //     success: function(data) {
                    //        // $(".varification-wa").html(data);
                    //
                    //     },
                    //     complete: function() {
                    //         $(".loader").hide();
                    //         $(".profile-tab").trigger("click");
                    //     }
                    // });
                    // }, 4500);
                }

            } else {
                swal("You have no permission")
                $(".save-wa-form-offer").prop("disabled", false);
            }




        },
        error: function() {
            swal("Something went wrong ! Please try again");
            $(".save-wa-form").prop("disabled", false);
        }
    });
})
$("body").on("click",".offer-checkout", function (e) {

    var form=document.getElementById('confirm-message-form');
    var formdata=new FormData(form);
    var formdata_object={};
    formdata.forEach(function(value, key){
        formdata_object[key] = value;
    });
    cart={...cart,...formdata_object}
    cart.cart=productList;
    $(".offer-checkout").prop("disabled",true);
    $(".loading-btn").css("display","inline");
    $.ajax({
        url: base_url+"/billing/checkout-offer",
        type: 'POST',
        data: cart,

        beforeSend: function () {

        },
        success: function (data) {
            $(".offer-checkout").prop("disabled",false);
            $(".loading-btn").css("display","none");
            if ((data.error_custom).length>0){
                $.notify({message:data.error_custom},{type: 'danger'});
            }
            if (data.status == false) {
                $(".error").html("");

                $.each($.parseJSON(data.errors), function (i, item) {
                    err = item;
                    field = $('[name="' + i + '"]');
                    field.next('span.error').html(item);
                    return;
                });

            } else if (data.status == true) {
                if (data.pay == '3') {
                    var form = data.form;
                    $('body').append(form);
                    $("#admin-payment-form").submit();
                } else {
                    window.location.href = base_url + "/invoice/offer/" + data.invoice_id;
                }
            }
        },
        complete: function () {
        },
        error: function () {

            swal("Something went wrong ! Please try again");
            $(".offer-checkout").prop("disabled",false);
            $(".loading-btn").css("display","none");
        }
    });
})
var offerProductList = {};
$('body').on('change', '.wellness-advisor-list-offer', function(e) {
    var uid = this.value;
    var product_id = $(this).data("id");
    var productarray_ = {};

    productarray_['product_id'] = product_id;
    productarray_['uid'] = uid;
    productList[$(this).data("index")]={...productList[$(this).data("index")],...{user_id:uid}}
    offerProductList[product_id] = productarray_;
    var user_view = '#user-profile--card'+product_id;
    $.ajax({
        url: base_url+'/billing/get-user-details',
        type: 'post',
        data:{
            uid:uid,
            product_id:product_id,
        },
        beforeSend: function() {
        },
        success: function(result) {
            $(e.target).parent().parent().find(".user-profile-offer-card").html(result.html)

        },
        complete:function(){

        },
        error:function(){

        }
    });



});
$('body').on('click', '.delivery-edit-offer-btn', function() {
    var pid = $(this).data("product");
    $(".new-delivery-address"+pid).html('');
    $(".edit-delivery-address"+pid).html('');
    $(".show-delivery-address"+pid).hide();
    var id = $(this).data("id");
    var uid = $(this).data("content");

    $.ajax({
        type: 'GET',
        url: base_url + '/billing/offer/get-shipping-address-form/' + id + '/' + uid+'/' + pid,
        beforeSend: function() {

        },
        success: function(result) {
            var lc = ".edit-delivery-address"+pid;
            $(lc).html(result);
        }

    });
});

$('body').on('click', '.delivery-cancel-btn-offer', function() {
    var id = $(this).data("id");

    $(".new-delivery-address").html('');
    $(".edit-delivery-address").html('');
    $(".show-delivery-address").show();

});
function fetchDeliveryAddressOffer(uid, product_id) {

    $.ajax({
        url: base_url+'/billing/get-offer-address-form',
        type: 'post',
        data:{
            uid:uid
        },
        beforeSend: function() {
        },
        success: function(result) {

            $(".offer-address").html(result.html)
        },
        complete:function(){

        },
        error:function(){

        }
    });

}
$("body").on("focus",".barcode",function (e) {
    $(e.target).parent("div").find("span.error").text("");
})
$("body").on("blur",".barcode",function (e) {

    var current_data=$(e.target).val();
    if(current_data=="")
    {
        $(e.target).parent("div").find("span.error").text("Barcode is required")
    }
    else
    {
        var val=$(".barcode").map(function (e){
            if($(this).val()!="")
            {
                return $(this).val();
            }
        }).get()
        var flag=false;
        val.forEach(function(first,first_index){
            val.forEach(function (second,second_index) {
                if(first==second && first_index!=second_index)
                    flag=true;
            })
        })
        if(flag)
        {
            $(e.target).parent("div").find("span.error").text("Barcode should be uniqe")
        }else
        {
            $.ajax({
                url: base_url+'/billing/validate-barcode',
                type: 'post',
                data:{
                    barcode:current_data
                },
                beforeSend: function() {

                    $(".offer-address").html('<div style="margin-right: 50%; margin-left: 50%;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>\n' +
                        '<span class="sr-only">Loading...</span></div>');
                },
                success: function(result) {
                    if(result.status)
                    {
                        var index=$(e.target).parent().parent().parent().find("select").data("index")
                        productList[index]={...productList[index],...{barcode:current_data}};
                    }
                    else
                    {
                        $(e.target).parent("div").find("span.error").text(result.message);

                    }
                },
                complete:function(){

                },
                error:function(){

                }
            });
        }
    }
})
$('body').on('click','.user-detials-procced',function (e) {

    $(".barcode").trigger("blur");
   $select.map(function (e) {
       var selectize=$select[e].selectize;
       selectize.trigger("blur");
   })
    if(($(".barcode").parent("div").find("span.error").text()!="")||($(".wellness-advisor-list-offer").parent().find("span.error").text()!=""))
    {

        return
    }
    $(".offer-address-tab").trigger('click');
    $.ajax({
        url: base_url+'/billing/get-offer-address-form',
        type: 'post',
        data:{
            uid:productList[0].user_id
        },
        beforeSend: function() {

            $(".offer-address   ").html('<div style="margin-right: 50%; margin-left: 50%;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>\n' +
                '<span class="sr-only">Loading...</span></div>');
        },
        success: function(result) {
            $(".offer-address").html(result.html)
        },
        complete:function(){

        },
        error:function(){

        }
    });

})

$('body').on('click', '.btn-save--delivary', function(e) {
    var actionurl = base_url + '/billing/address-edit';

    var uid = $(this).data("content");

    $.ajax({
        type: 'post',
        url: actionurl,
        data: $('#myform-edit-address-offer'+id).serialize(),
        beforeSend: function() {
            $(".btn-save--delivary").prop("disabled", true);

        },
        success: function(result) {
            if (result.status == false) {
                $(".btn-save--delivary").prop("disabled", false);
                $.each($.parseJSON(result.errors), function(i, item) {
                    err = item;
                    field = $('[name="' + i + '"]');
                    field.next('span.error').html(item);
                    return;
                });

            } else if (result.status == true) {
                $(".new-delivery-address"+id).html('');
                $(".edit-delivery-address"+id).html('');
                $(".address-form"+id).html('');
                fetchDeliveryAddressOffer(uid, productList[0].product_id);
            }
        },
        error: function() {
            swal("Something went wrong ! Please try again");
            $("#submit-btn").prop("disabled", false);
        }

    });
    return false;
});
$('body').on('click', '.delivery-add-offer-form', function() {
    var pid = $(this).data("id");
    var uid = productList[0].user_id;
    $(".new-delivery-address"+pid).html('');
    $(".edit-delivery-address"+pid).html('');
    $(".show-delivery-address"+pid).hide();
    $.ajax({
        type: 'GET',
        url: base_url + '/billing/offer/get-shipping-address-form/'+ uid ,
        beforeSend: function() {
            //
        },
        success: function(result) {
            $(".new-delivery-address").html(result);
        }
    });
});

$('body').on('click', '.btn-save-new-delivery-offer', function(e) {

    var uid = $(this).data("content");
    $(".payment-form").html('');
    var actionurl = base_url + '/billing/address-add';

    $.ajax({
        type: 'post',
        url: actionurl,
        data: $('#myform-new-address').serialize(),
        beforeSend: function() {
            $(".btn-save-new-delivery-offer").prop("disabled", true);

        },
        success: function(result) {
            $(".btn-save-new-delivery-offer").prop("disabled", false);
            if (result.status == false) {

                $.each($.parseJSON(result.errors), function(i, item) {
                    err = item;
                    field = $('[name="' + i + '"]');
                    field.next('span.error').html(item);
                    return;
                });

            } else if (result.status == true) {
                $(".new-delivery-address").html('');
                $(".edit-delivery-address").html('');
                $(".address-form").html('');
                fetchDeliveryAddressOffer(uid, productList[0].product_id);
            }
        },
        error: function() {
            swal("Something went wrong ! Please try again");
            $("#submit-btn").prop("disabled", false);
        }

    });
    return false;
});

$('body').on('click', '.checkout-offer', function(e) {

    $.ajax({
        url: base_url+"/billing/checkout-offer",
        type: 'POST',
        data: {
            'offer_id' : 3,
            'cart': [
                {
                    "product_id":2,
                    "user_id":101,
                    "barcode":1001001206602031

                },
                {
                    "product_id":2,/*Small*/
                    "user_id":102,
                    "barcode":1001001206915577

                },
                {
                    "product_id":3,/*Large*/
                    "user_id":105,
                    "barcode":1001001206271518

                }
            ],
            "address_id":1,
            'payment_type': 2,
            'card_tid': 1243234
        },

        beforeSend: function () {
        },
        success: function (data) {
            console.log(data)
            // $(".invoice").html(data);
            if ((data.errors).length()>0) {
                swal(data.errors);
            }else{
                if (data.pay == '3') {
                    var form = data.form;
                    $('body').append(form);
                    $("#admin-payment-form").submit();
                } else {
                    // window.location.href = base_url + "/invoice/" + data.invoice_id;
                }
            }
        },
        complete: function () {
        },
        error: function () {
            swal("Something went wrong ! Please try again");
            $(".checkout").prop("disabled", false);
        }
    });

});

