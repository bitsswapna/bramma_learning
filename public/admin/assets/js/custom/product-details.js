       
     //index page
     $(document).ready(function(){
        var table = $('#sampleTable').DataTable({ 
            "aaSorting": [],
            'order': [[1, 'desc']],
            "fnDrawCallback": function (oSettings) {
                if (oSettings.bSorted || oSettings.bFiltered || oSettings._iDisplayLength) {
                    j = 0;
                    for (var i = oSettings._iDisplayStart; i < oSettings.aiDisplay.length + oSettings._iDisplayStart; i++) {
                        $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[j]].nTr).find('span').text(i + 1);
                        j++;
                    }
                }
            },
            serverSide: true,
            scrollX: true,
            oLanguage: {
                sProcessing: '<svg width="30px"  height="30px"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-rolling" style="background: transparent;"><circle cx="50" cy="50" fill="none" stroke="#0fb963" stroke-width="10" r="35" stroke-dasharray="164.93361431346415 56.97787143782138" transform="rotate(228 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform></circle></svg>'
            },
            processing: true,
            "pageLength": 10,
            "columnDefs": [{'orderable': false, 'targets': [0, 8, 5]}],
            mark: true,
            fixedColumns: {},
            ajax: {
                url: base_url + '/get-products',
            },
            columns: [
                {data: 'sl#', name: 'Sl#'},
                {data: 'file_path', name: 'file_path'},
                {data: 'name', name: 'name'},
                {data: 'type', name: 'type'},
                {data: 'category', name: 'Category'},
                {data: 'short_desc', name: 'Short desc'},
                {data: 'mrp', name: 'mrp'},
                {data: 'status', name: 'status'},
                {data: 'stock', name: 'stock'},
                {data: 'action', name: 'Action'}
            ]
        });

         $.fn.dataTableExt.sErrMode = 'throw';

         $('#sampleTable').on( 'error.dt', function ( e, settings, techNote, message ) {
             // console.log( 'An error has been reported by DataTables: ', message );
             swal("Hey!", "Please Wait ! We are loading your datas!", "info")
                 .then(function(willDelete){
                     if (willDelete) {
                         location.reload(true);
                     }
                 });
         } ) ;
       
    });


        $(document).on('click', '.delete-user', function (e) {

            e.preventDefault();
            var token = $('meta[name="csrf-token"]').attr("content");
            var id = $(this).data("id");

            var data = {id: id, _token: token};
            var url = base_url + '/product/delete' ;

            swal({
                        title: "Are you sure?",
                        text: "Please Confirm BeforeDeleting!",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                .then(function(willDelete)  {
                        if (willDelete) {

                                                        
                                    $.ajax({
                                        url: url, type: 'POST', data: data,
                                        beforeSend: function () {

                                        },
                                        success: function (data) {
                                            location.reload();
                                            table.draw();
                                         
                                        },
                                        error: function (data) {
                                            table.draw();
                                          
                                        },
                                        complete: function () {

                                        }
                                    });
                                                    
                                    swal("Poof! Successfully Deleted!", {
                                    icon: "success",
                                    });
                        } else {
                            swal("Good Try Next Time!");
                        }
                });

       
        });

        //add page


        var slug = function(str) {
            str = str.replace(/^\s+|\s+$/g, ''); // trim
            str = str.toLowerCase();
    
            // remove accents, swap ñ for n, etc
            var from = "ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";
            var to = "aaaaaeeeeeiiiiooooouuuunc------";
            for (var i = 0, l = from.length; i < l; i++) {
                str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
            }
    
            str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
                .replace(/\s+/g, '-') // collapse whitespace and replace by -
                .replace(/-+/g, '-'); // collapse dashes
    
            return str;
        };
    
        $(document).on('change', '#product_name', function() {
            $slug = slug($(this).val());
            $('#slug').val($slug);
        });
    
        $(document).on('change', '#slug', function() {
            $slug = slug($(this).val());
            $('#slug').val($slug);
        });
    
        $(document).on('change', '#is_taxable', function() {
            var $taxvalue = $(this).val();
            if ($taxvalue == 1) {
                $('#taxType').show();
            } else {
                $('#taxType').hide();
            }
    
        });

         $(document).on('change', '#is_returnable', function() {
            var $returnvalue = $(this).val();
            if ($returnvalue == 1) {
                $('.is_return_div').show();
            } else {
                $('.is_return_div').hide();
            }
    
        });
    
        function readURL(input, img_con) {
    
            if (input.files && input.files[0]) {
                var reader = new FileReader();
    
                reader.onload = function(e) {
                    $('#' + img_con).find('img').attr('src', e.target.result);
                }
    
                reader.readAsDataURL(input.files[0]);
            }
        }
    
        $("#image").change(function() {
            readURL(this, 'prof_img');
            $(this).parents('.form-group').find("#uploadFile").val(this.value);
        });
    
       
      
    
    
        $(document).ready(function(){
            var data = {id: 'success'};
            var url = base_url + '/product/related-product' + '/';
                                
            $.ajax({
                url: url, type: 'GET', data: data,
                beforeSend: function () {
    
                },
                success: function (data) {
                    $('#related-products').html(data.html);
                },
                error: function (data) {
                   
                },
                complete: function () {
    
                }
            });
    
        });


        $(function() {
            // Multiple images preview in browser
            var imagesPreview = function(input, placeToInsertImagePreview) {

                if (input.files) {
                    var filesAmount = input.files.length;

                    for (i = 0; i < filesAmount; i++) {
                        var reader = new FileReader();

                        reader.onload = function(event) {
                            $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview).attr('style','margin-left: 2%;;width:200px;height:200px;');
                        }

                        reader.readAsDataURL(input.files[i]);
                    }
                }

            };

            $('#gallery_image').on('change', function() {
                $(".gallery-image").html("");
                imagesPreview(this, 'div.gallery-image');
            });
        });




        $(document).ready(function () {
  
            $("#add-more").click(function(e){
                $(".tax-container-vi").append($(".dummy-cont").html());
                $(".tax-content-remove").click(function(){
                    $(this).closest(".tax-content").remove();
                })
            });
            $(".tax-content-remove").click(function(){
                $(this).closest(".tax-content").remove();
            })

        });

