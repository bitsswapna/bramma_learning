 // datatable
 var product_id = null;
        
 var table = $('#productStockTable').DataTable({
     "aaSorting": [],
     'order': [[1, 'desc']],
     "fnDrawCallback": function (oSettings) {
         if (oSettings.bSorted || oSettings.bFiltered || oSettings._iDisplayLength) {
             j = 0;
             for (var i = oSettings._iDisplayStart; i < oSettings.aiDisplay.length + oSettings._iDisplayStart; i++) {
                 $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[j]].nTr).find('span').text(i + 1);
                 j++;
             }
         }
     },
     serverSide: true,
     scrollX: true,
     oLanguage: {
         sProcessing: '<svg width="30px"  height="30px"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-rolling" style="background: transparent;"><circle cx="50" cy="50" fill="none" stroke="#0fb963" stroke-width="10" r="35" stroke-dasharray="164.93361431346415 56.97787143782138" transform="rotate(228 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform></circle></svg>'
     },
     processing: true,
     "pageLength": 10,
     "columnDefs": [{'orderable': false, 'targets': [0, 4, 3]}],
     mark: true,
     fixedColumns: {},
     ajax: {
         url: base_url + '/product/get-batchProducts',
         data: function (d) {
             x = document.getElementById("product_stock").selectedIndex;
             d.product_id = document.getElementsByTagName("option")[x].value;
             d.unique_code = $('#unique_code_list').val();
         }


     },
     columns: [
         {data: 'sl#', name: 'Sl#'},
         {data: 'unique_code', name: 'unique_code'},
         {data: 'product_name', name: 'product_name'},
         {data: 'batch_name', name: 'batch_name'},
         {data: 'stock', name: 'stock'},
         {data: 'barcode', name: 'barcode'},
         {data: 'expiry_date', name: 'expiry_date'},
         {data: 'action', name: 'action'}
     ]
 });
 // table.buttons().container()
 //     .appendTo($('.product_stock', table.table().container()));
 $(document).ready(function() {
 $('#unique_code_list').select2({
     placeholder: "Search Unique Code ...",
     minimumInputLength: 1,

     ajax: {
         url:base_url+'/stock/getselectedstock',
         dataType: 'json',
         data: function (params) {
             return {
                 q: $.trim(params.term)
             };
         },
         processResults: function (data) {
             return {
                 results: data
             };
         },
         cache: true
     }

 });


 });

 $('#bt_remove').click(function () {
     $('#unique_code_list').val('0').trigger("change");
     table.draw();

 });

 $('#product_stock').change(function (e) {
     table.draw();
     e.preventDefault();
 });

 $('.filter_button').click(function (e) {
     var val = $('#unique_code_list').val();
     $('#code_value').val(val);
     table.draw();
     e.preventDefault();
 });



//add batch

 $('#add_batch_product').click(function () {
     $("#add_batch").modal();
 });

//adding batches

 $('#button_add_batch').click(function () {

      var valid = true;
      message = '';
      message_warning  = '';
      message_products = '';
     //  var products = $('select.product_stock option:selected').val(); 
      var batch_name =  $('#batch_name').val() ;
      var quantity =  $('#quantity').val() ;
     //alert(products);
     //    if(products == '') {
     //         var inputName ='Product';
     //         valid = false;
     //         message_products += 'Please enter your ' + inputName + '\n';
     //         $('#error_products').html(message_products) ;
     //     }
      
         if(batch_name == '') {
             var inputName = $('#batch_name').attr('name');
             valid = false;
             message += 'Please enter your ' + inputName + '\n';
             $('#error_batch_name').html(message) ;
         }
         if(quantity == '') {
             var inputName = $('#quantity').attr('name');
             valid = false;
              message_warning += 'Please enter your ' + inputName + '\n';
             $('#error_quantity').html(message_warning);
         }
    

     if(valid ){
     var $data =  $('#sumbit_batch_product').serialize();
     var actionurl =  base_url + '/add-product-batch' ;
         $.ajax({
             type: 'post',
             url: actionurl,
             data: $data,
             beforeSend: function () {

             },
            
             success: function (data) {
                 table.draw();
                $("#sumbit_batch_product")[0].reset();
                $.notify('<strong>Success</strong> Your Batch has been saved!', {
                  
                    z_index: 5000,
                });
                // var notify = $.notify('<strong>Saving</strong> Do not close this page...', {
                //      allow_dismiss: false,
                //      showProgressbar: true,
                //      z_index: 5000,
                //  });

                //  setTimeout(function() {
                //      notify.update({'type': 'success', 'message': '<strong>Success</strong> Your Batch has been saved!', 'progress': 25});
                //  }, 4500);
               
             },
             error: function (data) {
                 
             },
         });

     }
 });
 

 //editing batches
$(document).on('click','.edit_batch',function () {
 var $data ={id:$(this).data('id')};
 var actionurl =  base_url + '/update-stock' ;
 var title = "Update Stock";
 //alert($data);
 console.log(actionurl);
         $.ajax({
             type: 'get',
             url: actionurl,
             data: $data,
             beforeSend: function () {

             },
            
             success: function (result) {
                 console.log(result.html);
                 $('#product_view').html(result.html);
                 // $('.modal-title').html(title);
                 $("#order_product").modal();
             },
             error: function (data) {
                 
             },
         });

});



//updated edited batches
 $(document).on('click','#button_update_batch',function () {
    var $data =  $('#update_batch_product').serialize();
     var actionurl =  base_url + '/update-product-batch'  ;
     
         $.ajax({
             type: 'post',
             url: actionurl,
             data: $data,
             beforeSend: function () {

             },
            
             success: function (data) {
                 table.draw();
                 $.notify('<strong>Success</strong> Your Batch has been saved!', {
                  
                    z_index: 5000,
                });
            //    var notify = $.notify('<strong>Saving</strong> Do not close this page...', {
            //          allow_dismiss: false,
            //          showProgressbar: true,
            //          z_index: 5000,
            //      });

            //      setTimeout(function() {
            //          notify.update({'type': 'success', 'message': '<strong>Success</strong> Your page has been saved!', 'progress': 15});
            //      }, 100);
             },
             error: function (data) {
                 
             },
         });
 });

 //select 2  

 $(document).ready(function() {
    
     $('.product-select').select2();

     $.fn.dataTableExt.sErrMode = 'throw';

     $('#productStockTable').on( 'error.dt', function ( e, settings, techNote, message ) {
         console.log( 'An error has been reported by DataTables: ', message );
         swal("Hey!", "Please Wait ! We are loading your datas!", "info")
             .then(function(willDelete){
                 if (willDelete) {
                     location.reload(true);
                 }
             });
     } ) ;

 });

