 // Read a page's GET URL variables and return them as an associative array.
 function getUrlVars()
 {
     var vars = [], hash;
     var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
     for(var i = 0; i < hashes.length; i++)
     {
         hash = hashes[i].split('=');
         vars.push(hash[0]);
         vars[hash[0]] = hash[1];
     }
     return vars;
 }


var product_status = null;
var table = $('#productReturn').DataTable({
    dom: 'lBfrtip',
    buttons: [
        'copy', 'csv', 'excel', 'pdf', 'print'
    ],
    "aaSorting": [],
    'order': [[1, 'asc']],
    "fnDrawCallback": function (oSettings) {
        if (oSettings.bSorted || oSettings.bFiltered || oSettings._iDisplayLength) {
            j = 0;
            for (var i = oSettings._iDisplayStart; i < oSettings.aiDisplay.length + oSettings._iDisplayStart; i++) {
                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[j]].nTr).find('span').text(i + 1);
                j++;
            }
        }
    },
    serverSide: true,
    scrollX: true,
    oLanguage: {
        sProcessing: '<svg width="30px"  height="30px"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-rolling" style="background: transparent;"><circle cx="50" cy="50" fill="none" stroke="#0fb963" stroke-width="10" r="35" stroke-dasharray="164.93361431346415 56.97787143782138" transform="rotate(228 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform></circle></svg>'
    },
    processing: true,
    "pageLength": 15,
    "columnDefs": [{'orderable': false, 'targets': [0, 6,6]}],
    mark: true,
    fixedColumns: {},
    ajax: {
        url: base_url  + '/product-report/getProductReturns',
        data: function (d) {
            d.p_id =  getUrlVars()["p_id"];
            d.start_date =  getUrlVars()["start_date"];
            d.end_date =  getUrlVars()["end_date"];
        }
    },
    columns: [
        {data: 'sl#', name: 'Sl#'},
        {data: 'request_id', name: 'request_id'},
        {data: 'order_id', name: 'order_id'},
        {data: 'product_name', name: 'product_name'},
        {data: 'return_type', name: 'return_type'},
        {data: 'reason', name: 'reason'},
        {data: 'status', name: 'status'},
        {data: 'date', name: 'date'},
    ]
});
