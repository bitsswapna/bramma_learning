$(document).ready(function () {

var table_waalet_report = $('#user-wallet-report-table').DataTable({
    dom: 'lBfrtip',
    buttons: [
        'copy', 'csv', 'excel', 'pdf', 'print'
    ],
    "aaSorting": [],
    'order': [[1, 'desc']],
    "fnDrawCallback": function (oSettings) {
        if (oSettings.bSorted || oSettings.bFiltered || oSettings._iDisplayLength) {
            j = 0;
            for (var i = oSettings._iDisplayStart; i < oSettings.aiDisplay.length + oSettings._iDisplayStart; i++) {
                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[j]].nTr).find('span').text(i + 1);
                j++;
            }
        }
    },
    serverSide: true,
    scrollX: true,
    oLanguage: {
        sProcessing: '<svg width="30px"  height="30px"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-rolling" style="background: transparent;"><circle cx="50" cy="50" fill="none" stroke="#0fb963" stroke-width="10" r="35" stroke-dasharray="164.93361431346415 56.97787143782138" transform="rotate(228 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform></circle></svg>'
    },
    processing: true,
    serverSide: true,
    "pageLength": 15,
    "columnDefs": [{'orderable': false, 'targets': [0, 8, 7]}],
    mark: true,
    fixedColumns: {},
    ajax: {
        url: base_url + '/user_wallet/get-userWalletReport/' ,
        data: function (d) {
            x = document.getElementById("calculation_type").selectedIndex;
            d.calculation_type = document.getElementsByTagName("option")[x].value;
            d.user_id = $('#user_id').val();
            d.user_name =  $('#user_name').val();

        }
    },
    columns: [
        {data: 'DT_Row_Index', name: 'DT_Row_Index',searchable:false},
        {data: 'date', name: 'date'},
        {data: 'user_id', name: 'user_id'},
        {data: 'user_name', name: 'user_name'},
        {data: 'calculation_type', name: 'calculation_type'},
        {data: 'pv', name: 'pv',
            "className":  ' text-right',searchable:false},
        {data: 'pgv', name: 'pgv',
            "className":  ' text-right',searchable:false},
        {data: 'bv', name: 'bv',
            "className":  ' text-right',searchable:false},
        {data: 'price', name: 'price',
            "className":  ' text-right',searchable:false},
        {data: 'percentage', name: 'percentage',
            "className":  ' text-right,searchable:false'},
        {data: 'reward_amount', name: 'reward_amount',
            "className":  ' text-right',searchable:false},
        {data: 'reward_point', name: 'reward_point',
            "className":  ' text-right',searchable:false},
    ],
    initComplete: function () {

        // this.api().columns().every(function () {

            $('#uid').on('keyup', function(){
                var uidvalue = $('#uid').val();
                if(uidvalue.length > 0) {
                    table_waalet_report
                        .column(2)
                        .search($(this).val(), false, false, true)
                        .draw();
                }

            });
            $('#cal_type').on('keyup', function(){
                var caltype = $('#cal_type').val();
                if(caltype .length > 0) {
                    table_waalet_report
                        .column(4)
                        .search($(this).val(), false, false, true)
                        .draw();
                }

            });

        // });
    }
});


    $('.filter_button').click(function (e) {
        //  alert("hai");
        table_waalet_report.draw();
        e.preventDefault();
    });


    $.fn.dataTableExt.sErrMode = 'throw';

    $('#user-wallet-report-table').on( 'error.dt', function ( e, settings, techNote, message ) {
        console.log( 'An error has been reported by DataTables: ', message );
        swal("Hey!", "Please Wait ! We are loading your datas!", "info")
            .then(function(willDelete){
                if (willDelete) {
                    location.reload(true);
                }
            });
      } ) ;
});


