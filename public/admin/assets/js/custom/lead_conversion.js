morris-pgv-monthly$(document).ready(function () {
    var table = $('#vi-lead-conversion').DataTable({
        dom: 'lBfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "aaSorting": [],
        'order': [[1, 'desc']],
        "fnDrawCallback": function (oSettings) {
            if (oSettings.bSorted || oSettings.bFiltered || oSettings._iDisplayLength) {
                j = 0;
                for (var i = oSettings._iDisplayStart; i < oSettings.aiDisplay.length + oSettings._iDisplayStart; i++) {
                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[j]].nTr).find('span').text(i + 1);
                    j++;
                }
            }
        },
        serverSide: true,
        scrollX: true,
        oLanguage: {
            sProcessing: '<svg width="30px"  height="30px"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-rolling" style="background: transparent;"><circle cx="50" cy="50" fill="none" stroke="#0fb963" stroke-width="10" r="35" stroke-dasharray="164.93361431346415 56.97787143782138" transform="rotate(228 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform></circle></svg>'
        },
        processing: true,
        "pageLength": 10,
        "columnDefs": [{'orderable': false, 'targets': [0, 4, 5]}],
        mark: true,
        fixedColumns: {},
        ajax: {
            url: base_url + '/get-wellness-advisor-report',
            data: function (d) {
                d.user_id = $('#user_id').val();
                d.user_name =  $('#user_name').val();
            }
        },
        columns: [
            {data: 'DT_Row_Index', name: 'DT_Row_Index'},
            {data: 'user_id', name: 'user_id'},
            {data: 'name', name: 'name'},
            {data: 'child_count', name: 'child_count'},
            {data: 'child_wa_count', name: 'child_wa_count'},
            {data: 'child_customer_count', name: 'child_customer_count'},
            {data: 'parent_id', name: 'parent_id'},
            {data: 'parent', name: 'parent'}
        ]
    });



    $.fn.dataTableExt.sErrMode = 'throw';

    $('#vi-lead-conversion').on( 'error.dt', function ( e, settings, techNote, message ) {
        console.log( 'An error has been reported by DataTables: ', message );
        swal("Hey!", "Please Wait ! We are loading your datas!", "info")
            .then(function(willDelete){
                if (willDelete) {
                    location.reload(true);
                }
            });
    } ) ;


    $('.filter_button').click(function (e) {
        table.draw();
        e.preventDefault();
    });

});


//
// $('body').on('click', '#purchase-report', function (e) {
//     var id = $(this).data("id");
//     var url = base_url+"/get-user-product-report"
//     alert(id);
//     $(".user-purchase-report").html(id);
//     $.ajax({
//         url: url,
//         type: 'GET',
//         data:{
//             'id':id,
//         },
//         beforeSend: function(){
//             $("#if_loading").html("loading");
//         },
//         success: function(result) {
//             // Do something with the result
//             $("#user-purchase-report").html(result);
//
//         }
//     }).complete(function() {
//         $('.if_loading').hide();
//     });
// });


//purchase report


$('body').on('click', '#purchase-report-tab', function (e) {
    var wa_user_id = $(this).data("id");
    var url = base_url + "/get-user-purchase-report"
    $.ajax({
        url: url,
        type: 'GET',
        beforeSend: function () {
            $("#purchase-report").html("loading");
            $("#vi-purchase-report-table").dataTable().fnDestroy();
        },
        success: function (result) {
            // Do something with the result
            $("#purchase-report").html(result.html)

        }
    }).complete(function () {
        var product_status = null;
        var table = $('#wa-user-purchase-table').DataTable({
            "aaSorting": [],
            'order': [[1, 'desc']],
            "fnDrawCallback": function (oSettings) {
                if (oSettings.bSorted || oSettings.bFiltered || oSettings._iDisplayLength) {
                    j = 0;
                    for (var i = oSettings._iDisplayStart; i < oSettings.aiDisplay.length + oSettings._iDisplayStart; i++) {
                        $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[j]].nTr).find('span').text(i + 1);
                        j++;
                    }
                }
            },
            serverSide: true,
            scrollX: true,
            oLanguage: {
                sProcessing: '<svg width="30px"  height="30px"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-rolling" style="background: transparent;"><circle cx="50" cy="50" fill="none" stroke="#0fb963" stroke-width="10" r="35" stroke-dasharray="164.93361431346415 56.97787143782138" transform="rotate(228 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform></circle></svg>'
            },
            processing: true,
            "pageLength": 15,
            "columnDefs": [{'orderable': false, 'targets': [0, 4, 6]}],
            mark: true,
            fixedColumns: {},
            ajax: {
                url: base_url + '/order/get-orderDetails',
                data: function (d) {
                    d.wa_user_id = wa_user_id;
                    // x = document.getElementById("product_status").selectedIndex;
                    // d.product_status = document.getElementsByTagName("option")[x].value;
                    // d.start_date = $('#start_date').val();
                    //d.end_date =  $('#end_date').val();
                }
            },
            columns: [
                {data: 'sl#', name: 'Sl#'},
                {data: 'order_id', name: 'order_id'},
                {data: 'product', name: 'product'},
                {data: 'order_price', name: 'order_price'},
                {data: 'purchase_price', name: 'purchase_price'},
                {data: 'date', name: 'date'},
                {data: 'status', name: 'status'},
            ]
        });
    });
});


$(document).on('click', '.order_products', function (e) {
    $con = $(this);
    var $data = {data: $(this).data('value')};
    var actionurl = base_url + '/order-products';
    var title = "/Order Details";
    // console.log($data);
    $.ajax({
        type: 'get',
        dataType: "html",
        url: actionurl,
        data: $data,
        beforeSend: function () {

        },
        success: function (data) {
            $con.find('.display_products').html(data);
        },
        error: function (data) {

        },
    });
});

//vi-user-wallet-tab

$('body').on('click', '#vi-user-wallet-tab', function (e) {
    var id = $(this).data("id");
    var url = base_url + "/get-wa-user-wallet-report";
    $.ajax({
        url: url,
        type: 'GET',
        data: {
            'id': id,
        },
        beforeSend: function () {
            $("#user-wallet").html("loading");
            $("#vi-wa-user-wallet-report-table").dataTable().fnDestroy();
        },
        success: function (result) {
            // Do something with the result
            $("#user-wallet").html(result.html)
        }
    }).complete(function () {
        var table_wa_waalet_report = $('#vi-wa-user-wallet-report-table').DataTable({

            "aaSorting": [],
            'order': [[1, 'desc']],
            "fnDrawCallback": function (oSettings) {
                if (oSettings.bSorted || oSettings.bFiltered || oSettings._iDisplayLength) {
                    j = 0;
                    for (var i = oSettings._iDisplayStart; i < oSettings.aiDisplay.length + oSettings._iDisplayStart; i++) {
                        $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[j]].nTr).find('span').text(i + 1);
                        j++;
                    }
                }
            },
            serverSide: true,
            scrollX: true,
            oLanguage: {
                sProcessing: '<svg width="30px"  height="30px"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-rolling" style="background: transparent;"><circle cx="50" cy="50" fill="none" stroke="#0fb963" stroke-width="10" r="35" stroke-dasharray="164.93361431346415 56.97787143782138" transform="rotate(228 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform></circle></svg>'
            },
            processing: true,
            "pageLength": 10,
            "columnDefs": [{'orderable': false, 'targets': [0, 5, 6]}],
            mark: true,
            fixedColumns: {},
            ajax: {
                url: base_url + '/get-wa-user-wallet-report-table/' + id,
            },
            columns: [
                {data: 'sl#', name: 'sl#'},
                {data: 'calculation_type', name: 'calculation_type'},
                {data: 'pv', name: 'pv'},
                {data: 'price', name: 'price'},
                {data: 'pgv', name: 'pgv'},
                {data: 'percentage', name: 'percentage'},
                {data: 'reward_amount', name: 'reward_amount'},
                {data: 'reward_point', name: 'reward_point'},
            ]
        });

        table_wa_waalet_report.draw();
    });
});
$('body').on('click', '#vi-graph-reoprt-tab', function (e) {
    var id = $(this).data("id");
    var url = base_url + "/get-wa-graph-reoprt";
    var pv = 0;
    var pgv = 0;
    var cbv = 0;
    var wabv = 0;
    var child_pv = 0;
    var customerbv = 0;
    var barDataPGV =[];
    $.ajax({
        url: url,
        type: 'GET',
        data: {
            'id': id,
        },
        beforeSend: function () {
            $("#graph-report").html("loading");

        },
        success: function (result) {
            // Do something with the result
            $("#graph-report").html(result.html)
            pv = result.mlm['pv'];
            pgv = result.mlm['pgv'];
            cbv = result.company_mlm['pv'] * 25;
            wabv = result.mlm['pv'] * 25;
            child_pv = result.children_pv;
            barDataPGV = result.barDataPGV;

        }
    }).complete(function () {
        !function($) {
            "use strict";

            var Dashboard1 = function() {
                this.$realData = []
            };

            //creates Bar chart
            Dashboard1.prototype.createBarChart  = function(element, data, xkey, ykeys, labels, lineColors) {
                Morris.Bar({
                    element: element,
                    data: data,
                    xkey: xkey,
                    ykeys: ykeys,
                    labels: labels,
                    hideHover: 'auto',
                    resize: true, //defaulted to true
                    gridLineColor: '#eeeeee',
                    barSizeRatio: 0.2,
                    barColors: lineColors,
                    postUnits: 'k'
                });
            },

                Dashboard1.prototype.init = function() {

                    //creating bar chart

                    this.createBarChart('morris-pgv-monthly', barDataPGV, 'y', ['a'], ['PGV'], ['#3bafda']);
                },
                //init
                $.Dashboard1 = new Dashboard1, $.Dashboard1.Constructor = Dashboard1
        }(window.jQuery),

            //initializing
            function($) {
                "use strict";
                $.Dashboard1.init();
            }(window.jQuery);

    });
});


/*Sale details*/
function createSaleDatatable(id, filter) {
    var table_wa_sale_report = $('#vi-wa-user-sale-report-table').DataTable({

        "aaSorting": [],
        'order': [[1, 'desc']],
        "fnDrawCallback": function (oSettings) {
            if (oSettings.bSorted || oSettings.bFiltered || oSettings._iDisplayLength) {
                j = 0;
                for (var i = oSettings._iDisplayStart; i < oSettings.aiDisplay.length + oSettings._iDisplayStart; i++) {
                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[j]].nTr).find('span').text(i + 1);
                    j++;
                }
            }
        },
        serverSide: true,
        scrollX: true,
        oLanguage: {
            sProcessing: '<svg width="30px"  height="30px"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-rolling" style="background: transparent;"><circle cx="50" cy="50" fill="none" stroke="#0fb963" stroke-width="10" r="35" stroke-dasharray="164.93361431346415 56.97787143782138" transform="rotate(228 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform></circle></svg>'
        },
        processing: true,
        "pageLength": 10,
        "columnDefs": [{'orderable': false, 'targets': [0, 4, 5]}],
        mark: true,
        fixedColumns: {},
        ajax: {
            url: base_url + '/get-wa-user-sale-report-table/' + id + '/' + filter,

        },
        columns: [
            {data: 'sl#', name: 'sl#'},
            {data: 'orderid', name: 'orderid'},
            {data: 'source', name: 'source'},
            {data: 'type', name: 'type'},
            {data: 'pv', name: 'pv'},
            {data: 'status', name: 'status'},
        ]
    });
    table_wa_sale_report.draw();
}

/*Sale detail tab click*/
$('body').on('click', '#vi-sale-reoprt-tab', function (e) {
    var id = $(this).data("id");
    var url = base_url + "/get-wa-user-sale-report";
    $.ajax({
        url: url,
        type: 'GET',
        data: {
            'id': id,
        },
        beforeSend: function () {
            $("#sale-detail").html("loading");
            $("#vi-wa-user-sale-report-table").dataTable().fnDestroy();
        },
        success: function (result) {
            // Do something with the result
            $("#sale-detail").html(result.html)
        }
    }).complete(function () {
        createSaleDatatable(id, 'all')
        // table_wa_sale_report.draw();
    });
});

/*Sale detail filter*/
$(document).on('change', '#sale-detail-filter', function (e) {
    $("#vi-wa-user-sale-report-table").dataTable().fnDestroy();
    createSaleDatatable($("#user_id").val(), $("#sale-detail-filter").val())
});

