/**
 * Created by COMPUTER on 5/14/2018.
 */
var llmAdminApp = angular.module("llmAdminApp", []);
llmAdminApp.config([
    '$httpProvider',
    function($httpProvider) {
        $httpProvider.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";
    }
]);

llmAdminApp.config( ['$provide', function ($provide){
    $provide.decorator('$browser', ['$delegate', function ($delegate) {
        $delegate.onUrlChange = function () {};
        $delegate.url = function () { return ""};
        return $delegate;
    }]);
}]);

llmAdminApp.filter('DateFormatter', function() {
    return function(dateSTR) {
        if (dateSTR != undefined) {
            var o = dateSTR.replace(/-/g, "/"); // Replaces hyphens with slashes
            return Date.parse(o); // No TZ subtraction on this sample
        }
        return null;
    }
});