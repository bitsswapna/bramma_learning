<?php

namespace App\Http\Controllers;

use App\AdminUsersDetails;
use Illuminate\Http\Request;

class AdminUsersDetailsController extends Controller
{
    /**i
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AdminUsersDetails  $adminUsersDetails
     * @return \Illuminate\Http\Response
     */
    public function show(AdminUsersDetails $adminUsersDetails)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AdminUsersDetails  $adminUsersDetails
     * @return \Illuminate\Http\Response
     */
    public function edit(AdminUsersDetails $adminUsersDetails)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AdminUsersDetails  $adminUsersDetails
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AdminUsersDetails $adminUsersDetails)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AdminUsersDetails  $adminUsersDetails
     * @return \Illuminate\Http\Response
     */
    public function destroy(AdminUsersDetails $adminUsersDetails)
    {
        //
    }
}
