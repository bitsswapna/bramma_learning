<?php

namespace App\Http\Controllers;
use App\Http\Controllers\SiteWorkController;
use App\SiteWork;
use App\SiteWorkAmountCreaditCollected_By;
use App\SiteWorkAmountCreaditGivenBy;
use App\SiteWorkAmountDebitCollected_By;
use App\SiteWorkAmountDebitGivenBy;
use App\SiteWorkAmountDetails;
use App\SiteAmountCredit;
use App\SiteAmountDebit;
use App\Role;
use App\Site;
use App\User;
use App\Utilities;
use App\Vehicles;
use Carbon\Carbon;
use http\Env\Response;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;

use Illuminate\Http\Request;

class SiteWorkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $site=Site::select('id','title')->get();
        $vehi=Vehicles::select('id','vehicle_number')->get();
        $driver=User::select('id','name')->where('role_id', '=', 2)->get();
        $cleaner=User::select('id','name')->where('role_id', '=', 3)->get();
        return view('admin.Site_work.create')->with(['site'=> $site,'vehi'=>$vehi,'driver'=>$driver,'cleaner'=>$cleaner]);

        
    }
    public function view()
    {
        $site=SiteWork::select('id','titles')->get();
        $utility=Utilities::select('id','name')->get();
        $debit=Utilities::where('type',2)->get();
        $credit=Utilities::where('type',1)->get();
        $staff=User::where('role_id',4)->pluck('name','id');
        $admin=User::where('role_id',1)->select('id','name')->get();

        return view('admin.work-amount.create')->with(['title'=>$site,'utility'=>$utility,'debit'=>$debit,'credit'=>$credit,'staff'=>$staff,'admin'=>$admin]);


    }
      public function show()
     {
        return view('admin.Site_work.index');

     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
//      dd($request->all());
        $va = Validator::make($request->all(), [
         
            'titles'=>'required',
            'site'=>'required',
            'vehicle'=>'required',
            'driver'=>'required',
            'cleaner'=>'required',
           'workdate'=>'required',
           'vehipoint'=>'required',
           'vehiduration'=>'required',
          
           

        ]);
       
        if ($va->fails()) {
            return redirect()->back()->withErrors($va->errors())->withInput($request->all());
        }
      else
      {
        $insert= new SiteWork();
        $insert->titles=$request->input('titles');
        $insert->site_id =$request->input('site');
        $insert->vehicle_id =$request->input('vehicle');
        $insert->driver_id=$request->input('driver');
        $insert->cleaner_id=$request->input('cleaner');


        $insert->work_date=$request->input('workdate');
        $insert->vehicle_point=$request->input('vehipoint');
        $insert->vehicle_duration=$request->input('vehiduration');
        $insert->save();
        return redirect('site-work')->with('smessage','data inserted succefully');
      }
       
    
    }
    public function getworkSite(Request $request)
    {
        // dd('hi');
        $data = $request->get('form_data');
        $params = array();
        parse_str($data, $params);

        $data = SiteWork::
        leftjoin('sites', 'sites.id', '=', 'site_work.site_id')
        ->leftjoin('vehicles','vehicles.id','=','site_work.vehicle_id')
        ->leftjoin('users as driver','driver.id','=','site_work.driver_id')
        ->leftjoin('users as cleaner','cleaner.id','=','site_work.cleaner_id')
      

        ->select('site_work.*', 'sites.title','vehicles.vehicle_number','driver.first_name as drname','driver.name as dname','cleaner.name as cname')
        ->get()->where('deleted_at','=',NULL);
        // dd($data);
      

        // $data =SiteWork::get();
        return DataTables::of($data)
            ->editColumn('sl#', function ($model) {
                return '<span class="si_no"></span> ';
            })
            ->editColumn('titles', function ($model) {
                return $model->titles;
               
            })->editColumn('site', function ($model) {
                return $model->title;
               
            })
            ->editColumn('vehicle', function ($model) {
                return $model->vehicle_number;
            })
          
            ->editColumn('driver', function ($model) {
                
                return $model->dname;
            })
            ->editColumn('cleaner', function ($model) {
                return $model->cname;
            })
            
            ->editColumn('vehiclepoint', function ($model) {
                return $model->vehicle_point;
            })
            ->editColumn('vehiduration', function ($model) {
                return $model->vehicle_duration;
            })
          
           
           
           
            ->editColumn('action', function ($model) {
                $html = '';
                if (can('edit_admin_user')) {
                    $html = '<a href="' . url('admin_worksite/request/' . $model->id) . '" title="Edit Customer"><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp';
                }
                if (can('edit_admin_user')) {
                    $html .= '<a href="#" class="delete-user-website" data-id="' . $model->id . '" title="Delete Customer"><i class="glyphicon glyphicon-trash"></i></a>';
                }
                if ($html == '') {
                    return 'No permission';
                }
                return $html;
            })
            ->rawColumns(['sl#','titles', 'site', 'vehicle', 'driver', 'cleaner','vehiduration','action'])
            ->make(true);

    }
           public function edit($id)
            {
       
            $ed=SiteWork::join('sites','sites.id','=','site_work.site_id')
            ->join('vehicles','vehicles.id','=','site_work.vehicle_id')
            ->join('users as driver','driver.id','=','site_work.driver_id')
            ->join('users as cleaner','cleaner.id','=','site_work.cleaner_id')
            ->select('site_work.*', 'sites.address','vehicles.vehicle_number','driver.name as dname','cleaner.name as cname')->where('site_work.id',$id)->first();
            // dd($ed);
            $site=Site::select('id','title')->get();
            $vehi=Vehicles::select('id','vehicle_number')->get();
            $driver=User::select('id','name')->where('role_id', '=', 2)->get();
            $cleaner=User::select('id','name')->where('role_id', '=', 3)->get();

            return view('admin.Site_work.edit')->with(['ed'=>$ed,'site'=> $site,'vehi'=>$vehi,'driver'=>$driver,'cleaner'=>$cleaner]);

       
       
        
       }

       public function insert(Request $request)
       {
//      dump($request->utility);
//      dd($request->all());
        $va = Validator::make($request->all(), [

            'titles' => 'required',
            'date'=>'required',
          
            'utility' => 'required',
            'amount' => "required",
            'collectedby'=> "required",
            'givenby' =>'required',
          
           

            ]);
           // dd($va);
     
        
      if ($va->fails()) {
        
            return redirect()->back()->withErrors($va->errors())->withInput($request->all());
        }
         
        else
        {
            DB::beginTransaction();
try {


    $amount = new SiteWorkAmountDetails();
    $amount->work_site_id = $request->input('titles');
    $amount->date = $request->input('date');
    $amount->save();




        if (count($request->input('utilityd')) && is_array($request->input('utilityd'))) {
            foreach ($request->input('utilityd') as $key => $day) {
                if ($day != null) {

                $debited = new SiteAmountDebit();
                $debited->site_work_amount_id = $amount->id;
                $debited->site_id   = $request->input('utilityd.' . $key);
//                $debited->given_by = 1;
                $debited->amount = $request->input('amountd.' . $key);
//                $debited->collected_by = 1;
                $debited->save();

                foreach ($request->input('givenbyd') as $data => $by)
                {
                    $givenby =new SiteWorkAmountDebitGivenBy();
                    $givenby->site_work_amount_debit_id=$debited->id;
                    $givenby->user_id= $request->input('givenbyd.' . $data);
                    $givenby->save();
                }

                    foreach ($request->input('collectedbyd') as $data => $by)
                    {
                        $collected =new SiteWorkAmountDebitCollected_By();
                        $collected->site_work_amount_debit_id=$debited->id;
                        $collected->user_id= $request->input('collectedbyd.' . $data);
                        $collected->save();
                    }
            }
        }
    }


    if (is_array($request->utility) && count($request->utility) > 0) {

        foreach ($request->utility as $key => $uti) {

            if ($uti != null) {

                $credit = new SiteAmountCredit();
                $credit->site_work_amount_id = $amount->id;
                $credit->site_id  = $request->input('utility.' . $key);
//                $credit->given_by =1;
                $credit->amount = $request->input('amount.' . $key);
//                $credit->collected_by = 1;
                $credit->save();
                foreach ($request->input('givenby') as $data => $by)
                {
                    $givenby =new SiteWorkAmountCreaditGivenBy();
                    $givenby->site_work_amount_creadit_id=$debited->id;
                    $givenby->user_id= $request->input('givenby.' . $data);
                    $givenby->save();
                }

                foreach ($request->input('collectedby') as $data => $by)
                {
                    $collected =new SiteWorkAmountCreaditCollected_By();
                    $collected->site_work_amount_creadit_id=$debited->id;
                    $collected->user_id= $request->input('collectedby.' . $data);
                    $collected->save();
                }
            }
        }
    }


DB::commit();
    return redirect('amount-details')->with('smessage', 'data inserted successfully');
}catch (\Exception $exception) {
    DB::rollBack();
    return response()->json([
        'status' => false,
        'message' => $exception->getMessage()
    ]);
}
        }
       


       }
       public function amount()

       {
           return view('admin.work-amount.index');
       }

       public function debit()
       {
           return view('admin.work-amount.debit');
       }
       public function credit()
       {
        return view('admin.work-amount.credit');
       }

       public function getamounts(Request $request)
       {
           
       

       $data =SiteWorkAmountDetails::join('site_work','site_work.id','site_work_amount.work_site_id')
           ->select('site_work_amount.*','site_work.titles','site_work_amount.date as date')

           ->get();
        //    ($data);
       // $data =Vehicles ::get();

       return DataTables::of($data)
           ->editColumn('sl#', function ($model) {
               return '<span class="si_no"></span> ';
           })
           ->editColumn('site', function ($model) {
            return $model->titles;

           })



           ->editColumn('date', function ($model) {
               return date('d M Y ', strtotime($model->date));
           })
           ->editColumn('action', function ($model) {
               $html = '';
               if (can('edit_admin_user')) {
                   $html = '<a href="' . url('admin_amount/edit/' . $model->id) . '" title="Edit Customer"><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp';
               }
               if (can('edit_admin_user')) {
                   $html .= '<a href="#" class="delete-user-website" data-id="' . $model->id . '" title="Delete Customer"><i class="glyphicon glyphicon-trash"></i></a>';
               }
               if ($html == '') {
                   return 'No permission';
               }
               return $html;
           })
           ->rawColumns(['sl#', 'site','date', 'action'])
           ->make(true);

   }
  
   public function amountedite($id)
   {
    //    dd('hi');
    $site=SiteWork::select('id','titles')->get();
    $utility=Utilities::select('id','name')->get();
    $debit=Utilities::select('id','name')->where('type',2)->get();
    $credit=Utilities::where('type',1)->get();
    $staff=User::where('role_id',4)->select('name','id')->get();
    $admin=User::where('role_id',1)->select('id','name')->get();

       $ed= SiteWorkAmountDetails::find($id);
    
    //    $sitework=SiteWork::select('id','titles')->get();
    //    $uti=Utilities::select('id','name')->get();
    //    $type=SiteWorkAmountDetails::select('id','type')->get();
       
       return view('admin.work-amount.edt')->with(['ed'=>$ed,'title'=>$site,'utility'=>$utility,'debit'=>$debit,'credit'=>$credit,'staff'=>$staff,'admin'=>$admin]);
   }
    public function amountupdate(Request $request,$id)
    {
        $va = Validator::make($request->all(), [
            'titles' => 'required',
            'type' => 'required',
            'utility' => 'required',
            'amount' => "required|regex:/^\d+(\.\d{1,2})?$/",
            'collectedby'=> "required|regex:/^\d+(\.\d{1,2})?$/",
            'givenby' =>"required|regex:/^\d+(\.\d{1,2})?$/",

        ]);
   
        if ($va->fails()) {
        
            return redirect()->back()->withErrors($va->errors())->withInput($request->all());
        }
        else
        {
            $update=SiteWorkAmountDetails::find($id);
            $update->site_work_id=$request->input('titles');
            $update->type =$request->input('type');
            $update->utilities_id=$request->input('utility');
            $update->amount =$request->input('amount');
            $update->collected_by=$request->input('collectedby');
            $update->given_by=$request->input('givenby');
            $update->save();
            return redirect('amount-details')->with('smessage','data updated succefully');
 

        }
    }
    public function store(Request $request)
    {
        
    }
    public function update(Request $request,$id)
    {
      
        $val = Validator::make($request->all(), [
         
         
            'titles'=>'required',
            'site'=>'required',
            'vehicle'=>'required',
            'driver'=>'required',
            'cleaner'=>'required',
           'workdate'=>'required',
           'vehipoint'=>'required',
           'vehiduration'=>'required',
            
 
         ]);
        
         if ($val->fails()) {
             return redirect()->back()->withErrors($val->errors())->withInput($request->all());
         }
         else
        {
         $update= SiteWork::find($id);

        //  dd($update);
        $update->titles=$request->input('titles');
        $update->site_id =$request->input('site');
        $update->vehicle_id =$request->input('vehicle');
        $update->driver_id=$request->input('driver');
        $update->cleaner_id=$request->input('cleaner');
         $update->work_date=$request->input('workdate');
         $update->vehicle_point=$request->input('vehipoint');
         $update->vehicle_duration=$request->input('vehiduration');
         $update->save();

         return redirect('site-work')->with('smessage','data updated succefully');

        }
    
    }
    public function delete(Request $request)
    {

        $del = SiteWork::find($request->get('id'))->delete();
        return response(['status' => true, 'msg' => 'Succefully deleted'], 200);
    // dd($del);
     
    }

    public function destroy(Request $request)
    {
    //    dd($request->get('id'));
        $del=SiteWorkAmountDetails::find($request->get('id'))->delete();
        // dd($del);
      
        return response(['status' => true, 'msg' => 'Succefully deleted'], 200);
    }
}
