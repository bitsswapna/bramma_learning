<?php

namespace App\Http\Controllers\Admin;

/**
 * Created by PhpStorm.
 * User: COMPUTER
 * Date: 3/20/2018
 * Time: 1:35 PM
 */
use App\Facility;
use App\Http\Controllers\Controller;
use App\Jobs\UpdatePgv;
use App\Models\Ancestor;
use App\Models\BhSettings;
use App\Models\BmSettings;
use App\Models\GmSettings;
use App\Models\Mlm;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\RetailSettings;
use App\Models\SlabSettings;
use App\Models\TbbSettings;
use App\Models\TlSettings;
use App\Models\UserDetails;
use App\Models\UserWallet;
use Carbon\Carbon;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\MonthlyReport;

// use App\Test;

class SeederController extends Controller
{
    const BV_RATE = 25.00;
    const WA_ACTIVE = 50.00;
    const TL_ACTIVE = 2000;
    const PV_AMOUNT = 45.00;
    const ACTIVE_EXECUTIVE = 100000.00;
    const EXCUTIVE_BENIFIT = 1000000.00;
    const EXCUTIVE_BENIFIT_PERS = 12.00;
    const BH_POOL = 10.00;
    const BM_POOL = 6.00;
    const GM_POOL = 4.00;
    var $ma_parents = array(1);
    var $all_parents = array(1);
    public  function __construct()
    {
        Carbon:: useMonthsOverflow(false);
    }

    public function index(Request $request)
    {
        $this->userSeeder();
        return redirect()->back()->with('smessage', 'Tables Seeded');

        $password = $request->get('check_truncate_value');
        if ($password == 'python#123') {
            $this->userSeeder();
            return redirect()->back()->with('smessage', 'Tables Seeded');
        } else {
            return redirect()->back()->with('emessage', 'Password Incorrect');

        }


    }


    public function currentMonth(Request $request)
    {
        // UserWallet::where('calculation_type','!=','')->delete();

        DB::update(" update mlm set realtime_pgv=0, pgv =0,pv=0,bv=0,total_price=0,wa_active=0,active_executive=0,is_executive=0,is_bh=0,is_bm=0,is_gm=0,gsi_pv=0,ucr_pv=0");
        $fromDate = Carbon::now()->startOfMonth()->toDateString();

        $tillDate = Carbon::now()->endOfMonth()->toDateString();

        $orders=Order::whereBetween(DB::raw('date(created_at)'), [$fromDate, $tillDate])->where(["is_confirmed"=>1])->get();

        foreach ($orders as $order_key => $order) {
            $order_products=OrderProduct::selectRaw("sum(price) as total_price,sum(pv) as total_pv, sum(bv) as total_bv")->where('order_id',$order->id)->first();
            // dd($order_products);
            $user=Mlm::where('user_id',$order->user_id)->first();
            if($user)
            {
                if($user->role_id==1 || $user->role_id==0) //welness advisor
                {
                    $this->_updateMLM($user,$order_products);
                }
                elseif($user->role_id==4) //Customer
                {

                    $user=Mlm::where('user_id',$user->parent)->first();
                    if($user)
                        $this->_updateMLM($user,$order_products);
                }

            }
        }

//        $calculation=new CalculationController();
//        $calculation->calculateRetail();
//        $calculation->activeBenefit();
//        $report=new MonthlyReportController();
//        $report->monthly_report();

        dd("done");

    }
    public function currentMonthMLM(Request $request)
    {

//        UserWallet::where('calculation_type','!=','')->delete();
//        dd();
        DB::update(" update mlm set realtime_pgv=0, pgv =0,pv=0,bv=0,total_price=0,wa_active=0,active_executive=0,is_executive=0,is_bh=0,is_bm=0,is_gm=0,gsi_pv=0,ucr_pv=0");
        //$fromDate = Carbon::now()->subMonth(2)->startOfMonth()->toDateString();
        $fromDate = Carbon::now()->subMonth(1)->startOfMonth()->toDateString();
        $tillDate = Carbon::now()->subMonth(1)->endOfMonth()->toDateString();
        //$tillDate = Carbon::now()->subMonth(2)->endOfMonth()->toDateString();

        $orders=Order::whereBetween(DB::raw('date(created_at)'), [$fromDate, $tillDate])->where(["is_confirmed"=>1])->get();

        foreach ($orders as $order_key => $order) {
            $order_products=OrderProduct::selectRaw("sum(price) as total_price,sum(pv) as total_pv, sum(bv) as total_bv")->where('order_id',$order->id)->first();
            // dd($order_products);
            $user=Mlm::where('user_id',$order->user_id)->first();
            if($user)
            {
                if($user->role_id==1 || $user->role_id==0) //welness advisor
                {
                    $this->_updateMLM($user,$order_products);
                }
                elseif($user->role_id==4) //Customer
                {

                    $user=Mlm::where('user_id',$user->parent)->first();
                    if($user)
                        $this->_updateMLM($user,$order_products);
                }

            }
        }

        $calculation=new CalculationController();
        $calculation->calculateRetail();
        $calculation->activeBenefit();
        $report=new MonthlyReportController();
        $report->monthly_report();
       DB::update(" update mlm set realtime_pgv=0, pgv =0,pv=0,bv=0,total_price=0");
        dd("done");

    }
    //Seeder for april month mlm

    public function updateMLM(Request $request)
    {


        DB::update(" update mlm set realtime_pgv=0, pgv =0,pv=0,bv=0,total_price=0,wa_active=0,active_executive=0,is_executive=0,is_bh=0,is_bm=0,is_gm=0,gsi_pv=0,ucr_pv=0");
        $fromDate = Carbon::now()->subMonth()->startOfMonth()->toDateString();

        $tillDate = Carbon::now()->subMonth()->endOfMonth()->toDateString();

        $orders=Order::whereBetween(DB::raw('date(created_at)'), [$fromDate, $tillDate])->where(["is_confirmed"=>1])->get();
       
        foreach ($orders as $order_key => $order) {
            $order_products=OrderProduct::selectRaw("sum(price) as total_price,sum(pv) as total_pv, sum(bv) as total_bv")->where('order_id',$order->id)->first();
            // dd($order_products);
            $user=Mlm::where('user_id',$order->user_id)->first();
            if($user)
            {
                if($user->role_id==1 || $user->role_id==0) //welness advisor
                {
                    $this->_updateMLM($user,$order_products);
                }
                elseif($user->role_id==4) //Customer
                {
                   
                    $user=Mlm::where('user_id',$user->parent)->first();
                    if($user)
                        $this->_updateMLM($user,$order_products);
                }
                
            }
        }

        $calculation=new CalculationController();
        $calculation->calculateRetail();
        $calculation->activeBenefit();
        $report=new MonthlyReportController();
        $report->monthly_report();
       // DB::update(" update mlm set realtime_pgv=0, pgv =0,pv=0,bv=0,total_price=0");
        dd("done");

    }

    private function _updateMLM($user,$order_products) //update pv, total price and bv
    {
        $user->total_price=$user->total_price+$order_products->total_price;
        $user->pv=$user->pv+$order_products->total_pv;
        $user->bv=$user->bv+$order_products->total_bv;
        $user->save();
        
    }
    private function userSeeder()
    {
        $roll = array(1);
        $names = array(
            "Saanvi",
            "Anya",
            "Aadhya",
            "Aaradhya",
            "Ananya",
            "Pari",
            "Anika",
            "Navya",
            "Angel",
            "Diya",
            "Myra",
            "Sara",
            "Iraa",
            "Ahana",
            "Anvi",
            "Prisha",
            "Riya",
            "Aarohi",
            "Anaya",
            "Akshara",
            "Eva",
            "Shanaya",
            "Kyra",
            "Siya",
            "Aarav",
            "Vihaan",
            "Vivaan",
            "Ananya",
            "Diya",
            "Advik",
            "Kabir",
            "Anaya",
            "Aarav",
            "Vivaan",
            "Aditya",
            "Vivaan",
            "Vihaan",
            "Arjun",
            "Vivaan",
            "Reyansh",
            "Mohammed",
            "Sai",
            "Arnav",
            "Aayan",
            "Krishna",
            "Ishaan",
            "Shaurya",
            "Atharva",
            "Advik",
            "Pranav",
            "Advaith",
            "Aaryan",
            "Dhruv",
            "Kabir",
            "Ritvik",
            "Aarush",
            "Kian",
            "Darsh",
            "Veer",
            "Bedi",
            "Gandhi",
            "Parekh",
            "Kohli",
            "Ahluwalia",
            "Chandra",
            "Jha",
            "Khanna",
            "Bajwa",
            "Chawla",
            "Lal",
            "Anand",
            "Gill",
            "Chakrabarti",
            "Dubey",
            "Kapoor",
            "Khurana",
            "Modi",
            "Kulkarni",
            "Khatri",
            "Kaur",
            "Dhillon",
            "Kumar",
            "Gupta",
            "Naidu",
            "Das",
            "Jain",
            "Chowdhury",
            "Dalal",
            "Thakur",
            "Gokhale",
            "Apte",
            "Sachdev",
            "Mehta",
            "Ganguly",
            "Bhasin",
            "Mannan",
            "Ahuja",
            "Singh",
            "Bakshi",
            "Basu",
            "Ray",
            "Mani",
            "Datta",
            "Balakrishna",
            "Biswas",
            "Laghari",
            "Malhotra",
            "Dewan",
            "Purohit");
        $password = "\$2y\$10\$xeemf4sulSXoYzMy0EjxFOWI85nuu9zh2hmEqFc7jj0hFjCoMAADq";
        $user_entry = array("1", "2", "3", "4");
        //componey
        $user = new User;
        $user->id = 100;
        $user->role_id = 0;
        $user->name = "Admin";
        $user->first_name = "Admin";
        $user->email = "admin@vieroots.com";
        $user->password = $password;
        $user->status = "approved";
        $user->completed = 1;
        $user->active = 1;
        $user->user_entry = "3";
        $user->save();
        $this->mlmSeeder($user);
        for ($i = 2; $i < 2; $i++) {
            $name = $names[rand(0, 108)];
            $user = new User;
            $user->id = $i;
            $role_id = $roll[0];

            $user->role_id = $role_id;
            $user->name = $name;
            $user->active = 0;
            $user->first_name = $name;
            $user->email = strtolower("$name" . time() . rand(0, 1000000) . rand(0, 500000) . rand(0, 500000000) . "@gmail.com");
            $user->password = $password;
            $user->status = "approved";
            $user->completed = 1;

            $user->user_entry = $role_id == 3 ? $user_entry[rand(2, 3)] : $user_entry[rand(0, 2)];
            $user->save();
            UserDetails::insert(array("user_id" => $i, "gender" => "", "date_of_birth" => date("Y-m-d H:i:s"), "address_1" => "", "address_2" => "", "aadhar_card_no" => "", "pan_card_no" => "", "district_state" => "", "country" => "", "location" => "", "latitude" => "", "longitude" => ""));
            $this->mlmSeeder($user);
        }
    }

    private function mlmSeeder($user)
    {
        $mlm = new Mlm;
        $mlm->user_id = $user->id;
        $mlm->name = $user->name;
        $parent = $user->id == 1 ? 0 : ($user->role_id == 3 ? $this->ma_parents[rand(0, sizeof($this->ma_parents) - 1)] : $this->all_parents[rand(0, sizeof($this->all_parents) - 1)]);
        $mlm->parent = $parent;
        $mlm->paid_rank = 0;
        $mlm->payable_rank = 0;
        $mlm->role_id = $user->role_id;
        $mlm->depth = 0;
        $mlm->wa_active = 0;
        $total_amount = rand(10000.0000, 100000.000);
        $mlm->total_price = $total_amount;
        $pv = $total_amount / self::PV_AMOUNT;
        $mlm->pv = ($user->role_id == 1) || ($user->role_id == 0) ? $pv : 0;
        $mlm->bv = ($user->role_id == 1) || ($user->role_id == 0) ? $pv * self::BV_RATE : 0;
        $mlm->pgv = 0;
        $mlm->pvflow = 0;
        $mlm->save();


        $this->ancestorsSeeder($mlm);
    }

    private function ancestorsSeeder($mlm)
    {
        if ($mlm->user_id == 1)
            return;
        $ancestors = DB::select(DB::raw("SELECT REPLACE(user_id," . $mlm->parent . "," . $mlm->user_id . ") as user_id,parent_id,orders,role_id FROM ancestors WHERE user_id!=ancestors.parent_id AND user_id =" . $mlm->parent));
        $ancestors = array_map(function ($value) {
            return (array)$value;
        }, $ancestors);

        $parent_data = Mlm::where('user_id', $mlm->parent)->first();
        $count = Ancestor::where('user_id', $mlm->parent)->max('orders');
        array_push($ancestors, array('user_id' => $mlm->user_id, 'parent_id' => $mlm->parent, 'role_id' => $parent_data->role_id, 'orders' => $count ? $count + 1 : 1));
        array_push($ancestors, array('user_id' => $mlm->user_id, 'parent_id' => $mlm->user_id, 'role_id' => $mlm->role_id, 'orders' => $count ? $count + 1 : 1));
        Ancestor::insert($ancestors);
        UpdatePgv::dispatch($mlm->user_id, $mlm->pv);
        if ($mlm->role_id == 1)
            array_push($this->ma_parents, $mlm->user_id);
        if ($mlm->role_id != 4)
            array_push($this->all_parents, $mlm->user_id);
    }

//    public function clearTable(Request $request)
//    {
//
//        $password = $request->get('check_truncate_value');
//        if ($password == 'a') {
//            $tables = array("ancestors",
//                "failed_jobs",
//                "jobs",
//                "mlm",
//                "users",
//                "user_details",
//                "user_pgv",
//                "user_pv",
//                "user_permissions",
//                "user_payment_mode",
//                "user_staffs",
//                "user_wallets");
//            foreach ($tables as $table) {
//                DB::table($table)->truncate();
//            }
//            return redirect()->back()->with('smessage', 'Tables Dropped');
//
//        } else {
//            return redirect()->back()->with('emessage', 'Password Incorrect');
//
//        }
//
//
//    }

    public function calcRetail()
    {
        $wa = Mlm::where(['role_id' => 1])->get();
        $min = RetailSettings::min('range_to') - 1;
        $max = RetailSettings::max('range_to');

        foreach ($wa as $value) {

            if ($value->total_price > $min) {
                $current_amount = $value->total_price;

                $userwallet = new UserWallet;

                $salb_perc = DB::select(DB::raw("SELECT percentage from retail_settings where range_to <= " . $current_amount . " AND (range_from >= " . $current_amount . " OR range_from =-1 )"));

                $userwallet->reward_amount = (($current_amount) * $salb_perc[0]->percentage) / 100; //todo Retail on price 
                $userwallet->user_id = $value->user_id;
                $userwallet->percentage = $salb_perc[0]->percentage;
                $userwallet->calculation_type = 1;
                $userwallet->payed_status = 0;
                $userwallet->pv = $current_amount / self::PV_AMOUNT;
                $userwallet->price = $value->total_price;

                $userwallet->save();

            }

        }
        return redirect()->back()->with('smessage', 'Retail Calculated');
    }


    public function calcSlab()
    {
        $wa = Mlm::where(['role_id' => 1])->get();
        $slab_settings = SlabSettings::get();
        // dd($slab_settings);
        $min = SlabSettings::min('range_to') - 1;
        $max = SlabSettings::max('range_to');

        foreach ($wa as $value) {
            if ($value->total_price < self::ACTIVE_EXECUTIVE) {
                $value->active_executive = 0;
                $value->save();
            }
            if ($value->total_price > $min) {
                $current_amount = $value->total_price;

                $userwallet = new UserWallet;

                $salb_perc = DB::select(DB::raw("SELECT percentage from slab_settings where range_to <= " . $current_amount . " AND (range_from >= " . $current_amount . " OR range_from =-1 )"));

                $userwallet->reward_amount = ((($current_amount / self::PV_AMOUNT) * self::BV_RATE) * $salb_perc[0]->percentage) / 100; //todo Salab on pv
                $userwallet->user_id = $value->user_id;
                $userwallet->percentage = $salb_perc[0]->percentage;
                $userwallet->calculation_type = 2;
                $userwallet->payed_status = 0;
                $userwallet->price = $value->total_price;
                $userwallet->pv = $current_amount / self::PV_AMOUNT;
                $userwallet->bv = ($current_amount / self::PV_AMOUNT) * self::BV_RATE;

                $userwallet->save();

            }

        }
        return redirect()->back()->with('smessage', 'Slab Calculated');

    }

    public function findExecutive()
    {
        $needed_pgv = self::EXCUTIVE_BENIFIT / self::PV_AMOUNT;
        $max_order = Ancestor::max('orders');

        for ($i = $max_order; $i > 0; $i--) {
            $users = Ancestor::where(['orders' => $i])->whereRaw('user_id = parent_id')->where('role_id', 1)->get();
            foreach ($users as $key => $user) {
                $mlm = Mlm::where(['user_id' => $user->user_id, 'role_id' => 1])->where('pgv', '>=', $needed_pgv)->first();
                if (!$mlm) {
                    continue;
                }
                $mlm->is_executive = 1;
                $mlm->save();
                if ($mlm->active_executive == 0)
                    continue;
                $sum = Mlm::where(['parent' => $user->user_id, 'role_id' => 1])->sum("total_price");
                $userwallet = new UserWallet;
                $userwallet->user_id = $user->user_id;
                $userwallet->percentage = self::EXCUTIVE_BENIFIT_PERS;
                $userwallet->calculation_type = 3;
                $userwallet->payed_status = 0;
                $userwallet->reward_amount = ((($sum / self::PV_AMOUNT) * self::BV_RATE) * self::EXCUTIVE_BENIFIT_PERS) / 100.00;
                $userwallet->pv = $sum / self::PV_AMOUNT;
                $userwallet->bv = ($sum / self::PV_AMOUNT) * self::BV_RATE;
                $userwallet->price = $sum;

                $userwallet->save();

            }
        }
        return redirect()->back()->with('smessage', 'Executives Derived');

    }


    public function findExAmount()
    {
//        DB::update(" update mlm set realtime_pgv=pgv,tlpgv=pgv");
//        exit();
        // DB::table('mlm')->update(array('pgv' => 0));

        $max_order = Ancestor::max('orders');
        for ($i = $max_order; $i > 0; $i--) {
            $users = Ancestor::where(['orders' => $i])->whereRaw('user_id = parent_id')->where('role_id', 1)->get();
//            dump($users);

            foreach ($users as $key => $user) {
                $me = Mlm::where(['user_id' => $user->user_id])->first();
                $total_price = 0 + $me->total_price;


                $query = "call getsWA(" . $user->user_id . ")";
                $wausers = DB::select($query);

                //                     $users = array_map(function ($value) {
                //     return (array)$value;
                // }, $users);\

                foreach ($wausers as $child) {

                    if (($child->role_id == 1 && $child->is_executive == 0))
                        $total_price += $child->executive_amount;
                }

//dd($user,$total_price);

//                foreach ($childs as $child) {
//                    if (($child->role_id == 1 && $child->pgv < 2000) )
//                        $pgv += $child->pgv;
//
//                }


                $me->executive_amount = $total_price;


                $result = $me->save();
                //  if($me->user_id==88)
                //  dd($result);
            }
        }
        return redirect()->back()->with('smessage', 'Found BH Amount');


    }

    public function findbh()
    {
        $max_order = Ancestor::max('orders');
        $gen = BhSettings::orderBy('generation', 'ASC')->get();
        for ($i = $max_order; $i > 0; $i--) {
            $users = Ancestor::where(['orders' => $i])->whereRaw('user_id = parent_id')->where('role_id', 1)->get();
            foreach ($users as $key => $user) {
//                       $activeCount=Mlm::where(['parent'=>$user->user_id,'role_id'=>1])->count('*');
                $activeExecutiveCount = "call getExecutive(" . $user->user_id . "," . self::TL_ACTIVE . ")";
                $dt = DB::select($activeExecutiveCount);

                $activeCount = $dt ? count($dt) : 0;
                $mlm = Mlm::where(['user_id' => $user->user_id, 'role_id' => 1])->where('is_executive', 1)->first();

                if (!$mlm) {
                    continue;
                }
                $parents = array();
                foreach ($gen as $generation) {
                    if ($generation->executive_count <= $activeCount) {
                        $mlm->is_bh = 1;
                        $mlm->save();
                        $userwallet = new UserWallet;
                        $userwallet->user_id = $user->user_id;
                        $userwallet->percentage = $generation->percentage;
                        $userwallet->calculation_type = 4;
                        $userwallet->payed_status = 0;

                        if ($generation->generation == 0) {
                            $userwallet->reward_point = ((($mlm->executive_amount / self::PV_AMOUNT) * self::BV_RATE) * $generation->percentage) / 100;
                            $parents = array($mlm);
                            $pgv = $mlm->executive_amount;

                        } else {

                            $data = $this->calcChildBHAmount($parents);

                            $userwallet->reward_point = ((($data['pgv'] / self::PV_AMOUNT) * self::BV_RATE) * $generation->percentage) / 100;
                            $pgv = $data['pgv'];
                            $parents = $data['childs'];
                            //    if($mlm->user_id==8)
                            //     dd($data);
                        }
                        $userwallet->pv = 0;
                        $userwallet->price = $pgv;
                        $userwallet->pgv = $pgv / self::PV_AMOUNT;

                        $userwallet->save();


                    } else {
                        break;
                    }
                }

            }
        }
        $this->rewardbh();
        $wallet_users = UserWallet::where('calculation_type', 4)->where('reward_amount', '>', 0)->get();
        foreach ($wallet_users as $user) {
            $new_mlm = Mlm::find($user->user_id);
            $new_mlm->designation = 1;
            $new_mlm->save();
        }
        return redirect()->back()->with('smessage', 'BH Pool Calculation Done');

    }

    private function calcChildBHAmount($parents) //Calculate child pgv
    {

        $childs = array();
        $pgv = 0;
        foreach ($parents as $parent) {
            $user_id = $parent->user_id;
            $query = "call getExecutive(" . $user_id . "," . self::TL_ACTIVE . ")";

            $users = DB::select($query);

            //                     $users = array_map(function ($value) {
            //     return (array)$value;
            // }, $users);

            foreach ($users as $user) {

                $pgv += $user->executive_amount;
                array_push($childs, $user);
            }
        }

        return array('childs' => $childs, 'pgv' => $pgv);
    }

    private function rewardbh()
    {
        $company = MLM::where('user_id', 1)->first();
        $reward_point = UserWallet::where('calculation_type', 4)->sum('reward_point');
        if ($reward_point > 0) {
            $company_volume = ($company->pgv * self::BH_POOL) / 100.00;
            $single_point = $company_volume / $reward_point;
            $query = "update user_wallets set reward_amount=reward_point*" . $single_point . " WHERE calculation_type=4 ";

            $wausers = DB::select($query);
        }
    }


    /************************************** BM Calculation start here ***********************************/


    public function findBhAmount()
    {
//        DB::update(" update mlm set realtime_pgv=pgv,tlpgv=pgv");
//        exit();
        // DB::table('mlm')->update(array('pgv' => 0));

        $max_order = Ancestor::max('orders');
        for ($i = $max_order; $i > 0; $i--) {
            $users = Ancestor::where(['orders' => $i])->whereRaw('user_id = parent_id')->where('role_id', 1)->get();
//            dump($users);

            foreach ($users as $key => $user) {
                $me = Mlm::where(['user_id' => $user->user_id])->first();
                $total_price = 0 + $me->total_price;


                $query = "call getsWA(" . $user->user_id . ")";
                $wausers = DB::select($query);

                //                     $users = array_map(function ($value) {
                //     return (array)$value;
                // }, $users);\

                foreach ($wausers as $child) {

                    if (($child->role_id == 1 && $child->is_bh == 0))
                        $total_price += $child->bh_amount;
                }

//dd($user,$total_price);

//                foreach ($childs as $child) {
//                    if (($child->role_id == 1 && $child->pgv < 2000) )
//                        $pgv += $child->pgv;
//
//                }


                $me->bh_amount = $total_price;


                $result = $me->save();
                //  if($me->user_id==88)
                //  dd($result);
            }
        }
        return redirect()->back()->with('smessage', 'Found BM Amount');


    }


    public function findbm()
    {
        $max_order = Ancestor::max('orders');
        $gen = BmSettings::orderBy('generation', 'ASC')->get();
        for ($i = $max_order; $i > 0; $i--) {
            $users = Ancestor::where(['orders' => $i])->whereRaw('user_id = parent_id')->where('role_id', 1)->get();
            foreach ($users as $key => $user) {
//                       $activeCount=Mlm::where(['parent'=>$user->user_id,'role_id'=>1])->count('*');
                $activeExecutiveCount = "call getBh(" . $user->user_id . ")";
                $dup = DB::select("call getBmLEG(" . $user->user_id . ")");

                $dt = DB::select($activeExecutiveCount);

                $activeCount = $dt ? count($dt) : 0;
                $mlm = Mlm::where(['user_id' => $user->user_id, 'role_id' => 1])->where('is_executive', 1)->first();

                if (!$mlm) {
                    continue;
                }

                foreach ($dup as $val) {

                    $activeCount = $activeCount - ($val->count - 1);
                }


                $parents = array();
                foreach ($gen as $generation) {
                    if ($generation->bh_count <= $activeCount) {
                        $mlm->is_bm = 1;
                        $mlm->save();
                        $userwallet = new UserWallet;
                        $userwallet->user_id = $user->user_id;
                        $userwallet->percentage = $generation->percentage;
                        $userwallet->calculation_type = 5;
                        $userwallet->payed_status = 0;

                        if ($generation->generation == 0) {
                            $userwallet->reward_point = ((($mlm->bh_amount / self::PV_AMOUNT) * self::BV_RATE) * $generation->percentage) / 100;
                            $parents = array($mlm);
                            $pgv = $mlm->bh_amount;

                        } else {

                            $data = $this->calcChildBMAmount($parents);

                            $userwallet->reward_point = ((($data['pgv'] / self::PV_AMOUNT) * self::BV_RATE) * $generation->percentage) / 100;
                            $pgv = $data['pgv'];
                            $parents = $data['childs'];
                            //    if($mlm->user_id==8)
                            //     dd($data);
                        }
                        $userwallet->pv = 0;
                        $userwallet->price = $pgv;
                        $userwallet->pgv = $pgv / self::PV_AMOUNT;

                        $userwallet->save();


                    } else {
                        break;
                    }
                }

            }
        }
        $this->rewardbm();
        $wallet_users = UserWallet::where('calculation_type', 5)->where('reward_amount', '>', 0)->get();
        foreach ($wallet_users as $user) {
            $new_mlm = Mlm::find($user->user_id);
            $new_mlm->designation = 1;
            $new_mlm->save();
        }
        return redirect()->back()->with('smessage', 'BM Pool Calculation Done');

    }

    private function calcChildBMAmount($parents) //Calculate child pgv
    {

        $childs = array();
        $pgv = 0;
        foreach ($parents as $parent) {
            $user_id = $parent->user_id;
            $query = "call getBh(" . $user_id . ")";

            $users = DB::select($query);

            //                     $users = array_map(function ($value) {
            //     return (array)$value;
            // }, $users);

            foreach ($users as $user) {

                $pgv += $user->bh_amount;
                array_push($childs, $user);
            }
        }

        return array('childs' => $childs, 'pgv' => $pgv);
    }

    private function rewardbm()
    {
        $company = MLM::where('user_id', 1)->first();
        $reward_point = UserWallet::where('calculation_type', 5)->sum('reward_point');
        if ($reward_point > 0) {
            $company_volume = ($company->pgv * self::BM_POOL) / 100.00;
            $single_point = $company_volume / $reward_point;
            $query = "update user_wallets set reward_amount=reward_point*" . $single_point . " WHERE calculation_type=5 ";

            $wausers = DB::select($query);
        }
    }


    /*************************************************************BM calaculation stop here **********************/


    /************************************************GM calculation srart Here **********************************/


    public function findBmAmount()
    {
//        DB::update(" update mlm set realtime_pgv=pgv,tlpgv=pgv");
//        exit();
        // DB::table('mlm')->update(array('pgv' => 0));

        $max_order = Ancestor::max('orders');
        for ($i = $max_order; $i > 0; $i--) {
            $users = Ancestor::where(['orders' => $i])->whereRaw('user_id = parent_id')->where('role_id', 1)->get();
//            dump($users);

            foreach ($users as $key => $user) {
                $me = Mlm::where(['user_id' => $user->user_id])->first();
                $total_price = 0 + $me->total_price;


                $query = "call getsWA(" . $user->user_id . ")";
                $wausers = DB::select($query);

                //                     $users = array_map(function ($value) {
                //     return (array)$value;
                // }, $users);\

                foreach ($wausers as $child) {

                    if (($child->role_id == 1 && $child->is_bm == 0))
                        $total_price += $child->bm_amount;
                }

//dd($user,$total_price);

//                foreach ($childs as $child) {
//                    if (($child->role_id == 1 && $child->pgv < 2000) )
//                        $pgv += $child->pgv;
//
//                }


                $me->bm_amount = $total_price;


                $result = $me->save();
                //  if($me->user_id==88)
                //  dd($result);
            }
        }
        return redirect()->back()->with('smessage', 'Found GM Amount');


    }


    public function findgm()
    {
        $max_order = Ancestor::max('orders');
        $gen = GmSettings::orderBy('generation', 'ASC')->get();
        for ($i = $max_order; $i > 0; $i--) {
            $users = Ancestor::where(['orders' => $i])->whereRaw('user_id = parent_id')->where('role_id', 1)->get();
            foreach ($users as $key => $user) {
//                       $activeCount=Mlm::where(['parent'=>$user->user_id,'role_id'=>1])->count('*');
                $activeExecutiveCount = "call getBm(" . $user->user_id . ")";
                $dup = DB::select("call getGmLEG(" . $user->user_id . ")");

                $dt = DB::select($activeExecutiveCount);

                $activeCount = $dt ? count($dt) : 0;
                $mlm = Mlm::where(['user_id' => $user->user_id, 'role_id' => 1])->where('is_executive', 1)->first();

                if (!$mlm) {
                    continue;
                }

                foreach ($dup as $val) {

                    $activeCount = $activeCount - ($val->count - 1);
                }


                $parents = array();
                foreach ($gen as $generation) {
                    if ($generation->bh_count <= $activeCount) {
                        $mlm->is_gm = 1;
                        $mlm->save();
                        $userwallet = new UserWallet;
                        $userwallet->user_id = $user->user_id;
                        $userwallet->percentage = $generation->percentage;
                        $userwallet->calculation_type = 6;
                        $userwallet->payed_status = 0;

                        if ($generation->generation == 0) {
                            $userwallet->reward_point = ((($mlm->bm_amount / self::PV_AMOUNT) * self::BV_RATE) * $generation->percentage) / 100;
                            $parents = array($mlm);
                            $pgv = $mlm->bm_amount;

                        } else {

                            $data = $this->calcChildGMAmount($parents);

                            $userwallet->reward_point = ((($data['pgv'] / self::PV_AMOUNT) * self::BV_RATE) * $generation->percentage) / 100;
                            $pgv = $data['pgv'];
                            $parents = $data['childs'];
                            //    if($mlm->user_id==8)
                            //     dd($data);
                        }
                        $userwallet->pv = 0;
                        $userwallet->price = $pgv;
                        $userwallet->pgv = $pgv / self::PV_AMOUNT;

                        $userwallet->save();


                    } else {
                        break;
                    }
                }

            }
        }
        $this->rewardgm();
        $wallet_users = UserWallet::where('calculation_type', 6)->where('reward_amount', '>', 0)->get();
        foreach ($wallet_users as $user) {
            $new_mlm = Mlm::find($user->user_id);
            $new_mlm->designation = 1;
            $new_mlm->save();
        }
        return redirect()->back()->with('smessage', 'GM Pool Calculation Done');

    }

    private function calcChildGMAmount($parents) //Calculate child pgv
    {

        $childs = array();
        $pgv = 0;
        foreach ($parents as $parent) {
            $user_id = $parent->user_id;
            $query = "call getBm(" . $user_id . ")";

            $users = DB::select($query);

            //                     $users = array_map(function ($value) {
            //     return (array)$value;
            // }, $users);

            foreach ($users as $user) {

                $pgv += $user->bm_amount;
                array_push($childs, $user);
            }
        }

        return array('childs' => $childs, 'pgv' => $pgv);
    }

    private function rewardgm()
    {
        $company = MLM::where('user_id', 1)->first();
        $reward_point = UserWallet::where('calculation_type', 6)->sum('reward_point');
        if ($reward_point > 0) {
            $company_volume = ($company->pgv * self::GM_POOL) / 100.00;
            $single_point = $company_volume / $reward_point;
            $query = "update user_wallets set reward_amount=reward_point*" . $single_point . " WHERE calculation_type=6 ";

            $wausers = DB::select($query);
        }
    }


    /***********************************************GM calculation End here ************************************/


    public function findPGV()
    {
        DB::update(" update mlm set realtime_pgv=pgv,pgv=0");
        // DB::table('mlm')->update(array('pgv' => 0));
        $max_order = Ancestor::max('orders');
        for ($i = $max_order; $i > 0; $i--) {
            $users = Ancestor::where(['orders' => $i])->whereRaw('user_id = parent_id')->get();
            foreach ($users as $key => $user) {

                $me = Mlm::where(['user_id' => $user->user_id])->first();
                $childs = Mlm::where(['parent' => $user->user_id])->get();


                $pgv = 0 + $me->pv;

                foreach ($childs as $child) {
                    if (($child->role_id == 1 && $child->pv < 50) || ($child->role_id != 1))
                        $pgv += $child->pgv;

                }


                $me->pgv = $pgv;
                $me->tlpgv = $pgv;
                $result = $me->save();
                //  if($me->user_id==88)
                //  dd($result);

            }
        }


        $me = Mlm::where(['user_id' => 1])->first();

        $childs = Mlm::where(['parent' => $me->user_id])->get();

        $pgv = 0 + $me->pv;
        foreach ($childs as $child) {

            if (($child->role_id == 1 && $child->pv < 50) || ($child->role_id != 1))
                $pgv += $child->pgv;


        }

        $me->pgv = $pgv;
        $me->tlpgv = $pgv;
        $me->save();

    }

    public function findTLPGV()
    {
//        DB::update(" update mlm set realtime_pgv=pgv,tlpgv=pgv");
//        exit();
        // DB::table('mlm')->update(array('pgv' => 0));
        $max_order = Ancestor::max('orders');
        for ($i = $max_order; $i > 0; $i--) {
            $users = Ancestor::where(['orders' => $i])->whereRaw('user_id = parent_id')->where('role_id', 1)->get();
//            dump($users);

            foreach ($users as $key => $user) {
                $me = Mlm::where(['user_id' => $user->user_id])->first();
                $pgv = 0 + $me->pgv;


                $query = "call getsWA(" . $user->user_id . ")";

                $wausers = DB::select($query);

                //                     $users = array_map(function ($value) {
                //     return (array)$value;
                // }, $users);
                foreach ($wausers as $child) {

                    if (($child->role_id == 1 && $child->tl_status == 0))
                        $pgv += $child->pgv;
                }


//                foreach ($childs as $child) {
//                    if (($child->role_id == 1 && $child->pgv < 2000) )
//                        $pgv += $child->pgv;
//
//                }
                $activeTLCount = "call getTL(" . $user->user_id . "," . self::TL_ACTIVE . ")";
                $activecount = DB::select($activeTLCount);

                $tl_active = TlSettings::where('generation', count($activecount))->first();

                $me->tl_status = $tl_active->side_volume <= $pgv ? 1 : 0;

                $me->save();
                $me->tlpgv = $pgv;
                $result = $me->save();
                //  if($me->user_id==88)
                //  dd($result);
            }
        }


    }

    public function findTBB()
    {
        $parents = array();
        $max_order = Ancestor::max('orders');
        $gen = TbbSettings::orderBy('generation', 'ASC')->get();
        //  dd($gen);
        for ($i = $max_order; $i > 0; $i--) {
            $users = Ancestor::where(['orders' => $i])->whereRaw('user_id = parent_id')->where('role_id', 1)->get();


            foreach ($users as $key => $user) {
//                       $activeCount=Mlm::where(['parent'=>$user->user_id,'role_id'=>1])->count('*');
                $activeWLCount = "call getActiveWA(" . $user->user_id . "," . self::WA_ACTIVE . ")";
                $users = DB::select($activeWLCount);
                $activeCount = $users ? count($users) : 0;
                $mlm = Mlm::where(['user_id' => $user->user_id, 'role_id' => 1])->where('wa_active', 1)->first();
                if (!$mlm) {
                    continue;
                }
                foreach ($gen as $generation) {
                    if ($generation->activeLegCount <= $activeCount) {
                        $userwallet = new UserWallet;
                        $userwallet->user_id = $user->user_id;
                        $userwallet->percentage = $generation->percentage;
                        $userwallet->calculation_type = 3;
                        $userwallet->payed_status = 0;


                        if ($generation->generation == 1) {
                            $userwallet->reward_amount = (($mlm->pgv * self::BV_RATE) * $generation->percentage) / 100;
                            $parents = array($mlm);
                            $pgv = $mlm->pgv;
                        } else {
                            $data = $this->calcChildPv($parents);
                            $userwallet->reward_amount = (($data['pgv'] * self::BV_RATE) * $generation->percentage) / 100;
                            $pgv = $data['pgv'];
                            $parents = $data['childs'];
                            //    if($mlm->user_id==8)
                            //     dd($data);
                        }
                        $userwallet->pv = 0;
                        $userwallet->pgv = $pgv;
                        $mlm->save();
                        $userwallet->save();
                    } else {
                        break;
                    }
                }
            }
        }
    }

    private function calcChildPv($parents) //Calculate child pgv
    {

        $childs = array();
        $pgv = 0;
        foreach ($parents as $parent) {
            $user_id = $parent->user_id;
            $query = "call getActiveWA(" . $user_id . "," . self::WA_ACTIVE . ")";

            $users = DB::select($query);
            //                     $users = array_map(function ($value) {
            //     return (array)$value;
            // }, $users);
            foreach ($users as $user) {

                $pgv += $user->pgv;
                array_push($childs, $user);
            }
        }
        return array('childs' => $childs, 'pgv' => $pgv);
    }

    public function findTLcalc()
    {
        $parents = array();
        $max_order = Ancestor::max('orders');
        $gen = TlSettings::orderBy('generation', 'ASC')->get();


        for ($i = $max_order; $i > 0; $i--) {
            $users = Ancestor::where(['orders' => $i])->whereRaw('user_id = parent_id')->where('role_id', 1)->get();


            foreach ($users as $key => $user) {
//                       $activeCount=Mlm::where(['parent'=>$user->user_id,'role_id'=>1])->count('*');
                $activeTLCount = "call getTL(" . $user->user_id . "," . self::TL_ACTIVE . ")";
                $users = DB::select($activeTLCount);

                $activeCount = $users ? count($users) : 0;
                $mlm = Mlm::where(['user_id' => $user->user_id, 'role_id' => 1])->where('wa_active', 1)->where('tl_status', 1)->first();

                if (!$mlm) {
                    continue;
                }

                foreach ($gen as $generation) {

                    if ($generation->active_tl_count <= $activeCount) {

                        $userwallet = new UserWallet;
                        $userwallet->user_id = $user->user_id;
                        $userwallet->percentage = $generation->percentage;
                        $userwallet->calculation_type = 4;
                        $userwallet->payed_status = 0;


                        if ($generation->generation == 0) {
                            $userwallet->reward_amount = (($mlm->tlpgv) * $generation->percentage) / 100;
                            $parents = array($mlm);
                            $pgv = $mlm->tlpgv;

                        } else {

                            $data = $this->calcChildPGVTL($parents);
                            $userwallet->reward_amount = (($data['pgv']) * $generation->percentage) / 100;
                            $pgv = $data['pgv'];
                            $parents = $data['childs'];
                            //    if($mlm->user_id==8)
                            //     dd($data);
                        }
                        $userwallet->pv = 0;
                        $userwallet->pgv = $pgv;
                        if ($mlm->highest_tl_count > $activeCount)
                            $mlm->highest_tl_count = $activeCount;
                        $mlm->tl_count = $activeCount;
                        $mlm->save();
                        $userwallet->save();
                    } else {
                        break;
                    }
                }
            }
        }
    }

    private function calcChildPGVTL($parents) //Calculate child pgv
    {
        $parents = array();
        $childs = array();
        $pgv = 0;
        foreach ($parents as $parent) {
            $user_id = $parent->user_id;
            $query = "call getTL(" . $user_id . "," . self::TL_ACTIVE . ")";

            $users = DB::select($query);

            //                     $users = array_map(function ($value) {
            //     return (array)$value;
            // }, $users);

            foreach ($users as $user) {

                $pgv += $user->tlpgv;
                array_push($childs, $user);
            }
        }

        return array('childs' => $childs, 'pgv' => $pgv);
    }


}