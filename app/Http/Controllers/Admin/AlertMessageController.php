<?php

namespace App\Http\Controllers\Admin;


use App\Activity;
use App\Models\AlertMessage;
use App\Models\Cms;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AlertMessageController extends Controller
{
    /**
     * AlertMessage constructor.
     */
    public function __construct(){

    }

    /**
     * @return $this
     */
    public function index(){
        $messages = AlertMessage::join('cms', 'cms.slug', 'alert_messages.page_slug')->select('alert_messages.*','cms.title as cms_title','cms.slug as slug')->groupBy('cms.title')->get();
        // dd($messages[0]->page_slug);
        $cartMessage    = AlertMessage::where('page_slug','cart')->get();
        if(count($messages)>0){
            $homeMessage    = AlertMessage::where('page_slug',$messages[0]->page_slug)->get();
        }else{
            $homeMessage    =  array();
        }
        $checkoutMessage    = AlertMessage::where('page_slug','checkout')->get();
        return view('admin.alert-messages.index')->with(['messages' => $messages,'cartMessage' => $cartMessage,'homeMessage' => $homeMessage,'checkoutMessage' => $checkoutMessage]);
    }

    /**
     * @return $this
     */
    public function getAlertMessages(Request $request){
        $homeMessage    = AlertMessage::where('page_slug', $request->input('slug'))->get();
        return response()->json([
            'success' => true,
            'html' => view('admin.alert-messages.alert-message')->with(array(
                'homeMessage' => $homeMessage))->render()
        ], 200);
    }

    /**
     * @return $this
     */
    public function deleteAlertMessages(Request $request){
        AlertMessage::where('id', $request->input('id'))->delete();
        return "success";
    }

    /**
     * @return $this
     */
    public function changeData(Request $request){
        $alertupdate = AlertMessage::find($request->input('id'));
        $alertupdate->title = $request->input('title');
        $alertupdate->message = $request->input('message');
        $alertupdate->save();
        return "success";

    }


    /**
     * @return $this
     */
    public function createForm(){
        $pages = Cms::pluck('title','slug');
        return view('admin.alert-messages.create')->with(['pages' => $pages]);
    }


    public function create(Request $request){

        $r = [
            'pages' => 'required',
            'title' => 'required|max:200',
            'session_key' => 'required',
        ];

        $v = Validator::make($request->all(), $r);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())->withInput();
        }


        $alert = new AlertMessage();
        $alert->page_slug = $request->input('pages');
        $alert->title = $request->input('title');
        $alert->session_key = $request->input('session_key');
        $alert->message = ($request->input('message')) ? $request->input('message') : '';
        $alert->language = $request->input('language');

        $alert->save();

        /****************** Activity Adding ************/
        $activity = new Activity();
        $activity->user_id = Auth::id();
        $activity->relation_id = $alert->id;
        $activity->type = 'alert-messages';
        $activity->activity = "messages  Created by:";
        $activity->save();

        return redirect()->back()->with('smessage', 'New page added.');
    }




}
