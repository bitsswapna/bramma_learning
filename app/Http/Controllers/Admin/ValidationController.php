<?php

namespace App\Http\Controllers\Admin;


use App\Activity;
use App\Models\ProductTaxes;
use App\Models\StateList;
use App\Role;
use App\User;
use App\Models\Product;
use App\Models\Address;
use App\Models\Stock;
use App\Models\StockLog;
use App\Models\Mlm;
use App\Models\DynamicStatus;
use App\Models\UserPV;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Http\Controllers\Controller;
use App\UserDetails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Input;
use App\Models\OrderPayments;
use App\Jobs\UpdatePgv;
use Carbon\Carbon;


class ValidationController extends Controller
{
    private $_messages =  array(
        'username.required'=>'Please enter a valid username',
        'email.required'=>'Please enter a valid email id',
        'email.email'=>'Please enter a valid email format',
        'email.string'=>'Please enter a valid email',
        'email.unique'=>'This email Id already taken',
        'first_name.required'=>'Please enter your first name',
        'first_name.string'=>'Please enter your name',
        'first_name.max'=>'Please provide your name in :max characters',
        'last_name.required'=>'Please enter your last name',
        'last_name.string'=>'Please enter your last name',
        'last_name.max'=>'Please provide your name in :max characters',
        'address1.required'=>'Please enter a valid address',
        'address1.string'=>'Please enter a valid address',
        'address2.required'=>'Please enter a valid address',
        'address2.string'=>'Please enter a valid address',
        'location.required'=>'Please enter a valid location',
        'location.string'=>'Please enter a valid location',
        'city.required'=>'Please enter a valid city',
        'city.string'=>'Please enter a valid city',
        'state.required'=>'Please enter a valid state',
        'state.string'=>'Please enter a valid state',
        'postcode.required'=>'Please enter a valid pin code',
        'postcode.digits'=>'Please enter 6 digit post code',
        'country_id.required'=>'Please enter a valid country',
        'phone.required'=>'Please enter a valid phone number',
        'phone.regex'=>'Please enter valid phone number',
        'phone.digits'=>'Please enter 10 digit phone number',
        'mobile.required'=>'Please enter a valid mobile number',
        'mobile.regex'=>'Please enter valid mobile number',
        'mobile.digits'=>'Please enter 10 digit mobile number',
        'mobile.unique'=>'This mobile number already taken',
        'aadhar_card_no.required'=>'Please enter a valid aadhar card number',
        'aadhar_card_no.string'=>'Please enter a valid aadhar card number',
        'pan_card_no.required'=>'Please enter a valid pan card number',
        'pan_card_no.string'=>'Please enter a valid pan card number',
        'bank.required'=>'Please enter a valid bank',
        'bank.string'=>'Please enter a valid bank',
        'branch_code.required'=>'Please enter a valid branch code',
        'branch_code.string'=>'Please enter a valid branch code',
        'account_number.required'=>'Please enter a valid account number',
        'account_number.string'=>'Please enter a valid account number',
        'branch.required'=>'Please enter a valid branch',
        'branch.string'=>'Please enter a valid branch',
        'name_of_banker.required'=>'Please enter a valid name of banker',
        'name_of_banker.string'=>'Please enter a valid name of banker',
        'ifsc.required'=>'Please enter a valid ifsc',
        'ifsc.string'=>'Please enter a valid ifsc',

    );
    public function __construct()
    {

    }
    /*Form validation*/
    public function getAddressFormValidation(Request $request){
        $form_field = $request->input('form_field');
        $form_value = $request->input('form_value');
        $response['status'] = false;
        switch ($form_field){
            case 'phone':
            case 'mobile':
                $request['mobile'] = $form_value;
                $validated = \Validator::make($request->all(), [
                    'mobile'=>'required',
                ], $this->_messages);
                break;
            case 'first_name':
                $request['first_name'] = $form_value;
                $validated = \Validator::make($request->all(), [
                    'first_name' => 'required|string|max:100',
                ], $this->_messages);
                break;
             case 'email':
                $request['email'] = $form_value;
                $validated = \Validator::make($request->all(), [
                    'email' => 'required|string|email|max:225|unique:users',
                ], $this->_messages);
                break;
            case 'address1':
                $request['address1'] = $form_value;
                $validated = \Validator::make($request->all(), [
                    'address1' => 'required',
                ], $this->_messages);
                break;
            case 'aadhar_card_no':
                $request['aadhar_card_no'] = $form_value;
                $validated = \Validator::make($request->all(), [
                      'aadhar_card_no'=> 'required|regex:/[0-9]{12}/|digits:12|unique:user_details',
                ], $this->_messages);
                break;
            case 'pan_card_no':
                $request['pan_card_no'] = $form_value;
                $validated = \Validator::make($request->all(), [
                    'pan_card_no'=> 'required|regex:/([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})/|min:10|max:10|unique:user_details',
                ], $this->_messages);
                break;
            case 'bank':
                $request['bank'] = $form_value;
                $validated = \Validator::make($request->all(), [
                    'bank' => 'required',
                ], $this->_messages);
                break;
            case 'branch_code':
                $request['branch_code'] = $form_value;
                $validated = \Validator::make($request->all(), [
                    'branch_code' => 'required',
                ], $this->_messages);
                break;
            case 'account_number':
                $request['account_number'] = $form_value;
                $validated = \Validator::make($request->all(), [
                    'account_number' => 'required',
                ], $this->_messages);
                break;
            case 'branch':
                $request['branch'] = $form_value;
                $validated = \Validator::make($request->all(), [
                    'branch' => 'required',
                ], $this->_messages);
                break;
            case 'name_of_banker':
                $request['name_of_banker'] = $form_value;
                $validated = \Validator::make($request->all(), [
                    'name_of_banker' => 'required',
                ], $this->_messages);
                break;
            case 'ifsc':
                $request['ifsc'] = $form_value;
                $validated = \Validator::make($request->all(), [
                    'ifsc' => 'required',
                ], $this->_messages);
                break;

            case 'address2':
                $request['address2'] = $form_value;
                $validated = \Validator::make($request->all(), [
                    'address2' => 'required',
                ], $this->_messages);
                break;
            case 'city':
                $request['city'] = $form_value;
                $validated = \Validator::make($request->all(), [
                    'city' => 'required',
                ], $this->_messages);
                break;
            case 'location':
                $request['location'] = $form_value;
                $validated = \Validator::make($request->all(), [
                    'location' => 'required',
                ], $this->_messages);
                break;
            case 'state':
            case 'district_state':
                $request['state'] = $form_value;
                $validated = \Validator::make($request->all(), [
                    'state' => 'required',
                ], $this->_messages);
                break;
            case 'postcode':
            case 'pincode':
                $request['postcode'] = $form_value;
                $validated = \Validator::make($request->all(), [
                    'postcode'=>'required|digits:6',
                ], $this->_messages);
                break;

            default:
                $validated = array();
                break;
        }
        if ($validated) {
            if ($validated->fails()) {
                $response['status'] = false;
                $response['errors'] = $validated->getMessageBag()->toJson();

            } else {
                $response['status'] = true;
            }
        }
        return response()->json($response);
    }

}
