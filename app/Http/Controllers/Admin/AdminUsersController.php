<?php
/**
 * Created by PhpStorm.
 * User: COMPUTER
 * Date: 3/28/2018
 * Time: 11:17 AM
 */

namespace App\Http\Controllers\Admin;


use App\Activity;
use App\BankDetails;
use App\Http\Controllers\Controller;
use App\Libraries\Slug;
use App\Mail\AdminUserCreate;
use App\Mail\UserRegistration;
use App\Models\Address;
use App\Models\Ancestor;
use App\Models\CustomerCareCategory;
use App\Models\CustomerCareUsers;
use App\Models\Mlm;
use App\Models\StaffCustomer;
use App\Models\StateList;
use App\Models\UserDetails;
use App\Models\UserPaymentMode;
use App\Models\UserPermissions;
use App\Models\UserRegistrationRequest;
use App\Models\UserStaff;
use App\Models\WellnessAdvisors;
use App\Role;
use App\Setting;
use App\User;
use App\UsersAdd;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class AdminUsersController extends Controller
{

    /**
     * @return $this
     */
    var $ma_parents = array(1);
    var $all_parents = array(1);

    public function index()
    {
        return view('admin.admin-users.index');
    }

    public function waRequest()
    {
        return view('admin.admin-users.admin-user-wa');
    }

    public function userRequest()
    {
        return view('admin.admin-users.admin-user-website');
    }

    /**
     * @return table
     */
    public function getAdminUsers(Request $request)
    {
        $user_id = $request->get('user_id');
        $user_name = $request->get('user_name');
        $data = $request->get('form_data');
        $params = array();
        parse_str($data, $params);

        $data = User::join('roles', 'users.role_id', '=', 'roles.id')
            ->where(function ($query) use ($user_id, $user_name) {
                
                if ($user_id != '')
                    $query->where('users.id', $user_id);
                if ($user_name != '')
                    $query->where('users.name', 'like', '%' . $user_name . '%');
            })
            ->orderBy('users.id', 'DESC')
            ->select('users.*', 'roles.display_name')->get();
        return DataTables::of($data)
            ->editColumn('sl#', function ($model) {
                return '<span class="si_no"></span> ';
            })
            ->editColumn('user_id', function ($model) {
                return $model->id;
            })
            ->editColumn('name', function ($model) {
//                return $model->name;
                if ($model->getRoleDashboardType) {
                    // if ($model->getRoleDashboardType->user_type == 2) {
                    //     return '<a style="cursor: pointer;"  class=" link adminuser-login small"  data-url="{!! env(APP_URL)!!}/profile !!}"   data-user="' . $model->id . '" >&nbsp; ' . $model->name . ' <i class="fas fa-external-link-alt pull-left"></i></a>';
                    // } 
                    // else {
                        return $model->name;
                    // }
                }

            })

            ->editColumn('role', function ($model) {
                return '<span class="small badge badge-info">'.$model->display_name.'</span>';
            })
            ->editColumn('address', function ($model) {
              return $model->address;
            })
           
            ->editColumn('status', function ($model) {
                if (can('edit_admin_user')) {
                    $checked = '';
                    if ($model->active == 1) {
                        $checked = 'checked';
                    }
                    return '<div class="pretty p-switch p-fill" ><input type="checkbox" class="change-status" data-id="' . $model->id . '" ' . $checked . '><div class="state p-success" ><label ></label ></div></div>';
                }
            })
            ->editColumn('action', function ($model) {
                $html = '';
                if (can('edit_admin_user')) {
                    $html = '<a data-toggle="tooltip"  data-placement="top" data-html="true" title="Edit" class="btn action-button bg-primary-blue" href="' . url('admin_user/edit/' . $model->id) . '" ><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;';
                }
                //    if(can('edit_admin_user')){
                //        $html.= '<a data-toggle="tooltip"  data-placement="top" data-html="true" title="Delete" class="btn action-button bg-primary-red delete-user" href="#" data-id="'.$model->id.'" ><i class="fa fa-trash-o"></i></a>';
                //    }
                if (can('edit_admin_user')) {
                    $html .= '<a data-toggle="tooltip"  data-placement="top" data-html="true" title="view"   class="btn action-button bg-primary-light view-user" href="#" data-id="' . $model->id . '" ><i class="fa fa-eye"></i></a>';
                }
                if ($html == '') {
                    return 'No permission';
                }
                return $html;
            })
            ->rawColumns(['sl#','user_id','name', 'email', 'role', 'address','status', 'action'])
            ->make(true);
    }

    private function _getUsersName($id,$staff=0)
    {
        if($staff == 0){
            $user_id = $id;
        }else{

            $user_id = UserStaff::where('staff_id',$id)->first();
            $user_id =  $user_id->user_id;
        }
        $userData = User::where('id',$user_id)->first();
        if($userData){
            return    $userData->id . ': '.$userData->name;
        }else{
            return 'admin';
        }


    }


    public function usersDetails(Request $request)
    {
        $data = $request->get('id');
        $user_role = User::join('roles', 'users.role_id', '=', 'roles.id')
            ->where('users.id', $data)
            ->select('users.*','users.id as uid','roles.display_name as role_name')->first();;
        if ($user_role->role_id == 1 || $user_role->role_id == 4) {
            $users = User::join('roles', 'users.role_id', '=', 'roles.id')
                ->join('mlm', 'mlm.user_id', '=', 'users.id')
                ->where('users.id', $data)
                ->select('users.*','users.id as uid','mlm.parent as parent', 'roles.display_name as role_name')->first();
        } else {
            $users = $user_role;
        }
        // $states = StateList::pluck('state', 'id');
        $returnHTML = view('admin.admin-users.user-details-view', ['users' => $users, 'bank_details' => $user_role->getUserBankDetails])->render();
        return response()->json($returnHTML);
    }


    /**
     * @return table
     */
    public function getAdminUsersWA(Request $request)
    {
        $data = $request->get('form_data');
        $params = array();
        parse_str($data, $params);

        $data = WellnessAdvisors::get();
        return DataTables::of($data)
            ->editColumn('sl#', function ($model) {
                return '<span class="si_no"></span> ';
            })
            ->editColumn('name', function ($model) {
                return $model->name;
            })->editColumn('email', function ($model) {
                return $model->email;
            })
            ->editColumn('role', function ($model) {
                return "Wellness Advisor";
            })
            // ->editColumn('last_login', function ($model) {
            //     return $model->last_login;
            // })
            // ->editColumn('created_at', function ($model) {
            //     return $model->created_at;
            // })
            ->editColumn('status', function ($model) {
                return $model->mobile;
            })
            ->editColumn('action', function ($model) {
                $html = '';
                if (can('edit_admin_user')) {
                    $html = '<a href="' . url('admin_user/wa-request/' . $model->id) . '" title="Edit User"><i class="fa fa-eye"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp';
                }
                if (can('edit_admin_user')) {
                    $html .= '<a href="#" class="delete-user-website" data-id="' . $model->id . '" title="Delete User"><i class="fa fa-trash-o fa-lg"></i></a>';
                }
                if ($html == '') {
                    return 'No permission';
                }
                return $html;
            })
            ->rawColumns(['sl#', 'name', 'email', 'role', 'last_login', 'created_at', 'status', 'action'])
            ->make(true);
    }

    /*User Register from website*/
    public function getAdminUsersWeb(Request $request)
    {
        $data = $request->get('form_data');
        $params = array();
        parse_str($data, $params);

        $data = UserRegistrationRequest::get();
        return DataTables::of($data)
            ->editColumn('sl#', function ($model) {
                return '<span class="si_no"></span> ';
            })
            ->editColumn('name', function ($model) {
                return $model->name;
            })->editColumn('email', function ($model) {
                return $model->email;
            })
            ->editColumn('role', function ($model) {
                return "Customer";
            })
            // ->editColumn('last_login', function ($model) {
            //     return $model->last_login;
            // })
            // ->editColumn('created_at', function ($model) {
            //     return $model->created_at;
            // })
            ->editColumn('status', function ($model) {
                return $model->mobile;
            })
            ->editColumn('action', function ($model) {
                $html = '';
                if (can('edit_admin_user')) {
                    $html = '<a href="' . url('admin_user/user-request/' . $model->id) . '" title="Edit User" class="btn action-button bg-primary-blue btn-xs"><i class="fa fa-eye"></i></a>&nbsp;';
                }
                if (can('edit_admin_user')) {
                    $html .= '<a href="#" class="delete-user-website btn action-button bg-primary-red btn-xs" data-id="' . $model->id . '" title="Delete User"><i class="fa fa-trash fa-lg"></i></a>';
                }
                if ($html == '') {
                    return 'No permission';
                }
                return $html;
            })
            ->rawColumns(['sl#', 'name', 'email', 'role', 'last_login', 'created_at', 'status', 'action'])
            ->make(true);
    }

    /**
     * @return wa details
     */
    public function waRequestDetail($id)
    {
        $user = WellnessAdvisors::find($id);
        $wa = User::where("active", 1)->where('role_id', '1')->get();
        $roles = Role::where('id', '!=', 0)->get();
        return view('admin.admin-users.admin-user-wa-detail')->with(['user' => $user, 'wa' => $wa, 'roles' => $roles]);;
    }

    public function uesrRequestDetail($id)
    {
        $user = UserRegistrationRequest::find($id);
        return view('admin.admin-users.admin-user-website-detail')->with(['user' => $user]);;
    }

    /**
     * WA create form
     * @return $this
     */
    public function createForm()
    {
        $roles = Role::get();
        $states = StateList::pluck('state', 'id');
        return view('admin.admin-users.create')->with(['roles' => $roles, 'states' => $states]);
    }

    public function getCustomerCareCategoryForm()
    {
        $customer_care_category = CustomerCareCategory::where("status", 1)->orderBy('id', 'desc')->get();
        $returnHTML = view('admin.admin-users.customer-care-category-form')->with('customer_care_category', $customer_care_category)->render();
        return response()->json($returnHTML);
    }

    // public function getCustomerCareCategoryForm()
    // {
    //     $customer_care_category = CustomerCareCategory::where("status", 1)->orderBy('id', 'desc')->get();
    //     $returnHTML = view('admin.admin-users.customer-care-category-form')->with('customer_care_category', $customer_care_category)->render();
    //     return response()->json($returnHTML);
    // }

    // public function getWAForm($id)
    // {
    //     $states = StateList::pluck('state', 'id');
    //     $wa = User::where("active", 1)->where('role_id', '1')->get();
    //     $permissions = $this->_getUserPermissions();
    //     $payment_modes = $this->_getUserPaymentModes();
    //     $parent_ = array();
    //     if(Auth::user()->role_id == 10){
    //         $parent_ = UserStaff::where('staff_id', Auth::user()->id)->first();
    //     }
    //     $returnHTML = view('admin.admin-users.wellness-advisor-form')->with(['parent_wa_id' => $parent_,'permissions' => $permissions, 'payment_modes' => $payment_modes, 'wa' => $wa, 'id' => $id, 'role_id' => $id, 'states' => $states])->render();

    //     return response()->json($returnHTML);
    // }

    // public function getWAFormAdminUser($id)
    // {
    //     $states = StateList::pluck('state', 'id');
    //     $wa = User::where("active", 1)->where('role_id', '1')->get();
    //     $permissions = $this->_getUserPermissions();
    //     $payment_modes = $this->_getUserPaymentModes();
    //     $parent_ = array();

    //     if(Auth::user()->role_id == 10){
    //         $parent_ = UserStaff::where('staff_id', Auth::user()->id)->first();

    //     }
    //     $returnHTML = view('admin.admin-users.wellness-advisor-form-admin-user')->with(['parent_wa_id' => $parent_,'permissions' => $permissions, 'payment_modes' => $payment_modes, 'wa' => $wa, 'id' => $id, 'role_id' => $id, 'states' => $states])->render();

    //     return response()->json($returnHTML);
    // }

    // /*Private function to get user permission*/
    // private function _getUserPermissions()
    // {
    //     $result = ['billing' => 0, 'wa_add' => 0, 'staff_add' => 0, 'customer_wa_child' => 0];
    //     if (Auth::user()->role_id == 0) {
    //         $result = ['billing' => 1, 'wa_add' => 1, 'staff_add' => 1, 'customer_wa_child' => 1];
    //     } else if (Auth::user()->role_id == 1) {
    //         $result_ = UserPermissions::where('user_id', Auth::user()->id)->first();
    //         if ($result_) {
    //             $result = ['billing' => $result_->billing, 'wa_add' => $result_->wa_add, 'staff_add' => $result_->staff_add, 'customer_wa_child' => $result_->customer_wa_child];
    //         }
    //     }
    //     return $result;
    // }

    // /*Private function to get user permission*/
    // private function _getUserPaymentModes()
    // {
    //     $result = ['cash' => 0, 'cheque' => 0, 'swipe' => 0, 'online' => 0];
    //     if (Auth::user()->role_id == 0) {
    //         $result = ['cash' => 1, 'cheque' => 1, 'swipe' => 1, 'online' => 1];
    //     } else if (Auth::user()->role_id == 1) {
    //         $result_ = UserPaymentMode::where('user_id', Auth::user()->id)->first();
    //         if ($result_) {
    //             $result = ['cash' => $result_->cash, 'cheque' => $result_->cheque, 'swipe' => $result_->swipe, 'online' => $result_->online];
    //         }
    //     }
    //     return $result;
    // }

    public function getNormalForm()
    {
        $states = StateList::pluck('state', 'id');
        $returnHTML = view('admin.admin-users.basic-form', ['states' => $states])->render();
        return response()->json($returnHTML);
    }


    /**
     * WA from website tree
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */

    public function createWA(Request $request, $id)
    {
        // dd($id);

        if ($request->input("role_id") == '1') {
            $v = Validator::make($request->all(), [
                'parent_as_admin' => 'required_without:parent_id',
                'parent_id' => 'required_without:parent_as_admin',
                'first_name' => 'required|min:3|max:50',
//                'last_name' => 'required|max:50',
                'role_id' => 'required',
//                'mobile' => 'required|regex:/[0-9]{10}/|digits:10|unique:users',
                'mobile' => 'required',
                'email' => 'required|email|min:6|max:75|unique:users',
            ]);
        }
         else {
            $v = Validator::make($request->all(), [
                'parent_as_admin' => 'required_without:parent_id',
                'parent_id' => 'required_without:parent_as_admin',
                'first_name' => 'required|min:3|max:50',           
             'last_name' => 'required|min:3|max:50',
            'role_id' => 'required',
            'address' => 'required',
            'location' => 'required',
            'pincode' => 'required|digits:6',
            'mobile' => 'required|regex:/[0-9]{10}/|digits:10|unique:users',
            // 'mobile' => 'required',
            'email' => 'required|email|min:6|max:75|unique:users',
            'password' => 'required|min:6|max:30|confirmed',

            ]);
        }

        if ($v->fails()) {
            $response['status'] = false;
            $response['message'] = trans('messages.msg_reg_failed');
            $response['errors'] = $v->getMessageBag()->toJson();
            $response['url'] = '/';

        } else {
            $file = $request->file('image');

            /*Add new user*/
            $user = new User();
            $user->name = $request->input('first_name') . ' ' . $request->input('last_name');
            $user->first_name = $request->input('first_name');
            $user->last_name = $request->input('last_name');
            $user->mobile = $request->input('mobile');
            $user->email = $request->input('email');
            $user->role_id = $request->input('role_id');
            $user->password = \Hash::make($request->input('mobile'));

            $user->user_entry = 3;
            $user->active = 1;
            $file = $request->file('image');
//            $user->completed = 1;
            if ($request->hasFile('image')) {
                if ($request->file('image') != null) {
                    $user->avatar = ImageUploadWithPath($file, 'profile-images');
                }
            }
            $user->save();

            $roleID = $request->input('role_id');

            /***New User Create Success End***/

            if ($user) {

                if($request->input('sliced_pv')){
                    $user->is_sliced_pv = 1;
                    $user->save();
                }

                $this->_slugGeneration($user);
                $user_id = $user->id;
                $userDetails = $this->_saveUserDetails($user_id, $request);

                if (!$userDetails) {
                    $user->delete();
                    $response['status'] = false;
                    return response()->json($response);
                }

                $email_user = $user->email;
                if ($email_user) {
                    Mail::to($email_user)
                        ->queue(new UserRegistration('new_user_created_by_admin', $user->id));
                }

                $roleID = $request->input('role_id');
                if ($roleID == '1' || $roleID == '10') {
                    /*If user is WA / Staff then do the following*/
                }

                if ($roleID == '10') {
                    $user_staff = new UserStaff();
                    $user_staff->user_id = $request->input("parent_as_admin") ? Auth::user()->id : $request->input("parent_id");
                    $user_staff->staff_id = $user->id;
                    $user_staff->save();
                }

                if ($roleID == '5') {
                    /*Customer care*/
                    $customer_care_user = new CustomerCareUsers();
                    $customer_care_user->customer_care_category_id = $request->input('customer_care_category');
                    $customer_care_user->user_id = $user->id;
                    $customer_care_user->rank = 0;
                    $customer_care_user->status = 1;
                    $customer_care_user->save();
                }


                if ($roleID == '1' || $roleID == '2' || $roleID == '3' || $roleID == '4') {
                    $mlm = $this->_mlmAndAncestorSave($request, $user);
                    if (!$mlm) {
                        $user->delete();
                        $userDetails->delete();
                        $response['status'] = false;
                        return response()->json($response);
                    }
                }

                $bank = $this->_saveBankDetails($user_id, $request);
                if (!$bank) {
                    $user->delete();
                    $userDetails->delete();
                    if (isset($mlm)) {
                        $mlm->delete();
                    }
                    $response['status'] = false;
                    return response()->json($response);
                }

            }
            if ($request->input('role_id') == 1) {
                WellnessAdvisors::whereId($id)->delete();
            } else {
                UserRegistrationRequest::whereId($id)->delete();
            }
            /****************** Activity Adding ************/
            $activity = new Activity();
            $activity->user_id = Auth::id();
            $activity->relation_id = $user->id;
            $activity->type = 'user';
            $activity->activity = 'New user has been created by admin';
            $activity->save();

            $response['status'] = true;
            $response['message'] = trans('messages.msg_reg_success');

//        return redirect()->back()->with('smessage', 'New User added.');
        }
        return response()->json($response);
    }

    /*Insert Ancestor*/

    // private function _ancestorsSeeder($mlm)
    // {
    //     if ($mlm->user_id == 100)
    //         return;
    //     $ancestors = DB::select(DB::raw("SELECT REPLACE(user_id," . $mlm->parent . "," . $mlm->user_id . ") as user_id,parent_id,orders,role_id FROM ancestors WHERE user_id!=ancestors.parent_id AND user_id =" . $mlm->parent));
    //     $ancestors = array_map(function ($value) {
    //         return (array)$value;
    //     }, $ancestors);

    //     $parent_data = Mlm::where('user_id', $mlm->parent)->first();
    //     $count = Ancestor::where('user_id', $mlm->parent)->max('orders');
    //     array_push($ancestors, array('user_id' => $mlm->user_id, 'parent_id' => $mlm->parent, 'role_id' => $parent_data->role_id, 'orders' => $count ? $count + 1 : 1));
    //     array_push($ancestors, array('user_id' => $mlm->user_id, 'parent_id' => $mlm->user_id, 'role_id' => $mlm->role_id, 'orders' => $count ? $count + 1 : 1));
    //     Ancestor::insert($ancestors);
    //     if ($mlm->role_id == 1)
    //         array_push($this->ma_parents, $mlm->user_id);
    //     if ($mlm->role_id != 4)
    //         array_push($this->all_parents, $mlm->user_id);
    // }


    public function create(Request $request)
    {
    //  dd($request->all());
            $roleID = $request->input('role_id');
            
              
           
           
                $v = Validator::make($request->all(), [
                    'first_name' => 'required|min:3|max:50',
                    'role_id' => 'required',
                    // 'pincode' => 'required|digits:6',
//                    'mobile' => 'required|regex:/[0-9]{10}/|digits:10|unique:users',
                    'mobile' => 'required',
                    'email' => 'required|email|min:6|max:75|unique:users',
                    'password' => 'required|min:6|max:30|confirmed',

                ]);
            
            if ($v->fails()) {

                $response['permission'] = true;
                $response['message'] = trans('messages.msg_reg_failed');
                $response['errors'] = $v->getMessageBag()->toJson();
                $response['url'] = '/';
                $response['status'] = false;
                return response()->json($response);
            }
           
               else
               {

                if ($files = $request->file('file')) {
                    $carImage = time().'.'.$files->getClientOriginalExtension();
                    $request->image->move(public_path('item'), $carImage);
                    $user=User::create([
                        'image' => $carImage,
                    ]);
                }
              

            
                   
                   $user=User::create(['role_id'=>$roleID,
                   'name'=>$request->input('first_name') . ' ' . $request->input('last_name'),
                   'first_name'=>$request->input('first_name'),
                   'last_name'=>$request->input('last_name'),
                   
                   'mobile'=>$request->input('mobile'),
                   'email'=>$request->input('email'),

                   'password'=>\Hash::make($request->input('password')),

                   'address'=>$request->input('address'),
                   'genders'=>$request->input('gender'),
                  

                   
                   ]);
                //    dd($user);

            
               
               }
               

                /****************** Activity Adding ************/
                $activity = new Activity();
                $activity->user_id = Auth::id();
                $activity->relation_id = $user->id;
                $activity->type = 'user';
                $activity->activity = 'New user has been created by admin';
                $activity->save();
                /************************************************/

                $response['user_id'] = $user->id;
                $response['name']=$user->name;
                $response['status'] = true;
                $response['permission'] = true;
                $response['message'] = trans('messages.msg_reg_success');
               
              

           

            return response()->json($response);
        }
    
    private function _checkUserPermission($role_id)
    {
        $can_add = false;

        if (Auth::user()->role_id == 0) {
            $can_add = true;
        } elseif (Auth::user()->role_id == 10) {
            if ($role_id == 4) {
                $can_add = true;
            }
        } else {
            $permission = UserPermissions::where('user_id', Auth::user()->id)->first();
            if ($role_id == 1) {
                if ($permission) {
                    if ($permission->wa_add == 1) {
                        $can_add = true;
                    }
                }
            } elseif ($role_id == 10) {
                if ($permission) {
                    if ($permission->staff_add == 1) {
                        $can_add = true;
                    }
                }
            } else {
                $can_add = true;
            }

        }
        return $can_add;
    }

    private function _checkUserRegistrationStatus($request)
    {
        $user = User::where('email', '=', $request->email)->where('mobile', $request->mobile)->where('successfull_registration', 0)->first();
        if ($user) {
            $userDetails = UserDetails::where('user_id', $user->id)->first();
            $userBankDetails = BankDetails::where('user_id', $user->id)->first();
            $mlm = Mlm::where('user_id', $user->id)->first();
            $customerCare = CustomerCareUsers::where('user_id', $user->id)->first();
            $UserStaff = UserStaff::where('staff_id', $user->id)->first();
            $ancestors = Ancestor::where('user_id', $user->id)->get();
            $address = Address::where('user_id', $user->id)->get();
            if ($userDetails)
                $userDetails->delete();
            if ($mlm)
                $mlm->delete();
            if ($UserStaff)
                $UserStaff->delete();
            if ($customerCare)
                $customerCare->delete();
            if ($userBankDetails)
                $userBankDetails->delete();
            if ($user)
                $user->delete();
            if (count($ancestors)>0)
                $ancestors = Ancestor::where('user_id', $user->id)->delete();
            if (count($address)>0)
                Address::where('user_id', $user->id)->delete();

        }
    }

    private function _userSave($request)
    {
        $user = new User();
        $user->name = $request->input('first_name') . ' ' . $request->input('last_name');
        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        $user->mobile = $request->input('mobile');
        $user->email = $request->input('email');
        $user->role_id = $request->input('role_id');
        $user->password = \Hash::make($request->input('password'));
        $user->user_entry = 3;
        $user->active = 1;
        $file = $request->file('image');
//            $user->completed = 1;
        if ($request->hasFile('image')) {
            if ($request->file('image') != null) {
                $user->avatar = ImageUploadWithPath($file, 'profile-images');
            }
        }
        $user->save();
        return $user;
    }

    private function _slugGeneration($user)
    {
        $slug = Slug::createSlug($user->name);
        $tempUs = User::whereUsername($slug)->first();
        if ($tempUs) {
            $slug = Slug::createSlug($user->name . ' ' . $user->id);
        }
        $user->username = $slug;
        $user->save();
    }

    private function _saveUserDetails($user_id, $request)
    {
        /*User details*/
        $user_details = new UserDetails();
        $user_details->user_id = $user_id;
        $user_details->gender = $request->input('gender');
        $user_details->date_of_birth = $request->input('date_of_birth');
        $user_details->address_1 = $request->input('address1');
        $user_details->address_2 = $request->input('address2');
        $user_details->aadhar_card_no = $request->input('aadhar_card_no');
        $user_details->pan_card_no = $request->input('pan_card_no');
        $user_details->district_state = $request->input('district_state');
        $user_details->country = $request->input('country');
        $user_details->location = $request->input('location');
        $user_details->latitude = $request->input('latitude');
        $user_details->longitude = $request->input('longitude');
        $user_details->referral_id = (int)$user_id + 99999;
        $user_details->save();
        return $user_details;

    }

    private function _assignUserPermission($request, $user)
    {
        /*If user is WA / Staff then do the following*/
        /*User Permission*/
        $permissions_array = $request->input('permissions');
        if ($permissions_array) {
            $permission = new UserPermissions();
            $permission->user_id = $user->id;

            foreach ($permissions_array as $data) {
                $permission->$data = 1;
            }

            $permission->save();
        }
        /*User Payment Modes*/
        $payment_modes_array = $request->input('payment_modes');
        if ($payment_modes_array) {
            $user_payment = new UserPaymentMode();
            $user_payment->user_id = $user->id;
            foreach ($payment_modes_array as $data) {
                $user_payment->$data = 1;
            }
            $user_payment->save();
        }

    }

    private function _mlmAndAncestorSave($request, $user)
    {
        $parent = 100;/*Admin id*/
        if ($request->input("role_id") == '1' || $request->input("role_id") == '4') {
            if ($request->input("parent_as_admin")) {
                if (Auth::user()->role_id == 0 || Auth::user()->role_id == 1) {/*Admin OR WA*/

                    $parent = Auth::user()->id;
                } elseif (Auth::user()->role_id == 10) {/*Staff*/
                    $user_staff = UserStaff::where('staff_id', Auth::user()->id)->first();
                    if ($user_staff) {
//                        $parent = $user_staff->user_id;
                        if ($parent_details = User::find($user_staff->user_id)) {
                            $parent = ($parent_details->role_id == 1) ? $parent_details->id :100;
                        }
                    } else {
                        $parent = 100;/*Admin id*/
                    }
                } else {
                    //
                }
            } else {
                if ($parent_detail = User::find($request->input("parent_id"))) {
                    $parent = ($parent_detail->role_id == 1) ? $parent_detail->id :100;
                }
            }
        }
        /*MLM Creation*/
        $mlm = new Mlm();
        $mlm->name = $user->name;
        $mlm->user_id = $user->id;
        $mlm->role_id = $request->input('role_id');
        $mlm->parent = $parent;
        $mlm->paid_rank = 0;
        $mlm->depth = 0;
        $mlm->pv = 0;
        $mlm->bv = 0;
        $mlm->wa_active = 0;
        $mlm->active_executive = 0;
        $mlm->paid_rank = 0;
//                    $mlm->tl = 0;
        $mlm->save();
        if ($mlm) {
            $this->_ancestorsSeeder($mlm);
        }
        return $mlm;
    }

    private function _saveBankDetails($user_id, $request)
    {
        $bank = new BankDetails();
        $bank->user_id = $user_id;
        $bank->bank = ($request->input('bank')) ? $request->input('bank') : null;
        $bank->branch_code = ($request->input('branch_code')) ? $request->input('branch_code') : null;
        $bank->address = ($request->input('address')) ? $request->input('address') : null;
        $bank->name_of_banker = ($request->input('name_of_banker')) ? $request->input('name_of_banker') : null;
        $bank->branch = ($request->input('branch')) ? $request->input('branch') : null;
        $bank->ac_number = ($request->input('account_number')) ? $request->input('account_number') : null;
        $bank->ifsc = ($request->input('ifsc')) ? $request->input('ifsc') : null;
        $bank->save();
        return $bank;
    }

    private function _sendRegistrationConfirmationMail($user,$password)
    {
        $email = Setting::where('data_key','admin_email')->first();
        if ($email) {
            if ($email->value) {
//                Mail::to($email->value)
//                    ->queue(new UserRegistration('new_user_registration', $user->id));
            }
        }
        $email_user = $user->email;
//                $email = 'anujaks995@gmail.com';

        if ($email_user) {
//            Mail::to($email_user)
//                ->queue(new AdminUserCreate('new_user_created_by_admin', $user->id, $password));
        }
    }

    public function updateForm($id)
    {

        if (!$user = User::find($id)) {
            abort('404');
        }
       $user=User::find($id);
        $roles = Role::pluck('display_name', 'id');
      
       
        return view('admin.admin-users.edit')->with(['roles'=>$roles,'user'=>$user]);
    }

    private function _getUserPermissionsOnEdit($id = 0)
    {
        $result = ['billing' => 0, 'wa_add' => 0, 'staff_add' => 0, 'customer_wa_child' => 0];
        if ($id != 0) {
            $result_ = UserPermissions::where('user_id', $id)->first();
            if ($result_) {
                $result = ['billing' => $result_->billing, 'wa_add' => $result_->wa_add, 'staff_add' => $result_->staff_add, 'customer_wa_child' => $result_->customer_wa_child];
            }
        }
        // else if (Auth::user()->role_id == 0){
        //     $result = ['billing'=>1,'wa_add'=>1,'staff_add'=>1,'customer_wa_child'=>1];
        // }else if(Auth::user()->role_id == 1){
        //     $result_ = UserPermissions::where('user_id',Auth::user()->id)->first();
        //     if ($result_){
        //         $result = ['billing'=>$result_->billing,'wa_add'=>$result_->wa_add,'staff_add'=>$result_->staff_add,'customer_wa_child'=>$result_->customer_wa_child];
        //     }
        // }
        return $result;
    }

    private function _getUserPaymentModesOnEdit($id = 0)
    {
        $result = ['cash' => 0, 'cheque' => 0, 'swipe' => 0, 'online' => 0];
        if ($id != 0) {
            $result_ = UserPaymentMode::where('user_id', $id)->first();
            if ($result_) {
                $result = ['cash' => $result_->cash, 'cheque' => $result_->cheque, 'swipe' => $result_->swipe, 'online' => $result_->online];
            }
        }
        // else if (Auth::user()->role_id == 0){
        //     $result = ['cash'=>1,'cheque'=>1,'swipe'=>1,'online'=>1];
        // }else if(Auth::user()->role_id == 1){
        //     $result_ = UserPaymentMode::where('user_id',Auth::user()->id)->first();
        //     if ($result_) {
        //         $result = ['cash'=>$result_->cash,'cheque'=>$result_->cheque,'swipe'=>$result_->swipe,'online'=>$result_->online];
        //     }
        // }
        return $result;
    }

    /**
     * @param $id
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function update($id, Request $request)
    {
        // dd($request->all());
        if (!$id && !$user = User::find($id)) {
            abort('404');
        }
        $roleID = $request->input('role_id');

        $v = Validator::make($request->all(), [
            'first_name' => 'required|min:3|max:50',
            'role_id' => 'required',
//            'mobile' => 'required|regex:/[0-9]{10}/|digits:10|unique:users,mobile,' . $user->id,
            'mobile' => 'required',
            'email' => 'required|email'
           
            // 'confirm_password' => 'same:new_password',
        ]);


        if ($v->fails()) {
           
            $response['status'] = false;
            $response['message'] = trans('messages.msg_reg_failed');
            $response['errors'] = $v->getMessageBag()->toJson();
            $response['url'] = '/';

        } else {
            
            User::find($id)->update(['role_id'=>$roleID,
            'name'=>$request->input('first_name') . ' ' . $request->input('last_name'),
            'first_name'=>$request->input('first_name'),
            'last_name'=>$request->input('last_name'),
            'genders'=>$request->input('gender'),
            'mobile'=>$request->input('mobile'),
            'email'=>$request->input('email'),

            'password'=>\Hash::make($request->input('password')),

            'address'=>$request->input('address'),

            
            ]);

                /****************** Activity Adding ************/
                $activity = new Activity();
                $activity->user_id = Auth::id();
                $activity->relation_id = $id;
                $activity->type = 'user';
                $activity->activity = 'User has been edited by admin';
                $activity->save();
                $response['status'] = true;
                $response['message'] = trans('messages.msg_reg_success');
                return response()->json($response);
            }
        }
        
       
    

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function changeStatus(Request $request)
    {
        $adminUser = User::find($request->get('id'));
        if ($adminUser) {
            if ($adminUser->active == 1) {
                $adminUser->active = 0;
                $adminUser->save();

                /****************** Activity Adding ************/
                $activity = new Activity();
                $activity->user_id = Auth::id();
                $activity->relation_id = $adminUser->id;
                $activity->type = 'user';
                $activity->activity = 'Admin user status inactivated by:';
                $activity->save();

                return response(['status' => true, 'msg' => 'User status successfully updated'], 200);
            }
            $adminUser->active = 1;
            $adminUser->save();

            /****************** Activity Adding ************/
            $activity = new Activity();
            $activity->user_id = Auth::id();
            $activity->relation_id = $adminUser->id;
            $activity->type = 'user';
            $activity->activity = 'Admin user status activated by:';
            $activity->save();

            return response(['status' => true, 'msg' => 'User status successfully updated'], 200);


        }
        return response(['status' => false, 'msg' => 'System cannot find this user'], 400);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteUser(Request $request)
    {
        $user = User::find($request->post('id'));
        // Check if the selected user is admin

        if ($user) {
            /****************** Activity Adding ************/
            $activity = new Activity();
            $activity->user_id = Auth::id();
            $activity->relation_id = $user->id;
            $activity->type = 'user';
            $activity->activity = 'Admin user deleted by:';
            $activity->save();

//            DB::table('users')->where('id', $user->id)->delete();
            return response()->json([
                'status' => true,
                'msg' => 'Successfully deleted the user',
            ], 200);
        }
        return response()->json([
            'status' => false,
            'msg' => 'Something went wrong',
        ], 400);
    }

    /*Private function to get user permission*/

    public function deleteWA(Request $request)
    {
        $user = WellnessAdvisors::find($request->get('id'));
        // Check if the selected user is admin

        if ($user) {
            /****************** Activity Adding ************/
            $activity = new Activity();
            $activity->user_id = Auth::id();
            $activity->relation_id = $user->id;
            $activity->type = 'user';
            $activity->activity = 'WA deleted by:';
            $activity->save();

            WellnessAdvisors::where('id', $user->id)->delete();
            return response()->json([
                'status' => true,
                'msg' => 'Successfully deleted the user',
            ], 200);
        }
        return response()->json([
            'status' => false,
            'msg' => 'Something went wrong',
        ], 400);
    }


    /**/

    public function deleteUserWebsite(Request $request)
    {
        $user = UserRegistrationRequest::find($request->get('id'));
        // Check if the selected user is admin

        if ($user) {
            /****************** Activity Adding ************/
            $activity = new Activity();
            $activity->user_id = Auth::id();
            $activity->relation_id = $user->id;
            $activity->type = 'user';
            $activity->activity = 'WA deleted by:';
            $activity->save();

            UserRegistrationRequest::where('id', $user->id)->delete();
            return response()->json([
                'status' => true,
                'msg' => 'Successfully deleted the user',
            ], 200);
        }
        return response()->json([
            'status' => false,
            'msg' => 'Something went wrong',
        ], 400);
    }

    public function createWAParent()
    {
        return view('admin.admin-users.create-wa-tree');
    }

    public function createWAParentForm($id)
    {
        $roles = Role::where('id', '!=', 2)->get();
        $wa = User::where("active", 1)->where('role_id', '1')->get();
        return view('admin.admin-users.create-wa-tree')->with(['roles' => $roles, 'wa' => $wa, 'id' => $id]);
    }

    public function userLogin(Request $request)
    {
        $userKey = str_random(60);
        \App\User::where('id', $request->get('id'))->update(['token' => $userKey]);
        $url = env('APP_URL') . "/loginas/$userKey";
        if ($request->get('url')) {
            $url .= "?url=" . urlencode($request->get('url'));
        }
        return $url;
    }

    public function getSelectedUsers(Request $request)
    {
        $value = trim($request->q);

        if (empty($value)) {
            return \Response::json([]);
        }

        $tags = Mlm::where('role_id', 1)->where('user_id', 'like', '%' . $value . '%')
            ->orWhere('name', 'like', '%' . $value . '%')->where('role_id', 1)->limit(5)->get();

        $formatted_tags = [];
        if($request->me)
        {
            $user=Auth::user();
            $formatted_tags[] = ['id' => $user->id, 'text' =>"Me (".$user->name . ' ' . $user->id . ')'];
        }
        foreach ($tags as $tag) {
            $formatted_tags[] = ['id' => $tag->user_id, 'text' => $tag->name . '--(' . $tag->user_id . ')'];
        }

        return \Response::json($formatted_tags);
    }
    public function getSelectedWa(Request $request)
    {
        $value = trim($request->q);

        if (empty($value)) {
            return \Response::json([]);
        }

        $tags = User::where('role_id', '=', 1)
            ->where('id', 'like', '%' . $value . '%')
            ->orWhere('name', 'like', '%' . $value  . '%')->where('role_id', '=', 1)->limit(5)->get();

        $formatted_tags = [];

        foreach ($tags as $tag) {
            $formatted_tags[] = ['id' => $tag->id, 'text' => $tag->name . '--(' . $tag->id . ')'];
        }

        return \Response::json($formatted_tags);
    }
    private function _saveAddressDetails($user_id, $request)
    {
        $address = new Address();
        $address->user_id = $user_id;
        $address->type = 'home';
        $address->first_name = $request->input('first_name');
        $address->last_name = $request->input('last_name');
        $address->address1 = $request->input('address1');
        $address->address2 = $request->input('address2');
        $address->postcode = $request->input('pincode');
        $address->city = $request->input('location');
        $address->state = $request->input('district_state');
        $address->country_id = $request->input('country');
        $address->phone = $request->input('mobile');
        $address->status = 0;
        $address->save();
        return $address;

    }
    /*Create customer by staff*/
    public function createCustomerByStaff(Request $request)
    {
        $v = Validator::make($request->all(), [
            'parent_as_admin' => 'required_without:parent_id',
            'first_name' => 'required|min:3|max:50',
            'role_id' => 'required',
            'pincode' => 'required|digits:6',
            'location' => 'required',
            'district_state' => 'required',
            'date_of_birth' => 'required',
//            'mobile' => 'required|regex:/[0-9]{10}/|digits:10|unique:users',
            'mobile' => 'required',
            'email' => 'required|email|min:6|max:75|unique:users',
            'password' => 'required|min:6|max:30|confirmed',
            'address1' => 'required',
            'address2' => 'required',
        ]);


        if ($v->fails()) {
            $response['status'] = false;
            $response['permission'] = true;
            $response['message'] = trans('messages.msg_reg_failed');
            $response['errors'] = $v->getMessageBag()->toJson();
            $response['url'] = '/';

        } else {

            $file = $request->file('image');

            /***New User Create Start***/
            $user = new User();
            $user->name = $request->input('first_name') . ' ' . $request->input('last_name');
            $user->first_name = $request->input('first_name');
            $user->last_name = $request->input('last_name');
            $user->mobile = $request->input('mobile');
            $user->email = $request->input('email');
            $user->role_id = 4;
            $user->password = \Hash::make($request->input('password'));
            $user->user_entry = 3;
            $user->active = 1;
//            $user->completed = 1;
            if ($request->hasFile('image')) {
                if ($request->file('image') != null) {
                    $user->avatar = ImageUploadWithPath($file, 'profile-images');
                }
            }
            $user->save();

            // AdminUser username creation
            $slug = Slug::createSlug($user->name);

            $tempUs = User::whereUsername($slug)->first();

            if ($tempUs) {
                $slug = Slug::createSlug($user->name . ' ' . $user->id);
            }

            $user->username = $slug;
            $user->save();
            $user_id = $user->id;

            /***New User Create Success End***/

            if ($user_id) {
                $parent_ = UserStaff::where('staff_id', Auth::user()->id)->first();
                if ($parent_) {

                    $staff_customers = new StaffCustomer();
                    $staff_customers->customer_user_id = $user_id;
                    $staff_customers->staff_user_id = Auth::user()->id;
                    $staff_customers->save();

                    $parent = $parent_->user_id;

                    $this->_slugGeneration($user);
                    $user_id = $user->id;
                    $userDetails = $this->_saveUserDetails($user_id, $request);

                    if (!$userDetails) {
                        $user->delete();
                        $response['status'] = false;
                        return response()->json($response);
                    }
                    $mlm = $this->_mlmAndAncestorSave($request, $user);
                    if (!$mlm) {
                        $user->delete();
                        $userDetails->delete();
                        $response['status'] = false;
                        return response()->json($response);
                    }

                    $address_detail = $this->_saveAddressDetails($user_id, $request);
                    if (!$address_detail) {
                        $user->delete();
                        $userDetails->delete();
                        if (isset($mlm)) {
                            $mlm->delete();
                        }

                        $response['status'] = false;
                        return response()->json($response);
                    }
                }
            }
            /****************** Activity Adding ************/
            $activity = new Activity();
            $activity->user_id = Auth::id();
            $activity->relation_id = $user->id;
            $activity->type = 'user';
            $activity->activity = 'New user has been created by admin';
            $activity->save();
            $response['user_id'] = $user->id;
            $response['status'] = true;
            $response['permission'] = true;
            $response['message'] = trans('messages.msg_reg_success');

//        return redirect()->back()->with('smessage', 'New User added.');

        }

        return response()->json($response);

    }

    public function changeCtoWA(Request $request){
        if($user = User::find($request->input('user_id'))) {

            if ($mlm = Mlm::where('user_id', $request->input('user_id'))->first()){

                $permission = new UserPermissions();
                $permission->user_id = $user->id;

                $permission->billing = 1;
                $permission->wa_add = 0;
                $permission->staff_add = 0
;
                $permission->customer_wa_child = 0;
                $permission->save();

                /*User Payment Modes*/

                $user_payment = new UserPaymentMode();
                $user_payment->user_id = $user->id;
                $user_payment->cash = 0;
                $user_payment->cheque = 0;
                $user_payment->swipe = 0;
                $user_payment->online = 1;
                $user_payment->save();
                /*Save Bank details*/
                $bank = BankDetails::where('user_id',$user->id)->first();
                if (!$bank){
                    $bank = new BankDetails();
                    $bank->user_id = $user->id;
                    $bank->save();
                }

                /*Change role_id in Mlm*/
                $mlm->role_id = 1;
                $mlm->save();

                /*Change user role in User*/
                $user->role_id = 1;
                $user->save();

                if ($user_details = UserDetails::where('user_id',$request->input('user_id'))->first()){
                    $user_details->referral_id = (int)$user->id + 99999;
                    $user_details->save();
                }
                $response['status'] = true;
                $response['message'] = "User role change success";
            }
        }else{
            $response['status'] = false;
            $response['message'] = "User not found";
        }
        return response()->json($response);
    }
}
