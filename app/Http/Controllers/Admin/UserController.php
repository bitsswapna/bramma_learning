<?php

namespace App\Http\Controllers\Admin;
/**
 * Created by PhpStorm.
 * User: COMPUTER
 * Date: 3/20/2018
 * Time: 1:35 PM
 */
use App\Activity;
use App\BookingItem;
use App\Facility;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class UserController extends Controller
{
    public function __construct()
    {

    }

    /**
     * @see users list
     *
     * @return view user lists
     */
    public function index(){
        $users = User::where('role_id', 2)->get();
        return view('admin.user.index')->with(['users' => $users]);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getUsers(Request $request)
    {

        $data = $request->get('form_data');
        $params = array();
        parse_str($data, $params);

        $data = User::where(['role_id' => 2])->get();

        return DataTables::of($data)
            ->editColumn('s#', function ($model) {
                return '<span class="si_no"></span> ';
            })
            ->editColumn('date_of_registration', function ($model) {
                return '<small>'.date('M d Y h:i', strtotime($model->created_at)).'</small>';
            })->editColumn('username', function ($model) {
                return '<a href="#" class="loginas" data-slug="'.$model->username.'" data-page="user">'.$model->username.'</a>';
            })
            ->editColumn('name', function ($model) {
                return $model->name;
            })
            ->editColumn('email', function ($model) {
                return '<a href="mailto:'.$model->email.'">'.$model->email.'</a>';
            })
            ->editColumn('phone', function ($model) {
                return $model->mobile;
            })
            ->editColumn('number_of_facilities', function ($model) {
                $facilityCount = Facility::where(['user_id' => $model->id])->get()->count();
                if($facilityCount){
                    return '<a href="'.url('facilities?user='.$model->id).'" target="_blank">'.$facilityCount.' Facility <i class="fa fa-external-link" aria-hidden="true"></i></a>';
                }
                return 'No facility';
            })
            ->editColumn('number_of_orders', function ($model) {
                $orderCount = BookingItem::where(['user_id' => $model->id, 'payment_status' => 'succeed'])->get()->count();
                if($orderCount){
                    return '<a href="'.url('orders?user='.$model->id).'" target="_blank">'.$orderCount.' Order <i class="fa fa-external-link" aria-hidden="true"></i></a>';
                }
                return 'No order';
            })
            ->editColumn('last_logged_in', function ($model) {
                return date('d M Y H:i a', strtotime($model->last_login));
            })
            ->editColumn('email_verification', function ($model) {
                if($model->completed == 1){
                    return '<p class="text-success"><i class="fa fa-check fa-2x" aria-hidden="true"></i></p>';
                }
                return '<p class="text-danger"><i class="fa fa-times fa-2x" aria-hidden="true"></i></p>';
            })
            ->editColumn('approved', function ($model) {
                switch($model->status){
                    case 'w_f_a':
                        if(can('edit_user')){
                            return '<div class="dropdown show"><a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Pending</a><div class="dropdown-menu" aria-labelledby="dropdownMenuLink"><a class="dropdown-item change-status" href="#" data-id="'.$model->id.'" data-status="approved">Approve</a><a class="dropdown-item change-status" href="#" data-id="'.$model->id.'" data-status="rejected">Reject</a></div></div>';
                        }
                        return '<div class="alert alert-dark" role="alert"><strong>Approved</strong></div>';
                        break;
                    case 'approved':
                        return '<div class="alert alert-success" role="alert"><strong>Approved</strong></div>';
                        break;
                    case 'rejected':
                        return '<div class="alert alert-danger" role="alert"><strong>Rejected</strong></div>';
                        break;
                }
                return ;
            })
            ->editColumn('status', function ($model) {
                if(can('edit_user')){
                    $checked = '';
                    if($model->active == 1){
                        $checked = 'checked ';
                    }

                    $html = '<div class="pretty p-switch p-fill"><input type="checkbox" '.$checked.' data-id="'.$model->id.'" class="change-active"><div class="state p-success"><label></label></div></div>';
                    return $html;

                }
                if($model->active == 0){
                    return '<div class="alert alert-danger" role="alert"><strong>In active</strong></div>';
                }
                return '<div class="alert alert-success" role="alert"><strong>Active</strong></div>';
            })
            ->editColumn('action', function ($model) {
                if(can('delete_user')){
                    return '<a href="#" class="delete-user" data-id="'.$model->id.'"><i class="fa fa-trash"></i></a>';
                }
                return 'You don\'t have the permission';
            })

            ->rawColumns(['s#', 'date_of_registration', 'username', 'name', 'email', 'phone', 'number_of_facilities', 'number_of_orders', 'last_logged_in', 'email_verification', 'approved', 'status', 'action'])
            ->make(true);
    }

    /**
     * Change the status
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeStatus(Request $request){
        $user = User::find($request->get('id'));
        if($user){
            $user->status = $request->get('status');
            $user->save();

            if($request->get('status') == 'approved'){
                $work = 'User approved by:';
            }
            if($request->get('status') ==  'rejected'){
                $work = 'User rejected by:';
            }

            /****************** Activity Adding ************/
            $activity = new Activity();
            $activity->user_id = Auth::id();
            $activity->relation_id = $user->id;
            $activity->type = 'user';
            $activity->activity = $work;
            $activity->save();

            return response()->json([
                'status' => true,
                'msg' => 'Successfully changed the status',
            ], 200);
        }
    }

    /**
     * Change Active/Inactice user
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeActive(Request $request){
        $user = User::find($request->get('id'));
        if($user){
            $msg = '';
            if($user->active == 1){
                $user->active = 0;
                $msg = 'Successful Inactivated the user';

                $work = 'User inactivated by:';
            }else{
                $user->active = 1;
                $msg = 'Successful Activated the user';

                $work = 'User Activated by:';
            }
            $user->save();

            /****************** Activity Adding ************/
            $activity = new Activity();
            $activity->user_id = Auth::id();
            $activity->relation_id = $user->id;
            $activity->type = 'user';
            $activity->activity = $work;
            $activity->save();

            return response()->json([
                'status' => true,
                'msg' => $msg,
            ], 200);
        }
    }

    /**
     * Delete the user
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteUser(Request $request){
        $user = User::find($request->get('id'));
        if($user){

            /****************** Activity Adding ************/
            $activity = new Activity();
            $activity->user_id = Auth::id();
            $activity->relation_id = $user->id;
            $activity->type = 'user';
            $activity->activity = 'User deleted by:';
            $activity->save();

            DB::table('users')->where('id', $user->id)->delete();
            return response()->json([
                'status' => true,
                'msg' => 'Successfully deleted the user',
            ], 200);
        }
        return response()->json([
            'status' => false,
            'msg' => 'Something went wrong',
        ], 400);
    }

    /**
     * @param Request $request
     * @return string
     */
    public function userLogin(Request $request){
        $slug = $request->get('slug');
        $user = User::where('username', $slug)->first();
        if($user != null){
            $user_key = $this->getToken(12, $user->id);
        }
        $user->token = $user_key;
        $user->save();
        return USER_BASE_URL.'loginas/'.$user_key;
    }

    /**
     * @param $length
     * @param $seed
     * @return string
     */
    private function getToken($length, $seed){
        $token = "";
        $codeAlphabet = USER_KEY_ALPHA;
        $codeAlphabet.= USER_KEY_NUM;
        mt_srand($seed); // Call once. Good since $application_id is unique.
        for($i=0;$i<$length;$i++){
            $token .= $codeAlphabet[mt_rand(0,strlen($codeAlphabet)-1)];
        }
        return $token;
    }
}