<?php
/**
 * Created by PhpStorm.
 * User: COMPUTER
 * Date: 3/30/2018
 * Time: 10:10 AM
 */

namespace App\Http\Controllers\Admin;


use App\Activity;
use App\Models\Cms;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CmsController extends Controller
{
    /**
     * CmsController constructor.
     */
    public function __construct(){

    }

    /**
     * @return $this
     */
    public function index(){
        $cms = Cms::get();
        return view('admin.cms.index')->with(['cms' => $cms]);
    }

    /**
     * @return $this
     */
    public function createForm(){
        $target = ['_blank','_self','_parent','_top','framename'];
        return view('admin.cms.create')->with(['target' => $target]);
    }

    

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function create(Request $request){

        $r = [
            'title' => 'required|min:2|max:200',
            'slug' => 'required|max:200|unique:cms',
            'target' => 'required',
            'type' => 'required',
        ];

        $v = Validator::make($request->all(), $r);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())->withInput($request->except('password'));
        }

        $cms = new Cms();
        $cms->title = $request->input('title');
        $cms->sub_title = $request->input('sub_title');
        $cms->slug = $request->input('slug');
        $cms->status = $request->input('status');

        if($request->get('external')){
            $cms->external = 1;
            $cms->external_url = $request->input('external_url');
        }else{
            $cms->description = $request->input('description');
            $cms->meta_title = $request->input('meta_title');
            $cms->meta_key = $request->input('meta_key');
            $cms->meta_description = $request->input('meta_description');
        }
        foreach ($request->get('type') as $key => $page){
            $cms->$page = 1;
        }
        $cms->save();

        /****************** Activity Adding ************/
        $activity = new Activity();
        $activity->user_id = Auth::id();
        $activity->relation_id = $cms->id;
        $activity->type = 'email';
        $activity->activity = "Cms  Created by:";
        $activity->save();

        return redirect()->back()->with('smessage', 'New page added.');
    }

    /**
     * @param $id
     * @return $this
     */
    public function updateForm($id){
        $cms = Cms::find($id);
        $target = ['_blank','_self','_parent','_top','framename'];
        return view('admin.cms.edit')->with(['target' => $target, 'cms'=> $cms]);
    }

     /**
     * @return $this
     */
    public function updateHomeForm($id){
        $cms = Cms::find($id);
        $target = ['_blank','_self','_parent','_top','framename'];
        return view('admin.cms.homeEdit')->with(['target' => $target,'cms'=> $cms]);
    }

   
    /**
     * @param $id
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function update($id, Request $request){

        $r = [
            'title' => 'required|min:2|max:200',
//            'slug' => 'required|unique:cms,slug,'.$id,
            'target' => 'required',
            'type' => 'required',
        ];

//        if($request->get('external')){
//            $r['external_url'] = 'required';
//        }
        $v = Validator::make($request->all(), $r);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())->withInput($request->except('password'));
        }

        $cms = Cms::find($id);
        $cms->title = $request->input('title');
        $cms->sub_title = $request->input('sub_title');
//        $cms->slug = $request->input('slug');
        $cms->status = $request->input('status');

        if($request->has('type')){
            $cms->footer = in_array('footer', $request->get('type'))?1:0;
            $cms->page = in_array('page', $request->get('type'))?1:0;
            $cms->link = in_array('link', $request->get('type'))?1:0;
        }

        if($request->get('external')){
            $cms->external = 1;
            $cms->external_url = $request->input('external_url');
        }else{
            $cms->external = 0;
            $cms->description = $request->input('description');
            $cms->meta_title = $request->input('meta_title');
            $cms->meta_key = $request->input('meta_key');
            $cms->meta_description = $request->input('meta_description');
        }

        $cms->save();

        /****************** Activity Adding ************/
        $activity = new Activity();
        $activity->user_id = Auth::id();
        $activity->relation_id = $cms->id;
        $activity->type = 'email';
        $activity->activity = "Cms updated by:";
        $activity->save();

        return redirect()->back()->with('smessage', 'Page updated.');
    }

    public function changeStatus(Request $request){

        $cms = Cms::find($request->get('id'));
        if($cms){
            if($cms->status == 1){
                $cms->status = 0;
                $cms->save();

                /****************** Activity Adding ************/
                $activity = new Activity();
                $activity->user_id = Auth::id();
                $activity->relation_id = $cms->id;
                $activity->type = 'email';
                $activity->activity = "Cms inactivated by:";
                $activity->save();

                return response([ 'status' => true, 'msg' => 'Cms page status successfully updated' ], 200);
            }

            /****************** Activity Adding ************/
            $activity = new Activity();
            $activity->user_id = Auth::id();
            $activity->relation_id = $cms->id;
            $activity->type = 'email';
            $activity->activity = "Cms activated by:";
            $activity->save();

            $cms->status = 1;
            $cms->save();
            return response([ 'status' => true, 'msg' => 'Cms page status successfully updated' ], 200);
        }
        return response([ 'status' => false, 'msg' => 'System cannot find this cms page' ], 400);
    }
}