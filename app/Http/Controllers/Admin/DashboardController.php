<?php

namespace App\Http\Controllers\Admin;

/**
 * Created by Swapna.
 * User: COMPUTER
 * Date: 3/072019
 * Time: 3:11 AM
 */
use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function __construct()
    {
        Carbon:: useMonthsOverflow(false);
    }
    private $_fromDate;
    private $_tillDate;

    /**
     * @see dashboard
     *
     * @return view summary of site
     */
    public function index() 
    {
        return view('admin.dashboard.index');
    }

}
