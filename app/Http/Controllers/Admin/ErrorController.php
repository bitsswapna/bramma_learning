<?php
/**
 * Created by PhpStorm.
 * User: COMPUTER
 * Date: 3/27/2018
 * Time: 5:58 PM
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;

class ErrorController extends Controller
{

    public function noAccess(){
        return view('admin.no-access-content');
    }
}