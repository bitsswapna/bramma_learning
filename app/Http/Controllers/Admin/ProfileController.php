<?php
/**
 * Created by PhpStorm.
 * User: COMPUTER
 * Date: 6/1/2018
 * Time: 9:04 AM
 */

namespace App\Http\Controllers\Admin;


use App\Activity;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{

    /**
     * @return $this
     */
    public function index(){
        $user = User::find(Auth::id());
        return view('admin.profile.index')->with(['user' => $user]);
    }

    public function updateProfile(Request $request){
        $v = Validator::make($request->all(), [
            'first_name' => 'required|min:3|max:50',
            '                                                                                                                                                              ' => 'max:50',
            'image' => 'mimes:jpeg,bmp,png|max:20480'
        ]);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())->withInput($request->all());
        }
        $file = $request->image;

        $user = User::find(Auth::id());
        $user->name = $request->input('first_name') . ' ' . $request->input('last_name');
        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        if ($request->hasFile('image')) {
            if ($request->file('image') != null) {
                $user->avatar = ImageUploadWithPath($file, 'profile-images');
            }
        }
        $user->save();

        /****************** Activity Adding ************/
        $activity = new Activity();
        $activity->user_id = Auth::id();
        $activity->relation_id = $user->id;
        $activity->type = 'user';
        $activity->activity = 'User has been updated his profile by::';
        $activity->save();

        return redirect()->back()->with('smessage', 'Your profile has been updated successfully.');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function changePassword(){
        return view('admin.profile.change-password');
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function updatePassword(Request $request){

        $v = Validator::make($request->all(), [
            'current_password' => 'required',
            'password' => 'required|between:6,30|confirmed',
            'password_confirmation' => 'required|between:6,30'
        ]);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        }

        if(Auth::check()){
            if(!\Hash::check($request->get('current_password'), Auth::user()->password)){
                $error = ['current_password' => ['incorrect current password ']];
                return redirect()->back()->withErrors($error);

            }

            $user = User::whereId(Auth::id())->first();
            $user->password = \Hash::make($request->get('password'));
            $user->save();

            /****************** Activity Adding ************/
            $activity = new Activity();
            $activity->user_id = Auth::id();
            $activity->relation_id = $user->id;
            $activity->type = 'user';
            $activity->activity = 'User has been updated his password by::';
            $activity->save();

            return redirect()->back()->with('smessage', 'Your password has been updated successfully.');
        }

    }
}
