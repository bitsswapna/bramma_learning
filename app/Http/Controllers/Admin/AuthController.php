<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use App\User;

class AuthController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('guest', ['except' => ['logout']]);

    }

    //view login page
    public function index(){
        return view('admin.login');
    }

    /**
     * @param Request $request
     *
     * @return dashboard if authenticated
     *
     */
    public function login(Request $request){
        $credentials = $request->only('email', 'password');


        if (Auth::attempt($credentials, true, true)) {
            /* Check user Active */
            /*if (Auth::user()->active == 1) {
                Auth::logout();
                Session::flush();

                return Redirect::to('login')
                    ->with('emessage', 'Your account is not active. Please contact the administrator.')
                    ->withInput();
            }*/

          /*  $role_details = User::find(Auth::user()->id)?User::find(Auth::user()->id)->getRoleDashboardType:NULL;

            if ($role_details) {
                if ($role_details->user_type == 0) {*/

                    $auth = true; //Success
                    Auth::user()->last_login = new \DateTime;
                    Auth::user()->save();
                } else {
                    Auth::logout();
                    Session::flush();
                    $auth = false;
                    $message = trans('messages.msg_invalid_credentials');
                }
           /* }else{
                Auth::logout();
                Session::flush();
                $auth = false;
                $message = trans('messages.msg_invalid_credentials');
            }

        } else {
            $auth = false; //failed
            return redirect()->back()->with('emessage', 'Invalid Credentials!');
        }*/

        if ($request->ajax()) {
            return response()->json([
                'auth' => $auth,
                'intended' => URL::previous()
            ]);
        } else {
            return Redirect::to('/');
        }
    }

    public function logout(){
        $user = Auth::user();
        Auth::logout();
        return Redirect('/');
    }
}
