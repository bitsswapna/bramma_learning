<?php



namespace App\Http\Controllers\Admin;


use App\Models\Logs;
use App\Models\Organization;
use App\Models\Client;
use App\Models\Industries;
use App\Http\Controllers\Controller;
use App\Models\SubnIndustries;
use App\User;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ClientController extends Controller
{
    public function index()
    {
        return view('admin.clients.index');
    }
    public function getClients(Request $request)
    {
    $data = Client::select('clients.*')
        ->orderBy('id','DESC')
        ->get();

    try {
        return DataTables::of($data)
            ->addIndexColumn()
            ->editColumn('organisation_name', function ($model) {
                return $model->organisation_name;
            })->editColumn('promoter_name', function ($model) {
                return $model->promoter_name;
            })->editColumn('promoter_phone', function ($model) {
                return $model->promoter_phone;
            })
            ->editColumn('action', function ($model) {
                $html = '';
                if (can('edit_client')) {
                    $html = '<a data-toggle="tooltip"  data-placement="top" data-html="true" title="Edit" class="btn action-button bg-primary-blue" href="' . url('admin_user/edit/' . $model->id) . '" ><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;';
                }
                if (can('edit_admin_user')) {
                    $html .= '<a data-toggle="tooltip"  data-placement="top" data-html="true" title="view"   class="btn action-button bg-primary-light view-user" href="#" data-id="' . $model->id . '" ><i class="fa fa-eye"></i></a>';
                }
                if ($html == '') {
                    return 'No permission';
                }
                return $html;
            })
            ->rawColumns(['sl#','organisation_name', 'promoter_name', 'promoter_phone','action'])
            ->make(true);

    } catch (\Exception $e) {
    }
}
    public function createForm()
    {
        return view('admin.clients.create');
    }
public function create(Request $request)
{
//    dd($request->all());
    $validator = Validator::make($request->all(), [
        'organization_name' => 'required|string',
        'promotor_name' => 'required|string',
        'promotor_email_id' => 'required|email|unique:clients'
    ]);
    if ($validator->fails()) {
        return response()->json([
            'message' => $validator->errors(),
            'errors' => $validator->errors(),
            'status' => false
        ], 422);
    } else {

        $client = new Client();
        $client->user_id = Auth::user()->id;
        $client->organization_name = $request->input('organization_name');
        $client->organization_type_id = $request->input('organization_type_id');
        $client->industry_id = $request->input('industry_id');
        $client->sub_industry_id = $request->input('sub_industry_id');
        $client->promotor_name = $request->input('promotor_name');
        $client->promotor_email_id = $request->input('promotor_email_id');
        $client->promotor_phone_number = $request->input('promotor_phone_number');
        $client->save();

        $activity1 = new Logs();
        $activity1->model = "client";
        $activity1->model_id = $client->id;
        $activity1->direction = 'create';
        $activity1->key_code = "client_create";
        $activity1->auth_user_id = Auth::user()->id;
        $activity1->value = "New client created With Title " . $client->organization_name;
        $activity1->save();
        return response()->json([
            'status' => true,
            'message' => 'succeffully inserted',
        ], 200);

    }
}

public function edit($id,Request $request)
{
    $client = Client::join('organization_types', 'organization_types.id', 'clients.organization_type_id')
        ->join('industries', 'industries.id', 'clients.industry_id')
        ->select('clients.*', 'organization_types.type as organization_type', 'industries.industry as industry_name')
        ->where('clients.id', $id)
        ->first();
    return response()->json([
        'status' => true,
        'message' => 'client edit ',
        'data' => $client
    ], 200);
}
public function update($id,Request $request)
{
    $validator = Validator::make($request->all(), [
         'organization_name' => 'required|string',

    ]);
    if ($validator->fails()) {
        return response()->json([
            'message' => 'Please verify the input!',
            'errors' => $validator->errors(),
            'status' => false
        ], 422);
    } else {
        $client = Client::find($id);
        $client->organization_name = $request->input('organization_name');
        $client->organization_type_id = $request->input('organization_type_id');
        $client->industry_id = $request->input('industry_id');
        $client->sub_industry_id = $request->input('sub_industry_id');
        $client->promotor_name = $request->input('promotor_name');
        $client->promotor_email_id = $request->input('promotor_email_id');
        $client->promotor_phone_number = $request->input('promotor_phone_number');
        $client->save();

        $activity1 = new Logs();
        $activity1->model = "client";
        $activity1->model_id = $id;
        $activity1->direction = 'update';
        $activity1->key_code = "client_update";
        $activity1->auth_user_id = Auth::user()->id;
        $activity1->value = "Client updated With name " . $client->organization_name;
        $activity1->save();
        return response()->json([
            'status' => true,
            'message' => 'successfully updated',
        ], 200);
    }
}
    public function delete($id){

        $leadtypes = Client::find($id);
        $leadtypes->delete();
        return response()->json([
            'status' => true,
            'message' => 'Client Successfully Deleted',
            'data' => $leadtypes
        ], 200);
    }
}
