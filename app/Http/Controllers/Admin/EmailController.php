<?php
/**
 * Created by PhpStorm.
 * User: COMPUTER
 * Date: 5/9/2018
 * Time: 10:51 AM
 */

namespace App\Http\Controllers\Admin;

use App\Activity;
use App\EmailTemplate;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class EmailController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){
        $emails = EmailTemplate::get();
        return view('admin.email.index')->with(['emails' => $emails]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createForm(){
        return view('admin.email.create');
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function create(Request $request){

        $r = [
            'title' => 'required',
            'from' => 'required|email',
            'reply' => 'required|email',
            'key' => 'required',
            'subject' => 'required',
            'email_body' => 'required',
        ];

        $v = Validator::make($request->all(), $r);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())->withInput($request->all());
        }

        $email = new EmailTemplate();
        $email->title = $request->input('title');
        $email->key = $request->input('key');
        $email->from = $request->input('from');
        $email->reply = $request->input('reply');
        $email->subject = $request->input('subject');
        $email->email_body = $request->input('email_body');
        $email->created_by = Auth::id();

        $email->save();

        /****************** Activity Adding ************/
        $activity = new Activity();
        $activity->user_id = Auth::id();
        $activity->relation_id = $email->id;
        $activity->type = 'email';
        $activity->activity = "Email template created by:";
        $activity->save();

        return redirect()->back()->with('smessage', 'New email template added.');
    }

    /**
     * @param $id
     * @return $this
     */
    public function updateForm($id){
        $email = EmailTemplate::find($id);
        if($email != null){
            return view('admin.email.edit')->with(['email' => $email]);
        }
    }

    /**
     * @param $id
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function update($id, Request $request){
        $r = [
            'title' => 'required',
            'from' => 'required|email',
            'reply' => 'required|email',
            'subject' => 'required',
            'email_body' => 'required',
        ];

        $v = Validator::make($request->all(), $r);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())->withInput($request->all());
        }

        $email = EmailTemplate::find($id);
        $email->title = $request->input('title');
        $email->from = $request->input('from');
        $email->reply = $request->input('reply');
        $email->subject = $request->input('subject');
        $email->email_body = $request->input('email_body');

        $email->save();

        /****************** Activity Adding ************/
        $activity = new Activity();
        $activity->user_id = Auth::id();
        $activity->relation_id = $email->id;
        $activity->type = 'email';
        $activity->activity = "Email template updated by:";
        $activity->save();

        return redirect()->back()->with('smessage', 'Email template updated.');
    }
}