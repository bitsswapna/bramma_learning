<?php
/**
 * Created by PhpStorm.
 * User: COMPUTER
 * Date: 4/12/2018
 * Time: 3:48 PM
 */

namespace App\Http\Controllers;


use App\FacilityImage;
use Illuminate\Support\Facades\Config;

class MediaController extends Controller
{
    public function show($id)
    {

        $file_path = pathinfo($id);

        $output = "";
        $dir_name =$file_path['dirname'];
        $extension = $file_path['extension'];

        $file_name = $file_path['filename'];

        $month = explode('/',$dir_name)[2];
        $year = explode('/',$dir_name)[1];
        $folder = explode('/',$dir_name)[0];

        $file_name_array = explode("-", $file_name);


        $explode = explode('-', $file_name); // split all parts
        $end = '';
        $begin = '';
        if(count($explode) > 0){
            $end = array_pop($explode); // removes the last element, and returns it

            if(count($explode) > 0){
                $begin = implode('-', $explode); // glue the remaining pieces back together
            }
        }

        $size = end($file_name_array);
        $width = explode('x',$size)[0];
        $height = explode('x',$size)[1];
        $allowed = false;
        $image_sizes = Config::get('constants.image_sizes');
        foreach($image_sizes as $image_size){
            if ($width == $image_size[0] && $height == $image_size[1]) {
                $allowed = true;
                break;
            }
        }
        if($allowed){
            $path = url("media/").'/';
            $path_dir = public_path("media/$folder/");
            $ful_path = $path.$id;


            if (!is_dir($path_dir .'/'. $year . '/' . $month)) {
                mkdir($path_dir .'/'. $year . '/' . $month, 0755, true);
            }
            if (file_exists($ful_path)) {

                $output = $ful_path;
            }
            else{

                $destination_path = public_path('media/' . $id);
                array_pop($file_name_array);

                $original_path = public_path('media') . '/' . $dir_name . '/' . implode('-', $file_name_array) . '.' . $extension;

                img_resize($original_path, $destination_path, $width, $height, $extension);
                exit;

            }
        }

        $filename = basename($output);
        //$extension
        switch( $extension ) {
            case "gif": $ctype="image/gif"; break;
            case "png": $ctype="image/png"; break;
            case "jpeg":$ctype="image/jpeg";break;
            case "jpg": $ctype="image/jpeg"; break;
            default:
        }

        header('Content-type: ' . $ctype);
        readfile($output);
        exit;
    }

    public function copyImage($image)
    {
        // Image public path
        $source = public_path('media/facility-images/'.$image->image);
        if(!file_exists($source)){
            return false;
        }

        // Image public target path
        $target = public_path('media/facility-images/'.str_replace(basename($source), '', $image->image));
        $filename = time().basename($source);
        $target = $target.$filename;

        copy($source, $target); // Copy image to target

        $main_path = str_replace(basename($source), $filename, $image->image);

        $temp_image = new FacilityImage();
        $temp_image->image = $main_path;
        $temp_image->main = 0;
        $temp_image->type = 0;
        $temp_image->save();

        return $temp_image;

    }
}