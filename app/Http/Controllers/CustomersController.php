<?php

namespace App\Http\Controllers;
use Illuminate\Notifications\Notifiable;
use App\Customers;
use App\Activity;
use App\Site;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;


class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.customers.create');
    }
    public function view()
    {
        return view('admin.customers.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $va = Validator::make($request->all(), [
//            'first_name' => 'required|max:120',
//
//            'adress' => 'required|max:120',
//            'contnumber' => 'required|min:11|numeric',


        ]);


        if ($va->fails()) {
            return redirect()->back()->withErrors($va->errors())->withInput($request->all());
        }
     else
      {
        $insert= new Customers();
        $insert->name=$request->input('first_name');
          $insert->lastname=$request->input('last_name');
          $insert->email=$request->input('email');
        $insert->address=$request->input('adress');
        $insert->number=$request->input('contnumber');

        $insert->save();



        return back()->with('smessage','data inserted successfully');
        }



       



    }
    public function getCustomers(Request $request)
    {
        $data = $request->get('form_data');
        $params = array();
        parse_str($data, $params);

        $data =Customers::leftjoin('sites','customers.id','=','sites.customer_id')
        ->select('customers.*','sites.customer_id')->distinct()->
        get()->where('deleted_at','=',NULL);
        // dd($data);
        return DataTables::of($data)
            ->editColumn('sl#', function ($model) {
                return '<span class="si_no"></span> ';
            })
            ->editColumn('name', function ($model) {
                return $model->name;
            })->editColumn('address', function ($model) {
                return $model->address;
            })
            ->editColumn('mobile', function ($model) {
                return $model->number;
            })
            ->editColumn('status', function ($model) {
                if (can('edit_admin_user')) {
                    $checked = '';
                    if ($model->active == 1) {
                        $checked = 'checked';
                    }
                    return '<div class="pretty p-switch p-fill" ><input type="checkbox" class="change-status" data-id="' . $model->id . '" ' . $checked . '><div class="state p-success" ><label ></label ></div></div>';
                }
            })


            ->editColumn('action', function ($model) {
                $html = '';
                if (can('edit_admin_user')) {
                    $html = '<a href="' . url('admin_customer/request/' . $model->id) . '" title="Edit Customer"><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp';
                }
                if (can('edit_admin_user')) {
                    $html .= '<a href="#" class="delete-user-website" data-id="' . $model->id . '" title="Delete Customer"><i class="glyphicon glyphicon-trash"></i></a>';
                }
                if ($html == '') {
                    return 'No permission';
                }
                return $html;
            })
            ->rawColumns(['sl#', 'name', 'address', 'mobile', 'status','action'])
            ->make(true);
    }
    public function send($id)
    {
        $user = Customers::find($id);
        return view('admin.customers.edit')->with(['user' => $user]);

    }
    public function sendupdate(Request $request,$id)
    {
        $va = Validator::make($request->all(), [
            'name' => 'required|max:120',

            'adress' => 'required|max:120',
            'contnumber' => 'required|min:11|numeric',


        ]);

        if ($va->fails()) {
            return redirect()->back()->withErrors($va->errors())->withInput($request->all());
        }
        else
        {
            $updated= Customers::find($id);
            $updated->name=$request->input('name');
    
    
            $updated->address=$request->input('adress');
            $updated->number=$request->input('contnumber');
    
            $updated->save();
            return redirect('admin-customers')->with('smessage','data updated successfully');
        }

       






    }
    public function changeStatus(Request $request)
    {
        $adminUser = Customers::find($request->get('id'));
        if ($adminUser) {

            if ($adminUser->active == 1) {
                $adminUser->active = 0;
                $adminUser->save();



                return response(['status' => true, 'msg' => 'Customer status successfully updated'], 200);
            }
            $adminUser->active = 1;
            $adminUser->save();



            return response(['status' => true, 'msg' => 'Customer status successfully updated'], 200);


        }
        return response(['status' => false, 'msg' => 'System cannot find this user'], 400);
    }


    public function deleteCustomers(Request $request)
    {
        $customer=Site::where('customer_id',$request->get('id'))->first();
        $user = Customers::find($request->get('id'));
        // dd($customer);

        if($customer)
        {
            
    
    return response(['status' => true, 'msg' => 'Customer already registered in Site, cannot delete'], 200);
         

        }
    else
    {
        

        $user = Customers::find($request->get('id'))->delete();
        return response(['status' => true, 'msg' => 'Succefully deleted'], 200);

    }
       
           
            // Check if the selected user is admin
    
    
          
       
     

    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customers  $customers
     * @return \Illuminate\Http\Response
     */
    public function show(Customers $customers)
    {


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customers  $customers
     * @return \Illuminate\Http\Response
     */
    public function edit(Customers $customers)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customers  $customers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customers $customers)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customers  $customers
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customers $customers)
    {
        //
    }
}
