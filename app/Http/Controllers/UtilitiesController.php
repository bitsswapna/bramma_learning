<?php

namespace App\Http\Controllers;

use App\Utilities;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;

class UtilitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.Utilities.create');


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

    
        $va= Validator::make($request->all(), [
           
           'type'=>'required',
           
            'name' => 'required|max:120',
            'descrpton' => 'required|max:120',
           

        ]);
     
        
      if ($va->fails()) {
            return redirect()->back()->withErrors($va->errors())->withInput();
        }
        
            else
            {
                $file = $request->file('image');

                $insert= new Utilities();
                $insert->type=$request->input('type');

                $insert->key_word=$request->input('key_word');
                $insert->name=$request->input('name');
                $insert->key_word =$request->input('key_word');
                $insert->description=$request->input('descrpton');

                if ($request->hasFile('image')) {
                    if ($request->file('image') != null) {
                        $insert->image = ImageUploadWithPath($file, 'item');
                    }
                }
                $insert->save();
                return redirect('admin-utilites')->with('smessage','data inserted successfully');

            }
    
       
      


    }
    public function show()
    {
        return view('admin.Utilities.index');
        
    }
   public function getUtilites(Request $request)
     {
       
        $data = $request->get('form_data');
        $params = array();
        parse_str($data, $params);

        $data =Utilities ::get()->where('deleted_at','=',NULL);
        return DataTables::of($data)
            ->editColumn('sl#', function ($model) {
                return '<span class="si_no"></span> ';
            })
            ->editColumn('type', function ($model) {
                if($model->type==1)
                {
                    return 'Credit';
                }
                else{
                    return 'Debit';
                }
               
            })->editColumn('name', function ($model) {
                return $model->name;
            })
            ->editColumn('description', function ($model) {
                return $model->description;
            })
            ->editColumn('status', function ($model) {
                if (can('edit_admin_user')) {
                    $checked = '';
                    if ($model->active == 1) {
                        $checked = 'checked';
                    }
                    return '<div class="pretty p-switch p-fill" ><input type="checkbox" class="change-status" data-id="' . $model->id . '" ' . $checked . '><div class="state p-success" ><label ></label ></div></div>';
                }
            })
           
           
           
            ->editColumn('action', function ($model) {
                $html = '';
                if (can('edit_admin_user')) {
                    $html = '<a href="' . url('admin_utilite/request/' . $model->id) . '" title="Edit Customer"><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp';
                }
                if (can('edit_admin_user')) {
                    $html .= '<a href="#" class="delete-user-website" data-id="' . $model->id . '" title="Delete Customer"><i class="glyphicon glyphicon-trash"></i></a>';
                }
                if ($html == '') {
                    return 'No permission';
                }
                return $html;
            })
            ->rawColumns(['sl#', 'type', 'name', 'description','status', 'action'])
            ->make(true);

      }
      public function edit($id)
      {
        $ed = Utilities::find($id);
        return view('admin.Utilities.edit')->with(['ed' => $ed]);
      }
      public function utiltyupdate(Request $request,$id)
      {
//dd($request->all());
        $val = Validator::make($request->all(), [
            'type'=>'required',
           
            'name' => 'required|max:120',
            'descrpton' => 'required|max:120',
           

        ]);
        // dd("f");

        if ($val->fails()) {
            return redirect()->back()->withErrors($val->errors())->withInput($request->all());
        }
    $file=$request->hasFile('image');

        $update=Utilities::find($id);
        $update->type=$request->input('type');
      
       
        $update->name=$request->input('name');
        $update->key_word =$request->input('key_word');
        $update->description=$request->input('descrpton');

        $update->save();
          if ($request->hasFile('image')) {


             Utilities::where('id',$id)
                  ->update(['image'=> ImageUploadWithPath($request->image, 'item')]);
          }
        return redirect('admin-utilites')->with('smessage','data updated successfully');



      }
      public function deleteUtility(Request $request)
      {
          $del = Utilities::find($request->get('id'))->delete();
      
          return response(['status' => true, 'msg' => 'Succefully deleted'], 200);
        
          $del->delete();
              
      }
      public function changeStatus(Request $request)
      {
          $adminUser = Utilities::find($request->get('id'));
          if ($adminUser) {
              
              if ($adminUser->active == 1) {
                  $adminUser->active = 0;
                  $adminUser->save();
  
                
  
                  return response(['status' => true, 'msg' => 'User status successfully updated'], 200);
              }
              $adminUser->active = 1;
              $adminUser->save();
  
           
  
              return response(['status' => true, 'msg' => 'User status successfully updated'], 200);
  
  
          }
          return response(['status' => false, 'msg' => 'System cannot find this user'], 400);
      }
  


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Utilities  $utilities
     * @return \Illuminate\Http\Response
     */
   

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Utilities  $utilities
     * @return \Illuminate\Http\Response
     */
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Utilities  $utilities
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Utilities $utilities)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Utilities  $utilities
     * @return \Illuminate\Http\Response
     */
    public function destroy(Utilities $utilities)
    {
        //
    }
}
