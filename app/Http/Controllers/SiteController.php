<?php

namespace App\Http\Controllers;

use App\DynamicStatus;
use App\Site;
use App\Customers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;

use Illuminate\Support\Facades\Validator;

class SiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
       $insert=Customers::select('id','name')->get();
    //    dd($insert);

     return view('admin.Site.create')->with(['insert'=> $insert]);


    }
    public function getSite(Request $request)
    {

        $product_status = $request->get('product_status');

        $dynamicOrderStatus = DynamicStatus::pluck('name', 'id');

        $start_date = $request->get('start_date');
        $end_date = $request->get('end_date');
//dump($product_status);
//dump($start_date);
//dump($end_date);
//dd();
        $data = $request->get('form_data');
        $params = array();
        parse_str($data, $params);

        $data =Site ::get();
        return DataTables::of($data)
            ->editColumn('sl#', function ($model) {
                return '<span class="si_no"></span> ';
            })
            ->editColumn('address', function ($model) {
                return $model->address;
               
            })->editColumn('title', function ($model) {
                return $model->title;
            })
            ->editColumn('description', function ($model) {
                return $model->description;
            })
            ->editColumn('estiwork', function ($model) {
                return $model->estimated_work_in_day;
            })
            ->editColumn('estiexpense', function ($model) {
                return $model->estimated_expense;
            })
            ->editColumn('status', function ($model) {
                if ($model->if_cancelled == 1) {
                    $cancel = 'User Cancelled ';
                } else {
                    $cancel = '';
                }
                return '<span class="label ' . strtolower($model->getOrderStatus->class). '">' . $model->getOrderStatus->name . '</span><br>
            <span class="label label-cancelled">' . $cancel . '</span>';
            })

            ->editColumn('action', function ($model) use ($dynamicOrderStatus) {
                $product_status = $model->order_status;


                    $html = view('admin.Site.include')->with(['dynamicOrderStatus' => $dynamicOrderStatus, 'product_status' => $product_status, 'id' => $model->id,'mrp'=>number_format((float)$model->order_price, 2, '.', '')]);


                return $html;
            })
            ->rawColumns(['sl#', 'type', 'name', 'description','status', 'action'])
            ->make(true);

    }
    public function changeStatus(Request $request)
    {
        $adminUser = Site::find($request->get('id'));
        if ($adminUser) {
            
            if ($adminUser->active == 1) {
                $adminUser->active = 0;
                $adminUser->save();

              

                return response(['status' => true, 'msg' => 'User status successfully updated'], 200);
            }
            $adminUser->active = 1;
            $adminUser->save();

         

            return response(['status' => true, 'msg' => 'User status successfully updated'], 200);


        }
        return response(['status' => false, 'msg' => 'System cannot find this user'], 400);
    }


    public function deleteSite(Request $request)
    {
        $del = Site::find($request->get('id'));
    

      
        $del->delete();
            
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $va = Validator::make($request->all(), [
            'customer'=>'required',
            'adress' => 'required|max:120',
           'title'=> 'required',
           'descrption'=>'required|max:120',
           'estimwork'=>'required',
           'estexpensive'=>'required',
          
           

        ]);
       
        if ($va->fails()) {
            return redirect()->back()->withErrors($va->errors())->withInput($request->all());
        }
        else
        {
            $insert= new Site();
            $insert->customer_id=$request->input('customer');
           
            $insert->title=$request->input('title');
            $insert->address=$request->input('adress');
            $insert->description=$request->input('descrption');
            $insert->estimated_work_in_day=$request->input('estimwork');
            $insert->estimated_expense=$request->input('estexpensive');
            
            $insert->save();
            return redirect('sites')->with('smessage','data inserted succefully');

        }

       
    


    }
public function show()
{
 
    return view('admin.Site.index');

}


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Site  $site
     * @return \Illuminate\Http\Response
     */
  
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Site  $site
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      
            $ed=Customers::join('sites', 'customers.id', '=', 'sites.customer_id')
            ->select('sites.*', 'customers.name')->where('sites.id',$id)->first();
          
            return view('admin.Site.edit')->with(['ed'=>$ed]);

       
       
        
    }
    public function siteupdate(Request $request,$id)
    {
    //   dd('hi');

        $va = Validator::make($request->all(), [
           
            'adress' => 'required|max:120',
           'title'=> 'required',
           'descrption'=>'required|max:120',
           'estimwork'=>'required',
           'estexpensive'=>'required',
          
           

        ]);
    
        if ($va->fails()) {
            return redirect()->back()->withErrors($va->errors())->withInput($request->all());
        }
        else
        {
            $updated= Site::find($id);

            $updated->title=$request->input('title');
            $updated->address=$request->input('adress');
          
            $updated->description=$request->input('descrption');
            $updated->estimated_work_in_day=$request->input('estimwork');
            $updated->estimated_expense=$request->input('estexpensive');
            
            $updated->save();
            return redirect('sites')->with('smessage','data updated succefully');
        }

      
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Site  $site
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Site $site)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Site  $site
     * @return \Illuminate\Http\Response
     */
    public function destroy(Site $site)
    {
        //
    }
}
