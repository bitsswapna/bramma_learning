<?php

namespace App\Http\Controllers;

use App\VehiclesDetails;
use App\ClientDetails;
use App\VehicleDrivers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class VehiclesDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("admin.clientvehicle.createclient");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $val = Validator::make($request->all(), [
            'name' => 'required|min:3|max:50',
            'company' => 'required|min:3|max:50',
            'agreement' => 'required|min:3|max:50',
            'wrkduration' => 'required|min:3|max:50',
           
        ]);

        if ($val->fails()) {
            return redirect()->back()->withErrors($val->errors())->withInput($request->all());
        }
        $data = new ClientDetails();
        $data->name = $request->input('name');
        $data->company = $request->input('company');
        $data->agreement = $request->input('agreement');
        $data->start_date = $request->input('startdate');
        $data->end_date = $request->input('enddate');
        $data->work_duration = $request->input('wrkduration');
        $data->save();
      
     
        return redirect()->back()->with('message', 'New client added.');

        
    }
    public function display()
    {
        $all=ClientDetails::get();
        return view ('admin.clientvehicle.clientview')->with(['data'=>$all]);
    }
    public function editfirst($id)
    {
        $ed= ClientDetails::find($id);
        return view ('admin.clientvehicle.editclient')->with(['ed'=>$ed]);

    }
    public function updatefirst(Request $request,$id)
    {
        $val = Validator::make($request->all(), [
            'name' => 'required|min:3|max:50',
            'company' => 'required|min:3|max:50',
            'agreement' => 'required|min:3|max:50',
            'wrkduration' => 'required',
           
        ]);

        if ($val->fails()) {
            return redirect()->back()->withErrors($val->errors())->withInput($request->all());
        }
        $up=ClientDetails::find($id);
        $up->name = $request->input('name');
        $up->company = $request->input('company');
        $up->agreement = $request->input('agreement');
        $up->start_date = $request->input('startdate');
        $up->end_date = $request->input('enddate');
        $up->work_duration = $request->input('wrkduration');
     
        $up->save();

    
    return redirect()->back()->with('smessage', 'client updated');
}
public function clientdelete($id)
{

    $del=ClientDetails::find($id);
    $del->delete();
    return redirect()->back()->with('smessage', 'client deleted');

}
public function form()
{

}
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\VehiclesDetails  $vehiclesDetails
     * @return \Illuminate\Http\Response
     */
    public function show(VehiclesDetails $vehiclesDetails)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\VehiclesDetails  $vehiclesDetails
     * @return \Illuminate\Http\Response
     */
    public function edit(VehiclesDetails $vehiclesDetails)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\VehiclesDetails  $vehiclesDetails
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VehiclesDetails $vehiclesDetails)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\VehiclesDetails  $vehiclesDetails
     * @return \Illuminate\Http\Response
     */
    public function destroy(VehiclesDetails $vehiclesDetails)
    {
        //
    }
}
