<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Mail\OrderStatusChange;
use App\Mail\UserRegistration;
use App\Mail\UserOrderProduct;
use App\User;
use Illuminate\Support\Facades\Mail;

class NotificationController extends Controller
{
    /**
     * @return $this
     */
    public static function notifyUserRegistration($key, $user_id)
    {
//        $email = User::where("role_id",0)->pluck('email');
        $email = 'swapna.brammait@gmail.com';
        $subject = 'New registration';
        if ($email) {
            Mail::to($email)
                ->queue(new UserRegistration($key, $user_id));
        }
    }

}
