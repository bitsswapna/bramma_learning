<?php

namespace App\Http\Controllers;

use App\Vehicles;
use App\VehicleOwners;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class VehiclesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('admin.Vehicles.create');

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $va = Validator::make($request->all(), [

            'name'=>'required',
            'adress'=>'required',
            'number'=>'required', 

            'vehinumber'=>'required',

           'hrlyamount'=> 'required',




        ]);

        if ($va->fails()) {
            return redirect()->back()->withErrors($va->errors())->withInput($request->all());
        }

else
{
    $insert= new Vehicles();
    $insert->type=$request->input('type');
    $insert->vehicle_number=$request->input('vehinumber');
    $insert->hourly_amount=$request->input('hrlyamount');
    $insert->save();

    $input =new VehicleOwners();
    $input->vehicle_id= $insert->id;
    $input->name=$request->input('name');
    $input->address=$request->input('adress');
    $input->contact_number=$request->input('number');


     $input->save();
    return redirect('Vehicles')->with('smessage','data inserted successfully');

}
      

    }
        public function getVehicle(Request $request)
        {


        $data = $request->get('form_data');
        $params = array();
        parse_str($data, $params);

        $data = DB::table('vehicles')
        ->join('vehicle_owners', 'vehicles.id', '=', 'vehicle_owners.vehicle_id')

        ->select('vehicles.*', 'vehicle_owners.name','vehicle_owners.contact_number')
        ->get()->where('deleted_at','=',NULL);

        // $data =Vehicles ::get();

        return DataTables::of($data)
            ->editColumn('sl#', function ($model) {
                return '<span class="si_no"></span> ';
            })
            ->editColumn('type', function ($model) {
                if($model->type==1)
                {
                    return "Rent";

                }else if($model->type==2){
                    return "Partnership";
                }
                else{
                    return "Own Vehicle";
                }

            })


            ->editColumn('vehino', function ($model) {
                return $model->vehicle_number;
            })

            ->editColumn('hourlyamount', function ($model) {
                return $model->hourly_amount;
            })
            ->editColumn('name', function ($model) {
                return $model->name;
            })
            ->editColumn('contactno', function ($model) {
                return $model->contact_number;
            })

            ->editColumn('status', function ($model) {
                if (can('edit_admin_user')) {
                    $checked = '';
                    if ($model->active == 1) {
                        $checked = 'checked';
                    }
                    return '<div class="pretty p-switch p-fill" ><input type="checkbox" class="change-status" data-id="' . $model->id . '" ' . $checked . '><div class="state p-success" ><label ></label ></div></div>';
                }
            })



            ->editColumn('action', function ($model) {
                $html = '';
                if (can('edit_admin_user')) {
                    $html = '<a href="' . url('admin_vehi/edit/' . $model->id) . '" title="Edit Customer"><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp';
                }
                if (can('edit_admin_user')) {
                    $html .= '<a href="#" class="delete-user-website" data-id="' . $model->id . '" title="Delete Customer"><i class="glyphicon glyphicon-trash"></i></a>';
                }
                if ($html == '') {
                    return 'No permission';
                }
                return $html;
            })
            ->rawColumns(['sl#', 'type', 'name','contactno','vehino','hourlyamount','status', 'action'])
            ->make(true);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Vehicles  $vehicles
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return view('admin.Vehicles.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Vehicles  $vehicles
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $users=Vehicles::leftjoin('vehicle_owners', 'vehicles.id', '=', 'vehicle_owners.vehicle_id')
        ->select('vehicles.*', 'vehicle_owners.name','vehicle_owners.contact_number')->where('vehicles.id',$id)->first();


        // dd($users);
        return view('admin.Vehicles.edit')->with(['user'=>$users]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Vehicles  $vehicles
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $val = Validator::make($request->all(), [
           
            'name'=>'required',
            'adress'=>'required',
            'number'=>'required', 

            'vehinumber'=>'required',

           'hrlyamount'=> 'required',




        ]);

        if ($val->fails()) {
            return redirect()->back()->withErrors($val->errors())->withInput($request->all());
        }
else
{
    $update= Vehicles::find($id);

    $update->vehicle_number=$request->input('vehinumber');
    $update->hourly_amount=$request->input('hrlyamount');
    $update->save();

    if($update->save())
    {
        $up = VehicleOwners::where('vehicle_id',$id)->first();



    $up->name=$request->input('name');

    $up->contact_number=$request->input('number');


    $up->save();
    
    }
    return redirect('Vehicles')->with('smessage','data updated successfully');
}

      

    }
    
    public function destroy(Request $request)
    {
        $del = Vehicles::find($request->get('id'));



        $del->delete();
        return response(['status' => true, 'msg' => 'Succefully deleted'], 200);

    }
    public function changeStatus(Request $request)
    {
        $adminUser = Vehicles::find($request->get('id'));
        if ($adminUser) {

            if ($adminUser->active == 1) {
                $adminUser->active = 0;
                $adminUser->save();



                return response(['status' => true, 'msg' => 'Vehicles status successfully updated'], 200);
            }
            $adminUser->active = 1;
            $adminUser->save();



            return response(['status' => true, 'msg' => 'Vehicles status successfully updated'], 200);


        }
        return response(['status' => false, 'msg' => 'System cannot find this user'], 400);
    }





    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Vehicles  $vehicles
     * @return \Illuminate\Http\Response
     */

}
