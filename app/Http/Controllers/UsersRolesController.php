<?php

namespace App\Http\Controllers;

use App\UsersRoles;
use App\CustomersFeild;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class UsersRolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    
        return view('admin.duties.createrole');
    }
    public function createfirst(Request $request)
    {
        $val = Validator::make($request->all(), [
            'frstname' => 'required|max:120',
            'lastname' => 'required|max:120',
            'adress' => 'required|max:120',
            'mobno' => 'required|min:11|numeric',
           
        ]);

        if ($val->fails()) {
            return redirect()->back()->withErrors($val->errors())->withInput($request->all());
        }

        $insert= new UsersRoles();
        $insert->select_roles=$request->input('selectrole');
     
        $insert->first_name =$request->input('frstname');
        $insert->last_name =$request->input('lastname');
        $insert->address =$request->input('adress');
        $insert->mobile_no=$request->input('mobno');
        $insert->name=$request->input('frstname') .  $request->input('lastname');
        $insert->save();
        return redirect('duties')->with('data inserted succefully');


    }
    public function userslist()
    {
        $list=UsersRoles::get();
        return view('admin.duties.viewrole')->with(['list'=>  $list]);

    }
    public function editfirst($id)
    {
     $edt=UsersRoles::find($id);
     return view('admin.duties.editrole')->with(['ed'=>$edt]);

    }
    public function updatefirst(Request $request,$id)
    {
        $va = Validator::make($request->all(), [
            'frstname' => 'required|max:120',
            'lastname' => 'required|max:120',
            'adress' => 'required|max:120',
            'mobno' => 'required|min:11|numeric',
           
        ]);

        if ($va->fails()) {
            return redirect()->back()->withErrors($va->errors())->withInput($request->all());
        }
        $update=UsersRoles::find($id);
        $update->select_roles=$request->input('selectrole');
     
        $update->first_name =$request->input('frstname');
        $update->last_name =$request->input('lastname');
        $update->address =$request->input('adress');
        $update->mobile_no=$request->input('mobno');
        $update->name=$request->input('frstname') .  $request->input('lastname');
        $update->save();
        return redirect('duties')->with('data updated succefully');


    }
    public function deletefirst($id)
    {
        $del=UsersRoles::find($id);
        if ($del != null) {
            $del->delete();
            return redirect()->back()->with('message', 'New data deleted.');
           
        }
    
        return redirect()->back()->with(['message'=> 'Wrong ID!!']);
       

    }


    //list//

public function viewlist()
{
    return view('admin.duties.listbutton');


}
public function driverview()
{
    $lis=UsersRoles::get();
    return view('admin.duties.driverviewonly')->with(['driver'=>$lis]);
}
public function cleanerview()
{
    $liste=UsersRoles::get();
    return view('admin.duties.cleanerview')->with(['cleaner'=>$liste]);
}
public function fulllist()
{
    $full=UsersRoles::get();
    return view('admin.duties.onlyviewrole')->with(['full'=>$full]);
}
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UsersRoles  $usersRoles
     * @return \Illuminate\Http\Response
     */
    public function show(UsersRoles $usersRoles)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UsersRoles  $usersRoles
     * @return \Illuminate\Http\Response
     */
    public function edit(UsersRoles $usersRoles)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UsersRoles  $usersRoles
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UsersRoles $usersRoles)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UsersRoles  $usersRoles
     * @return \Illuminate\Http\Response
     */
    public function destroy(UsersRoles $usersRoles)
    {
        //
    }
}
