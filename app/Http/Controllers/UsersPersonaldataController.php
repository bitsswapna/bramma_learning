<?php

namespace App\Http\Controllers;

use App\Activity;
use App\BankDetails;
use App\Http\Controllers\Controller;
use App\Libraries\Slug;
use App\Mail\AdminUserCreate;
use App\Mail\UserRegistration;
use App\Models\Address;
use App\Models\Ancestor;
use App\Models\CustomerCareCategory;
use App\Models\CustomerCareUsers;
use App\Models\Mlm;
use App\Models\StaffCustomer;
use App\Models\StateList;
use App\Models\UserDetails;
use App\Models\UserPaymentMode;
use App\Models\UserPermissions;
use App\Models\UserRegistrationRequest;
use App\Models\UserStaff;
use App\Models\WellnessAdvisors;
use App\Role;
use App\Setting;
use App\User;
use App\UsersAdd;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;
 
use App\UsersPersonaldata;


class UsersPersonaldataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexin()
    {
    
        return view('admin.admin-users.admin-user-driver');

    }
    public function indx()
    {
    
        return view('admin.admin-users.admin-user-cleaner');

    }
    var $ma_parents = array(1);
    var $all_parents = array(1);

    public function index()
    {
        return view('admin.admin-users.index');
    }

    public function waRequest()
    {
        return view('admin.admin-users.admin-user-wa');
    }

    public function userRequest()
    {
        return view('admin.admin-users.admin-user-website');
    }

    /**
     * @return table
     */
    public function getAdminUsers(Request $request)
    {
        $user_id = $request->get('user_id');
        $user_name = $request->get('user_name');
        $data = $request->get('form_data');
        $params = array();
        parse_str($data, $params);

        $data = User::join('roles', 'users.role_id', '=', 'roles.id')
            ->where(function ($query) use ($user_id, $user_name) {
                $query->where('role_id', '!=', 1);
                if ($user_id != '')
                    $query->where('users.id', $user_id);
                if ($user_name != '')
                    $query->where('users.name', 'like', '%' . $user_name . '%');
            })
            ->orderBy('users.id', 'DESC')
            ->select('users.*', 'roles.display_name')->get();
        return DataTables::of($data)
            ->editColumn('sl#', function ($model) {
                return '<span class="si_no"></span> ';
            })
            ->editColumn('user_id', function ($model) {
                return $model->id;
            })
            ->editColumn('name', function ($model) {
//                return $model->name;
                if ($model->getRoleDashboardType) {
                    if ($model->getRoleDashboardType->user_type == 2) {
                        return '<a style="cursor: pointer;"  class=" link adminuser-login small"  data-url="{!! env(APP_URL)!!}/profile !!}"   data-user="' . $model->id . '" >&nbsp; ' . $model->name . ' <i class="fas fa-external-link-alt pull-left"></i></a>';
                    } else {
                        return $model->name;
                    }
                }

            })

            ->editColumn('role', function ($model) {
                return '<span class="small badge badge-info">'.$model->display_name.'</span>';
            })
            // ->editColumn('parent', function ($model) {
            //     if($model->getWAParent){
            //         return  $this->_getUsersName($model->getWAParent->parent,$staff=0);
            //     }else{
            //         if($model->display_name == 'Staff'){
            //             return $this->_getUsersName($model->id,$staff=1);
            //         }
            //         return 'admin' ;
            //     }
            // })
            ->editColumn('created_at', function ($model) {
                return date('d M Y', strtotime($model->created_at));
            })
            ->editColumn('entry_type', function ($model) {
                $entryType =  array('Direct Customer' => '1' ,'Referral Link' => '2','added by admin' => '3', 'Added by WA' => '4'  );
                return array_search($model->user_entry,$entryType);
            })
            ->editColumn('status', function ($model) {
                if (can('edit_admin_user')) {
                    $checked = '';
                    if ($model->active == 1) {
                        $checked = 'checked';
                    }
                    return '<div class="pretty p-switch p-fill" ><input type="checkbox" class="change-status" data-id="' . $model->id . '" ' . $checked . '><div class="state p-success" ><label ></label ></div></div>';
                }
            })
            ->editColumn('action', function ($model) {
                $html = '';
                if (can('edit_admin_user')) {
                    $html = '<a data-toggle="tooltip"  data-placement="top" data-html="true" title="Edit" class="btn action-button bg-primary-blue" href="' . url('admin_user/edit/' . $model->id) . '" ><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;';
                }
                //    if(can('edit_admin_user')){
                //        $html.= '<a data-toggle="tooltip"  data-placement="top" data-html="true" title="Delete" class="btn action-button bg-primary-red delete-user" href="#" data-id="'.$model->id.'" ><i class="fa fa-trash-o"></i></a>';
                //    }
                if (can('edit_admin_user')) {
                    $html .= '<a data-toggle="tooltip"  data-placement="top" data-html="true" title="view"   class="btn action-button bg-primary-light view-user" href="#" data-id="' . $model->id . '" ><i class="fa fa-eye"></i></a>';
                }
                if ($html == '') {
                    return 'No permission';
                }
                return $html;
            })
            ->rawColumns(['sl#','user_id','name', 'email', 'role', 'parent', 'created_at','entry_type','status', 'action'])
            ->make(true);
    }

    public function getAdminDrivers(Request $request)
    {
       
        $data = $request->get('form_data');
        $params = array();
        parse_str($data, $params);                                 

        $data = User::where('role_id', '=', 2)->get();
        
        return DataTables::of($data)
        
            ->editColumn('sl#', function ($model) {
                return '<span class="si_no"></span> ';
            })
            ->editColumn('name', function ($model) {
                return $model->name;
            })
            ->editColumn('email', function ($model) {
                return $model->email;
            })
            ->editColumn('role', function ($model) {
                return '<span class="small badge badge-info">'."Driver";
              
            })
            ->editColumn('mobile', function ($model) {
                return  $model->mobile;
            })
         
           
            

           
            ->rawColumns(['sl#', 'name', 'email', 'role','address','mobile','status', 'action'])
            ->make(true);
    }
    public function getAdminCleaner(Request $request)
    {
       
        $data = $request->get('form_data');
        $params = array();
        parse_str($data, $params);                                 

        $data = User::where('role_id', '=', 3)->get();
        
        return DataTables::of($data)
        
            ->editColumn('sl#', function ($model) {
                return '<span class="si_no"></span> ';
            })
            ->editColumn('name', function ($model) {
                return $model->name;
            })
            ->editColumn('email', function ($model) {
                return $model->email;
            })
            ->editColumn('role', function ($model) {
                return '<span class="small badge badge-info">'."Cleaner";
              
            })
            ->editColumn('mobile', function ($model) {
                return  $model->mobile;
            })
         
           
            

           
            ->rawColumns(['sl#', 'name', 'email', 'role','address','mobile','status', 'action'])
            ->make(true);
    }

    private function _getUsersName($id,$staff=0)
    {
        if($staff == 0){
            $user_id = $id;
        }else{

            $user_id = UserStaff::where('staff_id',$id)->first();
            $user_id =  $user_id->user_id;
        }
        $userData = User::where('id',$user_id)->first();
        if($userData){
            return    $userData->id . ': '.$userData->name;
        }else{
            return 'admin';
        }


    }


    public function usersDetails(Request $request)
    {
        $data = $request->get('id');
        $data= User::get();
        dd($data);
       
        // $states = StateList::pluck('state', 'id');
        $returnHTML = view('admin.admin-users.customer-details-view', ['users' => $data]);
        return response()->json($returnHTML);
    }


    /**
     * @return table
     */
    public function getAdminUsersWeb(Request $request)
    {
       
        $data = $request->get('form_data');
        $params = array();
        parse_str($data, $params);                                 

        $data = User::where('role_id', '!=', 1)->get();
        
        return DataTables::of($data)
        
            ->editColumn('sl#', function ($model) {
                return '<span class="si_no"></span> ';
            })
            ->editColumn('name', function ($model) {
                return $model->name;
            })
            ->editColumn('email', function ($model) {
                return $model->email;
            })
            ->editColumn('role', function ($model) {
                return "Customer";
              
            })
            ->editColumn('mobile', function ($model) {
                return  $model->mobile;
            })
            ->editColumn('address', function ($model) {
                return $model->address;
            })
           
           
            ->editColumn('status', function ($model) {
                return $model->status;
            })
             ->editColumn('action', function ($model) {
                $html = '';
                if (can('edit_admin_user')) {
                    $html = '<a href="' . url('admin_user/user-request/' . $model->id) . '" title="Edit User" class="btn action-button bg-primary-blue btn-xs"><i class="fa fa-eye"></i></a>&nbsp;';
                }
                if (can('edit_admin_user')) {
                    $html .= '<a href="#" class="delete-user-website btn action-button bg-primary-red btn-xs" data-id="' . $model->id . '" title="Delete User"><i class="fa fa-trash fa-lg"></i></a>';
                }
                if ($html == '') {
                    return 'No permission';
                }
                return $html;
            })


           
            ->rawColumns(['sl#', 'name', 'email', 'role','address','mobile','status', 'action'])
            ->make(true);
    }
    public function deleteUserWebsite(Request $request)
    {
        $user = User::find($request->get('id'));
        // Check if the selected user is admin

        if ($user) {
            /****************** Activity Adding ************/
            $activity = new Activity();
            $activity->user_id = Auth::id();
            $activity->relation_id = $user->id;
            $activity->type = 'user';
            $activity->activity = 'WA deleted by:';
            $activity->save();

            User::where('id', $user->id)->delete();
            return response()->json([
                'status' => true,
                'msg' => 'Successfully deleted the user',
            ], 200);
        }
        return response()->json([
            'status' => false,
            'msg' => 'Something went wrong',
        ], 400);
    }
    
    public function updateForm($id)
    {

        if (!$user = User::find($id)) {
            abort('404');
        }
       $user=User::find($id);
        $roles = Role::pluck('display_name', 'id');
      
       
        return view('admin.admin-users.edit')->with(['roles'=>$roles,'user'=>$user]);
    }

     
    public function createWA(Request $request,$id)
    {
    //    dd("hai");
        $val = Validator::make($request->all(), [
           
           
            'first_name' => 'required|min:3|max:50',           
         'last_name' => 'required|min:3|max:50',
        // 'role_id' => 'required',
        'address' => 'required',
       
     
        'mobile' => 'required|regex:/[0-9]{10}/|digits:10|unique:users',
        // 'mobile' => 'required',
        'email' => 'required|email|min:6|max:75|unique:users',
       
   
        ]);

        $update=User::find($id);
    $update->name=$request->input('first_name') . ' ' . $request->input('last_name');
    $update->first_name =$request->input('first_name');
    $update->last_name=$request->input('last_name');
    $update->gender =$request->input('gender');
   
    $update->dob =$request->input('date_of_birth');
    $update->email=$request->input('email');
    $update->mobile =$request->input('mobile');
  
    $update->address=$request->input('address');
   
  
    // $update->role_id=$request->input('role');
     $update->save();
     
                /****************** Activity Adding ************/
                $activity = new Activity();
                $activity->user_id = Auth::id();
                $activity->relation_id = $update->id;
                $activity->type = 'user';
                $activity->activity = 'New user has been created by admin';
                $activity->save();
                /************************************************/
return response()->json([
    'data'=>true,
    'message'=>"successfully inserted"
],200);
    }
    
  



 


    public function changeStatus(Request $request)
    {
        $adminUser = User::find($request->get('id'));
        if ($adminUser) {
            if ($adminUser->active == 1) {
                $adminUser->active = 0;
                $adminUser->save();

                /****************** Activity Adding ************/
                $activity = new Activity();
                $activity->user_id = Auth::id();
                $activity->relation_id = $adminUser->id;
                $activity->type = 'user';
                $activity->activity = 'Admin user status inactivated by:';
                $activity->save();

                return response(['status' => true, 'msg' => 'User status successfully updated'], 200);
            }
            $adminUser->active = 1;
            $adminUser->save();

            /****************** Activity Adding ************/
            $activity = new Activity();
            $activity->user_id = Auth::id();
            $activity->relation_id = $adminUser->id;
            $activity->type = 'user';
            $activity->activity = 'Admin user status activated by:';
            $activity->save();

            return response(['status' => true, 'msg' => 'User status successfully updated'], 200);


        }
        return response(['status' => false, 'msg' => 'System cannot find this user'], 400);
    }

    public function deleteWA(Request $request)
    {
        $user = User::find($request->get('id'));
        // Check if the selected user is admin

        if ($user) {
            /****************** Activity Adding ************/
            $activity = new Activity();
            $activity->user_id = Auth::id();
            $activity->relation_id = $user->id;
            $activity->type = 'user';
            $activity->activity = 'WA deleted by:';
            $activity->save();

            User::where('id', $user->id)->delete();
            return response()->json([
                'status' => true,
                'msg' => 'Successfully deleted the user',
            ], 200);
        }
        return response()->json([
            'status' => false,
            'msg' => 'Something went wrong',
        ], 400);
    }

    
    public function getAdminUsersWA(Request $request)
    {
        $data = $request->get('form_data');
        $params = array();
        parse_str($data, $params);

        $data = User::get();
        return DataTables::of($data)
            ->editColumn('sl#', function ($model) {
                return '<span class="si_no"></span> ';
            })
            ->editColumn('name', function ($model) {
                return $model->name;
            })->editColumn('email', function ($model) {
                return $model->email;
            })
            ->editColumn('role', function ($model) {
                return "Customer";
            })
            // ->editColumn('last_login', function ($model) {
            //     return $model->last_login;
            // })
            // ->editColumn('created_at', function ($model) {
            //     return $model->created_at;
            // })
            ->editColumn('status', function ($model) {
                return $model->mobile;
            })
            ->editColumn('action', function ($model) {
                $html = '';
                if (can('edit_admin_user')) {
                    $html = '<a href="' . url('admin_user/wa-request/' . $model->id) . '" title="Edit User"><i class="fa fa-eye"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp';
                }
                if (can('edit_admin_user')) {
                    $html .= '<a href="#" class="delete-user-website" data-id="' . $model->id . '" title="Delete User"><i class="fa fa-trash-o fa-lg"></i></a>';
                }
                if ($html == '') {
                    return 'No permission';
                }
                return $html;
            })
            ->rawColumns(['sl#', 'name', 'email', 'role', 'last_login', 'created_at', 'status', 'action'])
            ->make(true);
    }





    
    /**
     * @return wa details
     */
    public function waRequestDetail($id)
    {
       
        $user = User::find($id)->where("active", 1)->where('role_id', '!=',1)->get();
        $roles = Role::where('id', '!=', 0)->get();
        return view('admin.admin-users.admin-user-wa-detail')->with([ 'user' => $user, 'roles' => $roles]);;
    }

    public function uesrRequestDetail($id)
    {
        $user = User::find($id);
        return view('admin.admin-users.admin-user-website-detail')->with(['user' => $user]);;
    }

    /**
     * WA create form
     * @return $this
     */
    public function createForm()
    {
        dd('hh');
                $roles = Role::where('id', '!=', 2)->get();
        $states = StateList::pluck('state', 'id');
        return view('admin.admin-users.create')->with(['roles' => $roles, 'states' => $states]);
    }

 
   
  

    /*Private function to get user permission*/
   

  
    public function getNormalForm()
    {
        $states = StateList::pluck('state', 'id');
        $returnHTML = view('admin.admin-users-add.basic-form', ['states' => $states])->render();
        return response()->json($returnHTML);
    }


  
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        dd('hi');

        $v = Validator::make($request->all(), [
            'first_name' => 'required|min:3|max:50',
           
            'address' => 'required',
            'location' => 'required',
            'pincode' => 'required|digits:6',
//                    'mobile' => 'required|regex:/[0-9]{10}/|digits:10|unique:users',
            'mobile' => 'required',
            'email' => 'required|email|min:6|max:75|unique:users',
            'password' => 'required|min:6|max:30|confirmed',

        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())->withInput($request->all());
        }

        else
        {

    $file = $request->file('image');
    dd($file);
    $insert= new User();
    $insert->name=$request->input('first_name') . ' ' . $request->input('last_name');
    $insert->first_name =$request->input('first_name');
    $insert->last_name=$request->input('last_name');
    $insert->gender =$request->input('gender');
   
    $insert->dob =$request->input('date_of_birth');
    $insert->email=$request->input('email');
    $insert->mobile =$request->input('mobile');
    $insert->password =$request->input('password');
    $insert->address=$request->input('address');
    $insert->pin_code=$request->input('pincode');
    $insert->location=$request->input('location');
  
    if ($request->hasFile('image')) {
        if ($request->file('image') != null) {
            $insert->image = ImageUploadWithPath($file, 'item');
        }
    }
    
     $insert->save();
     
                /****************** Activity Adding ************/
                $activity = new Activity();
                $activity->user_id = Auth::id();
                $activity->relation_id = $insert->id;
                $activity->type = 'user';
                $activity->activity = 'New user has been created by admin';
                $activity->save();
                /************************************************/
       return response()->json([
        'data'=>true,
        'message'=>"successfully inserted"
         ],200);
    }
}
    public function update($id, Request $request)
    {
    
        dd($request->all());

        if (!$id && !$user = User::find($id)) {
            abort('404');
        }
        $roleID = $request->input('role_id');

        $v = Validator::make($request->all(), [
            'first_name' => 'required|min:3|max:50',
          
            'role_id' => 'required',
//            'mobile' => 'required|regex:/[0-9]{10}/|digits:10|unique:users,mobile,' . $user->id,
            'mobile' => 'required',
            'email' => 'required|email'
           
            // 'confirm_password' => 'same:new_password',
        ]);

        if ($v->fails()) {
           
            $response['status'] = false;
            $response['message'] = trans('messages.msg_reg_failed');
            $response['errors'] = $v->getMessageBag()->toJson();
            $response['url'] = '/';

        } else {
         
            User::find($id)->update(['role_id'=>$roleID,
            'name'=>$request->input('first_name') .  '  '  . $request->input('last_name'),
            'first_name'=>$request->input('first_name'),
            'last_name'=>$request->input('last_name'),
            'gender'=>$request->input('gender'),
            'mobile'=>$request->input('mobile'),
            'email'=>$request->input('email'),

            'password'=>\Hash::make($request->input('password')),

            'address'=>$request->input('address'),

            
            ]);

                /****************** Activity Adding ************/
                $activity = new Activity();
                $activity->user_id = Auth::id();
                $activity->relation_id = $id;
                $activity->type = 'user';
                $activity->activity = 'User has been edited by admin';
                $activity->save();
                $response['status'] = true;
                $response['message'] = trans('messages.msg_reg_success');
                return response()->json($response);
            }
        }
        
        
       

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UsersPersonaldata  $usersPersonaldata
     * @return \Illuminate\Http\Response
     */
    public function show(UsersPersonaldata $usersPersonaldata)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UsersPersonaldata  $usersPersonaldata
     * @return \Illuminate\Http\Response
     */
    public function edit(UsersPersonaldata $usersPersonaldata)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UsersPersonaldata  $usersPersonaldata
     * @return \Illuminate\Http\Response
     */
   

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UsersPersonaldata  $usersPersonaldata
     * @return \Illuminate\Http\Response
     */
    public function destroy(UsersPersonaldata $usersPersonaldata)
    {
        //
    }
}
