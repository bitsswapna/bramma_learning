<?php

namespace App\Http\Controllers;

use App\CustomersFeild;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;

class CustomersFeildController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.customers.createcustomers');
        
    }
    public function indx()
    {
        return view('admin.customers.request');
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createfirst(Request $request)
    {
        dd($request->all());
        $val = Validator::make($request->all(), [
            'frstname' => 'required|max:120',
            'lastname' => 'required|max:120',
            'place' => 'required|max:120',
            'adress' => 'required|max:120',
            'contnumber' => 'required|min:11|numeric',
            'companyname'=> 'required|max:120',

        ]);

        if ($val->fails()) {
            return redirect()->back()->withErrors($val->errors())->withInput($request->all());
        }

        $insert= new CustomersFeild();
        $insert->first_name=$request->input('first_name');
        $insert->last_name =$request->input('last_name');
        $insert->place=$request->input('place');
        $insert->address=$request->input('adress');
        $insert->contact_no=$request->input('contnumber');
        $insert->company_name=$request->input('companyname');
        $insert->name =$request->input('frstname') .   $request->input('lastname');
        $insert->save();
        return redirect('customers')->with('data inserted succefully');
    
    }
    // public function viewfirst()
    // {
    //     $view=CustomersFeild::get();
    //     return view('admin.customers.customersview')->with(['input'=>$view]);
    // }
    public function editaction($id)
    {
        $ed=CustomersFeild::find($id);
        return view('admin.customers.editcustomers')->with(['ed'=>$ed]);

    }
    public function updatefirst(Request $request,$id)
    {
        $va = Validator::make($request->all(), [
            'frstname' => 'required|max:120',
            'lastname' => 'required|max:120',
            'place' => 'required|max:120',
            'adress' => 'required|max:120',
            'contnumber' => 'required|min:11|numeric',
            'companyname'=> 'required|max:120',

        ]);

        if ($va->fails()) {
            return redirect()->back()->withErrors($va->errors())->withInput($request->all());
        }

        $update= CustomersFeild::find($id);
        $update->first_name=$request->input('frstname');
        $update->last_name =$request->input('lastname');
        $update->place=$request->input('place');
        $update->address=$request->input('adress');
        $update->contact_no=$request->input('contnumber');
        $update->company_name=$request->input('companyname');
        $update->name =$request->input('frstname') .   $request->input('lastname');
        $update->save();
        return redirect()->back()->with('message', 'New data updated.');
    
    
    }
    public function viewonly()
    {
        $customers=CustomersFeild::get();
        return view('admin.customers.custmerview')->with(['custom'=> $customers]);

    }
    public function deletefirst($id)
    {
        $delt=CustomersFeild::find($id);
        $delt->delete();
        return redirect()->back()->with('message', 'New data deleted.');

    }

    public function search(Request $request)
    {
        $serach=$request->get('search');
        $post=DB::table('customers_table')->where('name', 'like', '%'.$serach.'%')->paginate();
        return view('admin.customers.custmerview')->with(['custom'=>$post]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CustomersFeild  $customersFeild
     * @return \Illuminate\Http\Response
     */
    public function show(CustomersFeild $customersFeild)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CustomersFeild  $customersFeild
     * @return \Illuminate\Http\Response
     */
    public function edit(CustomersFeild $customersFeild)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CustomersFeild  $customersFeild
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CustomersFeild $customersFeild)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CustomersFeild  $customersFeild
     * @return \Illuminate\Http\Response
     */
    public function destroy(CustomersFeild $customersFeild)
    {
        //
    }
}
