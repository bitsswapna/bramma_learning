<?php

namespace App\Http\Controllers;

use App\VehicleDrivers;
use App\VehiclesDetails;
use App\ClientDetails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class VehicleDriversController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("admin.vehidrivers.vehidriverform");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
       
            
        $val = Validator::make($request->all(), [
            'name' => 'required|max:120',
            'phoneno' => 'required|min:11|numeric',
            'adress'=>'required',
            'salryduration'=>'required',
            'amount'=>'required',
            'vehino' => 'required',
            'vehitype' => 'required',
            'vhiperhur'=>'required',
            'rentervehicle'=>'required',

        ]);

        if ($val->fails()) {
            return redirect()->back()->withErrors($val->errors())->withInput($request->all());
        }
      $add=new VehicleDrivers();
      $add->name=$request->input('name');
      $add->phone_no=$request->input('phoneno');
      $add->addre_ss=$request->input('adress');
      $add->salary_duration=$request->input('salryduration');
      $add->amount=$request->input('amount');
      $add->save();
      
    //   dd($add);
      $insert= new VehiclesDetails();
      $insert->vehicle_no=$request->input("vehino");
      $insert->vehicle_type=$request->input("vehitype");
      $insert->vehicle_cost_per_hur=$request->input("vhiperhur");
      $insert->renter_vehicle=$request->input("rentervehicle");
      $insert->driver_id=$add->id;
      $insert->save();
      return redirect()->back()->with('message', 'New data added.');
    
    }

//vehicle details
public function display()
{
    $dis=DB::table('driver_details')
    ->join('vehicle_details','vehicle_details.driver_id','=','driver_details.id')
    ->select('driver_details.id','vehicle_details.driver_id')
    ->select('driver_details.id','driver_details.name','driver_details.phone_no','driver_details.addre_ss','driver_details.salary_duration','driver_details.amount','vehicle_details.*')
    ->get();
    // dd($dis);
    return view ('admin.vehidrivers.driverview')->with(['data'=>$dis]);
}



public function editdriver($id)
{
    $ed=VehiclesDetails::join('driver_details','driver_details.id','vehicle_details.driver_id')->where('vehicle_details.id',$id)->first();
  return view("admin.vehidrivers.editdriver")->with(['de'=>$ed]);

}
public function updatedata(Request $request,$id)

{

    $va = Validator::make($request->all(), [
        'name' => 'required|max:120',
        'phoneno' => 'required|min:11|numeric',
        'adress'=>'required',
        'salryduration'=>'required',
        'amount'=>'required',
        'vehino' => 'required',
        'vehitype' => 'required',
        'vhiperhur'=>'required',
        'rentervehicle'=>'required',


    ]);

    if ($va->fails()) {
        return redirect()->back()->withErrors($va->errors())->withInput($request->all());
    }
    $upd=VehicleDrivers::find($id);
    
    $upd->name=$request->input('name');
    $upd->phone_no=$request->input('phoneno');
    $upd->addre_ss=$request->input('adress');
    $upd->salary_duration=$request->input('salryduration');
    $upd->amount=$request->input('amount');
    $upd->save();
    if($upd->save())
    {

    $up = VehiclesDetails::where('driver_id',$id)->first();
    $up->vehicle_no =$request->input("vehino");
    $up->vehicle_type=$request->input("vehitype");
    $up->vehicle_cost_per_hur=$request->input("vhiperhur");
    $up->renter_vehicle =$request->input("rentervehicle");
    // $up->driver_id=$add->id;
  
   
    $up->save();
    }
    return redirect()->back()->with('message', 'New data updated.');
}

public function delete($id)//drivers details delete
{
    $del=VehiclesDetails::find($id);
    // dd($del);
    $del->delete();
    return redirect()->back()->with('message', 'New data deleted.');



}

// ********Client view************//

public function clientview()//drivers details
{
    $view=DB::table('driver_details')
    ->join('vehicle_details','vehicle_details.driver_id','=','driver_details.id')
    ->select('driver_details.id','vehicle_details.driver_id')
    ->select('driver_details.id','driver_details.name','driver_details.phone_no','driver_details.addre_ss','driver_details.salary_duration','driver_details.amount','vehicle_details.*')
    ->get();
    // dd($dis);
    return view ('admin.clientvehicle.clientdriversview')->with(['data'=>$view]);

}

public function status(Request $request,$id)//take rent vehicle
{
    $ap =VehiclesDetails::find($id);
    
   
    VehiclesDetails::where('id',$id)->where(['renter_vehicle' =>1])->update([
     
        'renter_vehicle' =>2,
     
        ]);
   
        return redirect()->back()->with('message', 'New data deleted.');
}

public function notification()
{

}
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
       
       

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\VehicleDrivers  $vehicleDrivers
     * @return \Illuminate\Http\Response
     */
    public function show(VehicleDrivers $vehicleDrivers)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\VehicleDrivers  $vehicleDrivers
     * @return \Illuminate\Http\Response
     */
    public function edit(VehicleDrivers $vehicleDrivers)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\VehicleDrivers  $vehicleDrivers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VehicleDrivers $vehicleDrivers)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\VehicleDrivers  $vehicleDrivers
     * @return \Illuminate\Http\Response
     */
    public function destroy(VehicleDrivers $vehicleDrivers)
    {
        //
    }
}
