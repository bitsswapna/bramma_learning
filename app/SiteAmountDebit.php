<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class SiteAmountDebit extends Model
{
    protected $table = 'site_work_amount_debit';
    public $timestamps = true;
    use SoftDeletes;

    public  function siteWorkAmountDebitGivenBy()
    {
        return $this->hasOne('App\SiteWorkAmountDebitGivenBy','site_work_amount_debit_id','id');
    }

    public  function siteWorkAmountDebitCollectedBy()
    {
        return $this->hasOne('App\SiteWorkAmountDebitCollected_By','site_work_amount_debit_id','id');
    }
    protected $fillable = [
        'site_work_amount_id', 'utilitie_id', 'amount', 'given_by','collected_by'
    ];
    protected $dates = ['deleted_at'];
}
