<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SiteWork extends Model
{
    protected $table = 'site_work';
    public $timestamps = true;
    use SoftDeletes;

    protected $dates = ['deleted_at'];
   

}
