<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteWorkAmountDebitCollected_By extends Model
{
    protected $table = 'site_work_amount_debit_collected_by';
    public $timestamps = true;
}
