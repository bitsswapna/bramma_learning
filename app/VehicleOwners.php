<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VehicleOwners extends Model
{
    protected $table='vehicle_owners';
    public $timestamps=true;
    protected $fillable = [ 'name', 'address','contact_number','vehicle_id'];
    use SoftDeletes;

    protected $dates = ['deleted_at'];

       
}
