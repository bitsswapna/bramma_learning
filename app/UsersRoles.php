<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UsersRoles extends Model
{
    protected $table='users_duties';
    public $timestamp=true;
    use SoftDeletes;
    protected $data = ['deleted_at'];

}
