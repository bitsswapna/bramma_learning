<?php
/**
 * Created by PhpStorm.
 * User: COMPUTER
 * Date: 6/1/2018
 * Time: 11:08 AM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
    protected $table = 'testimonials';
    public $timestamps = true;
}