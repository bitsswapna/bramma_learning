<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomersFeild extends Model
{
 
    protected $table='customers_table';
    public $timestamp=true;
    use SoftDeletes;
    protected $data = ['deleted_at'];
}
