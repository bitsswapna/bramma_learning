<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Utilities extends Model
{
    protected $table = 'utilities';
    public $timestamps = true;
    use SoftDeletes;

    protected $dates = ['deleted_at'];
}
