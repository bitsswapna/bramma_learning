<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehiclesDetails extends Model
{
    protected $table='vehicle_details';
    protected $fillable = ['vehicle_no','vehicle_type','vehicle_cost_per_hur','renter_vehicle','driver_id'];
    public $timestamps = true;
}
