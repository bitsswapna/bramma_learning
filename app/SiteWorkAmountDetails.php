<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SiteWorkAmountDetails extends Model
{
    protected $table = 'site_work_amount';
    public $timestamps = true;
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public  function siteWorkAmountDebit()
    {
        return $this->hasMany('App\SiteAmountDebit','site_work_amount_id','id');
    }

    public  function siteWorkAmountCreadit()
    {
        return $this->hasMany('App\SiteAmountCredit','site_work_amount_id','id');
    }

}
