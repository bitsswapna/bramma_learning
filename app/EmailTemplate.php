<?php
/**
 * Created by PhpStorm.
 * User: COMPUTER
 * Date: 5/9/2018
 * Time: 12:10 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class EmailTemplate extends Model
{
    protected $table = 'email_templates';
    public $timestamps = true;
}