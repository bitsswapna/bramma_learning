<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersPersonaldata extends Model
{
    protected $table='users_details';
    public $timestamps = true;
}
