<?php
/**
 * Created by PhpStorm.
 * User: COMPUTER
 * Date: 3/29/2018
 * Time: 6:03 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StateList extends Model
{
    protected $table = 'state_list';
    public $timestamps = true;

}