<?php
/**
 * Created by PhpStorm.
 * User: COMPUTER
 * Date: 3/30/2018
 * Time: 10:09 AM
 */

namespace  App\Models;


use Illuminate\Database\Eloquent\Model;

class Cms extends Model
{
    protected $table = 'cms';
    public $timestamps = true;
}