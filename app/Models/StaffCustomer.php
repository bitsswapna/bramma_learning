<?php
/**
 * Created by PhpStorm.
 * User: COMPUTER
 * Date: 3/30/2018
 * Time: 10:09 AM
 */

namespace  App\Models;

use Illuminate\Database\Eloquent\Model;

class StaffCustomer extends Model
{
    protected $table = 'staff_customers';
    public $timestamps = true;
}