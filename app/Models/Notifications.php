<?php
/**
 * Created by PhpStorm.
 * User: COMPUTER
 * Date: 3/29/2018
 * Time: 6:03 PM
 */

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Notifications extends Model
{
    protected $table = 'notifications';
    public $timestamps = true;
    public function getOrder(){
        return $this->belongsTo('App\Models\Order','data_id','id');
    }
    public function getUser($order_id){
        return User::join('orders','users.id','orders.user_id')
            ->where('orders.id',$order_id)
            ->select('users.*')
            ->first();
    }
    public function getBilledUser($order_id){
        return User::join('orders','users.id','orders.purchase_by')
            ->where('orders.id',$order_id)
            ->select('users.*')
            ->first();
    }
    public function getProduct($order_id){
        return Product::join('order_products','products.id','order_products.product_id')
            ->where('order_products.order_id',$order_id)
            ->select('products.*','order_products.mrp','order_products.price')
            ->first();

    }

}