<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AlertMessage extends Model
{
    protected $table = 'alert_messages';
    public $timestamps = true;
}
