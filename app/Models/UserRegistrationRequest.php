<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserRegistrationRequest extends Model
{
    protected $table = 'user_registration_requests';
    public $timestamps = false;

}