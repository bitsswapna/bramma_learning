<?php
/**
 * Created by PhpStorm.
 * User: COMPUTER
 * Date: 3/29/2018
 * Time: 6:03 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserPermissions extends Model
{
    protected $table = 'user_permissions';
    public $timestamps = true;
}