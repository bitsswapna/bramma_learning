<?php
/**
 * Created by PhpStorm.
 * User: COMPUTER
 * Date: 3/29/2018
 * Time: 6:03 PM
 */

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserStaff extends Model
{
    protected $table = 'user_staffs';
    public $timestamps = true;

    public function getUser()
    {
        return $this->belongsTo('App\User','user_id', 'id');

    }public function getUserDetail()
    {
        return $this->belongsTo('App\User','staff_id', 'id');

    }
    public function getStaff($id)
    {
        return User::find($id);

    }

}