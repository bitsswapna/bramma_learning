<?php
/**
 * Created by PhpStorm.
 * User: COMPUTER
 * Date: 4/16/2018
 * Time: 2:09 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = 'settings';
    public $timestamps = true;
}