<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class SiteAmountCredit extends Model
{
    protected $table = 'site_work_amount_creadit';
    public $timestamps = true;
    use SoftDeletes;

    public  function siteWorkAmountCreaditGivenBy()
    {
        return $this->hasOne('App\SiteWorkAmountCreaditGivenBy','site_work_amount_creadit_id','id');
    }

    public  function siteWorkAmountCreaditCollectedBy()
    {
        return $this->hasOne('App\SiteWorkAmountCreaditCollected_By','site_work_amount_creadit_id','id');
    }


    protected $dates = ['deleted_at'];
}
