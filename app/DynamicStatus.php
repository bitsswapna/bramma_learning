<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DynamicStatus extends Model
{
  protected $table='dynamic_status';
  public $timestamps=true;
}
