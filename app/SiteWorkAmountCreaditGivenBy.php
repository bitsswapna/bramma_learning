<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteWorkAmountCreaditGivenBy extends Model
{
    protected $table = 'site_work_amount_creadit_given_by';
    public $timestamps = true;
}
