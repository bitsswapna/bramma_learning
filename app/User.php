<?php

namespace App;

use App\Models\Ancestor;
use App\Models\UserPermissions;
use App\Models\UserWallet;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'username', 'first_name','genders','image', 'last_name', 'mobile', 'role_id', 'user_entry', 'password','address'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getUserDetails()
    {
        return $this->belongsTo('App\Models\UserDetails', 'id', 'user_id');

    }

    public function getRoleDashboardType()
    {
        return $this->belongsTo('App\Role', 'role_id', 'id');

    }

    public function getsDebitGivenByUser($uid,$did)
    {

        return $this->hasMany('App\SiteWorkAmountDebitGivenBy','user_id','id')->where(['user_id'=>$uid,'site_work_amount_debit_id'=>$did])->first();
    }

    public function getsDebitCollectedByUser($uid,$did)
    {

        return $this->hasMany('App\SiteWorkAmountDebitCollected_By','user_id','id')->where(['user_id'=>$uid,'site_work_amount_debit_id'=>$did])->first();
    }



    public function getsCreditGivenByUser($uid,$did)
    {

        return $this->hasMany('App\SiteWorkAmountCreaditGivenBy','user_id','id')->where(['user_id'=>$uid,'site_work_amount_creadit_id'=>$did])->first();
    }

    public function getsCreditDebitByUser($uid,$did)
    {

        return $this->hasMany('App\SiteWorkAmountCreaditCollected_By','user_id','id')->where(['user_id'=>$uid,'site_work_amount_creadit_id'=>$did])->first();
    }
}
