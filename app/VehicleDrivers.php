<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleDrivers extends Model
{
    protected $table='driver_details';
    public $timestamps = true;
}
