<?php
if (!function_exists('getRangeArray')) {
    function getRangeArray($min, $max)
    {
        $length = 0;
        $min = roundMinValue($min);

        if ($max >= 0 && $max <= 50) {
            $max = 50;
            $length = 10;
        } elseif ($max > 50 && $max <= 100) {
            $max = 100;
            $length = 10;
        } elseif ($max > 100 && $max <= 150) {
            $max = 150;
            $length = 10;
        } elseif ($max > 150 && $max <= 500) {
            $max = 500;
            $length = 50;
        } elseif ($max > 500 && $max <= 1000) {
            $max = 1000;
            $length = 100;
        } elseif ($max > 1000 && $max <= 1500) {
            dd(strlen(round($max)));
            $max = 1500;
            $length = 100;
        } elseif ($max > 1500 && $max <= 5000) {
            $max = 5000;
            $length = 500;
        } elseif ($max > 5000 && $max <= 10000) {
            $max = 10000;
            $length = 1000;
        } elseif ($max > 10000 && $max <= 15000) {
            $max = 15000;
            $length = 1000;
        } elseif ($max > 15000 && $max <= 50000) {
            $max = 50000;
            $length = 5000;
        } elseif ($max > 50000 && $max <= 100000) {
            $max = 100000;
            $length = 10000;
        }

        $ranges = range($min, $max, $length);
        $rang_arr = [];
        for ($i = 0; $i < count($ranges); $i++) {
            if (isset($ranges[$i + 1])) {
                $rang_arr[$i] = $ranges[$i] . '-' . $ranges[$i + 1];
            }
        }
        return $rang_arr;
    }
}

if (!function_exists('roundMinValue')) {
    function roundMinValue($val)
    {
        if ($val >= 0 && $val < 50) {
            return 0;
        } elseif ($val >= 50 && $val < 100) {
            return 50;
        } elseif ($val >= 100 && $val < 150) {
            return 100;
        } elseif ($val >= 150 && $val < 500) {
            return 150;
        } elseif ($val >= 500 && $val < 1000) {
            return 500;
        } elseif ($val >= 1000 && $val < 1500) {
            return 1000;
        } elseif ($val >= 1500 && $val < 5000) {
            return 1500;
        } elseif ($val >= 5000 && $val < 10000) {
            return 5000;
        } elseif ($val >= 10000 && $val < 15000) {
            return 10000;
        } elseif ($val >= 15000 && $val < 50000) {
            return 15000;
        } elseif ($val >= 50000 && $val < 100000) {
            return 50000;
        }
    }
}

if (!function_exists('getSetting')) {
    function getSetting($key)
    {
        $data = \App\Libraries\SettingClass::getSetting($key);
        return $data;
    }
}

if (!function_exists('getSettingDescription')) {
    function getSettingDescription($key)
    {
        $data = \App\Libraries\SettingClass::getSettingDescription($key);
        return $data;
    }
}

if (!function_exists('getClientIp')) {
    function getClientIp()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
        {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
        {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;

    }

}
if (!function_exists('getRoleInfo')) {
    function getRoleInfo($field)
    {
       $role = \App\Role::find(\Illuminate\Support\Facades\Auth::user()->role_id);
       if($role){
           return $role->$field?$role->$field:null;
       }
        return null;

    }

}
if (!function_exists('limitText')) {

    function limitText($text, $limit)
    {
        if (str_word_count($text, 0) > $limit) {
            $words = str_word_count($text, 2);
            $pos = array_keys($words);
            $text = substr($text, 0, $pos[$limit]) . '...';
        }
        return $text;
    }
}

if (!function_exists('bankList')){
    function bankList(){
        return \App\Models\Bank::pluck('name', 'id');
    }
}
if (!function_exists('productList')){
    function productList(){
        return \App\Models\Product::join('categories', 'categories.id', 'products.category_id')
            ->where('products.status','1')
            ->select('products.*','categories.slug as category')
            ->limit(9)
            ->get();
    }
}
if (!function_exists('footerAddress')){
    function footerAddress(){
        return \App\Models\Cms::where('slug','footer-content')->first();
    }
}

if (!function_exists('getAllNotifications')){
    function getAllNotifications(){
        return \App\Models\Notifications::where('status','1')
            ->orderBy('id','desc')
            ->limit(3)
            ->get();
    }
}
if (!function_exists('getWANotifications')){
    function getWANotifications(){
        return \App\Models\Notifications::where('status','1')
            ->where('parent_id',\Illuminate\Support\Facades\Auth::user()->id)
            ->orderBy('id','desc')
            ->limit(3)
            ->get();
    }
}

if (!function_exists('getAllNotificationsCount')){
    function getAllNotificationsCount(){
        return \App\Models\Notifications::where('status','1')
            ->orderBy('id','desc')
            ->get();
    }
}
if (!function_exists('getWANotificationsCount')){
    function getWANotificationsCount(){
        return \App\Models\Notifications::where('status','1')
            ->where('parent_id',\Illuminate\Support\Facades\Auth::user()->id)
            ->orderBy('id','desc')
            ->get();
    }
}
