-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 03, 2020 at 10:57 AM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `worksite`
--

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

DROP TABLE IF EXISTS `activities`;
CREATE TABLE IF NOT EXISTS `activities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `relation_id` int(11) DEFAULT NULL,
  `type` varchar(150) DEFAULT NULL,
  `activity` text,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`id`, `user_id`, `relation_id`, `type`, `activity`, `updated_at`, `created_at`) VALUES
(10, 1, 3, 'user', 'New user has been created by admin', '2019-12-15 02:12:49', '2019-12-15 02:12:49'),
(11, 1, 4, 'user', 'New user has been created by admin', '2019-12-15 02:18:28', '2019-12-15 02:18:28'),
(12, 1, 5, 'user', 'New user has been created by admin', '2019-12-15 02:23:43', '2019-12-15 02:23:43'),
(13, 1, 5, 'user', 'Admin user status activated by:', '2019-12-15 02:27:50', '2019-12-15 02:27:50'),
(14, 1, 4, 'user', 'Admin user status activated by:', '2019-12-15 05:24:45', '2019-12-15 05:24:45'),
(15, 1, 4, 'user', 'User has been edited by admin', '2019-12-15 05:38:27', '2019-12-15 05:38:27'),
(16, 1, 4, 'user', 'User has been edited by admin', '2019-12-15 05:38:43', '2019-12-15 05:38:43'),
(17, 1, 111, 'module', 'New module has been created by:', '2019-12-15 05:41:42', '2019-12-15 05:41:42'),
(18, 1, 1, 'role', 'New role has been edited by:', '2019-12-15 05:41:57', '2019-12-15 05:41:57'),
(19, 1, 112, 'module', 'New module has been created by:', '2019-12-15 10:04:27', '2019-12-15 10:04:27'),
(20, 1, 1, 'role', 'New role has been edited by:', '2019-12-15 10:04:49', '2019-12-15 10:04:49'),
(21, 1, 113, 'module', 'New module has been created by:', '2019-12-15 10:05:50', '2019-12-15 10:05:50'),
(22, 1, 114, 'module', 'New module has been created by:', '2019-12-15 10:06:02', '2019-12-15 10:06:02'),
(23, 1, 115, 'module', 'New module has been created by:', '2019-12-15 10:06:22', '2019-12-15 10:06:22'),
(24, 1, 1, 'role', 'New role has been edited by:', '2019-12-15 10:06:38', '2019-12-15 10:06:38'),
(25, 1, 3, 'user', 'Admin user status activated by:', '2019-12-15 23:54:34', '2019-12-15 23:54:34'),
(26, 1, 2, 'user', 'Admin user status activated by:', '2019-12-15 23:54:35', '2019-12-15 23:54:35'),
(27, 1, 5, 'user', 'Admin user status inactivated by:', '2019-12-16 02:30:11', '2019-12-16 02:30:11'),
(28, 1, 5, 'user', 'Admin user status activated by:', '2019-12-16 02:30:11', '2019-12-16 02:30:11'),
(29, 1, 5, 'user', 'Admin user status inactivated by:', '2019-12-16 02:30:16', '2019-12-16 02:30:16'),
(30, 1, 5, 'user', 'Admin user status activated by:', '2019-12-16 02:30:24', '2019-12-16 02:30:24'),
(31, 1, 5, 'user', 'Admin user status inactivated by:', '2019-12-16 02:30:35', '2019-12-16 02:30:35'),
(32, 1, 5, 'user', 'Admin user status activated by:', '2019-12-16 02:30:56', '2019-12-16 02:30:56'),
(33, 1, 5, 'user', 'Admin user status inactivated by:', '2019-12-16 05:04:21', '2019-12-16 05:04:21'),
(34, 1, 5, 'user', 'Admin user status activated by:', '2019-12-16 05:04:23', '2019-12-16 05:04:23'),
(35, 1, 5, 'user', 'Admin user status inactivated by:', '2019-12-16 05:32:55', '2019-12-16 05:32:55'),
(36, 1, 5, 'user', 'Admin user status activated by:', '2019-12-16 05:33:00', '2019-12-16 05:33:00'),
(37, 1, 1, 'user', 'Admin user status inactivated by:', '2019-12-16 05:43:25', '2019-12-16 05:43:25'),
(38, 1, 1, 'user', 'Admin user status activated by:', '2019-12-16 05:43:26', '2019-12-16 05:43:26'),
(39, 1, 1, 'user', 'Admin user status inactivated by:', '2019-12-16 05:43:35', '2019-12-16 05:43:35'),
(40, 1, 1, 'user', 'Admin user status activated by:', '2019-12-16 05:43:49', '2019-12-16 05:43:49'),
(41, 1, 4, 'user', 'Admin user status inactivated by:', '2019-12-16 05:44:11', '2019-12-16 05:44:11'),
(42, 1, 4, 'user', 'Admin user status activated by:', '2019-12-16 05:44:27', '2019-12-16 05:44:27'),
(43, 1, 1, 'user', 'Admin user status inactivated by:', '2019-12-16 05:44:52', '2019-12-16 05:44:52'),
(44, 1, 5, 'user', 'Admin user status inactivated by:', '2019-12-16 05:57:01', '2019-12-16 05:57:01'),
(45, 1, 5, 'user', 'Admin user status activated by:', '2019-12-16 05:57:52', '2019-12-16 05:57:52'),
(46, 1, 6, 'user', 'New user has been created by admin', '2019-12-28 04:57:36', '2019-12-28 04:57:36'),
(47, 1, 6, 'user', 'Admin user status activated by:', '2019-12-28 04:58:01', '2019-12-28 04:58:01'),
(48, 1, 4, 'user', 'User has been edited by admin', '2019-12-28 04:58:57', '2019-12-28 04:58:57'),
(49, 1, 5, 'user', 'User has been edited by admin', '2019-12-28 05:00:39', '2019-12-28 05:00:39'),
(50, 1, 5, 'user', 'User has been edited by admin', '2019-12-28 05:01:26', '2019-12-28 05:01:26'),
(51, 1, 5, 'user', 'User has been edited by admin', '2019-12-28 05:02:35', '2019-12-28 05:02:35'),
(52, 1, 4, 'user', 'User has been edited by admin', '2019-12-28 05:08:05', '2019-12-28 05:08:05'),
(53, 1, 2, 'user', 'User has been edited by admin', '2019-12-28 05:11:11', '2019-12-28 05:11:11'),
(54, 1, 6, 'user', 'Admin user status inactivated by:', '2019-12-28 05:13:08', '2019-12-28 05:13:08'),
(55, 1, 5, 'user', 'Admin user status inactivated by:', '2019-12-28 05:13:21', '2019-12-28 05:13:21'),
(56, 1, 5, 'user', 'Admin user status activated by:', '2019-12-28 05:13:31', '2019-12-28 05:13:31'),
(57, 1, 6, 'user', 'Admin user status activated by:', '2019-12-28 05:13:32', '2019-12-28 05:13:32'),
(58, 1, 4, 'user', 'User has been edited by admin', '2019-12-28 05:17:56', '2019-12-28 05:17:56'),
(59, 1, 4, 'user', 'User has been edited by admin', '2019-12-28 05:27:37', '2019-12-28 05:27:37'),
(60, 1, 4, 'user', 'User has been edited by admin', '2019-12-28 05:30:37', '2019-12-28 05:30:37'),
(61, 1, 4, 'user', 'User has been edited by admin', '2019-12-28 05:31:06', '2019-12-28 05:31:06'),
(62, 1, 4, 'user', 'User has been edited by admin', '2019-12-28 05:31:23', '2019-12-28 05:31:23'),
(63, 1, 5, 'user', 'User has been edited by admin', '2019-12-28 05:32:29', '2019-12-28 05:32:29'),
(64, 1, 4, 'user', 'User has been edited by admin', '2019-12-28 05:36:03', '2019-12-28 05:36:03'),
(65, 1, 5, 'user', 'User has been edited by admin', '2019-12-28 05:37:26', '2019-12-28 05:37:26'),
(66, 1, 6, 'user', 'User has been edited by admin', '2019-12-28 05:42:52', '2019-12-28 05:42:52'),
(67, 1, 6, 'user', 'User has been edited by admin', '2019-12-28 05:50:13', '2019-12-28 05:50:13'),
(68, 1, 6, 'user', 'User has been edited by admin', '2019-12-28 05:51:34', '2019-12-28 05:51:34'),
(69, 1, 6, 'user', 'User has been edited by admin', '2019-12-28 05:52:32', '2019-12-28 05:52:32'),
(70, 1, 5, 'user', 'User has been edited by admin', '2019-12-28 05:53:01', '2019-12-28 05:53:01'),
(71, 1, 4, 'user', 'User has been edited by admin', '2019-12-28 05:53:26', '2019-12-28 05:53:26'),
(72, 1, 2, 'user', 'User has been edited by admin', '2019-12-28 05:53:41', '2019-12-28 05:53:41'),
(73, 1, 7, 'user', 'New user has been created by admin', '2019-12-28 05:56:27', '2019-12-28 05:56:27'),
(74, 1, 7, 'user', 'Admin user status activated by:', '2019-12-28 05:56:59', '2019-12-28 05:56:59');

-- --------------------------------------------------------

--
-- Table structure for table `admin_modules`
--

DROP TABLE IF EXISTS `admin_modules`;
CREATE TABLE IF NOT EXISTS `admin_modules` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `parent_id` int(20) NOT NULL DEFAULT '0',
  `name` varchar(50) CHARACTER SET latin1 NOT NULL,
  `perifix` varchar(50) CHARACTER SET latin1 NOT NULL,
  `type` enum('parent','child') CHARACTER SET latin1 NOT NULL COMMENT 'two types parent and child',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=116 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_modules`
--

INSERT INTO `admin_modules` (`id`, `parent_id`, `name`, `perifix`, `type`, `created_at`, `updated_at`) VALUES
(35, 0, 'module', 'module', 'parent', '2019-08-17 07:25:23', '2019-08-17 07:25:23'),
(36, 0, 'Roles', 'roles', 'parent', '2019-08-17 07:25:42', '2019-08-31 10:33:14'),
(37, 0, 'Admin & User', 'admin_user', 'parent', '2019-08-17 07:25:53', '2019-08-17 07:25:53'),
(111, 0, 'customer', 'customer', 'parent', '2019-12-15 11:11:41', '2019-12-15 11:11:41'),
(112, 0, 'vehicle', 'vehicle', 'parent', '2019-12-15 15:34:27', '2019-12-15 15:34:27'),
(113, 0, 'utilities', 'utilities', 'parent', '2019-12-15 15:35:49', '2019-12-15 15:35:49'),
(114, 0, 'sites', 'sites', 'parent', '2019-12-15 15:36:02', '2019-12-15 15:36:02'),
(115, 0, 'site_work', 'site_work', 'parent', '2019-12-15 15:36:21', '2019-12-15 15:36:21');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
CREATE TABLE IF NOT EXISTS `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL,
  `address` varchar(191) NOT NULL,
  `number` varchar(11) NOT NULL,
  `status` int(11) DEFAULT '0',
  `active` int(11) DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `address`, `number`, `status`, `active`, `created_at`, `updated_at`) VALUES
(1, 'Eldhose Mathai', 'Kuzhippanathil(H),Mulanthuruthy P.O,Ernakulam(dist)', '9436521710', 0, 1, '2019-12-16 05:57:36', '2019-12-16 11:27:28'),
(4, 'Arjun Sugumaran', 'Kattadyil(H),\r\nVennikulam P.O,\r\nErnakulam(dist)', '8432012310', 0, 1, '2019-12-16 07:28:12', '2019-12-16 11:25:58'),
(5, 'Jeevan Raj', 'Kizhkambalum(H),Mulanthuruthy P.O,Ernakulam(dist)', '9465320300', 0, 1, '2019-12-16 12:39:49', '2019-12-26 05:50:36'),
(6, 'Sachin Tendulkar', 'THEVERA,Ernakulam(Dist)', '8131313202', 0, 1, '2019-12-28 05:27:23', '2020-01-03 10:57:02');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

DROP TABLE IF EXISTS `logs`;
CREATE TABLE IF NOT EXISTS `logs` (
  `id` bigint(200) NOT NULL AUTO_INCREMENT,
  `model` varchar(191) DEFAULT NULL,
  `model_id` bigint(191) DEFAULT NULL,
  `direction` varchar(191) DEFAULT NULL COMMENT 'CRUD',
  `key_code` varchar(191) DEFAULT NULL COMMENT 'if project remark changed then key can be project_project_status_remark',
  `auth_user_id` int(11) DEFAULT NULL,
  `value` text COMMENT 'if remark then remark, if project creation status',
  `is_read` int(2) NOT NULL DEFAULT '0' COMMENT '0-not read,1-read',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `model_id` (`model_id`),
  KEY `auth_user_id` (`auth_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_10_26_100624_create_client_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_key_index` (`key`),
  KEY `module_id` (`module_id`)
) ENGINE=InnoDB AUTO_INCREMENT=576 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`, `module_id`) VALUES
(171, 'browse_module', 'module', '2019-08-17 07:25:23', '2019-08-17 07:25:23', 35),
(172, 'read_module', 'module', '2019-08-17 07:25:23', '2019-08-17 07:25:23', 35),
(173, 'edit_module', 'module', '2019-08-17 07:25:23', '2019-08-17 07:25:23', 35),
(174, 'add_module', 'module', '2019-08-17 07:25:23', '2019-08-17 07:25:23', 35),
(175, 'delete_module', 'module', '2019-08-17 07:25:23', '2019-08-17 07:25:23', 35),
(176, 'browse_roles', 'roles', '2019-08-17 07:25:42', '2019-08-17 07:25:42', 36),
(177, 'read_roles', 'roles', '2019-08-17 07:25:42', '2019-08-17 07:25:42', 36),
(178, 'edit_roles', 'roles', '2019-08-17 07:25:42', '2019-08-17 07:25:42', 36),
(179, 'add_roles', 'roles', '2019-08-17 07:25:42', '2019-08-17 07:25:42', 36),
(180, 'delete_roles', 'roles', '2019-08-17 07:25:42', '2019-08-17 07:25:42', 36),
(181, 'browse_admin_user', 'admin_user', '2019-08-17 07:25:53', '2019-08-17 07:25:53', 37),
(182, 'read_admin_user', 'admin_user', '2019-08-17 07:25:53', '2019-08-17 07:25:53', 37),
(183, 'edit_admin_user', 'admin_user', '2019-08-17 07:25:53', '2019-08-17 07:25:53', 37),
(184, 'add_admin_user', 'admin_user', '2019-08-17 07:25:53', '2019-08-17 07:25:53', 37),
(185, 'delete_admin_user', 'admin_user', '2019-08-17 07:25:53', '2019-08-17 07:25:53', 37),
(551, 'browse_customer', 'customer', '2019-12-15 11:11:41', '2019-12-15 11:11:41', 111),
(552, 'read_customer', 'customer', '2019-12-15 11:11:41', '2019-12-15 11:11:41', 111),
(553, 'edit_customer', 'customer', '2019-12-15 11:11:42', '2019-12-15 11:11:42', 111),
(554, 'add_customer', 'customer', '2019-12-15 11:11:42', '2019-12-15 11:11:42', 111),
(555, 'delete_customer', 'customer', '2019-12-15 11:11:42', '2019-12-15 11:11:42', 111),
(556, 'browse_vehicle', 'vehicle', '2019-12-15 15:34:27', '2019-12-15 15:34:27', 112),
(557, 'read_vehicle', 'vehicle', '2019-12-15 15:34:27', '2019-12-15 15:34:27', 112),
(558, 'edit_vehicle', 'vehicle', '2019-12-15 15:34:27', '2019-12-15 15:34:27', 112),
(559, 'add_vehicle', 'vehicle', '2019-12-15 15:34:27', '2019-12-15 15:34:27', 112),
(560, 'delete_vehicle', 'vehicle', '2019-12-15 15:34:27', '2019-12-15 15:34:27', 112),
(561, 'browse_utilities', 'utilities', '2019-12-15 15:35:49', '2019-12-15 15:35:49', 113),
(562, 'read_utilities', 'utilities', '2019-12-15 15:35:49', '2019-12-15 15:35:49', 113),
(563, 'edit_utilities', 'utilities', '2019-12-15 15:35:50', '2019-12-15 15:35:50', 113),
(564, 'add_utilities', 'utilities', '2019-12-15 15:35:50', '2019-12-15 15:35:50', 113),
(565, 'delete_utilities', 'utilities', '2019-12-15 15:35:50', '2019-12-15 15:35:50', 113),
(566, 'browse_sites', 'sites', '2019-12-15 15:36:02', '2019-12-15 15:36:02', 114),
(567, 'read_sites', 'sites', '2019-12-15 15:36:02', '2019-12-15 15:36:02', 114),
(568, 'edit_sites', 'sites', '2019-12-15 15:36:02', '2019-12-15 15:36:02', 114),
(569, 'add_sites', 'sites', '2019-12-15 15:36:02', '2019-12-15 15:36:02', 114),
(570, 'delete_sites', 'sites', '2019-12-15 15:36:02', '2019-12-15 15:36:02', 114),
(571, 'browse_site_work', 'site_work', '2019-12-15 15:36:21', '2019-12-15 15:36:21', 115),
(572, 'read_site_work', 'site_work', '2019-12-15 15:36:22', '2019-12-15 15:36:22', 115),
(573, 'edit_site_work', 'site_work', '2019-12-15 15:36:22', '2019-12-15 15:36:22', 115),
(574, 'add_site_work', 'site_work', '2019-12-15 15:36:22', '2019-12-15 15:36:22', 115),
(575, 'delete_site_work', 'site_work', '2019-12-15 15:36:22', '2019-12-15 15:36:22', 115);

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
CREATE TABLE IF NOT EXISTS `permission_role` (
  `permission_id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` int(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=576 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(171, 1),
(172, 1),
(173, 1),
(173, 4),
(174, 1),
(175, 1),
(176, 1),
(177, 1),
(178, 1),
(178, 4),
(179, 1),
(180, 1),
(181, 1),
(181, 3),
(182, 1),
(183, 1),
(183, 3),
(184, 1),
(184, 3),
(185, 1),
(185, 3),
(186, 2),
(186, 3),
(186, 4),
(186, 5),
(186, 7),
(187, 2),
(187, 4),
(187, 5),
(187, 7),
(188, 2),
(188, 4),
(188, 5),
(188, 7),
(189, 2),
(189, 4),
(189, 5),
(189, 7),
(190, 2),
(190, 4),
(190, 5),
(190, 7),
(191, 2),
(191, 4),
(191, 5),
(191, 7),
(192, 2),
(192, 4),
(192, 5),
(192, 7),
(193, 2),
(193, 4),
(193, 5),
(193, 7),
(194, 2),
(194, 4),
(194, 5),
(194, 7),
(195, 2),
(195, 4),
(195, 5),
(195, 7),
(196, 4),
(196, 5),
(196, 7),
(197, 4),
(197, 5),
(197, 7),
(198, 4),
(198, 5),
(198, 7),
(199, 4),
(199, 5),
(199, 7),
(200, 4),
(200, 5),
(200, 7),
(201, 2),
(201, 4),
(201, 5),
(201, 7),
(202, 2),
(202, 4),
(202, 5),
(202, 7),
(203, 2),
(203, 4),
(203, 5),
(203, 7),
(204, 2),
(204, 4),
(204, 5),
(204, 7),
(205, 2),
(205, 4),
(205, 5),
(205, 7),
(206, 2),
(206, 4),
(206, 5),
(206, 7),
(207, 2),
(207, 4),
(207, 5),
(207, 7),
(208, 2),
(208, 4),
(208, 5),
(208, 7),
(209, 2),
(209, 4),
(209, 5),
(209, 7),
(210, 2),
(210, 4),
(210, 5),
(210, 7),
(211, 2),
(211, 4),
(211, 5),
(211, 7),
(212, 2),
(212, 4),
(212, 5),
(212, 7),
(213, 2),
(213, 4),
(213, 5),
(213, 7),
(214, 2),
(214, 4),
(214, 5),
(214, 7),
(215, 2),
(215, 4),
(215, 5),
(215, 7),
(216, 5),
(217, 5),
(218, 5),
(219, 5),
(220, 5),
(221, 2),
(221, 5),
(221, 7),
(222, 2),
(222, 5),
(222, 7),
(223, 2),
(223, 5),
(223, 7),
(224, 2),
(224, 5),
(224, 7),
(225, 2),
(225, 5),
(225, 7),
(231, 2),
(231, 5),
(231, 7),
(232, 2),
(232, 5),
(232, 7),
(233, 2),
(233, 5),
(233, 7),
(234, 2),
(234, 5),
(234, 7),
(235, 2),
(235, 5),
(235, 7),
(241, 2),
(241, 4),
(241, 5),
(242, 2),
(242, 4),
(242, 5),
(243, 2),
(243, 4),
(243, 5),
(244, 2),
(244, 4),
(244, 5),
(245, 2),
(245, 4),
(245, 5),
(246, 2),
(246, 4),
(246, 5),
(247, 2),
(247, 4),
(247, 5),
(248, 2),
(248, 4),
(248, 5),
(249, 2),
(249, 4),
(249, 5),
(250, 2),
(250, 4),
(250, 5),
(251, 2),
(251, 4),
(251, 5),
(252, 2),
(252, 4),
(252, 5),
(253, 2),
(253, 4),
(253, 5),
(254, 2),
(254, 4),
(254, 5),
(255, 2),
(255, 4),
(255, 5),
(256, 2),
(256, 4),
(256, 5),
(257, 2),
(257, 4),
(257, 5),
(258, 2),
(258, 4),
(258, 5),
(259, 2),
(259, 4),
(259, 5),
(260, 2),
(260, 4),
(260, 5),
(261, 2),
(261, 4),
(261, 5),
(262, 2),
(262, 4),
(262, 5),
(263, 2),
(263, 4),
(263, 5),
(264, 2),
(264, 4),
(264, 5),
(265, 2),
(265, 4),
(265, 5),
(266, 2),
(266, 4),
(266, 5),
(266, 7),
(267, 2),
(267, 4),
(267, 5),
(267, 7),
(268, 2),
(268, 4),
(268, 5),
(268, 7),
(269, 2),
(269, 4),
(269, 5),
(269, 7),
(270, 2),
(270, 4),
(270, 5),
(270, 7),
(271, 2),
(271, 4),
(271, 5),
(271, 7),
(272, 2),
(272, 4),
(272, 5),
(272, 7),
(273, 2),
(273, 4),
(273, 5),
(273, 7),
(274, 2),
(274, 4),
(274, 5),
(274, 7),
(275, 2),
(275, 4),
(275, 5),
(275, 7),
(281, 2),
(281, 4),
(281, 5),
(281, 7),
(282, 2),
(282, 4),
(282, 5),
(282, 7),
(283, 2),
(283, 4),
(283, 5),
(283, 7),
(284, 2),
(284, 4),
(284, 5),
(284, 7),
(285, 2),
(285, 4),
(285, 5),
(285, 7),
(286, 4),
(286, 5),
(287, 4),
(287, 5),
(288, 4),
(288, 5),
(289, 4),
(289, 5),
(290, 4),
(290, 5),
(291, 2),
(291, 5),
(292, 2),
(292, 5),
(293, 2),
(293, 5),
(294, 2),
(294, 5),
(295, 2),
(295, 5),
(296, 2),
(296, 4),
(296, 5),
(296, 7),
(297, 2),
(297, 4),
(297, 5),
(297, 7),
(298, 2),
(298, 4),
(298, 5),
(298, 7),
(299, 2),
(299, 4),
(299, 5),
(299, 7),
(300, 2),
(300, 4),
(300, 5),
(300, 7),
(301, 2),
(301, 5),
(301, 7),
(302, 2),
(302, 5),
(302, 7),
(303, 2),
(303, 5),
(303, 7),
(304, 2),
(304, 5),
(304, 7),
(305, 2),
(305, 5),
(305, 7),
(306, 2),
(306, 3),
(306, 4),
(306, 5),
(307, 2),
(307, 3),
(307, 4),
(307, 5),
(308, 2),
(308, 3),
(308, 4),
(308, 5),
(309, 2),
(309, 3),
(309, 4),
(309, 5),
(310, 2),
(310, 3),
(310, 4),
(310, 5),
(311, 2),
(311, 4),
(311, 5),
(312, 2),
(312, 4),
(312, 5),
(313, 2),
(313, 4),
(313, 5),
(314, 2),
(314, 4),
(314, 5),
(315, 2),
(315, 4),
(315, 5),
(316, 2),
(316, 4),
(316, 5),
(317, 2),
(317, 4),
(317, 5),
(318, 2),
(318, 4),
(318, 5),
(319, 2),
(319, 4),
(319, 5),
(320, 2),
(320, 4),
(320, 5),
(346, 2),
(346, 4),
(346, 5),
(347, 2),
(347, 4),
(347, 5),
(348, 2),
(348, 4),
(348, 5),
(349, 2),
(349, 4),
(349, 5),
(350, 2),
(350, 4),
(350, 5),
(351, 4),
(351, 7),
(352, 4),
(353, 4),
(354, 4),
(355, 4),
(356, 7),
(357, 7),
(358, 7),
(359, 7),
(361, 5),
(362, 5),
(363, 5),
(364, 5),
(365, 5),
(366, 5),
(367, 5),
(368, 5),
(369, 5),
(370, 5),
(371, 2),
(371, 5),
(371, 7),
(372, 2),
(372, 5),
(372, 7),
(373, 2),
(373, 5),
(373, 7),
(374, 2),
(374, 5),
(374, 7),
(375, 2),
(375, 5),
(375, 7),
(376, 5),
(376, 7),
(377, 5),
(377, 7),
(378, 5),
(378, 7),
(379, 5),
(379, 7),
(380, 5),
(380, 7),
(386, 4),
(386, 5),
(386, 7),
(387, 4),
(387, 5),
(387, 7),
(388, 4),
(388, 5),
(388, 7),
(389, 4),
(389, 5),
(389, 7),
(390, 4),
(390, 5),
(390, 7),
(391, 4),
(391, 5),
(392, 4),
(392, 5),
(393, 4),
(393, 5),
(394, 4),
(394, 5),
(395, 4),
(395, 5),
(406, 5),
(407, 5),
(408, 5),
(409, 5),
(410, 5),
(411, 5),
(412, 5),
(413, 5),
(414, 5),
(415, 5),
(421, 2),
(421, 4),
(421, 5),
(422, 2),
(422, 4),
(422, 5),
(423, 2),
(423, 4),
(423, 5),
(424, 2),
(424, 4),
(424, 5),
(425, 2),
(425, 4),
(425, 5),
(426, 2),
(426, 4),
(426, 5),
(427, 2),
(427, 4),
(427, 5),
(428, 2),
(428, 4),
(428, 5),
(429, 2),
(429, 4),
(429, 5),
(430, 2),
(430, 4),
(430, 5),
(431, 4),
(431, 5),
(431, 7),
(432, 4),
(432, 5),
(432, 7),
(433, 4),
(433, 5),
(433, 7),
(434, 4),
(434, 5),
(434, 7),
(435, 4),
(435, 5),
(435, 7),
(456, 2),
(457, 2),
(458, 2),
(459, 2),
(460, 2),
(476, 2),
(476, 7),
(477, 2),
(477, 7),
(478, 2),
(478, 7),
(479, 2),
(479, 7),
(480, 2),
(480, 7),
(481, 5),
(482, 5),
(483, 5),
(484, 5),
(485, 5),
(486, 5),
(487, 5),
(488, 5),
(489, 5),
(490, 5),
(491, 2),
(491, 5),
(492, 2),
(492, 5),
(493, 2),
(493, 5),
(494, 2),
(494, 5),
(495, 2),
(495, 5),
(496, 5),
(497, 5),
(498, 5),
(499, 5),
(500, 5),
(501, 5),
(502, 5),
(503, 5),
(504, 5),
(505, 5),
(506, 2),
(507, 2),
(508, 2),
(509, 2),
(510, 2),
(536, 2),
(537, 2),
(538, 2),
(539, 2),
(540, 2),
(551, 1),
(552, 1),
(553, 1),
(554, 1),
(555, 1),
(556, 1),
(557, 1),
(558, 1),
(559, 1),
(560, 1),
(561, 1),
(562, 1),
(563, 1),
(564, 1),
(565, 1),
(566, 1),
(567, 1),
(568, 1),
(569, 1),
(570, 1),
(571, 1),
(572, 1),
(573, 1),
(574, 1),
(575, 1);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(190) CHARACTER SET latin1 NOT NULL,
  `display_name` varchar(190) CHARACTER SET latin1 NOT NULL,
  `user_type` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `user_type`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'Admin', 1, '2019-08-13 12:27:47', '2019-10-26 08:48:59'),
(2, 'Driver', 'Driver', 2, '2019-08-13 12:27:47', '2019-08-15 08:06:24'),
(3, 'Cleaner', 'Cleaner', 3, '2019-08-13 12:27:47', '2019-08-23 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `sites`
--

DROP TABLE IF EXISTS `sites`;
CREATE TABLE IF NOT EXISTS `sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `address` varchar(250) NOT NULL,
  `title` varchar(22) NOT NULL,
  `description` varchar(191) NOT NULL,
  `estimated_work_in_day` varchar(12) NOT NULL,
  `estimated_expense` decimal(65,0) NOT NULL,
  `status` int(11) DEFAULT '0',
  `active` int(11) DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sites`
--

INSERT INTO `sites` (`id`, `customer_id`, `address`, `title`, `description`, `estimated_work_in_day`, `estimated_expense`, `status`, `active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(13, 1, 'Kuzhippanathil(H)Mulanthuruthy', 'EMK', 'haiiiii', '8 hr', '120', 0, 1, '2019-12-17 09:47:44', '2019-12-18 05:04:00', NULL),
(15, 5, 'Thottikattyil(h),Petta,Ernakulam', 'KIFRA', 'hai', '8 hr', '250', 0, 1, '2019-12-18 05:05:50', '2020-01-03 04:48:58', NULL),
(17, 4, 'Royal Palace,Kottayam', 'Rak', 'Nice one', '8 hr', '120', 0, 1, '2019-12-27 10:00:15', '2020-01-02 08:28:41', NULL),
(18, 6, 'Kundanoor,Ernakulam', 'Blasters', 'Master Blaster', '3hr', '120', 0, 1, '2019-12-28 05:28:27', '2019-12-28 10:14:05', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `site_work`
--

DROP TABLE IF EXISTS `site_work`;
CREATE TABLE IF NOT EXISTS `site_work` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titles` varchar(191) DEFAULT NULL,
  `site_id` int(11) NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `cleaner_id` int(11) NOT NULL,
  `work_date` datetime NOT NULL,
  `vehicle_point` varchar(191) NOT NULL,
  `vehicle_duration` varchar(191) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_work`
--

INSERT INTO `site_work` (`id`, `titles`, `site_id`, `vehicle_id`, `driver_id`, `cleaner_id`, `work_date`, `vehicle_point`, `vehicle_duration`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'Tex', 17, 24, 3, 5, '2020-01-29 00:00:00', 'royal', '120 days', '2020-01-02 10:27:59', '2020-01-03 09:26:51', NULL),
(5, 'Bits', 18, 20, 2, 7, '2020-02-01 00:00:00', 'kadavntra', '200 days', '2020-01-03 09:09:35', '2020-01-03 10:16:57', NULL),
(6, 'Giga', 13, 29, 4, 5, '2020-01-29 00:00:00', 'vytla', '100 days', '2020-01-03 09:23:34', '2020-01-03 09:23:34', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `site_work_amount_details`
--

DROP TABLE IF EXISTS `site_work_amount_details`;
CREATE TABLE IF NOT EXISTS `site_work_amount_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_work_id` int(11) NOT NULL,
  `type` int(11) NOT NULL COMMENT '1=credit,2=debit',
  `utilities_id` int(11) NOT NULL,
  `amount` tinyint(4) NOT NULL,
  `collected_by` tinyint(4) NOT NULL,
  `given_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

DROP TABLE IF EXISTS `state`;
CREATE TABLE IF NOT EXISTS `state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(191) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `state_list`
--

DROP TABLE IF EXISTS `state_list`;
CREATE TABLE IF NOT EXISTS `state_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` varchar(191) DEFAULT NULL,
  `title` varchar(191) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int(20) NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `active` int(11) DEFAULT NULL,
  `joining_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` datetime DEFAULT NULL,
  `password` varchar(220) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` text COLLATE utf8mb4_unicode_ci,
  `expires_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `role_id` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `role_id`, `first_name`, `last_name`, `gender`, `address`, `status`, `active`, `joining_date`, `email`, `mobile`, `email_verified_at`, `password`, `token`, `expires_at`, `remember_token`, `created_at`, `updated_at`, `deleted_at`, `last_login`) VALUES
(1, 'Bramma Learning', 1, 'Bramma', 'Learning', '1', NULL, 0, 0, NULL, 'admin@sitework.com', '5463746748', NULL, '$2y$10$4himm3/oQqkrK83v.CbQJOQxDYuU.6RWgtLXlHpMmSoZnZVc/cXwq', '', '2019-12-28 11:30:04', 'pybiZhY4RFh6WDSfE2devbVGqmQgy5VaWnpzrLOmFCDZ8PdXSHUoGmFWRvKA', '2019-08-14 05:38:59', '2019-12-28 11:30:04', NULL, '2019-12-28 11:30:04'),
(2, 'vivinmale', 2, 'vivin', 'male', '1', 'teast', 1, 1, NULL, 'vivinnvipi@gmail.com', '9539040176', NULL, '$2y$10$0ZzszPTq.qeLJlLj56K.C.x1eEuVyCo45ScXBHBp/XS/b38illb1q', NULL, '2019-12-28 11:23:41', NULL, '2019-12-15 07:39:21', '2019-12-28 11:23:41', NULL, NULL),
(3, 'akhilnm', 2, 'akhil', 'male', '1', 'address', 1, 1, NULL, 'akhil@gmail.com', '95746573546', NULL, '$2y$10$lt5NNE6T4Vz9MRfolwn7PuxpToWs9ErElj957.O9pIFB1KC1vq0Ju', NULL, '2019-12-16 05:24:34', NULL, '2019-12-15 07:42:49', '2019-12-16 05:24:34', NULL, NULL),
(4, 'amal krishnanKumar', 2, 'amal krishnan', 'Kumar', '1', 'kotyam', 1, 1, NULL, 'amal@gmail.com', '9748574758', NULL, '$2y$10$7Yf5mxDc5ZB9yCVpAVDp1OMhNuv8c79vqfQf/YpXbV5zmJC/fn7fe', NULL, '2019-12-28 11:23:26', NULL, '2019-12-15 07:48:28', '2019-12-28 11:23:26', NULL, NULL),
(5, 'RahulRaj', 3, 'Rahul', 'Raj', '1', 'ytshjf', 1, 1, NULL, 'rahul@gmail.com', '9847567364', NULL, '$2y$10$LrXY0KKQRLAmBABz4HGhiuFW5zB58V8SK2GiE5zuTiflXV.oe5q0i', NULL, '2019-12-28 11:23:01', NULL, '2019-12-15 07:53:43', '2019-12-28 11:23:01', NULL, NULL),
(6, 'RahulMenon', 2, 'Rahul', 'Menon', NULL, 'ttt', 1, 1, NULL, 'rahulmenon@gmail.com', '9465301840', NULL, '$2y$10$mBbppoy6AfIJTSPxiObPVe8wJhtP21WO/3OfS8l0tFDYLeYL0M6Ie', NULL, '2019-12-28 11:22:32', NULL, '2019-12-28 10:27:36', '2019-12-28 11:22:32', NULL, NULL),
(7, 'DewinT.D', 3, 'Dewin', 'T.D', NULL, 'Kotam', 1, 1, NULL, 'dewin@gmail.com', '7305618520', NULL, '$2y$10$rKloFT5jJZ3m6XTfeUt6MeNRgXQX69iM3VfOmxgJvCIU.tZWbmiRG', NULL, '2019-12-28 11:26:59', NULL, '2019-12-28 11:26:27', '2019-12-28 11:26:59', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `utilities`
--

DROP TABLE IF EXISTS `utilities`;
CREATE TABLE IF NOT EXISTS `utilities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT '1' COMMENT '1=credtit,2=debit',
  `name` varchar(191) NOT NULL,
  `description` varchar(191) NOT NULL,
  `status` int(11) DEFAULT '0',
  `active` int(11) DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `utilities`
--

INSERT INTO `utilities` (`id`, `type`, `name`, `description`, `status`, `active`, `created_at`, `updated_at`) VALUES
(2, 2, 'eldho', 'hello', 0, 1, '2019-12-16 08:47:24', '2019-12-31 06:46:06'),
(3, 1, 'jibin P', 'dsvv', 0, 1, '2019-12-16 08:49:03', '2020-01-03 07:33:13');

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

DROP TABLE IF EXISTS `vehicles`;
CREATE TABLE IF NOT EXISTS `vehicles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL DEFAULT '0' COMMENT '1=rent,2=partnership,3=own vehicle',
  `vehicle_number` varchar(191) NOT NULL,
  `hourly_amount` varchar(100) NOT NULL,
  `active` int(11) DEFAULT '1',
  `status` int(11) DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicles`
--

INSERT INTO `vehicles` (`id`, `type`, `vehicle_number`, `hourly_amount`, `active`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(20, 1, 'KL-07-1245', '25', 1, 0, '2019-12-26 07:07:43', '2019-12-27 05:27:28', NULL),
(24, 1, 'KL-08-1278', '50', 1, 0, '2019-12-27 05:30:30', '2019-12-27 09:18:00', NULL),
(25, 2, 'KL-07-2356', '100', 1, 0, '2019-12-27 06:54:52', '2019-12-27 09:17:50', NULL),
(28, 3, 'KL-09-4567', '100', 1, 0, '2019-12-31 10:39:53', '2019-12-31 10:39:53', NULL),
(29, 3, 'KL-05-8795', '200', 1, 0, '2019-12-31 10:44:47', '2019-12-31 10:44:47', NULL),
(31, 2, 'KL-07-3462', '200', 1, 0, '2020-01-03 05:24:19', '2020-01-03 05:24:19', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_owners`
--

DROP TABLE IF EXISTS `vehicle_owners`;
CREATE TABLE IF NOT EXISTS `vehicle_owners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vehicle_id` int(11) NOT NULL,
  `name` varchar(191) DEFAULT NULL,
  `address` varchar(191) DEFAULT NULL,
  `contact_number` varchar(191) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle_owners`
--

INSERT INTO `vehicle_owners` (`id`, `vehicle_id`, `name`, `address`, `contact_number`, `created_at`, `updated_at`, `deleted_at`) VALUES
(16, 20, 'Biby Paul', 'Ambatyil', '9430251200', '2019-12-26 07:07:43', '2019-12-27 08:13:21', NULL),
(22, 24, 'Siva Raj', 'Kozhikode', '9463021512', '2019-12-27 05:30:30', '2019-12-27 08:14:06', NULL),
(23, 25, 'Helly group', 'Ernakulam', '8465321012', '2019-12-27 06:54:53', '2019-12-27 08:13:36', NULL),
(26, 28, NULL, NULL, NULL, '2019-12-31 10:39:53', '2019-12-31 10:39:53', NULL),
(27, 29, NULL, NULL, NULL, '2019-12-31 10:44:47', '2019-12-31 10:44:47', NULL),
(28, 30, 'Siva', 'Thriponithura', '9845632010', '2020-01-03 05:23:04', '2020-01-03 05:23:04', NULL),
(29, 31, 'Bramma Group', 'Kadavanthra', '8842101010', '2020-01-03 05:24:19', '2020-01-03 05:24:19', NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
