<?php

$DOMAIN = env('DOMAIN');



Route::group(['domain' => "admin.$DOMAIN"], function () {

    //*****Login and Logout********//
    Route::get('login', 'Admin\AuthController@index');
    Route::post('login', 'Admin\AuthController@login');
    Route::get('logout', 'Admin\AuthController@logout');

    //*******************************************************************************************************
    //                                         RESET PASSWORD                                                   *
    //*******************************************************************************************************
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    // Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    // Route::post('password/reset', 'Auth\ResetPasswordController@reset');
    // Auth::routes();


    /* Route group for authenticated functionality */
    Route::group(['middleware' => ['admin', 'admin.permissions']], function () {
        Route::get('/', 'Admin\DashboardController@index');
        Route::get('/home', 'Admin\DashboardController@index');
        Route::get('/get-wellness-advisor', 'Admin\DashboardController@getWellnessAdvisor');
        Route::post('update-unassigned-pc', 'Admin\DashboardController@updateUnassignedPC');
        Route::get('no-access', 'Admin\ErrorController@noAccess');
        Route::get('profile', 'Admin\ProfileController@index');
        Route::post('profile', 'Admin\ProfileController@updateProfile');
        Route::get('change-password', 'Admin\ProfileController@changePassword');
        Route::post('change-password', 'Admin\ProfileController@updatePassword');
    });
    // Roles management section
    Route::group(['module' => 'roles'], function () {
        Route::get('roles', 'Admin\RoleController@index');
        Route::get('role/create', ['permission' => 'add', 'uses' => 'Admin\RoleController@createForm']);
        Route::post('role/create', ['permission' => 'add', 'uses' => 'Admin\RoleController@create']);
        Route::get('role/edit/{id}', ['permission' => 'edit', 'uses' => 'Admin\RoleController@updateForm']);
        Route::post('role/edit/{id}', ['permission' => 'edit', 'uses' => 'Admin\RoleController@update']);
        Route::post('role/change_status/{id}', ['permission' => 'edit', 'uses' => 'Admin\RoleController@changeStatus']);
        Route::post('role/delete/{id}', ['permission' => 'delete', 'uses' => 'Admin\RoleController@delete']);
    });

    // Module management section
    Route::group(['module' => 'module'], function () {
        Route::get('module', 'Admin\ModuleController@index');
        Route::get('module/create', ['permission' => 'add', 'uses' => 'Admin\ModuleController@createForm']);
        Route::post('module/create', ['permission' => 'add', 'uses' => 'Admin\ModuleController@create']);
        Route::get('module/edit/{id}', ['permission' => 'edit', 'uses' => 'Admin\ModuleController@updateForm']);
        Route::post('module/edit/{id}', ['permission' => 'edit', 'uses' => 'Admin\ModuleController@update']);
    });

    // User management section
    Route::group(['module' => 'user'], function () {
        Route::get('users', 'Admin\UserController@index');
        Route::get('get-users', ['permission' => 'browse', 'uses' => 'Admin\UserController@getUsers']);
        Route::post('user/change-status', ['permission' => 'edit', 'uses' => 'Admin\UserController@changeStatus']);
        Route::post('user/change-active', ['permission' => 'edit', 'uses' => 'Admin\UserController@changeActive']);
        Route::post('user/delete', ['permission' => 'delete', 'uses' => 'Admin\UserController@deleteUser']);
        Route::post('users/login', ['permissions' => 'browse', 'uses' => 'Admin\UserController@userLogin']);
    });

    // Admin user management section
    Route::group(['module' => 'admin_user'], function () {
        Route::get('admin_users', 'Admin\AdminUsersController@index');
        Route::post('admin_user/create', ['permission' => 'add', 'uses' => 'Admin\AdminUsersController@create']);
        Route::get('admin_user/edit/{id}', ['permission' => 'edit', 'uses' => 'Admin\AdminUsersController@updateForm']);
        Route::post('admin_user/edit/{id}', ['permission' => 'edit', 'uses' => 'Admin\AdminUsersController@update']);
        Route::post('admin-user/change_status', ['permission' => 'edit', 'uses' => 'Admin\AdminUsersController@changeStatus']);


        
        Route::get('admin_user/wa-request', 'Admin\AdminUsersController@waRequest');
        Route::get('admin_user/wa-request/{id}', 'Admin\AdminUsersController@waRequestDetail');
        Route::get('adminusers/details', 'Admin\AdminUsersController@usersDetails');
        Route::get('admin_user/user-request', 'Admin\AdminUsersController@userRequest');
        // Route::get('admin_user/send-greetings', 'Admin\AdminUsersController@sendGreetings');
        // Route::get('admin_user/user-request/{id}', 'Admin\AdminUsersController@uesrRequestDetail');

        // Route::get('get-admin-users', 'Admin\AdminUsersController@getAdminUsers');
        // Route::get('get-admin-users-website', 'Admin\AdminUsersController@getAdminUsersWA');
        // Route::get('get-admin-users-register-website', 'Admin\AdminUsersController@getAdminUsersWeb');
        Route::get('get-customer-care-category-form', 'Admin\AdminUsersController@getCustomerCareCategoryForm');
/*        Route::get('get-wellness-advisor-form/{id}', 'Admin\AdminUsersController@getWAForm');
      Route::get('get-wellness-advisor-form-admin-user/{id}', 'Admin\AdminUsersController@getWAFormAdminUser');*/
        Route::get('get-normal-form', 'Admin\AdminUsersController@getNormalForm');
        Route::get('admin_user/create', ['permission' => 'add', 'uses' => 'Admin\AdminUsersController@createForm']);
        Route::post('admin-user-wa/create/{id}', ['permission' => 'add', 'uses' => 'Admin\AdminUsersController@createWA']);
       
        Route::post('admin-user-wa/delete', ['permission' => 'edit', 'uses' => 'Admin\AdminUsersController@deleteUserWebsite']);
        Route::post('admin-user-user/delete', ['permission' => 'edit', 'uses' => 'Admin\AdminUsersController@deleteWA']);
        Route::post('admin-user/delete', ['permission' => 'edit', 'uses' => 'Admin\AdminUsersController@deleteUser']);
    });
    // Route::post('admin_user/create-customer', 'Admin\AdminUserrsController@createCustomerByStaff');
    // CMS management section
    Route::group(['module' => 'cms'], function () {
        Route::get('cms', 'Admin\CmsController@index');
        Route::get('cms/create', ['permission' => 'add', 'uses' => 'Admin\CmsController@createForm']);
        Route::post('cms/create', ['permission' => 'add', 'uses' => 'Admin\CmsController@create']);
        Route::get('cms/edit/{id}', ['permission' => 'edit', 'uses' => 'Admin\CmsController@updateForm']);
        Route::get('cms/homeedit/{id}', ['permission' => 'edit', 'uses' => 'Admin\CmsController@updateHomeForm']);
        Route::post('cms/edit/{id}', ['permission' => 'edit', 'uses' => 'Admin\CmsController@update']);
        Route::post('cms/change_status', ['permission' => 'edit', 'uses' => 'Admin\CmsController@changeStatus']);
    });
    // alert messages section
    Route::group(['module' => 'alert-messages'], function () {
        Route::get('alert-messages', 'Admin\AlertMessageController@index');
        Route::get('alert-messages/create', ['permission' => 'add', 'uses' => 'Admin\AlertMessageController@createForm']);
        Route::post('alert-messages/create', ['permission' => 'add', 'uses' => 'Admin\AlertMessageController@create']);
        Route::post('alert-messages/change-data', ['permission' => 'edit', 'uses' => 'Admin\AlertMessageController@changeData']);
        Route::post('alert-messages/get-alert-data', ['permission' => 'edit', 'uses' => 'Admin\AlertMessageController@getAlertMessages']);
        Route::post('alert-messages/delete-alert-data', ['permission' => 'edit', 'uses' => 'Admin\AlertMessageController@deleteAlertMessages']);

    });


    // Client management section
    Route::group(['module' => 'client'], function () {
        Route::get('client', 'Admin\ClientController@index');
        Route::get('get-clients', 'Admin\ClientController@getClients');
        Route::get('client/create', ['permission' => 'add', 'uses' => 'Admin\ClientController@createForm']);
    });

    Route::group(['module' => 'email_templates'], function () {
        Route::get('email-templates', 'Admin\EmailController@index');
        Route::get('email-template/create', ['permission' => 'add', 'uses' => 'Admin\EmailController@createForm']);
        Route::post('email-template/create', ['permission' => 'add', 'uses' => 'Admin\EmailController@create']);
        Route::get('email-template/edit/{id}', ['permission' => 'edit', 'uses' => 'Admin\EmailController@updateForm']);
        Route::post('email-template/edit/{id}', ['permission' => 'edit', 'uses' => 'Admin\EmailController@update']);
    });
    Route::get('logout', 'Admin\AuthController@logout');
    // Route::get('tree/getdata', 'Admin\DashboardController@getData');
    // Route::get('tree/gettree/{id}', 'Admin\DashboardController@getAjax');
    Route::post('adminusers/login', 'Admin\AdminUsersController@userLogin');

    Route::post('ajax/status-change', 'Admin\IndexMainController@index');
});



//vehicle project

 //Client details added
// Route::group(['module' => 'vehicle'], function () {
    Route::get('clientform','VehiclesDetailsController@index');
    Route::post('vehicle/create','VehiclesDetailsController@create');
    Route::get('clientview', 'VehiclesDetailsController@display');
    Route::get('data/edit/{id}','VehiclesDetailsController@editfirst');
    Route::post('updat/edit/{id}','VehiclesDetailsController@updatefirst');
    Route::get('data/delete/{id}','VehiclesDetailsController@clientdelete');
    //***** **/
    Route::get('clientviewdrivers', 'VehicleDriversController@clientview');//CLIENTS view drivers details 
    Route::get('/apply/data/{id}','VehicleDriversController@status');//checkbox apply client
    
    
    // });
    //details for vehicles & drivers
    Route::get('driversform','VehicleDriversController@index');
    Route::post('drivers/create','VehicleDriversController@create');
    Route::get('driversview','VehicleDriversController@display');
    
    Route::get('driver/edit/{id}','VehicleDriversController@editdriver');
    Route::post('update/edit/{id}','VehicleDriversController@updatedata');
    Route::get('driver/delete/{id}','VehicleDriversController@delete');
    
    ///ADMIN VIEW NOTIFICATION **********
    Route::get('notificationview','VehicleDriversController@notification');



    //new project//
    Route::get('customers','CustomersFeildController@index');
    Route::get('request','CustomersFeildController@indx');
    Route::post('customers/create','CustomersFeildController@createfirst');
    // Route::get('customers/view','CustomersFeildController@viewfirst');
    Route::get('customers/edit/{id}','CustomersFeildController@editaction');
    Route::post('updating/edit/{id}','CustomersFeildController@updatefirst');
    Route::get('customers/view','CustomersFeildController@viewonly');
    Route::get('customers/delete/{id}','CustomersFeildController@deletefirst');
    Route::get('/search','CustomersFeildController@search');//search button



        Route::post('admin_user/create', ['permission' => 'add', 'uses' => 'UsersPersonaldataController@create']);
     
        Route::get('admin_user/wa-request', 'UsersPersonaldataController@waRequest'); 
        Route::get('get-admin-users-details', 'UsersPersonaldataController@getAdminUsersWA');
        Route::post('admin-user/change_status', ['permission' => 'edit', 'uses' => 'UsersPersonaldataController@changeStatus']);
        Route::post('admin-user-user/delete', ['permission' => 'edit', 'uses' => 'UsersPersonaldataController@deleteWA']);
        Route::get('admin_user/wa-request/{id}', 'UsersPersonaldataController@waRequestDetail');
        Route::get('get-admin-users-register-website', 'UsersPersonaldataController@getAdminUsersWeb');
        Route::get('admin_user/user-request/{id}', 'UsersPersonaldataController@uesrRequestDetail');
        Route::post('admin-user-wa/delete', ['permission' => 'edit', 'uses' => 'UsersPersonaldataController@deleteUserWebsite']);
        Route::get('adminusers/details', 'UsersPersonaldataController@usersDetails');
      
        Route::get('admin_users', 'UsersPersonaldataController@index');
        Route::get('get-admin-users', 'UsersPersonaldataController@getAdminUsers');
        Route::get('admin_user/edit/{id}', ['permission' => 'edit', 'uses' => 'UsersPersonaldataController@updateForm']);
        Route::post('admin_user/edit/{id}', ['permission' => 'edit', 'uses' => 'UsersPersonaldataController@update']);
        Route::post('admin_update/edit/{id}', ['permission' => 'edit', 'uses' => 'UsersPersonaldataController@updated']);
        Route::post('admin-user-wa/create/{id}', ['permission' => 'add', 'uses' => 'UsersPersonaldataController@createWA']);

        Route::get('admin-drivers', 'UsersPersonaldataController@indexin');

        Route::get('get-admin-drivers', 'UsersPersonaldataController@getAdminDrivers');

        Route::get('admin-cleaner', 'UsersPersonaldataController@indx');
        Route::get('get-admin-cleaner', 'UsersPersonaldataController@getAdminCleaner');
       

        //customers
        Route::get('admin-customer','CustomersController@index');
        Route::post('insert/customers','CustomersController@create');
        Route::get('get-admin-customers','CustomersController@getCustomers');
        Route::get('admin-customers','CustomersController@view');
        Route::post('admin-customer/change_status','CustomersController@changeStatus');
        Route::get('admin_customer/request/{id}','CustomersController@send');
        Route::post('admin_customer/edit/{id}','CustomersController@sendupdate');
        Route::post('admin-customer/delete','CustomersController@deleteCustomers');

        //Utitilites//
        Route::get('admin-utilite','UtilitiesController@index');
        Route::post('insert/utilites','UtilitiesController@create');
        Route::get('admin-utilites','UtilitiesController@show');
        Route::post('admin-utility/change_status','UtilitiesController@changeStatus');
        Route::get('get-admin-utilites','UtilitiesController@getUtilites');
        Route::get('admin_utilite/request/{id}','UtilitiesController@edit');
        Route::post('admin_utilite/update/{id}','UtilitiesController@utiltyupdate');
        Route::post('admin-utilite/delete','UtilitiesController@deleteUtility');

        //site//
        Route::get('site','SiteController@index');
        Route::post('sites/insert','SiteController@create');
        Route::get('sites','SiteController@show');
        Route::get('get-admin-site','SiteController@getSite');
        Route::post('admin-site/change_status','SiteController@changeStatus');
        Route::post('admin-site/delete','SiteController@deleteSite');
        Route::get('admin_site/edit/{id}','SiteController@edit');
        Route::post('admin_sites/update/{id}','SiteController@siteupdate');

        //vehicle//
        Route::get('Vehicle','VehiclesController@index');
        Route::post('details/insert','VehiclesController@create');
        Route::get('Vehicles','VehiclesController@show');
        Route::get('get-admin-vehicle','VehiclesController@getVehicle');
        Route::get('admin_vehi/edit/{id}','VehiclesController@edit');
        Route::post('vehicle/update/{id}','VehiclesController@update');
        Route::post('admin-vehicle/delete','VehiclesController@destroy');
        Route::post('admin-vehicle/change_status','VehiclesController@changeStatus');

        //site work
        Route::get('site-works','SiteWorkController@index');
        Route::post('sitework/insert','SiteWorkController@create');
        Route::get('site-work','SiteWorkController@show');
        Route::get('get-admin-worksite','SiteWorkController@getworkSite');
        Route::get('admin_worksite/request/{id}','SiteWorkController@edit');
        Route::post('admin_worksite/update/{id}','SiteWorkController@update');
        Route::post('admin-worksite/delete','SiteWorkController@delete');
        //amount details

        Route::get('amount-detail','SiteWorkController@view');
        Route::post('amount/insert','SiteWorkController@insert');
        Route::get('amount-details','SiteWorkController@amount');
        Route::get('debit-amount','SiteWorkController@debit');
        Route::get('credit-amount','SiteWorkController@credit');
      
        Route::get('get-admin-amounts','SiteWorkController@getamounts');
        Route::get('admin_amount/edit/{id}','SiteWorkController@amountedite');
        Route::post('amount/update/{id}','SiteWorkController@amountupdate');
        Route::post('admin-amount/delete','SiteWorkController@destroy');
